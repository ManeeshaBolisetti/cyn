
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
// import AddLenin from './Components/Admin/AddLenin';

import Routing from './Routing';

function App() {
  // console.log({ REACT_APP_API_ENDPOINT: process.env.REACT_APP_API_ENDPOINT })
  return (
    
    <div className="App">
      
           
      <Routing/>

    </div>
  );
}

export default App;
