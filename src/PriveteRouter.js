import React from 'react';
import { Redirect, Route } from "react-router-dom";
// import AuthService from './Components/Authentication/AuthService';
// import { ACCESS_TOKEN_NAME } from '../constants/apiContants';
function PrivateRoute({ children, ...rest }) {
    // const user=(sessionStorage.getItem("user"))
    // console.log('user');
    // console.log(user)
    // console.log(AuthService.getCurrentUser());
    return (
      <Route
        {...rest}
        render={({ location }) =>
        sessionStorage.getItem("user") ? (
            children
          ) : (
            <Redirect
              to={{
                pathname: "/login",
                state: { from: location }
              }}
            />
          )
        }
      />
//    <div> {user}</div>
    );
  }
export default PrivateRoute;