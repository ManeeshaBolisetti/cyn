import React from 'react';
import UserList from './UserList';
import NavBar from './../NavBar';


const OrderList = () => {
    const data = [
        {
            name: 'manisha',
            mobile: '9059330037',
            title: 'react',
            Registration: '24/08/2021',
            counts: 2,
            locks: 1,
        },
        {
            name: 'jishwan',
            mobile: '9059330037',
            title: 'react',
            Registration: '24/08/2021',
            counts: 3,
            locks: 2,
        },
        {
            name: 'vijay',
            mobile: '9059330037',
            title: 'react',
            Registration: '24/08/2021',
            counts: 5,
            locks: 1,
        }
    ]


    return (
        <div>
            <NavBar />
            {console.log(data)}
           {
               data.map((n)=>
                 <UserList 
                 name={n.name}
                 mobile={n.mobile}
                 title={n.title}
                 Registration={n.Registration}
                 counts={n.counts}
                 locks={n.locks}
                 
                 />
               )
           }
            {/* <UserList name='manisha'/> */}
 </div>
            );
 }
            export default OrderList;