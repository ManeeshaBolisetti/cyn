import { useState,useRef } from 'react';
import { Form, Button } from 'react-bootstrap';
import Card from '../../Card/Card';
import classes from './ProdInput.Module.css';
// import Example from './Example';
import { CheckBoxSelection, Inject, MultiSelectComponent } from '@syncfusion/ej2-react-dropdowns';

const ProdInput=(props)=>{
    const productNameRef=useRef();
   
   
    const submitHandler=(event)=>{
        event.preventDefault();
        const product=productNameRef.current.value;
      
        const productData={
            product:product,
          
        };
        console.log(productData);
        props.onSaveproductData(productData);

    }
    

    return(
        <div>
             <div className={classes.input}>
        <Card className={classes.modal}>
          <header className={classes.header}>
            <h2>{props.name}</h2>
          </header>
          <Form
           onSubmit={submitHandler}
           > <div className={classes.content}>
  
            <Form.Control 
            type='text'
            placeholder={props.cName}
            //  value={enterdCountry}
             ref={productNameRef}
              // onChange={cNameChangeHandler}
              />
                    </div>
            <footer className={classes.actions}>
              <Button
                onClick={props.onClose}
              >cancel</Button>
              <Button
                type='submit'
              >Add</Button>
            </footer></Form>
        </Card>
      </div>

        </div>
    );

}
export default ProdInput;