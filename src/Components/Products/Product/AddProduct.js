import ProdInput from "./ProdInput";
import {React,useState} from 'react';
// import CategoryInput from './CategoryInput';
import { Form,Button } from 'react-bootstrap';
import { TiFolderAdd } from 'react-icons/ti';
const AssignProducts=(props)=>{
    const [showForm, setshowForm] = useState(false);
    const onShowForm = () => {
        setshowForm(true);
    }
    const onHideForm = () => {
        setshowForm(false);
    }
    const saveProductDataHandler = (enterdProductData) => {
        const productData = {
            ...enterdProductData,
            id: Math.random().toString()
        };
        console.log(productData)
        props.onAddProduct(productData);
        setshowForm(false);
    };
    return(
        <div>
             {showForm && <ProdInput
            name='Add Product'
            cName='ProductName'
            
            
            onClose={onHideForm}
            onSaveproductData={saveProductDataHandler}
           />}
            <Form><Button className='add' style={{ backgroundColor: '#275788' }} onClick={onShowForm}><TiFolderAdd fontSize='30' /> Add Product </Button></Form>
        
            
        </div>
    );
}
export default AssignProducts;