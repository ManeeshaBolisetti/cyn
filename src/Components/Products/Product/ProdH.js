import { React, useState } from 'react';
import { AgGridReact, AgGridColumn } from 'ag-grid-react';

import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import AddProduct from './AddProduct';
import './Product.css';
function actionCellRenderer(params) {
    let eGui = document.createElement("div");
    
    let editingCells = params.api.getEditingCells();
    // checks if the rowIndex matches in at least one of the editing cells
    let isCurrentRowEditing = editingCells.some((cell) => {
        return cell.rowIndex === params.node.rowIndex;
    });

    if (isCurrentRowEditing) {
        eGui.innerHTML = `
  <button  class="action-button update"  data-action="update"> update  </button>`+
            ` <button  class="action-button cancel"  data-action="cancel" > cancel </button>`
            ;
    } else {
        eGui.innerHTML = `
  <a class="fa fa-pencil-square action-button edit"  data-action="edit" ></a>
  <a class="fa fa-trash action-button delete" data-action="delete" >  </a>
  `;
    }

    return eGui;
}


const rowData = [
    { id: 1,  product: 'reaclMe' },
    { id: 2, product: 'oppo' },
    { id: 3, product: 'vivo' },
    { id: 1,  product: 'reaclMe' },
    { id: 2, product: 'oppo' },
    { id: 3, product: 'vivo' },
    
    { id: 1,  product: 'reaclMe' },
    { id: 2, product: 'oppo' },
    { id: 3, product: 'vivo' },
    
    { id: 1,  product: 'reaclMe' },
    { id: 2, product: 'oppo' },
    { id: 3, product: 'vivo' },
    { id: 1,  product: 'reaclMe' },
    { id: 2, product: 'oppo' },
    { id: 3, product: 'vivo' },
    { id: 1,  product: 'reaclMe' },
    { id: 2, product: 'oppo' },
    { id: 3, product: 'vivo' },
    { id: 1,  product: 'reaclMe' },
    { id: 2, product: 'oppo' },
    { id: 3, product: 'vivo' },
]
const defaultColDef = {
    flex: 1,
    sortable: true,
    resizable: true,
    filter: true,
}
const ProdH = () => {
    let gridApi;
    const [product, setProduct] = useState(rowData);
    const columnDefs = [

        // { headerName: "Flag", field: "cFlag"}, 
       
        {
            field: "product", editable: true,
            // cellRenderer: countryCellRenderer,

        },
        {
            headerName: 'Action',
            minWidth: 150,
            cellRenderer: actionCellRenderer,
            editable: false,
            colId: "action"

        },


    ]
    const defaultColDef = { sortable: true,
        editable: true,
         flex: 1,
       //    filter: true,
          minWidth: 200,
          resizable: true,
          floatingFilter: true,
       //  floatingFilter: true
        }
        const onGridReady = (params) => {
            gridApi = params.api;
            //   gridColumnApi = params.columnApi;
        };
    const AddProductHandler = product => {
        console.log(product);
        // console.log('in App.js');
        setProduct(prevExp => {
            
            return [product, ...prevExp];
        });
        
    };
    const onPaginationChange = (pageSize) => {
        gridApi.paginationSetPageSize(Number(pageSize))
    }
    const onCellClicked = (params) => {
        // Handle click event for action cells
        if (params.column.colId === "action" && params.event.target.dataset.action) {
            let action = params.event.target.dataset.action;

            if (action === "edit") {
                params.api.startEditingCell({
                    rowIndex: params.node.rowIndex,
                    // gets the first columnKey
                    colKey: params.columnApi.getDisplayedCenterColumns()[0].colId
                });
            }

            if (action === "delete") {
                params.api.applyTransaction({
                    remove: [params.node.data]
                });
            }

            if (action === "update") {
                params.api.stopEditing(false);
            }

            if (action === "cancel") {
                params.api.stopEditing(true);
            }
        }
    }

    const onRowEditingStarted = (params) => {
        params.api.refreshCells({
            columns: ["action"],
            rowNodes: [params.node],
            force: true
        });
    }
    const onRowEditingStopped = (params) => {
        params.api.refreshCells({
            columns: ["action"],
            rowNodes: [params.node],
            force: true
        });
    }


    return (
        <div>
            <AddProduct onAddProduct={AddProductHandler} />
            <select onChange={(e) => onPaginationChange(e.target.value)}>
                <option value='10'>10</option>
                <option value='25'>25</option>
                <option value='50'>50</option>
                <option value='100'>100</option>
            </select>
            <div className="ag-theme-alpine" style={{ height: '400px' }}>
                <AgGridReact
                    onRowEditingStopped={onRowEditingStopped}
                    onRowEditingStarted={onRowEditingStarted}
                    onCellClicked={onCellClicked}
                    editType="fullRow"
                    suppressClickEdit={true}
                    pagination={true}
                    paginationPageSize={10}
                    columnDefs={columnDefs}
                    rowData={product}
                    defaultColDef={defaultColDef}
                    onGridReady={onGridReady}>



                </AgGridReact>
            </div>
        </div>
    );
}
export default ProdH;