import { useState, useRef } from 'react';
import { Form, Button } from 'react-bootstrap';
import Card from '../Card/Card';
import classes from './ProductInput.Module.css';
import Alert from '@material-ui/lab/Alert';
// import axios from 'axios';
import { CheckBoxSelection, Inject, MultiSelectComponent } from '@syncfusion/ej2-react-dropdowns';
import UseFetch from '../UseFetch';
// import Select from 'react-select';

const EditProductInput = (props) => {
  const [flash, setFlash] = useState(null);
 const color=props.color;

  const styles = {
    alert: {
      zIndex: '1500',
    },
  }
  const Invalid = '';
  let value = props.val
  // console.log(value)
  const [valid, setValid] = useState(Invalid);
  let pImage = value.length === 1 ? value[0].productImage : '';
  const [image, setImage] = useState(pImage);
  // const image=pImage;
  // const [images, setImages] = useState('');
  let pCountryId = value.length === 1 ? value[0].countryId : '';
  // let pCountries = value.length === 1 ? value[0].countries : '';
  let pIsUsed = value.length === 1 ? value[0].isUsed : '';
  // let pCategoryRef = value.length === 1 ? value[0].categoryRef : '';
  let pCategoryId = value.length === 1 ? value[0].categoryId : '';
  const [optionValue,setOptionValue]=useState(pCategoryId);
  let pIsDuplicate = value.length === 1 ? value[0].isDuplicate : '';
  let pProductId = value.length === 1 ? value[0].productId : '';
  let pProductName = value.length === 1 ? value[0].productName : '';


  console.log(optionValue)
  // const [category, setCategory] = useState([]);
  // const [countries, setCountries] = useState([]);
  const categoryIdRef = useRef();
  const productNameRef = useRef();
  const productImageRef = useRef();
  const isusedRef = useRef();
  const countryNameRef = useRef();
  const isDuplicateRef = useRef();
  // console.log(pCategoryId,pCategoryRef,pCountries,pImage,pCountryId,pIsDuplicate,pIsUsed,pProductId)
  const countries = UseFetch('/admin/countriesList');
  // const categoryNameRef = useRef();

  // useEffect(() => {

  //   axios.get('https://appdev.connectyourneed.com/admin/countriesList')
  //     .then(response => {
  //       const country = response.data.data;
  //       // console.log(country);

  //       setCountries(country)
  //     });

  //   // empty dependency array means this effect will only run once (like componentDidMount in classes)
  // }, []);
  const fields = { text: 'countryName', value: 'countryId' };
  let colorValues = pCountryId.map((n) => {
    let colour = n;
    return colour
  });
  // console.log(colorValues)
  const onTagging = (e) => {
    // set the current selected item text as class to chip element.
    e.setClass(e.itemData[fields.text].toLowerCase());
  };
  // console.log()
  const category = UseFetch('/admin/categoriesList');
  const optionItems = category.map((categories) => <option value={categories.categoryId} >{categories.categoryName}</option>)

  // console.log(images)
  const submitHandler = (event) => {
    event.preventDefault();
    setFlash(true);
    setTimeout(() => {
      setFlash(null);
      // setFlashE(null);
    }, 5000);
    // setImages(productImageRef.current.files[0]);
    const CategoryId = categoryIdRef.current.value;
    const ProductName = productNameRef.current.value;
    const ProductImage = pImage;
    const IsUsed = isusedRef.current.value;
    const Countries = countryNameRef.current.value;
    const IsDuplicate = isDuplicateRef.current.value;
    const productId = pProductId;
    const uImages = productImageRef.current.files[0];

    console.log(ProductImage);
    // if (CategoryId.trim().length === 0 || ProductName.trim().length === 0 || IsUsed.trim().length === 0) {
    //   setValid('Invalid Country or code or currency');
    //   return console.log('Invalid Country and code and currency');

    // }
    const productData = {
      productId: productId,
      countries: Countries,
      categoryId: CategoryId,
      productName: ProductName,
      productImage: ProductImage,
      isUsed: IsUsed,

      isDuplicate: IsDuplicate,
      // productId: productId,
      image: uImages,



    };
    console.log(productData);
    props.onSaveproductData(productData);
    setValid('');
  }
  const handleChange = (event) => {
    setImage(productImageRef.current.files[0]);
    //   setImage( 
    //     URL.createObjectURL(event.target.files[0])
    //  )

  }
  const categoryChange=(event)=>{
    setOptionValue(event.target.value);
  
  }
  const RemoveImage = () => {
    const RemoveImage = {
      imageUrl: pImage,
    };
    setFlash(true);
    setTimeout(() => {
      setFlash(null);
      // setFlashE(null);
    }, 5000);
    props.onRemoveImage(RemoveImage);
    
  }
  

  return (
    <div>
      <div className={classes.input}>
        <Card className={classes.modal}>
          <header className={classes.header}>
            <h2>Update Product</h2>
          </header>
          {
            flash
              ? <Alert style={styles.alert} severity={color}>{props.stat}</Alert>
              : null
          }
           <button onClick={RemoveImage}>Remove</button>
          <Form
            onSubmit={submitHandler}
          > <div className={classes.content}>


              <Form.Control as="select" value={optionValue} ref={categoryIdRef} 
              onChange={categoryChange}
              >
                {optionItems}
              </Form.Control>
              <Form.Control
                type='text'
                defaultValue={pProductName}
                placeholder='product Name'
                ref={productNameRef}
              />
              <Form.Group style={{ display: 'flex' }}>
                <Form.Control
                  type='file'
                  onChange={handleChange}
                  placeholder={props.cName}
                  ref={productImageRef}
                /><br /><img className={classes.image} src={image}

                  alt='previw' />
                 
              </Form.Group>
              <Form.Group style={{ display: 'flex' }}>
                <Form.Label style={{ display: 'flex' }}>Duplicate:</Form.Label>
                <Form.Control as="select" ref={isDuplicateRef} defaultValue={pIsDuplicate}>
                  {/* <option value='false'>False</option> */}
                  <option value='true'>True</option>
                  <option value='false'>False</option>
                </Form.Control>
                <Form.Label style={{ display: 'flex' }}>IsUsed:</Form.Label>
                <Form.Control as="select" ref={isusedRef} defaultValue={pIsUsed}>

                  <option value='false'>False</option>
                  <option value='true'>True</option>
                </Form.Control>
              </Form.Group>
              <MultiSelectComponent id="checkbox" className='dropdown '
                dataSource={countries}
                // fields={fields}
                fields={fields}
                // value={fields.value}
                ref={countryNameRef}
                placeholder="Select Countries"
                mode="CheckBox"
                selectAllText="Select All"
                unSelectAllText="unSelect All"
                tagging={onTagging}
                value={colorValues}
                showSelectAll={true}>
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent>

            </div>

            <h4>{valid}</h4>
            <footer className={classes.actions}>
              <Button className={classes.btn}
                onClick={props.onClose}
              >cancel</Button>
              <Button
                type='submit'
              >Update</Button>
            </footer></Form>
        </Card>
      </div>

    </div>
  );

}
export default EditProductInput;