import { React, useState } from 'react';
import { Form } from 'react-bootstrap';
import {CgAddR  } from 'react-icons/cg';
import EditProductInput from "./EditProductInput";
// import axios from 'axios';
import api from './../Api';
const EditProduct = (props) => {
    let statu = '';
    let value=props.value
    const [showForm, setshowForm] = useState(false);
    const [status, setStatus] = useState(statu);
    const [color,setColor]=useState('');
    const onShowForm = () => {
        if(value.length===1){
        setshowForm(true);
    }else{
        alert('please select edit Row')
    }
    }
    const onHideForm = () => {
        setshowForm(false);
    };
    const RemoveImage=(image)=>{
        const RemoveData={
            ...image
        }
        // let url = 'admin/removeBlob'
        // axios.post(url, RemoveData )
        api.RemoveBlob(RemoveData)
        .then(response => {
            setStatus({ data:response.data.message });
        console.log(response.data.message)
        setColor('success')
        // props.onUpdateProduct({data:response.data.data});
        setTimeout(() => {
            setColor('')
            setStatus('')
        //   setshowForm(false)
          // setFlashE(null);
        }, 3000);
        
        // window.location.reload();
         } )
        .catch(error => {
            if (error.response) {
                setStatus({ data:(error.response.data.message )});
                setColor('error')
                console.log(error.response);
            }else{
                setStatus({ data: error.message });
                setColor('error')
                console.error('There was an error!', error);

            }
           
        });
       

    }
    const saveProductDataHandler = (enterdProductData) => {
        const productData = {
            ...enterdProductData,

        };
        console.log(enterdProductData.categoryId);

        let formData = new FormData();
        formData.append('categoryId', enterdProductData.categoryId);
        
        formData.append('productId', enterdProductData.productId);

        formData.append('productName', enterdProductData.productName);
        formData.append('productImage', enterdProductData.productImage);
        
        formData.append('image', enterdProductData.image );
        formData.append('isUsed', enterdProductData.isUsed);
        formData.append('isDuplicate',enterdProductData.isDuplicate);
        // formData.append('image', enterdProductData.image );
        for (let i = 0; i < enterdProductData.countries.length; i++) {
            formData.append('countries[]', enterdProductData.countries[i])
        }


        console.log(enterdProductData.uImages);
        console.log(formData)
        // console.log();
        // let url = '/Admin/updateProduct'
        // axios.post(url, formData )
        api.updateProduct(formData)
        .then(response => {
            setStatus({ data:response.data.message });
        console.log(response.data.message)
        setColor('success')
        props.onUpdateProduct({data:response.data.data});
        setTimeout(() => {
            setColor('')
            setStatus('')
          setshowForm(false)
          // setFlashE(null);
        }, 3000);
        
        // window.location.reload();
         } )
        .catch(error => {
            if (error.response) {
                setStatus({ data:(error.response.data.message )});
                setColor('error')
                console.log(error.response);
            }else{
                setStatus({ data: error.message });
                setColor('error')
                console.error('There was an error!', error);

            }
           
        });
       
        // props.onUpdateProduct(productData);
        console.log(productData);
        // setshowForm(false);
    }

    return (
        <div>
            {showForm && <EditProductInput

                name='Add Product'
                cName='Product Name'
                stat={status.data}
                color={color}
                onClose={onHideForm}
                onSaveproductData={saveProductDataHandler}
                onRemoveImage={RemoveImage}
                val={value}
            />}
            <Form><div   onClick={onShowForm}><CgAddR color='green'  /> Update</div></Form>
        </div>
    );
}
export default EditProduct;




