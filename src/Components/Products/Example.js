
import { CheckBoxSelection, Inject, MultiSelectComponent } from '@syncfusion/ej2-react-dropdowns';

const Example=()=> {

    // define the JSON of data
    var sportsData = [
        { Id: 'game1', Game: 'Badminton' },
        { Id: 'game2', Game: 'Football' },
        { Id: 'game3', Game: 'Tennis' },
        { Id: 'game4', Game: 'Golf' },
        { Id: 'game5', Game: 'Cricket' },
        { Id: 'game6', Game: 'Handball' },
        { Id: 'game7', Game: 'Karate' },
        { Id: 'game8', Game: 'Fencing' },
        { Id: 'game9', Game: 'Boxing' }
    ];
    let array=['game3', 'game4', 'game5']
    console.log([array])
    // maps the appropriate column to fields property
    const fields=  { text: 'Game', value: 'Id' };
    const colorValues =array;
    const onTagging = (e) => {
        // set the current selected item text as class to chip element.
        e.setClass(e.itemData[fields.text].toLowerCase());
    };
        return (
             // specifies the tag for render the MultiSelect component
            <MultiSelectComponent id="checkbox" dataSource={sportsData}
                fields={fields} placeholder="Select game" mode="CheckBox"
                tagging={onTagging}
                value={colorValues}
                selectAllText="Select All" unSelectAllText="unSelect All" showSelectAll={true}>
                <Inject services={[CheckBoxSelection]} />
            </MultiSelectComponent>
        );
    }

export default Example;