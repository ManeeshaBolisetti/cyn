import { useState, useRef } from 'react';
import { Form, Button } from 'react-bootstrap';
import Card from '../Card/Card';
import classes from './ProductInput.Module.css';
import Alert from '@material-ui/lab/Alert';
// import axios from 'axios';
import { CheckBoxSelection, Inject, MultiSelectComponent } from '@syncfusion/ej2-react-dropdowns';
import UseFetch from '../UseFetch';

const Productinput = (props) => {
  const [flash, setFlash] = useState(null);
  const color=props.color;
  console.log(props.stat)
  const styles = {
    alert: {
      zIndex: '1500',
    },
  }
  const Invalid = '';
  const [valid, setValid] = useState(Invalid);
  // const [category, setCategory] = useState([]);
  // const [countries, setCountries] = useState([]);
  const categoryIdRef = useRef();
  const productNameRef = useRef();
  const productImageRef = useRef();
  const isusedRef = useRef();
  const countryNameRef = useRef();
  const isDuplicateRef=useRef();
  const productIdNameRef=useRef();
  
  const countries=UseFetch('/admin/countriesList');
  // const categoryNameRef = useRef();

  // useEffect(() => {

  //   axios.get('https://appdev.connectyourneed.com/admin/countriesList')
  //     .then(response => {
  //       const country = response.data.data;
  //       // console.log(country);

  //       setCountries(country)
  //     });

  //   // empty dependency array means this effect will only run once (like componentDidMount in classes)
  // }, []);
  const fields = { text: 'countryName', value: 'countryId' };
  const category=UseFetch('admin/categoriesList');

  // useEffect(() => {

  //   axios.get('https://appdev.connectyourneed.com/admin/categoriesList')
  //     .then(response => {
  //       const category = response.data.data;
  //       // console.log(category);

  //       setCategory(category)
  //     });

  // }, []);
  const optionItems = category.map((categories) => <option value={categories.categoryId}>{categories.categoryName}</option>)

  const submitHandler = (event) => {
    event.preventDefault();
    setFlash(true);
    setTimeout(() => {
      setFlash(null);
      // setFlashE(null);
    }, 5000);

    const CategoryId = categoryIdRef.current.value;
    const ProductName = productNameRef.current.value;
    const ProductImage = productImageRef.current.files[0];
    const IsUsed = isusedRef.current.value;
    const Countries = countryNameRef.current.value;
    const IsDuplicate=isDuplicateRef.current.value;
    const ProductIdOld=productIdNameRef.current.value;
    console.log(ProductImage);
    if (CategoryId.trim().length === 0 || ProductName.trim().length === 0 || IsUsed.trim().length === 0) {
      setValid('Invalid Country or code or currency');
      return console.log('Invalid Country and code and currency');

    }
    const productData = {
      categoryId: CategoryId,
      productName: ProductName,
      productImage: ProductImage,
      isUsed: IsUsed,
      countries: Countries,
      isDuplicate:IsDuplicate,
      oldId:ProductIdOld


    };
    console.log(productData);
    props.onSaveproductData(productData);
    setValid('');
  }


  return (
    <div>
      <div className={classes.input}>
        <Card className={classes.modal}>
          <header className={classes.header}>
            <h2>{props.name}</h2>
          </header>
          {/* <h4>{props.stat}</h4> */}
          {
        flash
        ? <Alert style={styles.alert} severity={color}>{props.stat}</Alert>
        : null
      }
          <Form
            onSubmit={submitHandler}
          > <div className={classes.content}>


              <Form.Group controlId="exampleForm.ControlSelect1">
              <Form.Control
                type='Number'
                placeholder='Old ProductId'
                ref={productIdNameRef}
              /><br/>

                <Form.Control as="select" ref={categoryIdRef}>
                  {optionItems}
                </Form.Control></Form.Group>
           
              <Form.Control
                type='text'
                placeholder='product Name'
                ref={productNameRef}
              /><br/>
              <Form.Control
                type='file'
                placeholder={props.cName}
                ref={productImageRef}
              /><br />

              <Form.Group style={{display:'flex'}}>
              <Form.Label style={{display:'flex'}}>Duplicate:</Form.Label>
                <Form.Control as="select" ref={isDuplicateRef} >
                  <option value='false'>False</option>
                  <option value='true'>True</option>
                </Form.Control>
                <Form.Label style={{display:'flex'}}>IsUsed:</Form.Label>
                <Form.Control as="select" ref={isusedRef} >
                  <option value='false'>False</option>
                  <option value='true'>True</option>
                </Form.Control>
                </Form.Group>
                <MultiSelectComponent id="checkbox" className='dropdown '
                dataSource={countries}
                // fields={fields}
                fields={fields}
              // value={fields.value}
                ref={countryNameRef}
                placeholder="Select Countries"
                mode="CheckBox"
                selectAllText="Select All"
                unSelectAllText="unSelect All"
                showSelectAll={true}>
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent>
            </div>
            <h4>{valid}</h4>
            <footer className={classes.actions}>
              <Button className={classes.btn}
                onClick={props.onClose}
              >cancel</Button>
              <Button
                type='submit'
                
              >Add</Button>
            </footer></Form>
        </Card>
      </div>

    </div>
  );

}
export default Productinput;