import { React, useState } from 'react';
import { Form } from 'react-bootstrap';
import {CgAddR  } from 'react-icons/cg';
import Productinput from "./ProductInput";
// import axios from 'axios';
import api from './../Api';
const AssignProducts = (props) => {
    let statu = '';
    const [showForm, setshowForm] = useState(false);
    // const [flash, setFlash] = useState(null);
    const [status, setStatus] = useState(statu);
    const [color,setColor]=useState('');
    const onShowForm = () => {
        setshowForm(true);
    };
    const onHideForm = () => {
        setshowForm(false);
    };
    const saveProductDataHandler = (enterdProductData) => {
        const productData = {
            ...enterdProductData,

        };
        // console.log(enterdProductData.categoryImage);

        let formData = new FormData();
        formData.append('categoryId', enterdProductData.categoryId);
        formData.append('productName', enterdProductData.productName);
        formData.append('productImage', enterdProductData.productImage);
        formData.append('isUsed', enterdProductData.isUsed);
        formData.append('isDuplicate',enterdProductData.isDuplicate);
        formData.append('oldProductId',enterdProductData.oldId);
        // formData.append('countries[]', [enterdProductData.countries] );
        for (let i = 0; i < enterdProductData.countries.length; i++) {
            formData.append('countries[]', enterdProductData.countries[i])
        }


        console.log(enterdProductData.productName);
        // console.log();
        // let url = '/Admin/addProduct'
        // axios.post(url, formData)
        api.addProduct(formData)
        .then(response => {
            setStatus(response.data.message );
            
            props.onAddProduct({data:response.data.data});

        console.log(response.data.message)
        setColor('success')
        // setFlash(true);
        setTimeout(() => {
            setColor('')
            setStatus('')
          setshowForm(false)
          // setFlashE(null);
        }, 3000);
    
        
        // window.location.reload();
         } )
        .catch(error => {
            if (error.response) {
                setStatus(error.response.data.message );
                setColor('error')
                // setFlash(true);
                 
                
                console.log(error.response.data.message);
            }else{
                setStatus(error.message );
                setColor('error')
                console.error('There was an error!', error);

            }
           
        });
        
        // props.onAddProduct(productData);
        console.log(productData);
        // setshowForm(false);
    }

    return (
        <div>
            {showForm && <Productinput

                name='Add Product'
                cName='Product Name'
                stat={status}
                color={color}
                onClose={onHideForm}
                onSaveproductData={saveProductDataHandler}
            />}
            <Form><div  onClick={onShowForm}><CgAddR color='green'  /> Add</div></Form>
        </div>
    );
}
export default AssignProducts;




