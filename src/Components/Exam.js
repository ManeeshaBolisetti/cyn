import {React,useRef ,useState} from 'react';
import { FaInfo } from "react-icons/fa";
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-enterprise';
import { CheckBoxSelection, Inject, MultiSelectComponent } from '@syncfusion/ej2-react-dropdowns';


import { TiEdit } from "react-icons/ti";
import{RiDeleteBinFill} from "react-icons/ri";
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import {Row,Col} from 'react-bootstrap';
import { IoCheckbox } from "react-icons/io5";
 import { Dropdown } from 'semantic-ui-react';
 import 'semantic-ui-css/semantic.min.css';






var filterParams = {
    comparator: function (filterLocalDateAtMidnight, cellValue) {
      var dateAsString = cellValue;
      if (dateAsString == null) return -1;
      var dateParts = dateAsString.split('/');
      var cellDate = new Date(
        Number(dateParts[2]),
        Number(dateParts[1]) - 1,
        Number(dateParts[0])
      );
      if (filterLocalDateAtMidnight.getTime() === cellDate.getTime()) {
        return 0;
      }
      if (cellDate < filterLocalDateAtMidnight) {
        return -1;
      }
      if (cellDate > filterLocalDateAtMidnight) {
        return 1;
      }
    },
    browserDatePicker: true,
    minValidYear: 2000,
  };
  const rowData = [
    {id:1, 
         garment: "Scot",
    category:'shirt',
    // garment:'scot',
        // category:'male',
        service:'dryWash',
        qty:'1',
        amt:'1',
        total:'1',
        remark:'false',
        factory:'a',
        date:'21/12/2008',
     }]

const defaultColDef = { sortable: true,
     editable: true,
      flex: 1,
    //    filter: true,
       minWidth: 200,
       resizable: true,
       floatingFilter: true,
    //  floatingFilter: true
     }


  
function Exam() {
    // const [gridApi,setGridApi]=useState()
    const [product, setProduct] = useState(rowData);
    const garmentNameRef=useRef();
    const categoryNameRef=useRef();
    const serviceNameRef=useRef();
    const qtyRef=useRef();
    const amtRef=useRef();
    const totalRef=useRef();
    const remarkRef=useRef();
    const factoryRef=useRef();
    const dateRef=useRef();

    const submitHandling=(event)=>{
        event.preventDefault();
        const garment=garmentNameRef.current.value;
        const category=categoryNameRef.current.value;
        const service=serviceNameRef.current.value;
        const qty=qtyRef.current.value;
        const amt=amtRef.current.value;
        const total=totalRef.current.value;
        const remark=remarkRef.current.value;
        const factory=factoryRef.current.value;
        const date=dateRef.current.value;
        // console.log(garment,category,service,qty,amt,total,remark);
        const garnmentDate={
            garment:garment,
            category:category,
            service:service,
            qty:qty,
            amt:amt,
            total:total,
            remark:remark,
            factory:factory,
            date:date,
            id: Math.random().toString()

        }
        console.log(garnmentDate);
        setProduct(prevExp=>{
            console.log(garnmentDate);
            return[garnmentDate, ...prevExp];
        })
        
    }
    
   
    

    const submitHandler=(event)=>{
        event.preventDefault();
        const garment=garmentNameRef.current.value;
        console.log(garment);
    }
    // let gridApi;
    const onGridReady=(params)=>{
        console.log("grid is ready")
        // let gridApi=params.api
        
      }
  const columnDefs= [
    { headerName: "Garment", field: "garment",filter: 'agTextColumnFilter',tooltipField:'name' },
    // { headerName: "Garment", field: " garment",filter: 'agTextColumnFilter',tooltipField:'name' },
    { headerName: "Category", field: "category",filter: 'agTextColumnFilter',tooltipField:'name' },
     { headerName: "service", field: "service",filter: 'agTextColumnFilter',tooltipField:'name' },
     {headerName: "QTY",field: "qty",filter: 'agTextColumnFilter',tooltipField:'name'},
    { headerName: "Total AMT", field: "amt",filter: 'agNumberColumnFilter',tooltipField:'name' },
    { headerName: "Remarks", field: "total",filter: 'agNumberColumnFilter',tooltipField:'name' },
    { headerName: "Factory", field: "remark",filter: 'agNumberColumnFilter',tooltipField:'name' },
    { headerName: "Factory", field: "factory",filter: 'agNumberColumnFilter',tooltipField:'name' },
   
{ headerName: "Due Date", 
    field: "date",
    tooltipField:'name',
    filter: 'agDateColumnFilter',
    filterParams: filterParams, 
},
    
    
    {headerName:'Action',field: "name",
 cellRendererFramework:(params)=><div><TiEdit fontSize='30' color='#17a2b8'/><RiDeleteBinFill fontSize='30' color="#17a2b8"/>
</div>
  },
    ]
    var sportsData = [
        { Game: 'Badminton' },
        { Game: 'Football' },
        { Game: 'Tennis' },
        { Game: 'Golf' },
        { Game: 'Cricket' },
        { Game: 'Handball' },
        { Game: 'Karate' },
        { Game: 'Fencing' },
        { Game: 'Boxing' }
      ];
      const fields = { text: 'Game', value: 'Game' };
    
      // const countryOptions = [
      //   { key: 'af', value: 'af', text: 'shirt' },
      //   { key: 'ax', value: 'ax',  text: 'pant' },
      //   { key: 'al', value: 'al',  text: 'blazzer' },
      //   { key: 'dz', value: 'dz',  text: 'trouser' },
      //   { key: 'as', value: 'as',  text: 'saree' },
      //   { key: 'ad', value: 'ad',  text: 'frock' },
      //   { key: 'ao', value: 'ao',  text: 'household' },
      //   { key: 'ai', value: 'ai',  text: 'pillow covers' },
      //   { key: 'ag', value: 'ag',  text: 'tops' },
      //   { key: 'ar', value: 'ar', text: 'blanket' },
        
      // ]
      const serviceOptions = [
        { key: 'af', value: 'af', text: 'dry cleaning' },
        { key: 'ax', value: 'ax',  text: 'washing' },
        { key: 'al', value: 'al',  text: 'stain removal' },
        { key: 'dz', value: 'dz',  text: 'ironing' },
        { key: 'as', value: 'as',  text: 'petrol wash' },
        { key: 'ad', value: 'ad',  text: 'chemical wash' },
        { key: 'ao', value: 'ao',  text: 'silicon wash' },
        { key: 'ai', value: 'ai',  text: 'polishing' },
        { key: 'ag', value: 'ag',  text: 'steaming' },
        { key: 'ar', value: 'ar', text: 'rolling' },]
        const genderOptions = [
          { key: 'af', value: 'af', text: 'men' },
          { key: 'ax', value: 'ax',  text: 'women' },
          { key: 'al', value: 'al',  text: 'children' },
          { key: 'dz', value: 'dz',  text: 'house hold' },]
          
          const remarkOptions = [
            { key: 'af', value: 'af', text: 'stains' },
            { key: 'af', value: 'af', text: 'null' },
            { key: 'ax', value: 'ax',  text: 'dirty' },
            { key: 'al', value: 'al',  text: 'ink' },
            { key: 'dz', value: 'dz',  text: 'others' },]
            const factoryOptions = [
              { key: 'af', value: 'af', text: 'A' },
              { key: 'af', value: 'af', text: 'B' },
              { key: 'ax', value: 'ax',  text: 'C' },
              { key: 'al', value: 'al',  text: 'D' },
              { key: 'dz', value: 'dz',  text: 'E' },]
      
   
    return (
      <div >

           <div>
                  
                  <br/>
                  
         
            </div> 
               
               <div class="container billscreen">

      <Form onSubmit={submitHandler}>
          <input type='text'
           placeholder='enter yout name'
           ref={garmentNameRef}
           />
           <button type='submit'>submit</button>
          </Form>        
<Form>
  <Row>
    <Col xs="auto">
      <Form.Control placeholder="Customer Name" />
    </Col>
    <Col xs="auto">
      <Form.Control placeholder="Bill Number" />
    </Col>
    <Col>
    <FaInfo className='circle' color='white' size={30}/>
    </Col>
    
  </Row>
</Form>
<br/>

<Form onSubmit={submitHandling}
//  onSaveproductData={saveProductDataHandler}  onAddProduct={AddProductHandler}
 >
<Form.Row>
     <Form.Group as={Col} controlId="formGridState">
      <Form.Label className="formhead">Garment</Form.Label>
      
      <MultiSelectComponent id="checkbox"
                dataSource={sportsData}
                fields={fields}
                ref={garmentNameRef}
                placeholder="SelectGarnment"
                // mode="CheckBox"
                // selectAllText="Select All"
                // unSelectAllText="unSelect All"
                showSelectAll={true}>
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent>

      
    </Form.Group>

    <Form.Group as={Col} controlId="formGridState">
      <Form.Label className="formhead">Category</Form.Label>
       <Dropdown
    placeholder='Select gender'
    fluid
    search
    selection
    options={genderOptions}
    ref={categoryNameRef}
  /> 
    </Form.Group>
    
    <Form.Group as={Col} controlId="formGridState">
      <Form.Label className="formhead">Service</Form.Label>
     <Dropdown
    placeholder='Select service'
    fluid
    search
    selection
    ref={serviceNameRef}
    options={serviceOptions}
  /> 
    </Form.Group>
    
     
     
     <Form.Group as={Col} controlId="formGridCity">
      <Form.Label className="formhead">QTY</Form.Label>
      <Form.Control
       className="mt-1" 
       type="number"
       ref={qtyRef}
       />
    </Form.Group> 


    <Form.Group as={Col} controlId="formGridCity">
      <Form.Label className="formhead">N.AMT</Form.Label>
      <Form.Control className="mt-1" type='text' ref={amtRef}/>
    </Form.Group> 

     <Form.Group as={Col} controlId="formGridCity">
      <Form.Label className="formhead">Total.AMT</Form.Label>
      <Form.Control className="mt-1" type='text' ref={totalRef}/>
    </Form.Group> 

    <Form.Group as={Col} controlId="formGridState">
      <Form.Label className="formhead">Remarks</Form.Label>
       <Dropdown
    placeholder='Select remarks'
    fluid
    search
    selection
    ref={remarkRef}
    options={remarkOptions}
  /> 
    </Form.Group>
    
    <Form.Group as={Col} controlId="formGridState">
       <Form.Label class="formhead">Factory</Form.Label>
      <Dropdown
    placeholder='Select factory'
    fluid
    ref={factoryRef}
    search
    selection
    options={factoryOptions}
  /> 
    </Form.Group>

    <Form.Group as={Col} controlId="formGridEmail">
      <Form.Label className="formhead">Due Date</Form.Label>
      <Form.Control type="date"  ref={dateRef}placeholder="Due Date" className="mt-1" />
    </Form.Group>
    <Form.Group as={Col} controlId="formGridEmail">
     <Form.Label className="mt-4">
     
    {/* <FaCheckSquare fontSize='30' color='#17a2b8' className="mt-4"/> */}
    <button type='submit'>submit</button>
    </Form.Label> 
    </Form.Group>

     

  </Form.Row>
</Form>
  <br/>
  <Row>
      <Col xs={12} md={8} >
       
      <div className="ag-theme-alpine" style={ {height: '450px'} }>
            
            <AgGridReact
            columnDefs={columnDefs}
            rowData={product}
            defaultColDef={defaultColDef}
            onGridReady={onGridReady}
            enableBrowserTooltips={true}
            pagination={true}
            paginationPageSize={10}
            > 
        </AgGridReact>
            </div>

      </Col>
      <Col className='bg-info'>
      <h3 className="text-center font-weight-bolder text-red">Receipt</h3>

      <Form>
      <Form.Group as={Row} controlId="formHorizontalEmail">
    <Form.Label column sm={4} className="text-white billlab m-3">
      Bill Number
    </Form.Label>
    <Col sm={6}>
      <Form.Control type="number" placeholder="Bill Number" className="m-3" />
    </Col>
  </Form.Group>

   

<Form.Group as={Row} controlId="formHorizontalEmail">
    <Form.Label column sm={4} className="text-white billlab m-3">
      T.PC'S
    </Form.Label>
    <Col sm={6}>
      <Form.Control type="number" placeholder="total pieces" className="m-3"/>
    </Col>
  </Form.Group>

  <Form.Group as={Row} controlId="formHorizontalEmail">
    <Form.Label column sm={4} className="text-white billlab m-3">
      N.AMT
    </Form.Label>
    <Col sm={6}>
      <Form.Control type="number" placeholder="Net Amount" className="m-3" />
    </Col>
  </Form.Group>


  <Form.Row className="align-items-center">
  <Form.Label column sm={2} className="text-white billlab m-3">
      Email
    </Form.Label>
    <Col xs="auto" className="my-1">
      
      <Form.Control
        as="select"
        className="mr-sm-2"
        id="inlineFormCustomSelect"
        custom
      >
        <option value="0">Choose...</option>
        <option value="1">One</option>
        <option value="2">Two</option>
        <option value="3">Three</option>
      </Form.Control>
    </Col>
    <Col xs="auto" className="my-1">
      <Form.Control type="number" placeholder="value" />
    </Col>
    <Col xs="auto" className="my-1">
    <IoCheckbox size={40} className="mt-1"/>
    </Col>
  </Form.Row>
  
 
  <Form.Group as={Row} controlId="formHorizontalEmail">
    <Form.Label column sm={4} className="text-white billlab m-3">
      T.AMT
    </Form.Label>
    <Col sm={6}>
      <Form.Control type="number" placeholder="Total Amount" className="m-3" />
    </Col>
  </Form.Group>

  <Form.Group as={Row} controlId="formHorizontalEmail">
    <Form.Label column sm={4} className="text-white billlab m-3">
      GST@18%
    </Form.Label>
    <Col sm={6}>
      <Form.Control type="number" placeholder="Adding GST" className="m-3" />
    </Col>
  </Form.Group>



  <Form.Group as={Row} controlId="formHorizontalEmail">
    <Form.Label column sm={4} className="text-white billlab m-3">
      GRAND TOTAL
    </Form.Label>
    <Col sm={6}>
      <Form.Control type="number" placeholder="Amount" className="m-3" />
    </Col>
  </Form.Group>
<Form.Row>
  
  <Col>
  
  </Col>
  <Col> 
 
  </Col>
  <Col>
  <Button variant="danger">
    check out
  </Button>
  </Col>
</Form.Row>
<Form.Row>
<Form.Label column sm={10} className="billhead mt-5">
<Button variant="success" size="lg" >
    check out
  </Button>
    </Form.Label>
</Form.Row>
      </Form>


      </Col>
      </Row>  
                  
  </div>
    </div>       
              
    );
}

export default Exam;





