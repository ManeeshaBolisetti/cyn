import React from 'react';
// import {Button} from 'react-bootstrap';
import { FcRefresh } from "react-icons/fc";
function Refresh(){
    function refreshPage() {
        window.location.reload();
      }
return(
    <div onClick={refreshPage}  >
        Refresh <FcRefresh size='20'/>
    </div>
);
}
export default Refresh;