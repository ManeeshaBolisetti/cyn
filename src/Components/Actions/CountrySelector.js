import { CheckBoxSelection, Inject, MultiSelectComponent } from '@syncfusion/ej2-react-dropdowns';
import { useRef } from 'react';
import UseFetch from '../UseFetch';

const CountrySelector=(props)=>{
    const countries=UseFetch('/admin/countriesList');
    const fields = { text: 'countryName', value: 'countryId' };
    const countriesRef=useRef();
    props.Countries(countries);
    return(
        <MultiSelectComponent id="checkbox" className='dropdown '
        dataSource={countries}
        // fields={fields}
        fields={fields}
      // value={fields.value}
        ref={countriesRef}
        placeholder="Select Countries"
        mode="CheckBox"
        selectAllText="Select All"
        unSelectAllText="unSelect All"
        showSelectAll={true}>
        <Inject services={[CheckBoxSelection]} />
      </MultiSelectComponent>
     
    );
} 
export default CountrySelector;
