import {React, useState} from 'react';
import DistanceInput from './DistanceInput';
import { Form } from 'react-bootstrap';
import { TiFolderAdd } from 'react-icons/ti';
// import api from './../AxiosP';
// import axios from 'axios';
import api from './../Api';
const AddGender=(props)=>{
    let statu='';
    const [showForm, setshowForm] = useState(false);
    const [status,setStatus]=useState(statu);
    const [color,setColor]=useState('');
    
    
    const saveDistanceDataHandler = (enterdDistanceData) => {
        const DistanceData = {
            ...enterdDistanceData,
            
            // id: Math.random().toString()
        };
        console.log(DistanceData)
        // axios.post('/admin/addDistances', DistanceData)
        api.addDistance(DistanceData)
        .then(response => {
            setStatus({ data:response.data.message });
        props.onAddDistance(response.data.data);
        setColor('success')
        setTimeout(() => {
            setColor('')
            setStatus('')
          setshowForm(false)
        }, 3000);
         } )
        .catch(error => {
            if (error.response) {
                setStatus({ data:(error.response.data.message )});
                setColor('error')
            }else{
                setStatus({ data: error.message });
                setColor('error')
                console.error('There was an error!', error);

            }
           
        });
       
        // fetch('https://appdev.connectyourneed.com/admin/addDistances', {

        //     method: "POST",
        //     headers: { 'Content-Type': 'application/json' },
        //     body: JSON.stringify(DistanceData)
        // }).then((response) => {
        //     if(!response.ok){
        //         if(response.status===500){
        //             setStatus('Alredy existed');
        //         }else
        //             setStatus(response.message)
                
        //     //    setStatus(response.status) ;
        //     } 
        //     else return response.json();
        // })
        //  .then((data) => 
        // {
        //    if(data){
        //     setStatus(data.message);
        //     console.log(data);
        //     // props.onAddCountry(countryData);

        //    }
        //     // console.log(data);
        //     // console.log(data.message);
            
        // })
        // console.log(DistanceData)
        

        // setshowForm(false);
    };
    const onShowForm = () => {
        setshowForm(true);
    }
    const onHideForm = () => {
        setshowForm(false);
        setStatus("Add A Gender");
    }
    return(
        <div>
           {showForm &&
             <DistanceInput
            name='Add Distance'
            // cName='Country Name'
            countries='select countries'
            stat={status.data}
                color={color}
            onClose={onHideForm}
            onSaveDistanceData={saveDistanceDataHandler}
            // onCancel={stopEditHandler}
             />}
            
                {/* <h4 >Admin/Add Merchant</h4> */}

                <Form><div onClick={onShowForm}><TiFolderAdd fontSize='20' color='green' /> Add Distance </div></Form>
 
        </div>
    );
}
export default AddGender;