import {React, useState} from 'react';
import EditDistanceInput from './EditDistanceInput';
import { Form } from 'react-bootstrap';
import { TiFolderAdd } from 'react-icons/ti';
// import axios from 'axios';
import api from './../Api';
// import axios from './../AxiosP';
const EditGender=(props)=>{
    let statu='';
    let value=props.value
    const [showForm, setshowForm] = useState(false);
    const [status,setStatus]=useState(statu);
    const [color,setColor]=useState('');
    
    
    const saveDistanceDataHandler = (enterdDistanceData) => {
        const DistanceData = {
            ...enterdDistanceData,
            
            // id: Math.random().toString()
        };
        console.log(DistanceData)
        // axios.post('admin/updateDistance', DistanceData)
        api.UpdateDistance(DistanceData)
        .then(response => {
            setStatus({ data:response.data.message });
        console.log(response.data.message)
      
        props.onUpdateDistance(response.data.data);
        setColor('success')
        // setFlash(true);
        setTimeout(() => {
            setColor('')
            setStatus('')
          setshowForm(false)
          // setFlashE(null);
        }, 3000);
        // window.location.reload();
         } )
        .catch(error => {
            if (error.response) {
                setStatus({ data:(error.response.data.message )});
                setColor('error')
                // console.log(error.response.data.message);
            }else{
                setStatus({ data: error.message });
                console.error('There was an error!', error);
                setColor('error')

            }
           
        });
       
        // fetch('https://appdev.connectyourneed.com/admin/addDistances', {

        //     method: "POST",
        //     headers: { 'Content-Type': 'application/json' },
        //     body: JSON.stringify(DistanceData)
        // }).then((response) => {
        //     if(!response.ok){
        //         if(response.status===500){
        //             setStatus('Alredy existed');
        //         }else
        //             setStatus(response.message)
                
        //     //    setStatus(response.status) ;
        //     } 
        //     else return response.json();
        // })
        //  .then((data) => 
        // {
        //    if(data){
        //     setStatus(data.message);
        //     console.log(data);
        //     // props.onAddCountry(countryData);

        //    }
        //     // console.log(data);
        //     // console.log(data.message);
            
        // })
        console.log(DistanceData)
        props.onUpdateDistance(DistanceData);

        // setshowForm(false);
    };
    const onShowForm = () => {
        if(value.length===1){
        setshowForm(true);
    }else{
        alert('please select edit Row')
    }
    }
   
    const onHideForm = () => {
        setshowForm(false);
        setStatus("update A Gender");
    }
    return(
        <div>
           {showForm &&
             <EditDistanceInput
            name='Edit Distance'
            // cName='Country Name'
            countries='select countries'
            stat={status.data}
                color={color}
            onClose={onHideForm}
            onSaveDistanceData={saveDistanceDataHandler}
            val={value}
            // onCancel={stopEditHandler}
             />}
            
                {/* <h4 >Admin/Add Merchant</h4> */}

                <Form><div onClick={onShowForm}><TiFolderAdd fontSize='20' color='green' />
                 Update </div></Form>
 
        </div>
    );
}
export default EditGender;