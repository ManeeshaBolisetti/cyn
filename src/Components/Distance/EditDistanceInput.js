import { useState, useRef } from 'react';
import { Form, Button } from 'react-bootstrap';
// import { form } from 'react-dom-factories';
import UseFetch from './../UseFetch';
import Card from '../Card/Card';
import classes from './DistanceInput.Module.css';
import Alert from '@material-ui/lab/Alert';
import { CheckBoxSelection, Inject, MultiSelectComponent } from '@syncfusion/ej2-react-dropdowns';

const DistanceInput = (props) => {
  const [flash, setFlash] = useState(null);
  const color=props.color;
  const styles = {
    alert: {
      zIndex: '1500',
    },
  }
    let value=props.val
  const Invalid = '';
  const valid = (Invalid);
  // const [countries, setCountries] = useState([]);
//   const formRef = useRef();
  const minDistanceRef = useRef();
  const maxDistanceRef = useRef();
  const countryRef = useRef();
  const unitsRef=useRef();
  console.log(value);
  // let dCountries=value.length === 1 ? value[0].countries: '';
  let dCountryId=value.length === 1 ? value[0].countryId: ''
  // let cImage=value.length === 1 ? value[0].categoryImage: '';
  let dMin=value.length === 1 ? value[0].minDistance: '';
  let dMax=value.length === 1 ? value[0].maxDistance: '';
  let dUnits=value.length === 1 ? value[0].units: '';
  let dId=value.length === 1 ? value[0].distanceId: '';
//   let dStatus=value.length === 1 ? value[0].status: '';
  // console.log(dCountries,dMin,dMax,dId)
  // useEffect(() => {

  //   axios.get('https://appdev.connectyourneed.com/admin/countriesList')
  //     .then(response => {
  //       const country = response.data.data;
  //       // console.log(country);

  //       setCountries(country)
  //     });

  //   // empty dependency array means this effect will only run once (like componentDidMount in classes)
  // }, []);
  const countries=UseFetch('/admin/countriesList')
  // const fields = { text: 'countryName', value: 'countryId' };
  // const colorValues= ([dCountryId]);
  const fields = { text: 'countryName', value: 'countryId' };
   let colorValues= dCountryId.map((n)=>{
      let colour=n;
      return colour
  });
  const onTagging = (e) => {
    // set the current selected item text as class to chip element.
    e.setClass(e.itemData[fields.text].toLowerCase());
};
  console.log(colorValues)
   const submitHandler = (event) => {
    setFlash(true);
    setTimeout(() => {
      setFlash(null);
      
      // setFlashE(null);
    }, 5000);
    event.preventDefault();
    const minDistance = minDistanceRef.current.value;
    const maxDistance = maxDistanceRef.current.value;
    const country = countryRef.current.value;
   const units=unitsRef.current.value;
    // if (minDistance.trim().length === 0 || maxDistance.trim().length=== 0 ) {
    //   setValid('Invalid Country or code or currency');
    //   return console.log('Invalid Country and code and currency');

    // }

    const distanceData = {
      minDistance:minDistance,
        maxDistance: maxDistance,
        countries: country,
        distanceId:dId,
        units:units,
    };
    console.log(distanceData);
    props.onSaveDistanceData(distanceData);
    // minDistanceRef.current.value = '';
    // maxDistanceRef.current.value='';
    // countryRef.current.value = '';

  }
  return (
    <div className={classes.input}>
      <Card className={classes.modal}>
        <header className={classes.header}>
          <h2>{props.name}</h2>
        </header>
        {
        flash
        ? <Alert style={styles.alert} severity={color}>{props.stat}</Alert>
        : null
      }
        <Form onSubmit={submitHandler} > <div className={classes.content}>

          <Form.Control
            type='text'
            placeholder='minimunm distance'
            defaultValue={dMin}
            //  value={enterdCountry}
            ref={minDistanceRef}
          // onChange={cNameChangeHandler}
          />
          <br />
          <Form.Control
            type='text'
            placeholder='maximum distance'
            defaultValue={dMax}
            //  value={enterdCountry}
            ref={maxDistanceRef}
          // onChange={cNameChangeHandler}
          />
          <Form.Control
            type='text'
            placeholder='units'
            defaultValue={dUnits}
            //  value={enterdCountry}
            ref={unitsRef}
          // onChange={cNameChangeHandler}
          />
          {/* <Form.Control
            type='text'
            placeholder={props.countries}
            //  value={enterdCode}
            ref={countryRef}
          // onChange={codeChangeHandler}
          /><br /> */}
          {/* {console.log(dCountries[0])} */}
          <MultiSelectComponent id="chip-customization" className='dropdown '
                dataSource={countries}
                
                // fields={fields}
                fields={fields}
                ref={countryRef}
                placeholder={props.countries}
                mode="CheckBox"
                selectAllText="Select All"
                unSelectAllText="unSelect All"
                tagging={onTagging}
                value={colorValues}
                // tagging={onTagging = onTagging}
                showSelectAll={true}>
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent><br />
          
          <h4>{valid}</h4>
         
        </div>
          <footer className={classes.actions}>
            <Button className={classes.btn}
              onClick={props.onClose}
            >cancel</Button>
            <Button
              type='submit'
            >Update</Button>
          </footer></Form>
      </Card>
    </div>
  );
}
export default DistanceInput;