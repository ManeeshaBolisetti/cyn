import {useState,useEffect} from 'react';
// import axios from './AxiosP';
// import axios from 'axios';
import api from './Api';
export default function UseFetch(url){
   
    const [data,setData]= useState([]);

    useEffect(()=>{
    api.get(url)
        .then(response => {
            let data = response.data.data;
            setData(data);
            // console.log(data)
        },
            error => {
                console.log(error);
            }

        );
    },[url]);
    return data;
}