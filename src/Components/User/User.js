import { React, useState } from 'react';
import { AgGridReact } from 'ag-grid-react';
import api from './../Api';
import 'ag-grid-enterprise';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import * as moment from 'moment';
import Search from './../Country/Search';
import { RiFolderDownloadLine } from "react-icons/ri";
import { Button, Row, Col } from 'react-bootstrap';
import Refresh from '../Actions/Refresh';
import Sidebar from './../../Components/Sidebar';
import NavBar from '../NavBar';
// import CustomLoadingOverlay from './../B2B/B2BRequest/customLoadingOverlay';

var filterParams = {
    comparator: function (filterLocalDateAtMidnight, cellValue) {
        var dateAsString = cellValue;
        if (dateAsString == null) return -1;
        var dateParts = dateAsString.split('/');
        var cellDate = new Date(
            Number(dateParts[2]),
            Number(dateParts[1]) - 1,
            Number(dateParts[0])
        );

        if (filterLocalDateAtMidnight.getTime() === cellDate.getTime()) {
            return 0;
        }

        if (cellDate < filterLocalDateAtMidnight) {
            return -1;
        }

        if (cellDate > filterLocalDateAtMidnight) {
            return 1;
        }
    },
    browserDatePicker: true,
    minValidYear: 2000,
};
const User = () => {
    // let category = UseFetch('/admin/categoriesList');
    // const rowData = [cate]
    const [user, setUser] = useState([]);
    // const [users, setUsers] = useState([]);
    const [gridApi, setGridApi] = useState(null);
    // const [girdColumnApi, setGridColumnApi] = useState(null);
    // const [hideColumn, setHideColumn] = useState(true);
    // const [suppressClickEdit, setSuppressClickEdit] = useState(true);
    // const [edit, setEdit] = useState('Edit');
    // const [rowData, setRowData] = useState(null);

    // useEffect(() => {

    //     api.getUser()
    //         .then(response => {
    //             let user = response.data.data;
    //             setUser(user);
    //             console.log(user);
    //         });

    // }, []);
    let userList = [];
    console.log(user)
    if (user != null && user.length > 0) {

        user.map((n) => {
            let ucDate = moment(n.userCreatedAt).format('DD/MM/YYYY');
            let ctime = moment(n.userCreatedAt).format('h:mma');
    //   let utime = moment(n.updatedAt).format('h:mma');

            return userList.push({
                'userName': n.userName,
                'userEmail': n.userEmail,
                'userMobile': n.userMobile,
                'userProfilePic': n.userProfilePic,
                'userLocation': n.userLocation,
                'currentCountryCode': n.currentCountryCode,
                'countryCode': n.countryCode,
                'deviceType': n.deviceType,
                'userCreatedAt': ucDate,
                'ctime':ctime,
        // 'utime':utime,

            })



        })
    }






    const columnDefs = [

        // { headerName: "Flag", field: "cFlag"}, 

        {
            field: "userName", filter: 'agSetColumnFilter',
            // editable: false,
        },
        {
            field: "userEmail", filter: 'agSetColumnFilter',
            // editable: false,
        },
        {
            field: "userMobile", filter: 'agSetColumnFilter',
            // editable: false,
        },
        {
            field: "userProfilePic",
            filter: 'agSetColumnFilter',
            cellRenderer: countryCellRenderer,
            // editable: false,
        },
        {
          headerName:'Location',  field: "userLocation.locationName", filter: 'agSetColumnFilter',
          
        },
        {
            field: "currentCountryCode", filter: 'agSetColumnFilter',
          
        },
        {
            field: "countryCode", filter: 'agSetColumnFilter',
          
        },
        {
            field: "deviceType", filter: 'agSetColumnFilter',
          
        },
        {
            field: "userCreatedAt", filter: 'agDateColumnFilter',cellRenderer: createdDate,
            filterParams: filterParams,
            // editable: false,
        },
        // {
        //     field: "updatedAt", filter: 'agDateColumnFilter',
        //     filterParams: filterParams,
        // },
       
    ]
    const defaultColDef = {
        sortable: true,

        flex: 1,
        //    filter: true,
        minWidth: 200,
        resizable: true,
        floatingFilter: true,
        
        // floatingFilter: true
    }
    const onGridReady = (params) => {
        
        params.api.showLoadingOverlay();
        setGridApi(params);
        api.getUser()
                .then(response => {
                    let user = response.data.data;
                    setUser(user);
                    params.api.applyTransaction({add:user})
                    // console.log(user);
                });
    }
    const pageSize = (pageSize) => {
        // console.log(pageSize);
        gridApi.api.paginationSetPageSize(Number(pageSize))
    }

    const onExportClick = () => {
        gridApi.api.exportDataAsCsv();
    }
  
    const onFilterTextChange = (e) => {

        gridApi.api.setQuickFilter(e.target.value);
    }
   

    return (
        <div>
            <NavBar />
            <Sidebar />
            <Row className='con'>


                {/* <Col xs={10}><h1>Add Catgory</h1></Col> */}
                <Col xs={11}>
                    <div className='d-flex justify-content-between'><h2>Users</h2>
                        <Search onChange={onFilterTextChange} /></div>
                    <Row>
                        <Col xs={10} style={{ display: '-webkit-box' }}>

                           <Button className='add' onClick={() => onExportClick()}>
                                Export to Excel  <RiFolderDownloadLine color='green' />
                            </Button>

                            <Button className='add' ><Refresh /></Button>
                            </Col>

                        <Col xs={2} >
                           
                        </Col>

                    </Row>

                    <div className="ag-theme-alpine" style={{ height: '400px' }}>
                        <AgGridReact

                            
                            // suppressClickEdit={suppressClickEdit}
                            pagination={true}
                            paginationPageSize={25}
                            columnDefs={columnDefs}
                            rowData={userList}
                            defaultColDef={defaultColDef}
                            onGridReady={onGridReady}>

                        </AgGridReact>
                        <div className='selectbox' style={{
                            padding: '10px',
                            marginTop: '-10px', fontSize: 'small',
                        }}> Page Size: <select onChange={(e) => pageSize(e.target.value)}>
                                <option value='10'>10</option>
                                <option value='25'>25</option>
                                <option value='50'>50</option>
                                <option value='100'>100</option>
                            </select></div>
                    </div>
                </Col>

            </Row>
        </div>
    );
}
export default User;

function countryCellRenderer(params) {
    var image = (params.data.userProfilePic);
    var logo;
    if (image != null){
         logo =
        
        '<img   width="40" alt="none" height="40" src=" ' + image +
        '">';
    
    }
    else{
         logo ='No Image';
    }
    return (

        '<span style="cursor: default;">' + logo + '</span>'
    );
}


function createdDate(params) {
    var time = (params.data.ctime);
    var date = (params.data.userCreatedAt);
    
    return (
  
        '<span style="cursor: default;">' +date+' ' +time+ '</span>'
    );
  }
//   function updatedDate(params) {
//     var time = (params.data.utime);
//     var date = (params.data.updatedAt);
    
//     return (
  
//         '<span style="cursor: default;">' +date+' ' +time+ '</span>'
//     );
//   }