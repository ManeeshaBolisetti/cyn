import { React, useState, useEffect } from 'react';
import { AgGridReact } from 'ag-grid-react';
import api from './../Api';
import 'ag-grid-enterprise';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import * as moment from 'moment';
import Search from './../Country/Search';
import { RiFolderDownloadLine } from "react-icons/ri";
import { Button, Row, Col } from 'react-bootstrap';
import Refresh from '../Actions/Refresh';
import Sidebar from './../../Components/Sidebar';
import NavBar from '../NavBar';

var filterParams = {
    comparator: function (filterLocalDateAtMidnight, cellValue) {
        var dateAsString = cellValue;
        if (dateAsString == null) return -1;
        var dateParts = dateAsString.split('/');
        var cellDate = new Date(
            Number(dateParts[2]),
            Number(dateParts[1]) - 1,
            Number(dateParts[0])
        );

        if (filterLocalDateAtMidnight.getTime() === cellDate.getTime()) {
            return 0;
        }

        if (cellDate < filterLocalDateAtMidnight) {
            return -1;
        }

        if (cellDate > filterLocalDateAtMidnight) {
            return 1;
        }
    },
    browserDatePicker: true,
    minValidYear: 2000,
};
const QrCodes = () => {
    // let category = UseFetch('/admin/categoriesList');
    // const rowData = [cate]
    const [user, setUser] = useState([]);
    // const [users, setUsers] = useState([]);
    const [gridApi, setGridApi] = useState(null);
    // const [girdColumnApi, setGridColumnApi] = useState(null);
    // const [hideColumn, setHideColumn] = useState(true);
    // const [suppressClickEdit, setSuppressClickEdit] = useState(true);
    // const [edit, setEdit] = useState('Edit');
    // const [rowData, setRowData] = useState(null);

    useEffect(() => {

        api.getQrCodes()
            .then(response => {
                let user = response.data.data;
                setUser(user);
                console.log(user);
            });

    }, []);
    let userList = [];
    if (user != null && user.length > 0) {

        user.map((n) => {
            let rDate = moment(n.requestDate).format('DD/MM/YYYY');

            let ctime = moment(n.requestDate).format('h:mma');
            // let utime = moment(n.updatedAt).format('h:mma');
            return userList.push({
                'userName': n.userRef.userName,
                'userCode': n.userRef.countryCode,
                'userMobile': n.userRef.userMobile,
                'genderPreference': n.genderPreference,
                'qrTurnOn': n.qrTurnOn,
                'productName': n.productRef.productName,
                'countryCode': n.countryCode,
                'personType': n.personTypeRef.personType,
                'productType': n.productType,
                'title': n.title,
                'minAmount': n.minAmount,
                'maxAmount': n.maxAmount,
                'connectRange': n.connectRange,
                'requestDate': rDate,
                'ctime':ctime,
        // 'utime':utime,

            })



        })
    }






    const columnDefs = [
       
        {
            field: "userName", filter: 'agSetColumnFilter',
            // editable: false,
        },
        {
            field: "userCode", filter: 'agSetColumnFilter',
            // editable: false,
        },
        {
            field: "userMobile", filter: 'agSetColumnFilter',
            // editable: false,
        },
        {
            field: "genderPreference",
            filter: 'agSetColumnFilter',
            // editable: false,
        },
        {
            field: "qrTurnOn", filter: 'agSetColumnFilter',

        },
        
        {
            field: "productName", filter: 'agSetColumnFilter',

        },
        {
            field: "countryCode.countryName", filter: 'agSetColumnFilter',

        },
        {
            field: "personType", filter: 'agSetColumnFilter',

        },
        {
            field: "productType", filter: 'agSetColumnFilter',

        },
        {
            field: "title", filter: 'agSetColumnFilter',

        },
       
        {
            field: "minAmount", filter: 'agSetColumnFilter',

        },
        {
            field: "maxAmount", filter: 'agSetColumnFilter',

        },
        
        {
            field: "connectRanget",  filter: 'agSetColumnFilter',
            // editable: false,
        },
        {
            field: "requestDate", filter: 'agDateColumnFilter',cellRenderer: createdDate,
            filterParams: filterParams,
        },

    ]
    const defaultColDef = {
        sortable: true,

        flex: 1,
        //    filter: true,
        minWidth: 200,
        resizable: true,
        floatingFilter: true,

        // floatingFilter: true
    }
    const onGridReady = (params) => {
        console.log("grid is ready")
        setGridApi(params);
        // setGridColumnApi(params.columnApi);
    }
    const pageSize = (pageSize) => {
        console.log(pageSize);
        gridApi.api.paginationSetPageSize(Number(pageSize))
    }

    const onExportClick = () => {
        gridApi.api.exportDataAsCsv();
    }

    const onFilterTextChange = (e) => {

        gridApi.api.setQuickFilter(e.target.value);
    }


    return (
        <div>
            <NavBar />
            <Sidebar />
            <Row className='con'>


                {/* <Col xs={10}><h1>Add Catgory</h1></Col> */}
                <Col xs={11}>
                    <div className='d-flex justify-content-between'><h2>QrCodes</h2>
                        <Search onChange={onFilterTextChange} /></div>
                    <Row>
                        <Col xs={10} style={{ display: '-webkit-box' }}>

                            <Button className='add' onClick={() => onExportClick()}>
                                Export to Excel  <RiFolderDownloadLine color='green' />
                            </Button>

                            <Button className='add' ><Refresh /></Button>
                        </Col>

                        <Col xs={2} >

                        </Col>

                    </Row>

                    <div className="ag-theme-alpine" style={{ height: '400px' }}>
                        <AgGridReact


                            // suppressClickEdit={suppressClickEdit}
                            pagination={true}
                            paginationPageSize={25}
                            columnDefs={columnDefs}
                            rowData={userList}
                            defaultColDef={defaultColDef}
                            onGridReady={onGridReady}>

                        </AgGridReact>
                        <div className='selectbox' style={{
                            padding: '10px',
                            marginTop: '-10px', fontSize: 'small',
                        }}> Page Size: <select onChange={(e) => pageSize(e.target.value)}>
                                <option value='10'>10</option>
                                <option value='25'>25</option>
                                <option value='50'>50</option>
                                <option value='100'>100</option>
                            </select></div>
                    </div>
                </Col>

            </Row>
        </div>
    );
}
export default QrCodes;

// function countryCellRenderer(params) {
//     var image = (params.data.userProfilePic);
//     var logo =

//         '<img style="background-color: brown; "  width="50" alt="image" height="30" src=" ' + image +
//         '">';
//     return (

//         '<span style="cursor: default;">' + logo + '</span>'
//     );
// }


function createdDate(params) {
    var time = (params.data.ctime);
    var date = (params.data.requestDate);
    
    return (
  
        '<span style="cursor: default;">' +date+' ' +time+ '</span>'
    );
  }
//   function updatedDate(params) {
//     var time = (params.data.utime);
//     var date = (params.data.updatedAt);
    
//     return (
  
//         '<span style="cursor: default;">' +date+' ' +time+ '</span>'
//     );
//   }