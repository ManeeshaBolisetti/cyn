
//import useState hook to create menu collapse state
import React, { useState } from "react";

//import react pro sidebar components
import {
  ProSidebar,
  Menu,
  MenuItem,
  SidebarHeader,
  SidebarFooter,
  SidebarContent,
  SubMenu,
} from "react-pro-sidebar";
import {
  // BrowserRouter as Router,
  // Switch,
  // Route,
  Link
} from "react-router-dom";
//import icons from react icons
import {FcGlobe,FcBusiness } from "react-icons/fc";
import {  FiLogOut, FiArrowLeftCircle, FiArrowRightCircle } from "react-icons/fi";
import { FaProductHunt,FaAmbulance } from "react-icons/fa";
import { GiPathDistance} from "react-icons/gi";
import { SiSmartthings } from "react-icons/si";
import { BsPersonBoundingBox } from "react-icons/bs";
import { RiGenderlessFill } from "react-icons/ri";

import { ImVideoCamera } from "react-icons/im";
//import sidebar css from react-pro-sidebar module and our custom css 
import "react-pro-sidebar/dist/css/styles.css";
import "./Sidebar.css";


const Sidebar = () => {
  
    //create initial menuCollapse state using useState hook
    const [menuCollapse, setMenuCollapse] = useState(true)

    //create a custom function that will change menucollapse state from false to true and true to false
  const menuIconClick = () => {
    //condition checking to change state from true to false and vice versa
    menuCollapse ? setMenuCollapse(false) : setMenuCollapse(true);
  };

  return (
    <>
      <div id="header">
          {/* collapsed props to change menu size using menucollapse state */}
        <ProSidebar collapsed={menuCollapse}>
          <SidebarHeader>
          <div className="logotext">
              {/* small and big change using menucollapse state */}
              <p>{menuCollapse ? "CYN" : "ConnectYourNeed"}</p>
            </div>
            <div className="closemenu" onClick={menuIconClick}>
                {/* changing menu collapse icon on click */}
              {menuCollapse ? (
                <FiArrowRightCircle/>
              ) : (
                <FiArrowLeftCircle/>
              )}
            </div>
          </SidebarHeader>
          <SidebarContent>
            <Menu iconShape="square">
              <MenuItem active={true} icon={<FcGlobe /> }>
              <Link to="/country"> <p style={{color:'white'}}>Country</p></Link>
               
              </MenuItem>
              <MenuItem icon={<SiSmartthings />}> <Link to="/category"> <p style={{color:'white'}}>Category</p></Link></MenuItem>
              <MenuItem icon={<BsPersonBoundingBox />}> <Link to="/person"> <p style={{color:'white'}}>PersonTypes</p></Link></MenuItem>
              <MenuItem icon={<RiGenderlessFill />}> <Link to="/gender"> <p style={{color:'white'}}>Gender</p></Link></MenuItem>
              <MenuItem icon={<GiPathDistance />}> <Link to="/distance"> <p style={{color:'white'}}>Distance</p></Link></MenuItem>
              <MenuItem icon={<FaProductHunt />}> <Link to="/products"> <p style={{color:'white'}}>Products</p></Link></MenuItem>
              <MenuItem icon={<ImVideoCamera />}> <Link to="/demo"> <p style={{color:'white'}}>Video Links</p></Link></MenuItem>
              
              {/* <MenuItem icon={<FaAmbulance />}>Emergency</MenuItem> */}
              <SubMenu title="Emergency" icon={<FaAmbulance  />}>
      
      <MenuItem> <Link to="/blood"> <p style={{color:'white'}}>Blood Group</p></Link></MenuItem>
      <MenuItem> <Link to="/emergency"> <p style={{color:'white'}}>Emergency Type</p></Link></MenuItem>
      {/* <MenuItem>Marketing Department Login</MenuItem> */}
    </SubMenu>
    <SubMenu style={{color:'white'}} title="B2B" icon={<FcBusiness  />}>
      
      <MenuItem> <Link to="/website"> <p style={{color:'white'}}>Website</p></Link></MenuItem>
      <MenuItem> <Link to="/socialMedia"> <p style={{color:'white'}}>SocialMedia</p></Link></MenuItem>
      
      <MenuItem> <Link to="/subscription"> <p style={{color:'white'}}>Subscriptions</p></Link></MenuItem>
      <MenuItem> <Link to="/b2b"> <p style={{color:'white'}}>B2b requests</p></Link></MenuItem>
      
      {/* <MenuItem>Marketing Department Login</MenuItem> */}
    </SubMenu>
    {/* <SubMenu title="GeneralSettings" icon={<MdSettingsBrightness  />}>
      <MenuItem>Category and Products</MenuItem>
      <MenuItem>Emergency Needs</MenuItem>
      <MenuItem>Ad Prices</MenuItem>
      <MenuItem>Verfication Prices</MenuItem>
      
    </SubMenu>
    <SubMenu title="Departments" icon={<FcDepartment  />}>
      <MenuItem>EmergencyList</MenuItem>
      
    </SubMenu>
              <SubMenu title="profile" icon={<FiUsers  />}>
      <MenuItem>Users List</MenuItem>
      <MenuItem>Requests</MenuItem>
      <MenuItem>Enquires</MenuItem>
      <MenuItem>Feedbacks</MenuItem>
    </SubMenu> */}
    {/* <SubMenu title="Settings" icon={<BiCog  />}>
              <MenuItem >Edit Profile</MenuItem>
              <MenuItem >My Inbox</MenuItem>
              
              </SubMenu>         */}
            </Menu>
          </SidebarContent>
          <SidebarFooter>
            <Menu iconShape="square">
              <MenuItem icon={<FiLogOut />}>Logout</MenuItem>
            </Menu>
          </SidebarFooter>
        </ProSidebar>
      </div>
    </>
  );
};

export default Sidebar;
