import { useState, useRef } from 'react';
import { Form, Button } from 'react-bootstrap';
import Card from '../../Card/Card';
import classes from './SocialMedia.Module.css';
// import axios from 'axios';
import { CheckBoxSelection, Inject, MultiSelectComponent } from '@syncfusion/ej2-react-dropdowns';
import UseFetch from '../../UseFetch';
import Alert from '@material-ui/lab/Alert';
const SocialMediaInput = (props) => {
  const [flash, setFlash] = useState(null);
  const color=props.color;
  const styles = {
    alert: {
      zIndex: '1500',
    },
  }
  const Invalid = '';
  let value=props.val
  const [valid, setValid] = useState(Invalid);
  let sImage=value.length === 1 ? value[0].image: '';
  const [image,setImage]=useState({file:sImage});
  
  let sCountryId=value.length === 1 ? value[0].countryId: '';
  // let sCountries=value.length === 1 ? value[0].countries: '';
 
  let sSocilaMediaId=value.length === 1 ? value[0].socilaMediaId: '';
  let sApp=value.length === 1 ? value[0].app: '';
  
  // const [category, setCategory] = useState([]);
  // const [countries, setCountries] = useState([]);
  const appRef = useRef();
  const imageRef = useRef();
  const countryNameRef = useRef();

  const countries=UseFetch('/admin/countriesList');
  const fields = { text: 'countryName', value: 'countryId' };
  let colorValues= sCountryId.map((n)=>{
    let colour=n;
    return colour
});
// console.log(colorValues)
const onTagging = (e) => {
  // set the current selected item text as class to chip element.
  e.setClass(e.itemData[fields.text].toLowerCase());
};
//   const category=UseFetch('admin/categoriesList');
//   const optionItems = category.map((categories) => <option value={categories.categoryId}>{categories.categoryName}</option>)

  const submitHandler = (event) => {
    event.preventDefault();
    setFlash(true);
    setTimeout(() => {
      setFlash(null);
      
      // setFlashE(null);
    }, 5000);
    const app = appRef.current.value;
    // const image = image;
    const Countries = countryNameRef.current.value;
    const images=imageRef.current.files[0];
    console.log(image);
   
    const SocialMediaData = {
        app: app,
        socailMediaIcon:sImage ,
       countries: Countries,
       countryId: sCountryId,
       image:images,
       socilaMediaId:sSocilaMediaId,


    };
    console.log(SocialMediaData);
    props.onSaveSocialMediaData(SocialMediaData);
    setValid('');
  }

  const handleChange=(event)=> {
    
    setImage( {
     file: URL.createObjectURL(event.target.files[0])
   })
   
 }
 const RemoveImage = () => {
  const RemoveImage = {
    imageUrl: sImage,
  };
  setFlash(true);
  setTimeout(() => {
    setFlash(null);
    // setFlashE(null);
  }, 5000);
  props.onRemoveImage(RemoveImage);
  
}

  return (
    <div>
      <div className={classes.input}>
        <Card className={classes.modal}>
          <header className={classes.header}>
            <h2>{props.name}</h2>
          </header>
          {
        flash
        ? <Alert style={styles.alert} severity={color}>{props.stat}</Alert>
        : null
      }
      
      <button onClick={RemoveImage}>Remove</button>
          <Form
            onSubmit={submitHandler}
          > <div className={classes.content}>


              
              <Form.Control
                type='text'
                defaultValue={sApp}
                placeholder='app Name'
                ref={appRef}
              /><br/>
              
              <Form.Control
                type='file'
                onChange={ handleChange}
                placeholder='Image'
                ref={imageRef}
              /><br />
              <img className={classes.image}  src={image.file} alt='previw' />

                <MultiSelectComponent id="checkbox" className='dropdown '
                dataSource={countries}
                // fields={fields}
                fields={fields}
              // value={fields.value}
                ref={countryNameRef}
                placeholder="Select Countries"
                mode="CheckBox"
                selectAllText="Select All"
                unSelectAllText="unSelect All"
                tagging={onTagging}
                value={colorValues}
                showSelectAll={true}>
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent>
             
            </div>
            <h4>{valid}</h4>
            <footer className={classes.actions}>
              <Button className={classes.btn}
                onClick={props.onClose}
              >cancel</Button>
              <Button
                type='submit'
              >Update</Button>
            </footer></Form>
        </Card>
      </div>

    </div>
  );

}
export default SocialMediaInput;