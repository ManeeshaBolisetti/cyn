import { React, useState } from 'react';
import { Form } from 'react-bootstrap';
import {CgAddR  } from 'react-icons/cg';
import SocialMediaInput from "./SocialMediaInput";
// import axios from 'axios';
import api from './../../Api';
const AddSocialMedia = (props) => {
    // const [flash, setFlash] = useState(null);
    // const [errFlash, setErrFlash] = useState(null);
    const [color,setColor]=useState('');
    
    let statu = '';
    const [showForm, setshowForm] = useState(false);
    const [status, setStatus] = useState(statu);
    // const [errStatus,setErrStatus]=useState('');
    const onShowForm = () => {
        setshowForm(true);
    };
    const onHideForm = () => {
        setshowForm(false);
    };
    const saveSocialMediaDataHandler = (enterdSocialMediaData) => {
        const SocialMediaData = {
            ...enterdSocialMediaData,

        };
        // console.log(enterdWebsiteData.categoryImage);

        let formData = new FormData();
        formData.append('app', enterdSocialMediaData.app);
        formData.append('image', enterdSocialMediaData.image);
        // formData.append('countries[]', [enterdWebsiteData.countries] );
        for (let i = 0; i < enterdSocialMediaData.countries.length; i++) {
            formData.append('countries[]', enterdSocialMediaData.countries[i])
        }

        // let url = '/Admin/addSocialMedia'
        // axios.post(url, formData )
        api.addSocial(formData)
        .then(response => {
           
           setStatus(response.data.message );
           props.onAddSocialMedia(response.data.data);
        setColor('success')
        // setFlash(true);
        setTimeout(() => {
            setColor('')
            setStatus('')
          setshowForm(false)
          // setFlashE(null);
        }, 3000);
         } )
        .catch(error => {
            if (error.response) {
                setStatus(error.response.data.message);
                // setStatus (error.response.data.message) ;
                setColor('error')
                // console.log(error.response.data.message);
            }else{
                setStatus ({data:(error.message)}) ;
                setColor('error')
            }
           
        });
       
        // props.onAddSocialMedia(SocialMediaData);
        console.log(SocialMediaData);
        // setshowForm(false);
    }
// console.log(status)
    return (
        <div>
            {showForm && <SocialMediaInput

                name='Add SocialMedia'
                cName='SocialMedia Image'
                stat={status}
                color={color}
                onClose={onHideForm}
                onSaveSocialMediaData={saveSocialMediaDataHandler}
            />}
            <Form><div  onClick={onShowForm}><CgAddR color='green'  /> Add</div></Form>
        </div>
    );
}
export default AddSocialMedia;




