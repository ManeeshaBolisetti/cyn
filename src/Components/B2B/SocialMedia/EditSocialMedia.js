import { React, useState } from 'react';
import { Form } from 'react-bootstrap';
import {CgAddR  } from 'react-icons/cg';
import EditSocialMediaInput from "./EditSocialMediaInput";
import api from './../../Api';
const EditSocialMedia = (props) => {
    
    let value=props.value
    const [showForm, setshowForm] = useState(false);
    const [status, setStatus] = useState('');
    const [color,setColor]=useState('');
    const onShowForm = () => {
        if(value.length===1){
        setshowForm(true);
    }else{
        alert('please select edit Row')
    }
    }
    const onHideForm = () => {
        setshowForm(false);
    };
    const RemoveImage=(image)=>{
        const RemoveData={
            ...image
        }
        // let url = 'admin/removeBlob'
        // axios.post(url, RemoveData )
        api.RemoveBlob(RemoveData)
        .then(response => {
            setStatus({ data:response.data.message });
        console.log(response.data.message)
        setColor('success')
        // props.onUpdateProduct({data:response.data.data});
        setTimeout(() => {
            setColor('')
            setStatus('')
        //   setshowForm(false)
          // setFlashE(null);
        }, 3000);
        
        // window.location.reload();
         } )
        .catch(error => {
            if (error.response) {
                setStatus({ data:(error.response.data.message )});
                setColor('error')
                console.log(error.response);
            }else{
                setStatus({ data: error.message });
                setColor('error')
                console.error('There was an error!', error);

            }
           
        });
       

    }

    const saveSocialMediaDataHandler = (enterdSocialMediaData) => {
        const SocialMediaData = {
            ...enterdSocialMediaData,

        };
        // console.log(enterdWebsiteData.categoryImage);

        let formData = new FormData();
        formData.append('app', enterdSocialMediaData.app);
        formData.append('socailMediaIcon', enterdSocialMediaData.socailMediaIcon);
        formData.append('image',enterdSocialMediaData.image);
        formData.append('socialMediaId', enterdSocialMediaData.socilaMediaId);
        // formData.append('countries[]', [enterdWebsiteData.countries] );
        for (let i = 0; i < enterdSocialMediaData.countries.length; i++) {
            formData.append('countries[]', enterdSocialMediaData.countries[i])
        }

        // let url = 'admin/updateSocialMedia'

        // axios.post(url, formData )
        api.updateSocial(formData)
        .then(response => {
            props.onUpdateSocialMedia(response.data.data);
           setStatus({data:response.data.message})    
        
        setColor('success')
        // setFlash(true);
        setTimeout(() => {
            setColor('')
            setStatus('')
          setshowForm(false)
          // setFlashE(null);
        }, 3000);;
         } )
        .catch(error => {
            if (error.response) {
                // setStatus({ data:(error.response.data.message )});
                setStatus({ data:(error.response.data.message )});
                setColor('error')
                // console.log(error.response.data.message);
            }else{
                setStatus({ data: error.message });
                setColor('error')
            }
           
        });
       
        props.onUpdateSocialMedia(SocialMediaData);
        console.log(SocialMediaData);
        // setshowForm(false);
    }

    return (
        <div>
            {showForm && <EditSocialMediaInput

                name='Update SocialMedia'
                cName='SocialMedia Image'
                stat={status.data}
                onRemoveImage={RemoveImage}
                color={color}
                val={value}
                onClose={onHideForm}
                onSaveSocialMediaData={saveSocialMediaDataHandler}
            />}
            <Form><div  onClick={onShowForm}><CgAddR color='green'  /> Update</div></Form>
        </div>
    );
}
export default EditSocialMedia;




