import { useState, useRef } from 'react';
import { Form, Button } from 'react-bootstrap';
import Card from '../../Card/Card';
import classes from './SocialMedia.Module.css';
// import axios from 'axios';
import { CheckBoxSelection, Inject, MultiSelectComponent } from '@syncfusion/ej2-react-dropdowns';
import UseFetch from '../../UseFetch';

import Alert from '@material-ui/lab/Alert';

const SocialMediaInput = (props) => {
  const [flash, setFlash] = useState(null);
  const color=props.color;
  const styles = {
    alert: {
      zIndex: '1500',
    },
  }
  const Invalid = '';
  const [valid, setValid] = useState(Invalid);
  // const [category, setCategory] = useState([]);
  // const [countries, setCountries] = useState([]);
  const appRef = useRef();
  const imageRef = useRef();
  const countryNameRef = useRef();

  const countries=UseFetch('/admin/countriesList');
  const fields = { text: 'countryName', value: 'countryId' };
//   const category=UseFetch('admin/categoriesList');
//   const optionItems = category.map((categories) => <option value={categories.categoryId}>{categories.categoryName}</option>)

  const submitHandler = (event) => {
    event.preventDefault();
    setFlash(true);
    setTimeout(() => {
      setFlash(null);
      
      // setFlashE(null);
    }, 5000);
    const app = appRef.current.value;
    const image = imageRef.current.files[0];
    const Countries = countryNameRef.current.value;
    console.log(image);
   
    const SocialMediaData = {
        app: app,
       image: image,
      countries: Countries,


    };
    console.log(SocialMediaData);
    props.onSaveSocialMediaData(SocialMediaData);
    setValid('');
  }


  return (
    <div>
      <div className={classes.input}>
        <Card className={classes.modal}>
          <header className={classes.header}>
            <h2>{props.name}</h2>
          </header>
          {/* <h4>{props.stat}</h4> */}
          {
        flash
        ? <Alert style={styles.alert} severity={color}>{props.stat}</Alert>
        : null
      }
          <Form
            onSubmit={submitHandler}
          > <div className={classes.content}>


              
              <Form.Control
                type='text'
                placeholder='app Name'
                ref={appRef}
              /><br/>
              
              <Form.Control
                type='file'
                placeholder='Image'
                ref={imageRef}
              /><br />

                <MultiSelectComponent id="checkbox" className='dropdown '
                dataSource={countries}
                // fields={fields}
                fields={fields}
              // value={fields.value}
                ref={countryNameRef}
                placeholder="Select Countries"
                mode="CheckBox"
                selectAllText="Select All"
                unSelectAllText="unSelect All"
                showSelectAll={true}>
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent>
             
            </div>
            <h4>{valid}</h4>
            <footer className={classes.actions}>
              <Button className={classes.btn}
                onClick={props.onClose}
              >cancel</Button>
              <Button
                type='submit'
              >Add</Button>
            </footer></Form>
        </Card>
      </div>

    </div>
  );

}
export default SocialMediaInput;