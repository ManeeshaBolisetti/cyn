import { useRef,useState } from 'react';
import { Form, Button } from 'react-bootstrap';
// import { form } from 'react-dom-factories';
import UseFetch from './../../UseFetch';
import Card from '../../Card/Card';
import classes from './SubscriptionInput.Module.css';
import Alert from '@material-ui/lab/Alert';
import { CheckBoxSelection, Inject, MultiSelectComponent } from '@syncfusion/ej2-react-dropdowns';


const SubscriptionInput = (props) => {
  const [flash, setFlash] = useState(null);
  const color=props.color;
  const styles = {
    alert: {
      zIndex: '1500',
    },
  }
  // const Invalid = '';
  // const [valid, setValid] = useState(Invalid);
  // const [countries, setCountries] = useState([]);
  //   const formRef = useRef();
  const priceRef = useRef();
  const countryRef = useRef();
  const validityRef = useRef();
  const validitytypeRef = useRef();
  const offerPriceRef = useRef();
  const fromCurrencyRef = useRef();
  const infoRef=useRef();
  // useEffect(() => {

  //   axios.get('https://appdev.connectyourneed.com/admin/countriesList')
  //     .then(response => {
  //       const country = response.data.data;
  //       // console.log(country);

  //       setCountries(country)
  //     });

  //   // empty dependency array means this effect will only run once (like componentDidMount in classes)
  // }, []);
  const countries = UseFetch('/admin/countriesList')
  const fields = { text: 'countryName', value: 'countryId' };
  // const optionItems = countries.map((country) => <option value={country.countryId} key={country.countryId}>{country.countryName}</option>)


  const submitHandler = (event) => {
    event.preventDefault();
    setFlash(true);
    setTimeout(() => {
      setFlash(null);
      
      // setFlashE(null);
    }, 5000);
    const price = priceRef.current.value;
    const country = countryRef.current.value;
    const validity = validityRef.current.value;
    const validityType = validitytypeRef.current.value;
    const offerPrice = offerPriceRef.current.value;
    const fromCurrency = fromCurrencyRef.current.value;
    const info=infoRef.current.value;
    const subscriptionData = {
      price: price,
      offerPrice: offerPrice,
      countries: country,
      fromCurrency: fromCurrency,
      validity: validity,
      validityType: validityType,
      info:info,
      //  offerPrice:offerPrice,
    };
    console.log(subscriptionData);
    props.onSaveSubscriptionData(subscriptionData);
    // validitytypeRef.current.value = '';
    // validityRef.current.value='';
    // countryRef.current.value = '';

  }
  return (
    <div className={classes.input}>
      <Card className={classes.modal}>
        <header className={classes.header}>
          <h2>{props.name}</h2>
        </header>
        {
        flash
        ? <Alert style={styles.alert} severity={color}>{props.stat}</Alert>
        : null
      }
        <Form onSubmit={submitHandler} > <div className={classes.content}>

          <Form.Control
            type='number'
            step="0.01"
            placeholder='enter Price'
            //  value={enterdCountry}
            ref={priceRef}
          // onChange={cNameChangeHandler}
          />
          <Form.Control
            type='number'
            step="0.01"
            placeholder='enter Offer Price'
            //  value={enterdCountry}
            ref={offerPriceRef}
          // onChange={cNameChangeHandler}
          />
          {/* <Form.Control as="select" ref={countryRef}>
                  {optionItems}
                </Form.Control> */}

          <MultiSelectComponent id="checkbox" className='dropdown '
            dataSource={countries}
            // fields={fields}
            fields={fields}
            // value={fields.value}
            ref={countryRef}
            placeholder="Select Countries"
            mode="CheckBox"
            selectAllText="Select All"
            unSelectAllText="unSelect All"
            showSelectAll={true}>
            <Inject services={[CheckBoxSelection]} />
          </MultiSelectComponent>
          <Form.Control
            type='text'
            placeholder='enter Currency'
            //  value={enterdCountry}
            ref={fromCurrencyRef}
          // onChange={cNameChangeHandler}
          />
           <Form.Control
            type='text'
            placeholder='enter Info'
            //  value={enterdCountry}
            ref={infoRef}
          // onChange={cNameChangeHandler}
          />
          <br />
          <Form.Control
            type='text'
            placeholder='valdity'
            //  value={enterdCountry}
            ref={validityRef}
          // onChange={cNameChangeHandler}
          />
          <Form.Control as="select" ref={validitytypeRef} >
            <option value='day'>day</option>
            <option value='days'>days</option>
            <option value='Month'>Month</option>
            <option value='Months'>Months</option>
            <option value='Year'>Year</option>
            <option value='Years'>Years</option>
          </Form.Control>
          {/* <Form.Control
            type='text'
            placeholder='validityType'
            //  value={enterdCode}
            ref={validitytypeRef}
          // onChange={codeChangeHandler}
          /><br />
           */}
          {/* <h4>{valid}</h4> */}

        </div>
          <footer className={classes.actions}>
            <Button className={classes.btn}
              onClick={props.onClose}
            >cancel</Button>
            <Button
              type='submit'
            >Add</Button>
          </footer></Form>
      </Card>
    </div>
  );
}
export default SubscriptionInput;