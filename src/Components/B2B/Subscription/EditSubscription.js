import {React, useState} from 'react';
import EditSubscriptionInput from './EditSubscriptionInput';
import { Form} from 'react-bootstrap';
import { TiFolderAdd } from 'react-icons/ti';
import api from './../../Api';
const EditSubscription=(props)=>{

    let statu='';
    let value=props.value
    const [showForm, setshowForm] = useState(false);
    const [status,setStatus]=useState(statu);
    const [color,setColor]=useState('');
    
    
    const saveSubscriptionDataHandler = (enterdSubscriptionData) => {
        const SubscriptionData = {
            ...enterdSubscriptionData,
            
            // id: Math.random().toString()
        };
        console.log(SubscriptionData)
        // axios.post('admin/updateB2bSubscriptions', SubscriptionData)
        api.updateSubscriptions(SubscriptionData)
        .then(response => {
            setStatus({ data:response.data.message });
        props.onUpdateSubscription(response.data.data);
        
        setColor('success')
        // setFlash(true);
        setTimeout(() => {
            setColor('')
            setStatus('')
          setshowForm(false)
          // setFlashE(null);
        }, 3000);
         } )
         .catch(error => {
            if (error.response) {
                // setStatus({ data:(error.response.data.message )});
                setStatus({ data:(error.response.data.message )});
                setColor('error')
                // console.log(error.response.data.message);
            }else{
                setStatus({ data: error.message });
                setColor('error')
            }
           
        });
       
        console.log(SubscriptionData)
        // props.onUpdateSubscription(SubscriptionData);

        // setshowForm(false);
    };
    const onShowForm = () => {
        if(value.length===1){
        setshowForm(true);
    }else{
        alert('please select edit Row')
    }
    }
    const onHideForm = () => {
        setshowForm(false);
        setStatus("Add A Subscription");
    }
    return(
        <div>
           {showForm &&
             <EditSubscriptionInput
            name='Add Subscription'
            // cName='Country Name'
            countries='select countries'
            stat={status.data}
            color={color}
            val={value}
            onClose={onHideForm}
            onSaveSubscriptionData={saveSubscriptionDataHandler}
            // onCancel={stopEditHandler}
             />}
            
                {/* <h4 >Admin/Add Merchant</h4> */}

                <Form><div onClick={onShowForm}><TiFolderAdd fontSize='20' color='green' /> Update </div></Form>
 
        </div>
    );
}
export default EditSubscription;