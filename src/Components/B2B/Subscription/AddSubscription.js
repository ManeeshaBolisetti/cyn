import {React, useState} from 'react';
import SubscriptionInput from './SubscriptionInput';
import { Form } from 'react-bootstrap';
import { TiFolderAdd } from 'react-icons/ti';
// import api from './../AxiosP';
// import axios from 'axios';
import api from './../../Api';
const AddSubscription=(props)=>{
    
    let statu='';
    const [showForm, setshowForm] = useState(false);
    const [status,setStatus]=useState(statu);
    const [color,setColor]=useState('');
    
    const saveSubscriptionDataHandler = (enterdSubscriptionData) => {
        const SubscriptionData = {
            ...enterdSubscriptionData,
            
            // id: Math.random().toString()
        };
        console.log(SubscriptionData)
        api.addSubscription(SubscriptionData)
        // axios.post('/admin/addB2bSubscriptions', SubscriptionData)
        .then(response => {
            setStatus(response.data.message );
       
        props.onAddSubscription(response.data.data);
        
        setColor('success')
        // setFlash(true);
        setTimeout(() => {
            setColor('')
            setStatus('')
          setshowForm(false)
          // setFlashE(null);
        }, 3000);;
         } )
         .catch(error => {
            if (error.response) {
                // setStatus({ data:(error.response.data.message )});
                setStatus(error.response.data.message );
                setColor('error')
                // console.log(error.response.data.message);
            }else{
                setStatus( error.message );
                setColor('error')
            }
           
        });
        // fetch('https://appdev.connectyourneed.com/admin/addDistances', {

        //     method: "POST",
        //     headers: { 'Content-Type': 'application/json' },
        //     body: JSON.stringify(DistanceData)
        // }).then((response) => {
        //     if(!response.ok){
        //         if(response.status===500){
        //             setStatus('Alredy existed');
        //         }else
        //             setStatus(response.message)
                
        //     //    setStatus(response.status) ;
        //     } 
        //     else return response.json();
        // })
        //  .then((data) => 
        // {
        //    if(data){
        //     setStatus(data.message);
        //     console.log(data);
        //     // props.onAddCountry(countryData);

        //    }
        //     // console.log(data);
        //     // console.log(data.message);
            
        // })
        console.log(SubscriptionData)
        // props.onAddSubscription(SubscriptionData);

        // setshowForm(false);
    };
    const onShowForm = () => {
        setshowForm(true);
    }
    const onHideForm = () => {
        setshowForm(false);
        setStatus("Add A Subscription");
    }
    return(
        <div>
           {showForm &&
             <SubscriptionInput
            name='Add Subscription'
            // cName='Country Name'
            countries='select countries'
            stat={status}
                color={color}
            onClose={onHideForm}
            onSaveSubscriptionData={saveSubscriptionDataHandler}
            // onCancel={stopEditHandler}
             />}
            
                {/* <h4 >Admin/Add Merchant</h4> */}

                <Form><div onClick={onShowForm}><TiFolderAdd fontSize='20' color='green' /> Add </div></Form>
 
        </div>
    );
}
export default AddSubscription;