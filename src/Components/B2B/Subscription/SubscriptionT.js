import { React, useState,useEffect} from 'react';
// import AddSocialMedia from './AddSocialMedia';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-enterprise';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import * as moment from 'moment';
// import { FcDeleteDatabase, FcEditImage, FcPrint } from "react-icons/fc";
// import { BsFillStopFill } from "react-icons/bs";/
import { RiFolderDownloadLine } from "react-icons/ri";
// import { print } from 'react-to-print';
import { Button , Row, Col, } from 'react-bootstrap';
import Search from './../../Country/Search';
import './../../Products/Product.css';
// import UseFetch from '../../UseFetch';
import Refresh from './../../Actions/Refresh';
import AddSubscription from './AddSubscription';
import EditSubscription from './EditSubscription';
import CustomTooltip from './../../customTooltip.jsx';
import api from './../../Api';
var filterParams = {
  comparator: function (filterLocalDateAtMidnight, cellValue) {
    var dateAsString = cellValue;
    if (dateAsString == null) return -1;
    var dateParts = dateAsString.split('/');
    var cellDate = new Date(
      Number(dateParts[2]),
      Number(dateParts[1]) - 1,
      Number(dateParts[0])
    );

    if (filterLocalDateAtMidnight.getTime() === cellDate.getTime()) {
      return 0;
    }

    if (cellDate < filterLocalDateAtMidnight) {
      return -1;
    }

    if (cellDate > filterLocalDateAtMidnight) {
      return 1;
    }
  },
  browserDatePicker: true,
  minValidYear: 2000,
};
const SubscriptionT = () => {
  // let subscription=[]
  // const subscription=UseFetch('/admin/allB2bSubscriptionsList');
  const [subscription,setSubscription]=useState([]);
  // setSubscription( UseFetch('/admin/allB2bSubscriptionsList'));
  let subscriptionList=[];
  // const rowData = [Products]
    const [subscriptionL,setSubscriptionL] = useState([]);
    const [gridApi, setGridApi] = useState(null);
    // const [girdColumnApi, setGridColumnApi] = useState(null);
    // const [suppressClickEdit, setSuppressClickEdit] = useState(true);
    // const [edit, setEdit] = useState('Edit');
    console.log(subscription)
    useEffect(() => {

     api.getSubscriptions()
          .then(response => {
              let Website = response.data.data;
              setSubscription(Website);
              // console.log(category);
          });
  
  }, []);
    // subscription.length > 0
		// &&
    if (subscription != null && subscription.length >0){
      subscription.map((n)=>{
        
      let uDate = moment(n.updatedAt).format('DD/MM/YYYY');
      // let ctime = moment(n.createdAt).format('h:mma');
      let utime = moment(n.updatedAt).format('h:mma');
        let country = n.countries.map((c) => {

          let count = c.countryName
          return count;
  
          // let countries=c.countryName
          // return countries
        })
        let countryId = n.countries.map((c) => {
  
          let count = c.countryId
          return count;
  
          // let countries=c.countryName
          // return countries
        })
      
     subscriptionList.push({
      
       'price':n.price,
       'offerPrice':n.offerPrice,
       'validity':n.validity,
       'validityType':n.validityType,
       'countryName':country,
      //  'countryCode':n.country.countryCode,
       'countryId':countryId,
       'info':n.info,
       'fromCurrency':'USD',
       'b2bSubscriptionId':n.b2bSubscriptionId,
       'updatedAt':uDate,
      //  'ctime':ctime,
       'utime':utime,
 
   })
   return subscriptionList;
 });}
//
        // console.log(country)
    //     subscriptionList.push({
    //         'country':country,
    //         'price':n.price,
    //         'offerPrice':n.offerPrice,
    //         'validity':n.validity,
           
        
       
        

    // })
   
    // const [product, setProduct] = useState([products]);
    
    // console.log(product)
    // setProduct(products);
    // useEffect(() => {


    //     axios.get('https://appdev.connectyourneed.com/admin/productsList')
    //         .then(response => {
    //             const products = response.data.data;
    //             setProduct(products);
    //             console.log(products)
    //         },
    //             error => {
    //                 console.log(error);
    //             }

    //         );

    // }, []);

    const columnDefs = [
        {
            field: "price", filter: 'agSetColumnFilter', editable: true,
        },

        {
            field: "offerPrice",  filter: 'agSetColumnFilter', editable: true,
            // cellRenderer: countryCellRenderer,

        },
       
        {
            // headerName: 'Category',
            field: "validity", filter: 'agSetColumnFilter', editable: true,
            // cellRenderer: countryCellRenderer,

        },
        {
          // headerName: 'Category',
          field: "validityType", filter: 'agSetColumnFilter',  editable: true,
          // cellRenderer: countryCellRenderer,

      },
      {
        // headerName: 'Category',
        field: "fromCurrency", filter: 'agSetColumnFilter', editable: true,
        // cellRenderer: countryCellRenderer,

    },
        {
          // headerName: 'Category',
          field: "info", filter: 'agSetColumnFilter', editable: true,
          // cellRenderer: countryCellRenderer,

      },
      
        {
            headerName:'Country',
            field: "countryName", 
            
            tooltipField: 'countryName',
            filter: 'agSetColumnFilter',
            tooltipComponentParams: { type: 'success' },
            cellEditor:"agSelectCellEditor",
             
        },
        {
          field: "updatedAt", filter: 'agDateColumnFilter',cellRenderer :updatedDate,
          filterParams: filterParams,
        },
        // {
        //     // headerName: 'Category',
        //     field: "validityType", filter: 'agSetColumnFilter', tooltipField: 'productName', editable: true,
        //     // cellRenderer: countryCellRenderer,

        // },
      //   {
      //     field: "isUsed", tooltipField: 'productName', filter: 'agSetColumnFilter', editable: true,
      //     // cellRenderer: countryCellRenderer,
      // },

        // {
        //     headerName: 'Action',
        //     minWidth: 150,
        //     cellRenderer: actionCellRenderer,
        //     editable: false,
        //     colId: "action",
        //     hide: true,

        // },


    ]
    const defaultColDef = {
        sortable: true,
        editable: true,
        flex: 1,
        //    filter: true,
        minWidth: 200,
        resizable: true,
        floatingFilter: true,
        tooltipComponent: 'customTooltip',
        //  floatingFilter: true
    }
    const onGridReady = (params) => {
        console.log('grid ready');
        setGridApi(params);
        // setGridColumnApi(params.columnApi);
      };
    const AddSubscriptionHandler = subscription => {
      setSubscription(prevExp => {
            console.log(subscription);
            return [subscription , ...prevExp];
        });

    };
    const EditSubscriptionHandler = subscription => {
      setSubscription(prevExp => {
        console.log(subscription);
        return [subscription , ...prevExp];
    });

  };
    const pageSize = (pageSize) => {
        console.log(pageSize);
         gridApi.api.paginationSetPageSize(Number(pageSize))
      }
    
      // const showEdit = () => {
      //   girdColumnApi.setColumnVisible("action", false)
      //   // setSuppressClickEdit(false);
      //   setEdit('EditMode')
      //   }
      // const stopEdit = () => {
      //   // setSuppressClickEdit(true);
      //   setEdit('Edit')
      // }
      // const onRemoveSelected = () => {
      //   var selectedData = gridApi.api.getSelectedRows();
      //   var res = gridApi.api.applyTransaction({ remove: selectedData });
      //   printResult(res);
      // };
      const onCellValueChanged = (event) => {
        console.log(
          'onCellValueChanged: ' + event.colDef.field + ' = ' + event.newValue
        );
      };
    
      const onRowValueChanged = (event) => {
        var data = event.data;
        console.log(
          'onRowValueChanged: (' +
          data.productName +
          ', ' +
          data.productImage +
          ', ' +
          data.isUsed +
          ', ' +
          data.countries +
          ', ' +
          data.categoryRef.categoryName +
          
          
          ')'
        );
      };
      const onExportClick = () => {
        gridApi.api.exportDataAsCsv();
      }
      // const onBtPrint = () => {
      //   const api = gridApi.api;
      //   setPrinterFriendly(api);
      //   setTimeout(function () {
      //     print();
      //     setNormal(api);
      //   }, 2000);
      // };
      const onFilterTextChange = (e) => {
    
        gridApi.api.setQuickFilter(e.target.value);
      }
      const onSelectionChanged = () => {
        var selectedRows = gridApi.api.getSelectedRows();
        console.log(selectedRows);
        setSubscriptionL(selectedRows);
        
        // document.querySelector('#selectedRows').innerHTML =
        //   selectedRows.length === 1 ? selectedRows[0].countryCode : '';
      };
    
    return (
        <div>
          <div className='d-flex justify-content-between'><h2>Subscriptions </h2>
      <Search onChange={onFilterTextChange} /></div>
      <Row>
        <Col xs={10} style={{display:'-webkit-box' }}>

        <Button className='add' ><AddSubscription onAddSubscription={AddSubscriptionHandler} />
       </Button> <Button className='add' ><EditSubscription value={subscriptionL}  onUpdateSubscription={EditSubscriptionHandler}></EditSubscription>
</Button>
          {/* <Button className='add' onClick={() => onRemoveSelected()}>Remove <FcDeleteDatabase /></Button > */}
          {/* <Button className='add' onClick={showEdit}>{edit} <FcEditImage /></Button > */}
          {/* <Button className='add' onClick={stopEdit}>stop <BsFillStopFill color='darkred' /></Button > */}
          <Button className='add' onClick={() => onExportClick()}>
            Export to Excel  <RiFolderDownloadLine color='green' />
          </Button >
          <Button className='add' ><Refresh/></Button>
          {/* <Button className='add' onClick={() => onBtPrint()}>Print <FcPrint /></Button > */}
          </Col>

        <Col xs={2}>
          {/* pageSize: <select onChange={(e) => pageSize(e.target.value)}>
            <option value='10'>10</option>
            <option value='25'>25</option>
            <option value='50'>50</option>
            <option value='100'>100</option>
          </select> */}
        </Col>

      </Row>

                  

                   
            <div className="ag-theme-alpine" style={{ height: '400px' }}>
                <AgGridReact
                   editType="fullRow"
                   rowSelection={'multiple'}
                   onCellValueChanged={onCellValueChanged}
                   onRowValueChanged={onRowValueChanged}
         
                   onSelectionChanged={onSelectionChanged}
                  //  suppressClickEdit={suppressClickEdit}
                   pagination={true}
                   paginationPageSize={25}
                    columnDefs={columnDefs}
                    
          tooltipShowDelay={1000}
          tooltipMouseTrack={true}
          frameworkComponents={{ customTooltip: CustomTooltip }}
                    rowData={subscriptionList}

                    defaultColDef={defaultColDef}
                    onGridReady={onGridReady}>



                </AgGridReact>
                <div className='selectbox' style={{ 
          padding: '10px',
          marginTop: '-10px',fontSize: 'small',}}> Page Size: <select onChange={(e) => pageSize(e.target.value)}>
                        <option value='10'>10</option>
                        <option value='25'>25</option>
                        <option value='50'>50</option>
                        <option value='100'>100</option>
                    </select></div>
            </div>
        </div>
    );
}
export default SubscriptionT;

// function createdDate(params) {
//   var time = (params.data.ctime);
//   var date = (params.data.createdAt);
  
//   return (

//       '<span style="cursor: default;">' +date+' ' +time+ '</span>'
//   );
// }
function updatedDate(params) {
  var time = (params.data.utime);
  var date = (params.data.updatedAt);
  
  return (

      '<span style="cursor: default;">' +date+' ' +time+ '</span>'
  );
}
// function countryCellRenderer(params) {
//     var image = (params.data.image);
//     var logo =
//         // params.data.catName;
//         // '<img border="0" width="50" alt="image" height="30" src="' +
//         // params.data.categoryImage +
//         // '">';
//         '<img border="0" width="50" alt="image" height="30" src=" ' + image +
//         '">';
//     return (

//         '<span style="cursor: default;">' + logo + '</span>'
//     );
// }

// function printResult(res) {
//     console.log('---------------------------------------');
//     if (res.add) {
//       res.add.forEach(function (rowNode) {
//         console.log('Added Row Node', rowNode);
//       });
//     }
//     if (res.remove) {
//       res.remove.forEach(function (rowNode) {
//         console.log('Removed Row Node', rowNode);
//       });
//     }
//     if (res.update) {
//       res.update.forEach(function (rowNode) {
//         console.log('Updated Row Node', rowNode);
//       });
//     }
//   }
//   function setPrinterFriendly(api) {
//     const eGridDiv = document.querySelector('#myGrid');
//     api.setDomLayout('print');
//   }
//   function setNormal(api) {
//     const eGridDiv = document.querySelector('#myGrid');
//     eGridDiv.style.width = '700px';
//     eGridDiv.style.height = '200px';
//     api.setDomLayout(null);
//   }
  