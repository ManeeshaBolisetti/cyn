import { useState, useRef} from 'react';
import { Form, Button } from 'react-bootstrap';
// import { form } from 'react-dom-factories';
import UseFetch from './../../UseFetch';
import Card from '../../Card/Card';
import classes from './SubscriptionInput.Module.css';
import Alert from '@material-ui/lab/Alert';
import { CheckBoxSelection, Inject, MultiSelectComponent } from '@syncfusion/ej2-react-dropdowns';


const EditSubscriptionInput = (props) => {
  const [flash, setFlash] = useState(null);
  const color=props.color;
  const styles = {
    alert: {
      zIndex: '1500',
    },
  }
 
  // const Invalid = '';
  let value=props.val
  // const [valid, setValid] = useState(Invalid);
  
  
  let sPrice=value.length === 1 ? value[0].price: '';
  let sOfferPrice=value.length === 1 ? value[0].offerPrice: '';
  let sValidity=value.length === 1 ? value[0].validity: '';
  let sValidityType=value.length === 1 ? value[0].validityType: '';
  // let sInfo=value.length=== 1? value[0].info: '';
  
  const [optionValue,setOptionValue]=useState(sValidityType);
  // let sCountryName=value.length === 1 ? value[0].country: '';
  // let sCountryCode=value.length === 1 ? value[0].countryCode: '';
  let sCountryId=value.length === 1 ? value[0].countryId: '';
  let sInfo=value.length === 1 ? value[0].info: '';
  let fromCurrency=value.length === 1 ? value[0].fromCurrency: '';
  let sB2bSubscriptionId=value.length === 1 ? value[0].b2bSubscriptionId: '';
  console.log(sB2bSubscriptionId);
  // const [countries, setCountries] = useState([]);
//   const formRef = useRef();
  const priceRef = useRef();
  const countryRef = useRef();
  const validityRef = useRef();
  const validitytypeRef=useRef();
  const offerPriceRef=useRef();
  const fromCurrencyRef = useRef();
  const infoRef=useRef();
  // useEffect(() => {

  //   axios.get('https://appdev.connectyourneed.com/admin/countriesList')
  //     .then(response => {
  //       const country = response.data.data;
  //       // console.log(country);

  //       setCountries(country)
  //     });

  //   // empty dependency array means this effect will only run once (like componentDidMount in classes)
  // }, []);
  const countries=UseFetch('/admin/countriesList')
  const fields = { text: 'countryName', value: 'countryId' };
  let colorValues = sCountryId.map((n) => {
    let colour = n;
    return colour
  });
  const onTagging = (e) => {
    // set the current selected item text as class to chip element.
    e.setClass(e.itemData[fields.text].toLowerCase());
  };
//   const optionItems = countries.map((country) => <option value={country.countryId}>{country.countryCode}</option>)


   const submitHandler = (event) => {
    event.preventDefault();
    setFlash(true);
    setTimeout(() => {
      setFlash(null);
      
      // setFlashE(null);
    }, 5000);
    const price = priceRef.current.value;
    const country = countryRef.current.value;
    const validity= validityRef.current.value;
    const validityType=optionValue;
    const offerPrice=offerPriceRef.current.value;
    const fromCurrency = fromCurrencyRef.current.value;
    const info=infoRef.current.value;

    const subscriptionData = {
     price:price,
     offerPrice:offerPrice,
     countries:country,
     fromCurrency: fromCurrency,
     validity:validity,
     validityType:validityType,
     b2bSubscriptionId:sB2bSubscriptionId,
     info:info,
     
     
     
    };
    console.log(subscriptionData);
    props.onSaveSubscriptionData(subscriptionData);
    // validitytypeRef.current.value = '';
    // validityRef.current.value='';
    // countryRef.current.value = '';

  }
  const optionChange=(event)=>{
    setOptionValue(event.target.value);
  
  }
  return (
    <div className={classes.input}>
      <Card className={classes.modal}>
        <header className={classes.header}>
          <h2>{props.name}</h2>
        </header>
        {
        flash
        ? <Alert style={styles.alert} severity={color}>{props.stat}</Alert>
        : null
      }
        <Form onSubmit={submitHandler} > <div className={classes.content}>

          <Form.Control
            type='number'
            step="0.01"
            defaultValue={sPrice}
            placeholder='enter Price'
            //  value={enterdCountry}
            ref={priceRef}
          // onChange={cNameChangeHandler}
          />
          <Form.Control
            type='number'
            step="0.01"
            placeholder='enter Offer Price'
            defaultValue={sOfferPrice}
            //  value={enterdCountry}
            ref={offerPriceRef}
          // onChange={cNameChangeHandler}
          />
            <MultiSelectComponent id="checkbox" className='dropdown '
                dataSource={countries}
                // fields={fields}
                fields={fields}
                // value={fields.value}
                ref={countryRef}
                placeholder="Select Countries"
                mode="CheckBox"
                selectAllText="Select All"
                unSelectAllText="unSelect All"
                tagging={onTagging}
                value={colorValues}
                showSelectAll={true}>
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent>
              <Form.Control
            type='text'
            placeholder='enter Currency'
            defaultValue={fromCurrency}
            //  value={enterdCountry}
            ref={fromCurrencyRef}
          // onChange={cNameChangeHandler}
          />
          <Form.Control
            type='text'
            placeholder='valdity'
            defaultValue={sValidity}
            //  value={enterdCountry}
            ref={validityRef}
          // onChange={cNameChangeHandler}
          />
          {/* <Form.Control
            type='text'
            // defaultValue={s}
            placeholder='validityType'
            //  value={enterdCode}
            ref={validitytypeRef}
          // onChange={codeChangeHandler}
          /> */}
           <Form.Control as="select" ref={validitytypeRef} value={optionValue} onChange={optionChange} >
            <option value='day'>day</option>
            <option value='days'>days</option>
            <option value='month'>Month</option>
            <option value='months'>Months</option>
            <option value='year'>Year</option>
            <option value='years'>Years</option>
          </Form.Control>
          <Form.Control
            type='text'
            placeholder='enter Information'
            defaultValue={sInfo}
            //  value={enterdCountry}
            ref={infoRef}
          // onChange={cNameChangeHandler}
          />
          {/* <h4>{valid}</h4> */}
         
        </div>
          <footer className={classes.actions}>
            <Button className={classes.btn}
              onClick={props.onClose}
            >cancel</Button>
            <Button
              type='submit'
            >Add</Button>
          </footer></Form>
      </Card>
    </div>
  );
}
export default EditSubscriptionInput;