import { React, useState } from 'react';
import { Form } from 'react-bootstrap';
import {CgAddR  } from 'react-icons/cg';
import WebsiteInput from "./WebsiteInput";
import api from './../../Api';
const AddWebsite = (props) => {
    // let statu = '';
    const [showForm, setshowForm] = useState(false);
    const [status, setStatus] = useState(null);
    const [color,setColor]=useState('');
    // const [suStatus, setSuStatus] = useState(null);
    const onShowForm = () => {
        setshowForm(true);
    };
    const onHideForm = () => {
        setshowForm(false);
    };
    const saveWebsiteDataHandler = (enterdWebsiteData) => {
        // const WebsiteData = {
        //     ...enterdWebsiteData,

        // };
        // console.log(enterdWebsiteData.categoryImage);

        let formData = new FormData();
        formData.append('app', enterdWebsiteData.app);
        formData.append('image', enterdWebsiteData.image);
        // formData.append('countries[]', [enterdWebsiteData.countries] );
        for (let i = 0; i < enterdWebsiteData.countries.length; i++) {
            formData.append('countries[]', enterdWebsiteData.countries[i])
        }

        // let url = '/Admin/addWebsite'
        // axios.post(url, formData )
        api.addWebsite(formData)
        .then(response => {
            setStatus(response.data.message );
        props.onAddWebsite(response.data.data);
        setColor('success')
        // setFlash(true);
        setTimeout(() => {
            setColor('')
            setStatus('')
          setshowForm(false)
          // setFlashE(null);
        }, 3000);
        
        // window.location.reload();
         } )
         .catch(error => {
            if (error.response) {
                // setStatus({ data:(error.response.data.message )});
                setStatus(error.response.data.message );
                setColor('error')
                // console.log(error.response.data.message);
            }else{
                setStatus( error.message );
                setColor('error')
            }
           
        });
       
        // console.log(WebsiteData);
        // setshowForm(false);
    }

    return (
        <div>
            {showForm && <WebsiteInput

                name='Add Website'
                cName='Website Image'
                stat={status}
                color={color}
                onClose={onHideForm}
                onSaveWebsiteData={saveWebsiteDataHandler}
            />}
            <Form><div   onClick={onShowForm}><CgAddR color='green'  /> Add</div></Form>
        </div>
    );
}
export default AddWebsite;




