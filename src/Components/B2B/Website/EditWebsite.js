import { React, useState } from 'react';
import { Form} from 'react-bootstrap';
import {CgAddR  } from 'react-icons/cg';
import EditWebsiteInput from "./EditWebsiteInput";
// import axios from 'axios';
import api from './../../Api';
const EditWebsite = (props) => {
    let statu = '';
    let value=props.value
    const [showForm, setshowForm] = useState(false);
    const [status, setStatus] = useState(statu);
    const [color,setColor]=useState('');
    const onShowForm = () => {
        if(value.length===1){
        setshowForm(true);
    }else{
        alert('please select edit Row')
    }
    }
    
    const onHideForm = () => {
        setshowForm(false);
    };
    const RemoveImage=(image)=>{
        const RemoveData={
            ...image
        }
        // let url = 'admin/removeBlob'
        // axios.post(url, RemoveData )
        api.RemoveBlob(RemoveData)
        .then(response => {
            setStatus({ data:response.data.message });
        console.log(response.data.message)
        setColor('success')
        // props.onUpdateProduct({data:response.data.data});
        setTimeout(() => {
            setColor('')
            setStatus('')
        //   setshowForm(false)
          // setFlashE(null);
        }, 3000);
        
        // window.location.reload();
         } )
        .catch(error => {
            if (error.response) {
                setStatus({ data:(error.response.data.message )});
                setColor('error')
                console.log(error.response);
            }else{
                setStatus({ data: error.message });
                setColor('error')
                console.error('There was an error!', error);

            }
           
        });
       

    }
    const saveWebsiteDataHandler = (enterdWebsiteData) => {
        // const WebsiteData = {
        //     ...enterdWebsiteData,

        // };
        // console.log(enterdWebsiteData.categoryImage);

        let formData = new FormData();
        formData.append('app', enterdWebsiteData.app);
        formData.append('webIcon', enterdWebsiteData.webIcon);
        
        formData.append('image', enterdWebsiteData.image);
        formData.append('webId', enterdWebsiteData.webId);
        // formData.append('countries[]', [enterdWebsiteData.countries] );
        for (let i = 0; i < enterdWebsiteData.countries.length; i++) {
            formData.append('countries[]', enterdWebsiteData.countries[i])
        }

        // let url = 'admin/updateWebLinks'
        // axios.post(url, formData )
        api.updateWebsite(formData)
        .then(response => {
            setStatus({ data:response.data.message });
            
            props.onUpdateWebsite(response.data.data);
            setColor('success')
            // setFlash(true);
            setTimeout(() => {
                setColor('')
                setStatus('')
              setshowForm(false)
              // setFlashE(null);
            }, 3000);
        // console.log(response.data.message)
        // window.location.reload();
         } )
         .catch(error => {
            if (error.response) {
                // setStatus({ data:(error.response.data.message )});
                setStatus({ data:(error.response.data.message )});
                setColor('error')
                // console.log(error.response.data.message);
            }else{
                setStatus({ data: error.message });
                setColor('error')
            }
           
        });
       
       
        // console.log(WebsiteData);
        // setshowForm(false);
    }

    return (
        <div>
            {showForm && <EditWebsiteInput

                name='Add Website'
                cName='Website Image'
                stat={status.data}
                color={color}
                onRemoveImage={RemoveImage}
                onClose={onHideForm}
                onSaveWebsiteData={saveWebsiteDataHandler}
                val={value}
            />}
            <Form><div   onClick={onShowForm}><CgAddR color='green'  /> Update</div></Form>
        </div>
    );
}
export default EditWebsite;




