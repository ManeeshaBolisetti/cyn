import { useState, useRef } from 'react';
import { Form, Button } from 'react-bootstrap';
import Card from '../../Card/Card';
import classes from './WebsiteInput.Module.css';

import Alert from '@material-ui/lab/Alert';
// import axios from 'axios';
import { CheckBoxSelection, Inject, MultiSelectComponent } from '@syncfusion/ej2-react-dropdowns';
import UseFetch from '../../UseFetch';

const EditWebsiteInput = (props) => {
  const [flash, setFlash] = useState(null);
  const color=props.color;
  
  const styles = {
    alert: {
      zIndex: '1500',
    },
  }
  const Invalid = '';
  let value=props.val
  const [valid, setValid] = useState(Invalid);
  let wImage=value.length === 1 ? value[0].image: '';
  const [image,setImage]=useState({file:wImage});
  
  let wCountryId=value.length === 1 ? value[0].countryId: '';
  // let wCountries=value.length === 1 ? value[0].countries: '';
 
  let wWebId=value.length === 1 ? value[0].webId: '';
  let wApp=value.length === 1 ? value[0].app: '';
  
console.log(image)
  // const [category, setCategory] = useState([]);
  // const [countries, setCountries] = useState([]);
  const appRef = useRef();
  const imageRef = useRef();
  const countryNameRef = useRef();

  const countries=UseFetch('/admin/countriesList');
  const fields = { text: 'countryName', value: 'countryId' };
  let colorValues= wCountryId.map((n)=>{
    let colour=n;
    return colour
});
// console.log(colorValues)
const onTagging = (e) => {
  // set the current selected item text as class to chip element.
  e.setClass(e.itemData[fields.text].toLowerCase());
};
//   const category=UseFetch('admin/categoriesList');
//   const optionItems = category.map((categories) => <option value={categories.categoryId}>{categories.categoryName}</option>)

  const submitHandler = (event) => {
    event.preventDefault(); event.preventDefault();
    setFlash(true);
    setTimeout(() => {
      setFlash(null);
      
      // setFlashE(null);
    }, 5000);

    const app = appRef.current.value;
    // const image = image;
    const Countries = countryNameRef.current.value;
    const images=imageRef.current.files[0];
    console.log(image);
   
    const WebsiteData = {
        app: app,
        webIcon: wImage,
        image:images,
      countries: Countries,
      countryId: wCountryId,
      webId:wWebId,


    };
    console.log(WebsiteData);
    props.onSaveWebsiteData(WebsiteData);
    setValid('');
  }
  const handleChange=(event)=> {
    
    setImage( {
     file: URL.createObjectURL(event.target.files[0])
   })
   
 }
 const RemoveImage = () => {
  const RemoveImage = {
    imageUrl: wImage,
  };
  setFlash(true);
  setTimeout(() => {
    setFlash(null);
    // setFlashE(null);
  }, 5000);
  props.onRemoveImage(RemoveImage);
  
}




  return (
    <div>
      <div className={classes.input}>
        <Card className={classes.modal}>
          <header className={classes.header}>
            <h2>{props.name}</h2>
          </header>
          {
        flash
        ? <Alert style={styles.alert} severity={color}>{props.stat}</Alert>
        : null
      }
      
      <button onClick={RemoveImage}>Remove</button>
          <Form
            onSubmit={submitHandler}
          > <div className={classes.content}>


              
              <Form.Control
                type='text'
                defaultValue={wApp}
                placeholder='app Name'
                ref={appRef}
              />
              <Form.Control
                type='file'
                onChange={ handleChange}
                placeholder='Image'
                ref={imageRef}
              /><br />
              <img className={classes.image}  src={image.file} alt='previw' />

                <MultiSelectComponent id="checkbox" className='dropdown '
                dataSource={countries}
                // fields={fields}
                fields={fields}
              // value={fields.value}
                ref={countryNameRef}
                placeholder="Select Countries"
                mode="CheckBox"
                selectAllText="Select All"
                unSelectAllText="unSelect All"
                tagging={onTagging}
                value={colorValues}
                showSelectAll={true}>
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent>
             
            </div>
            <h4>{valid}</h4>
            <footer className={classes.actions}>
              <Button className={classes.btn}
                onClick={props.onClose}
              >cancel</Button>
              <Button
                type='submit'
              >Update</Button>
            </footer></Form>
        </Card>
      </div>

    </div>
  );

}
export default EditWebsiteInput;