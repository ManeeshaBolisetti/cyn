import { React, useState } from 'react';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-enterprise';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import { RiFolderDownloadLine } from "react-icons/ri";
import { Button, Row, Col } from 'react-bootstrap';
import Search from './../../Country/Search';
import NavBar from '../../NavBar';
import Sidebar from '../../Sidebar';
import UseFetch from '../../UseFetch';


const B2BR = () => {
    let b2br = UseFetch('/Admin/b2bRequests');
    const [gridApi, setGridApi] = useState(null);
    // const [girdColumnApi, setGridColumnApi] = useState(null);
   console.log(b2br)
    // const [suppressClickEdit, setSuppressClickEdit] = useState(true);
    // const [edit, setEdit] = useState('Edit');
    const rowData = [
        { id: 1, name: "Rahul", age: 19, phoneNumber: 9876543210, birthYear: 2001, date: '24/08/2008',status:'New'  },
        { id: 2, name: "David", age: 17, phoneNumber: 9827654310, birthYear: 2003, date: '23/09/2009',status:'New' },
        { id: 3, name: "Dan", age: 25, phoneNumber: 9765438210, birthYear: 1995, date: '22/07/2008',status:'In-progress' },
        { id: 4, name: "Rahul", age: 19, phoneNumber: 9876543210, birthYear: 2001, date: '21/06/2008',status:'Approved' },
        { id: 5, name: "David", age: 17, phoneNumber: 9827654310, birthYear: 2003, date: '26/04/2008',status:'Approved' },
        { id: 6, name: "Dan", age: 25, phoneNumber: 9765438210, birthYear: 1995, date: '28/08/2008',status:'New'  },
        { id: 7, name: "Rahul", age: 19, phoneNumber: 9876543210, birthYear: 2001, date: '29/08/2008',status:'In-progress' },
        { id: 8, name: "David", age: 17, phoneNumber: 9827654310, birthYear: 2003, date: '30/08/2008',status:'New'  },
        { id: 9, name: "Dan", age: 25, phoneNumber: 9765438210, birthYear: 1995, date: '31/08/2009',status:'New'  },
        { id: 10, name: "Rahul", age: 19, phoneNumber: 9876543210, birthYear: 2001, date: '22/08/2008',status:'Approved' },
        { id: 11, name: "David", age: 17, phoneNumber: 9827654310, birthYear: 2003, date: '21/08/2008',status:'In-progress' },
        { id: 3, name: "Dan", age: 25, phoneNumber: 9765438210, birthYear: 1995, date: '26/08/2008' ,status:'In-progress'}]
    

    const columnDefs = [
        // { headerName: "ID", field: "id" },
        { headerName: "Ticket Id", field: "phoneNumber", filter: 'agSetColumnFilter', tooltipField: 'name' },
       
        { headerName: "UserName", field: "name", filter: 'agSetColumnFilter' },
        { headerName: "Mobile", field: "phoneNumber", filter: 'agSetColumnFilter', tooltipField: 'name' },
        // { headerName: "Lenin Name", field: "age", filter: 'agSetColumnFilter', tooltipField: 'name' },
        { headerName: "Email", field: "name", filter: 'agSetColumnFilter' },
        { headerName: "Country", field: "phoneNumber", filter: 'agSetColumnFilter', tooltipField: 'name' },
        { headerName: "Title", field: "phoneNumber", filter: 'agSetColumnFilter', tooltipField: 'name' },
         { headerName: "Description", field: "id" },
        { headerName: "Info", field: "id" },
       
        {
            headerName: "Created Date",
            field: "date",
            tooltipField: 'name',
            filter: 'agDateColumnFilter',
            filterParams: filterParams,
        },
        {
            headerName: "Updated Date",
            field: "date",
            tooltipField: 'name',
            filter: 'agDateColumnFilter',
            filterParams: filterParams,
        },
     
          // { headerName: "R", field: "birthYear", filter: 'agSetColumnFilter', tooltipField: 'name' },

        
    ]

    const defaultColDef = {
        sortable: true,
        editable: true,
        flex: 1,

        minWidth: 200,
        resizable: true,
        floatingFilter: true,
        //  floatingFilter: true
    }
    const onGridReady = (params) => {
        console.log('grid ready');
        setGridApi(params);
        // setGridColumnApi(params.columnApi);
    };
    // const AddCountryHandler = product => {
    //     console.log(product);
    //     // setCountry(prevExp => {
    //     //   return [product, ...prevExp];
    //     // });

    // };
    const pageSize = (pageSize) => {
        console.log(pageSize);
        gridApi.api.paginationSetPageSize(Number(pageSize))
    }

    // const showEdit = () => {
    //     girdColumnApi.setColumnVisible("action", false)
    //     setSuppressClickEdit(false);
    //     setEdit('EditMode')
    // }
    // const stopEdit = () => {
    //     setSuppressClickEdit(true);
    //     setEdit('Edit')
    // }
    // const onRemoveSelected = () => {
    //     var selectedData = gridApi.api.getSelectedRows();
    //     var res = gridApi.api.applyTransaction({ remove: selectedData });
    //     printResult(res);
    // };
    const onCellValueChanged = (event) => {
        console.log(
            'onCellValueChanged: ' + event.colDef.field + ' = ' + event.newValue
        );
    };

    const onRowValueChanged = (event) => {
        var data = event.data;
        console.log(
            'onRowValueChanged: (' +
            data.countryName +
            ', ' +
            data.countryCode +
            ', ' +
            data.currency +
            ')'
        );
    };
    const onExportClick = () => {
        gridApi.api.exportDataAsCsv();
    }
    // const onBtPrint = () => {
    //     const api = gridApi.api;
    //     setPrinterFriendly(api);
    //     setTimeout(function () {
    //         print();
    //         setNormal(api);
    //     }, 2000);
    // };
    const onFilterTextChange = (e) => {

        gridApi.api.setQuickFilter(e.target.value);
    }

    return (
        <div>
            <NavBar/>
            <Sidebar/>
            <Row className='con'>
                <Col xs={11}>
            <div className='d-flex justify-content-between' >
                <h2>User Requests</h2>
                <Search onChange={onFilterTextChange} /></div>
                <Button className='add' onClick={() => onExportClick()}>
                        Export to Excel  <RiFolderDownloadLine color='green' />
                    </Button >
            <Row >
                <Col xs={10} style={{ display: 'flex' }}>
                    {/* <Refresh/> */}
                    {/* <AddCountry
                        onAddCountry={AddCountryHandler}
                    /> */}
                    {/* <Button className='add' onClick={() => onRemoveSelected()}>Remove <FcDeleteDatabase /></Button > */}
                    {/* <Button className='add' onClick={showEdit}>{edit} <FcEditImage /></Button > */}
                    {/* <Button className='add' onClick={stopEdit}>stop <BsFillStopFill color='darkred' /></Button > */}
                    

                    {/* <Button className='add' onClick={() => onBtPrint()}>Print <FcPrint /></Button ></Col> */}

               </Col>

            </Row>
            <div id="myGrid" className="ag-theme-alpine" style={{ height: '450px' }}>
                <AgGridReact

                    // onCellClicked={onCellClicked}
                    editType="fullRow"
                    rowSelection={'multiple'}
                    onCellValueChanged={onCellValueChanged}
                    onRowValueChanged={onRowValueChanged}

                    // suppressClickEdit={suppressClickEdit}
                    pagination={true}
                    paginationPageSize={25}
                    // paginationAutoPageSize={true}
                    columnDefs={columnDefs}
                    rowData={rowData}
                    defaultColDef={defaultColDef}
                    onGridReady={onGridReady}>



                </AgGridReact>
                <div className='selectbox' style={{
                    padding: '10px',
                    marginTop: '-10px', fontSize: 'small',
                }}> Page Size: <select onChange={(e) => pageSize(e.target.value)}>
                        <option value='10'>10</option>
                        <option value='25'>25</option>
                        <option value='50'>50</option>
                        <option value='100'>100</option>
                    </select></div>
            </div>
</Col></Row>        </div>

    );
}
export default B2BR;

var filterParams = {
    comparator: function (filterLocalDateAtMidnight, cellValue) {
      var dateAsString = cellValue;
      if (dateAsString == null) return -1;
      var dateParts = dateAsString.split('/');
      var cellDate = new Date(
        Number(dateParts[2]),
        Number(dateParts[1]) - 1,
        Number(dateParts[0])
      );
      if (filterLocalDateAtMidnight.getTime() === cellDate.getTime()) {
        return 0;
      }
      if (cellDate < filterLocalDateAtMidnight) {
        return -1;
      }
      if (cellDate > filterLocalDateAtMidnight) {
        return 1;
      }
    },
    browserDatePicker: true,
    minValidYear: 2000,
  };
//   var ragCellClassRules = {
//     'rag-green-outer': function (params) {
//       return params.value === 'New';
//     },
//     'rag-amber-outer': function (params) {
//       return params.value === 'In-progress';
//     },
//     'rag-red-outer': function (params) {
//       return params.value === 'Approved';
//     },
//   };
//   function ragRenderer(params) {
//     return '<span class="rag-element">' + params.value + '</span>';
//   }