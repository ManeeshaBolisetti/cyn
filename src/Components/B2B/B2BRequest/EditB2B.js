import { useState, useRef } from 'react';
import { Form, Button } from 'react-bootstrap';
import Card from '../../Card/Card';
import classes from './Display.Module.css';
// import axios from 'axios';
import api from './../../Api';
import { FcDeleteDatabase } from "react-icons/fc";
import { FiEdit } from 'react-icons/fi';
import {AiOutlineCloseCircle} from 'react-icons/ai';

import Alert from '@material-ui/lab/Alert';
// import { ImageGroup } from 'semantic-ui-react';
// import { CheckBoxSelection, Inject, MultiSelectComponent } from '@syncfusion/ej2-react-dropdowns';
// import axios from 'axios';

const EditB2B = (props) => {
    const [flash, setFlash] = useState(null);
        const [Errflash, setErrFlash] = useState(null);
        const styles = {
          alert: {
            zIndex: '1500',
          },
        }
    console.log(props.value)
    let value = props.value
    const user = value.length === 1 ? value[0].userRef : '';
    const b2bId = value.length === 1 ? value[0].b2bRequestId : '';
    const paymentDetails = value.length === 1 ? value[0].paymentDetails : '';
    const img = value.length === 1 ? value[0].bannerImage : '';
    const sub = value.length === 1 ? value[0].subBanner : '';
    const multi = value.length === 1 ? value[0].multiBanner : '';
    console.log(multi)
    const multibanner = multi.map((n) => {
        let multiImg = {
            'image': n,
        }
        return multiImg;
    }
    )
    console.log(multibanner);

    // const multiImg={
    //     'image':multi,
    // }
    // console.log(multiImg);
    const userId = user.userId;
    const [images, setImages] = useState({ file: img })
    const [multiImage, setMultiImage] = useState(multibanner);
    const [image, setImage] = useState(sub);
    // const Invalid = '';
    // const [valid, setValid] = useState(Invalid);
    const [status, setStatus] = useState('');
    const [errStatus,setErrStatus]=useState('');
    const bannerImageRef = useRef();
    const subBannerImageRef = useRef();
    const multiBannerImageRef = useRef();

    const BannerUpdate = (event) => {
        event.preventDefault();
        // setImages(bannerImageRef.current.files[0])

        const bannerImage = bannerImageRef.current.files[0]
        const bannerData = {
            bannerImage: bannerImage,

        };
        props.onSaveBannerData(bannerData);
        console.log(bannerData)
        let formData = new FormData();
        formData.append('image', bannerData.bannerImage);
        formData.append('userId', userId);
        formData.append('b2bRequestId', b2bId);
        formData.append('status', 'banner');
        // console.log();
        // let url = 'admin/uploadB2bApprovedBanners'
        // axios.post(url, formData)
        api.Approvebanner(formData)
            .then(response => {
                setStatus({ data: response.data.message });
                setFlash(true);
                    setTimeout(() => {
                      setFlash(null);
                      // setFlashE(null);
                    }, 5000);
                setImages('')
                setImages({ file: response.data.data.bannerImage });
                // window.location.reload();
            })
            .catch(error => {
                if (error.response) {
                    setErrStatus (error.response.data.message) ;
                        setErrFlash(true);
                        setTimeout(() => {
                          setErrFlash(null);
                          // setFlashE(null);
                        }, 5000);
                    // console.log(error.response.data.message);
                } else {
                    setStatus( error.message );
                    setErrStatus (error.response.data.message) ;
                    setErrFlash(true);
                    setTimeout(() => {
                        setErrFlash(null);
                      // setFlashE(null);
                    }, 5000);
                    console.error('There was an error!', error);

                }

            });

    }
    const RemoveBanner = () => {
        const B2BRemoveBanner = {
            userId: userId,
            b2bRequestId: b2bId,
            imageUrl: images.file,
            status: 'banner',
            // deviceToken:"null",

            // id: Math.random().toString()
        };
        console.log(B2BRemoveBanner)
        api.removeB2BBanner(B2BRemoveBanner)
        // axios.post('admin/removeB2BBannerImage', B2BRemoveBanner)
            .then(response => {
                setStatus({ data: response.data.message });
                setFlash(true);
                    setTimeout(() => {
                      setFlash(null);
                      // setFlashE(null);
                    }, 5000);
                setImages('')
                console.log(response.data.message)

                // window.location.reload();
            })
            .catch(error => {
                if (error.response) {
                    setErrStatus (error.response.data.message) ;
                        setErrFlash(true);
                        setTimeout(() => {
                            setErrFlash(null);
                          // setFlashE(null);
                        }, 5000);
                } else {
                    setErrStatus (error.response.data.message) ;
                        setErrFlash(true);
                        setTimeout(() => {
                            setErrFlash(null);
                          // setFlashE(null);
                        }, 5000);

                }

            });

    }
    const SubBannerUpdate = (event) => {
        event.preventDefault();

        // setImages(bannerImageRef.current.files[0])

        const subBannerImage = subBannerImageRef.current.files[0]
        console.log(subBannerImage)

        let formData = new FormData();
        formData.append('image', subBannerImage);
        formData.append('userId', userId);
        formData.append('b2bRequestId', b2bId);
        formData.append('status', 'SubBanner');
        // console.log();
        // let url = 'admin/uploadB2bApprovedBanners'
        api.Approvebanner(formData)
        // axios.post(url, formData)
            .then(response => {
                setStatus({ data: response.data.message });
                setFlash(true);
                    setTimeout(() => {
                      setFlash(null);
                      // setFlashE(null);
                    }, 5000);
                setImage(response.data.data.subBanner);
                // window.location.reload();
            })
            .catch(error => {
                if (error.response) {
                    setErrStatus (error.response.data.message) ;
                    setErrFlash(true);
                    setTimeout(() => {
                        setErrFlash(null);
                      // setFlashE(null);
                    }, 5000);
                } else {
                    setErrStatus (error.response.data.message) ;
                    setErrFlash(true);
                    setTimeout(() => {
                        setErrFlash(null);
                      // setFlashE(null);
                    }, 5000);

                }

            });

    }
    const SubRemoveBanner = () => {
        const B2BRemoveBanner = {
            userId: userId,
            b2bRequestId: b2bId,
            imageUrl: image,
            status: 'sub Banner',
            // deviceToken:"null",

            // id: Math.random().toString()
        };
        console.log(B2BRemoveBanner)
        api.removeB2BBanner(B2BRemoveBanner)
        // axios.post('admin/removeB2BBannerImage', B2BRemoveBanner)
            .then(response => {
                setStatus({ data: response.data.message });
                setFlash(true);
                    setTimeout(() => {
                      setFlash(null);
                      // setFlashE(null);
                    }, 5000);
                setImage('')
                console.log(response.data.message)

                // window.location.reload();
            })
            .catch(error => {
                if (error.response) {
                    setErrStatus (error.response.data.message) ;
                        setErrFlash(true);
                        setTimeout(() => {
                            setErrFlash(null);
                          // setFlashE(null);
                        }, 5000);
                } else {
                    setErrStatus (error.response.data.message) ;
                        setErrFlash(true);
                        setTimeout(() => {
                            setErrFlash(null);
                          // setFlashE(null);
                        }, 5000);

                }

            });

    }
    const MultiBannerUpdate = (event) => {
        event.preventDefault();
        // setImages(bannerImageRef.current.files[0])

        const multibannerImage = multiBannerImageRef.current.files
        console.log(multibannerImage)
        // setMultiImage(multibannerImage)
        // console.log(multiImage)

        // setImages({})
        // const bannerData = {
        //     bannerImage: bannerImage,

        // };
        // props.onSaveBannerData(bannerData);
        // console.log(bannerData)
        let formData = new FormData();
        for (let i = 0; i < multibannerImage.length; i++) {
            formData.append('images', multibannerImage[i]);
        }
        // formData.append('images',multibannerImage );
        formData.append('userId', userId);
        formData.append('b2bRequestId', b2bId);
        // console.log();
        // let url = 'admin/uploadB2bApprovedMultiBanners'
        // axios.post(url, formData)
        api.updateMultiB(formData)
            .then(response => {
                setStatus({ data: response.data.message });
                setFlash(true);
                    setTimeout(() => {
                      setFlash(null);
                      // setFlashE(null);
                    }, 5000);
                // console.log(response.data.data.bannerMultiImages)

                setMultiImage(response.data.data.bannerMultiImages);
                // setImages({file:response.data.data.bannerImage});
                // window.location.reload();
            })
            .catch(error => {
                if (error.response) {
                    setErrStatus (error.response.data.message) ;
                    setErrFlash(true);
                    setTimeout(() => {
                        setErrFlash(null);
                      // setFlashE(null);
                    }, 5000);
                } else {
                    setErrStatus (error.response.data.message) ;
                    setErrFlash(true);
                    setTimeout(() => {
                        setErrFlash(null);
                      // setFlashE(null);
                    }, 5000);

                }

            });

    }
   
    const SubmitHandler = (event) => {
        event.preventDefault();

        const ActiveData = {
            'userId': userId,
            'b2bRequestId': b2bId,
            'validity': paymentDetails.validity,
            'validityType': paymentDetails.validityType,
            'status': 'Active',


            // id: Math.random().toString()
        };
        console.log(ActiveData)
        // axios.post('admin/approveB2bRequest', ActiveData)
        api.approveB2bRequest(ActiveData)
            .then(response => {
                setStatus({ data: response.data.message });
                setFlash(true);
                    setTimeout(() => {
                      setFlash(null);
                      // setFlashE(null);
                    }, 5000);
                //   props.onUpdate(response.data.data) z
                  console.log(response.data) 
                // window.location.reload();
            })
            .catch(error => {
                if (error.response) {
                    setErrStatus (error.response.data.message) ;
                    setErrFlash(true);
                    setTimeout(() => {
                        setErrFlash(null);
                      // setFlashE(null);
                    }, 5000);
                } else {
                    setErrStatus (error.message) ;
                        setErrFlash(true);
                        setTimeout(() => {
                            setErrFlash(null);
                          // setFlashE(null);
                        }, 5000);

                }

            })


    }
    const handleChange = (event) => {

        setImages({
            file: URL.createObjectURL(event.target.files[0]),

        })

    }
    const deleteMulti=(n)=>{
        console.log(n)
        const B2BRemoveBanner = {
            userId: userId,
            b2bRequestId: b2bId,
            imageUrl: n.image,
            
            // deviceToken:"null",

            // id: Math.random().toString()
        };
        console.log(B2BRemoveBanner)
        // axios.post('admin/removeB2BMultiImage', B2BRemoveBanner)
        api.removeB2BMulti(B2BRemoveBanner)
            .then(response => {
                setStatus({ data: response.data.message });
                setFlash(true);
                    setTimeout(() => {
                      setFlash(null);
                      // setFlashE(null);
                    }, 5000);
                setImage('')
                console.log(response.data.message)

                // window.location.reload();
            })
            .catch(error => {
                if (error.response) {
                    setErrStatus (error.response.data.message) ;
                        setErrFlash(true);
                        setTimeout(() => {
                            setErrFlash(null);
                          // setFlashE(null);
                        }, 5000);
                } else {
                    setErrStatus (error.response.data.message) ;
                        setErrFlash(true);
                        setTimeout(() => {
                            setErrFlash(null);
                          // setFlashE(null);
                        }, 5000);

                }

            });

    }

    

    return (
        <div className={classes.input}>

            <Card className={classes.modal}>
                {/* <ul>
        { countries.map(person => <li>{person.countryName}</li>)}
      </ul> */}
                <header className={classes.header}>
                    <h2>Upload Banners</h2>
                    <AiOutlineCloseCircle size={35} color='white'
                        onClick={props.onClose}
                    />
                </header>
                {
        flash
        ? <Alert style={styles.alert} severity="success">{status.data}</Alert>
        : null
      }{
        Errflash
        ? <Alert style={styles.alert} severity="error">{errStatus}</Alert>
        : null
      }
                {/* <h4>{stat}</h4> */}
                <div className={classes.content}>

                    Banner Image:
                    <Form.Control
                        type='file'
                        placeholder='Select BannerImage'
                        ref={bannerImageRef}
                        onChange={handleChange}

                    />      <img className={classes.image} src={images.file}
                        alt='previw' />

                    <FiEdit size={25} color='rgb(64 110 78)' onClick={BannerUpdate} />
                    < FcDeleteDatabase size={25} color='#9b040b' onClick={RemoveBanner} />
                    <br />
                    SubBanner Image:
                    <Form.Control
                        type='file'
                        placeholder='Select SubBannerImage'
                        ref={subBannerImageRef}
                    // onChange={handleChange}

                    />      <img className={classes.image} src={image}
                        alt='previw' />

                    <FiEdit size={25} color='rgb(64 110 78)' onClick={SubBannerUpdate} />
                    <FcDeleteDatabase size={25} color='#9b040b' onClick={SubRemoveBanner} />
                    <br />
                    MultiImage
                   <div className={classes.multi}> <Form.Control
                        type='file'
                        placeholder='Select Sub BannerImage'
                        multiple
                        ref={multiBannerImageRef}
                    // onChange={handleChange}

                    />
                    <FiEdit size={25} color='rgb(64 110 78)' onClick={MultiBannerUpdate}/>
                    </div> <div className={classes.multiBanner}>    {
                        multiImage.map((n) => {
                            return (
                                <div >
                                    {/* <input  type='text' value={n.image} onClick={multiExample}/> */}
                                    <img className={classes.image} src={n.image} alt='display' />
                                    {/* {onSaveUpdateM(n.image)} */}
                                    {/* <button ><input className={classes.inputmulti}  type='text' value={n.image} onClick={multiExample}/></button> */}
                                    <FcDeleteDatabase type='submit'
                                    size={25} color='#9b040b' 
                                    onClick={() => deleteMulti(n)} 
                                     />
                                </div>)

                        })}</div>

                    {/* <img className={classes.image} src={multiImage.file}
                    alt='previw' /> */}
                    {/* <button onClick={MultiRemoveBanner}> Remove</button> */}


                    {/* <h4>{valid}</h4> */}
                </div>
                <footer className={classes.actions}>
                    
                    <Button className={classes.submit} onClick={SubmitHandler}
                        type='submit'
                    // onClick={props.onClose}
                    >Add</Button>
                    {/* <Button className={classes.btn}
                            onClick={props.onClose}
                        >close</Button> */}
                </footer>
            </Card>
        </div>
    );
}
export default EditB2B;