import { React, useState } from 'react';
// import AddCountry from './AddCountry';
import { AgGridReact } from 'ag-grid-react';
import * as moment from 'moment';
import 'ag-grid-enterprise';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import { FcEditImage } from "react-icons/fc";
import { TiEdit } from 'react-icons/ti';
import { RiFolderDownloadLine } from "react-icons/ri";
import { Button, Row, Col } from 'react-bootstrap';
import Search from '../../Country/Search';
import NavBar from '../../NavBar';
import Sidebar from '../../Sidebar';
import Refresh from '../../Actions/Refresh';
import EditB2B from './EditB2B';
import Notes from './Notes';
import Alert from '@material-ui/lab/Alert';
import api from './../../Api';
import EditStatus from './EditStatus';
import Display from './Display';
import CustomLoadingOverlay from './customLoadingOverlay.jsx';

var filterParams = {
    comparator: function (filterLocalDateAtMidnight, cellValue) {
        var dateAsString = cellValue;
        if (dateAsString == null) return -1;
        var dateParts = dateAsString.split('/');
        var cellDate = new Date(
            Number(dateParts[2]),
            Number(dateParts[1]) - 1,
            Number(dateParts[0])
        );

        if (filterLocalDateAtMidnight.getTime() === cellDate.getTime()) {
            return 0;
        }

        if (cellDate < filterLocalDateAtMidnight) {
            return -1;
        }

        if (cellDate > filterLocalDateAtMidnight) {
            return 1;
        }
    },
    browserDatePicker: true,
    minValidYear: 2000,
};

const B2BM = () => {
    // const classes=useStyles();
    const [flash, setFlash] = useState(null);
    const [Errflash, setErrFlash] = useState(null);
    const styles = {
        alert: {
            zIndex: '1500',
        },
    }
    let statu = '';
    const [showForm, setshowForm] = useState(false);
    const [note, setNote] = useState(false);
    const [info, setInfo] = useState(false);
    const [b2bm, setB2Bm] = useState([]);
    // let b2bm = UseFetch('/Admin/b2bRequests')
    // useEffect(() => {

    //     api.getB2bRequests()
    //         .then(response => {
    //             let category = response.data.data;
    //             setB2Bm(category);
    //             // console.log(category);
    //         });

    // }, []);
    const onGridReady = (params) => {
        params.api.showLoadingOverlay();
        console.log('grid ready');
        // onBtShowLoading();
        setGridApi(params);
        setGridColumnApi(params.columnApi);
        api.getB2bRequests()
        .then(response => {
           
            let category = response.data.data;
            setB2Bm(category);
            
            // params.api.applyTransaction({add:category})
           
            // console.log(category);
        });

    };
    let b2bmList = [];
    // const b2bUpdate = [];
    // console.log(b2bm)
    const [gridApi, setGridApi] = useState(null);
    const [girdColumnApi, setGridColumnApi] = useState(null);
    const [b2b, setB2b] = useState('');
    const [status, setStatus] = useState(statu);
    const [errStatus, setErrStatus] = useState('');
    // const [userId, setUserId] = useState('');
    // const [b2bRequestId, setB2bRequestId] = useState('');
    // const [image, setImage] = useState('');

    // const [userName,setUserName]=useState('');

    const [suppressClickEdit, setSuppressClickEdit] = useState(true);
    const [edit, setEdit] = useState('Edit');
    // const [load, setLoad] = useState('Load');
    // const onShowForm = () => {

    //     setshowForm(true);
    // }
    // const onShowForm = () => {
    //     if(b2b.length===1 ){
    //     setshowForm(true);
    // }else{
    //     alert('please select edit Row');
    // }
    // }
    const onHideNote = () => {
        setNote(false);
    }
    const NotesHandler = () => {
        console.log('clicked')
        var selectedRows = gridApi.api.getSelectedRows();
        setNote(true);
        console.log(selectedRows)
    }
    // const onBtShowLoading = () => {
    //     gridApi.api.showLoadingOverlay();
    //   };
    const onShowForm = () => {
        if (b2b.length === 1) {
            // setshowForm(true)
            if (b2b[0].status === 'in progress' || b2b[0].status === 'In progress' || b2b[0].status === 'Active') {
                setshowForm(true);
            }
            else {
                alert('status is not In progress');
                setshowForm(false);
            }

        } else {
            alert('please select edit Row');
        }
    }
    const onHideForm = () => {
        setshowForm(false);
        // setStatus("Add A Subscription");
    }

    const onShowInfo = () => {
        if (b2b.length === 1) {
            // setshowForm(true)
            setInfo(true);

        } else {
            alert('please select edit Row');
        }
    }
    const onHideInfo = () => {
        setInfo(false);
        // setStatus("Add A Subscription");
    }
    const m = moment(new Date('2021-10-21T05:21:20.936Z'));
    console.log(m.format('h:mma'))
    if (b2bm != null && b2bm.length > 0) {
        b2bm.map((n) => {

            let rDate = moment(n.requestDate).format('DD/MM/YYYY');
            let eDate = moment(n.expiryDate).format('DD/MM/YYYY');
            let time = moment(n.requestDate).format('h:mma');
            let etime = moment(n.expiryDate).format('h:mma');
            let location = n.locations.map((c) => {

                let locationName = c.locationName
                return locationName;
            })
            let multiBannerImage = n.bannerMultiImages.map((c) => {
                let multiBanner = c.image
                return multiBanner;
            })


            let socialMediaList = n.socialMediaList.map((c) => {

                let app = c.app
                return app;
            })
            // let socialMediaValue= n.socialMediaList.map((c) => {

            //     let app = c.value
            //     return app;
            // })
            let uploadImages = n.uploadImages.map((c) => {

                let image = c.image
                return image;
            })
            let webList = n.webList.map((c) => {

                let weblist = c.app
                return weblist;
            })
          
            // let multiBannerImage=n.bannerMultiImages((c)=>{
            //     let multiBanner=c.image
            //     return multiBanner;
            // })

            // let userName=n.userRef.userName
            return b2bmList.push({
                'ticketNumber': n.ticketNumber,
                'userRef': n.userRef,
                'name': n.name,
                'businessName':n.businessName,
                'phoneNumber': n.phoneNumber,
                'email': n.email,
                'productName': n.cynRequestRef,
                'productImage': n.cynRequestRef,
                'personType': n.cynRequestRef,
                'b2bRequestId': n.b2bRequestId,
                'bannerImage': n.bannerImage,
                'subBanner': n.subBanner,
                'multiBanner': multiBannerImage,
                'uploadImages': uploadImages,
                'description': n.description,
                'couponCode': n.couponCode,
                'offer': n.offer,
                'expiryDate': eDate,
                'status': n.status,
                'info': n.info,
                'country': n.country.countryCode,
                'requestDate': rDate,
                'allCountry': n.allCountry,
                'locations': location,
                'socialMediaList': socialMediaList,
                'webList': webList,
                'paymentDetails': n.paymentDetailes,
                'supportNotes': n.supportNotesRef,
                'time': time,
                'etime': etime,
                'socialMediaValue': n.socialMediaList,
                'webvalue':n.webList,
            })



        })
    }

    const columnDefs = [
        // { headerName: "ID", field: "id" },
        { headerName: "Ticket Id", field: "ticketNumber", filter: 'agSetColumnFilter', editable: false },
        // { headerName: "UserName", field: "userRef.userName", editable: false, filter: 'agSetColumnFilter', tooltipField: 'name' },
        // { headerName: "Lenin Name", field: "age", filter: 'agSetColumnFilter', tooltipField: 'name' },
        {
            headerName: "Name",
            field: "name", editable: false,
            filter: 'agSetColumnFilter'
            // tooltipField: 'name',
            // filter: 'agDateColumnFilter',
            // filterParams: filterParams,
        },
        {
            headerName: "Business Name",
            field: "businessName", editable: false,
            filter: 'agSetColumnFilter'
            // tooltipField: 'name',
            // filter: 'agDateColumnFilter',
            // filterParams: filterParams,
        },
        { headerName: "phoneNumber", editable: false, field: "phoneNumber", filter: 'agSetColumnFilter', tooltipField: 'name' },

        {
            headerName: " Status",
            field: "status",
            cellClassRules: ragCellClassRules,
            filter: 'agSetColumnFilter',
            cellRenderer: ragRenderer,
            cellEditor: 'agSelectCellEditor',
            cellEditorParams: {
                values: ['in progress', 'Rejected', 'Refunded'],
            },




        },


        // { headerName: "email", field: "email", editable: false },
        // { headerName: "productName", field: "productName.productRef.productName", editable: false },
        {
            headerName: "product", field: "productImage.productRef.productName",

            filter: 'agSetColumnFilter',
            editable: false,
            cellRenderer: productCellRenderer,
        },
        {
            headerName: "personType", field: "personType.personTypeRef.personType",

            filter: 'agSetColumnFilter', editable: false
        },
        // {
        //     field: "bannerImage", editable: false,
        //     cellRenderer: bannerCellRenderer,
        // },
        // {
        //     field: "subBanner", editable: false,
        //     cellRenderer: subBannerCellRenderer,
        // },
        // {
        //     field: "multiBanner", editable: false,
        //     cellRenderer: multiBannerCellRenderer,
        // },
        // {
        //     field: "uploadImages", editable: false,
        //     // cellRenderer: 'ArrayOfImage', 
        // },
        {
            field: "locations",
            filter: 'agSetColumnFilter', editable: false
        },
        {
            field: "info",
            filter: 'agSetColumnFilter', editable: false
        },
        {
            field: "description",
            filter: 'agSetColumnFilter', editable: false
        },
        // { field: "couponCode", editable: false },
        // { field: "offer", editable: false },

        {
            field: "requestDate", filter: 'agDateColumnFilter',cellRenderer: reqDate,
            filterParams: filterParams, editable: false
        },
        
        {
            field: "expiryDate", filter: 'agDateColumnFilter',cellRenderer :expDate,
            filterParams: filterParams, editable: false
        },
        // { field: "couponCode", editable: false },

        {
            headerName: 'Country',
            field: "country.countryCode",
            filter: 'agSetColumnFilter', editable: false
        },
        // { field: "allCountry", editable: false },
        // { field: "locations", editable: false },
        // { field: "socialMediaList", editable: false },
        // { field: "webList", editable: false },
        {
            headerName: 'paymentType',
            filter: 'agSetColumnFilter', field: "paymentDetails.paidAmount", editable: false, cellRenderer: paymentType,
        },
        // { field: "paymentDetails.currency", editable: false },
        {
            headerName: 'paymentDetails',
            filter: 'agSetColumnFilter', field: "paymentDetails.validity", editable: false, cellRenderer: paymentDetails,
        },
        // { field: "paymentDetails.validityType", editable: false },
        // { field: "paymentDetails.transactionId", editable: false },

        {
            headerName: 'Payment Status',
            filter: 'agSetColumnFilter', field: "paymentDetails.isSuccess", editable: false
        },

        {
            headerName: 'Notes', field: "name",
            cellRendererFramework: (params) => <div><TiEdit fontSize='30' color='#275788' onClick={NotesHandler} />

            </div>,


        },

        // { headerName: "R", field: "birthYear", filter: 'agSetColumnFilter', tooltipField: 'name' },


    ]

    const defaultColDef = {
        sortable: true,
        editable: true,
        flex: 1,

        minWidth: 200,
        resizable: true,
        floatingFilter: true,
        customLoadingOverlay: CustomLoadingOverlay,
        loadingOverlayComponent: 'customLoadingOverlay',
      loadingOverlayComponentParams: { loadingMessage: 'One moment please...' },
     
        //  floatingFilter: true
    }
   
    // const AddCountryHandler = product => {
    //     console.log(product);
    //     // setCountry(prevExp => {
    //     //   return [product, ...prevExp];
    //     // });

    // };
    const pageSize = (pageSize) => {
        console.log(pageSize);
        gridApi.api.paginationSetPageSize(Number(pageSize))
    }
    const EditStatusHandler = website => {
        setB2Bm(prevExp => {
            // console.log(product);
            return [website, ...prevExp];
        });
        console.log(website)
    }
    const showEdit = () => {
        alert('Are you go to Edit Mode')
        girdColumnApi.setColumnVisible("action", false)
        setSuppressClickEdit(false);
        setEdit('EditMode')
    }
    const stopEdit = () => {
        if(edit === 'EditMode'){
            alert('Are Quit Mode!')
        setSuppressClickEdit(true);
        setEdit('Edit')
        }
        else{
         setEdit('Edit')   
        }
        
    }
    // const onRemoveSelected = () => {
    //     var selectedData = gridApi.api.getSelectedRows();
    //     var res = gridApi.api.applyTransaction({ remove: selectedData });
    //     printResult(res);
    // };
    const onCellValueChanged = (event) => {
        console.log(
            'onCellValueChanged: ' + event.colDef.field + ' = ' + event.newValue
        );
    };
    const saveBannerDataHandler = (bannerData) => {
        const BannerImage = {
            ...bannerData,
        }
        console.log(BannerImage)
        // setImage(BannerImage)
        console.log(bannerData);
    }
    // console.log(image)
    let B2BDataBanner, B2BDataRemove;

    const onRowValueChanged = (event) => {
        const data = event.data;
        if (data.status === 'in progress') {


            const B2BData = {
                userId: data.userRef.userId,
                b2bRequestId: data.b2bRequestId,
                // validity:data.paymentDetails.validity,
                // validityType:data.paymentDetails.validityType,
                status: data.status,
                deviceToken: "null",

                // id: Math.random().toString()
            };
            console.log(B2BData)
            // axios.post('admin/updateB2bRequest', B2BData)
            api.updateB2bReq(B2BData)
                .then(response => {
                    setStatus({ data: response.data.message });
                    // setB2Bm([catgory.data, ...prevExp])
                    setFlash(true);
                    setTimeout(() => {
                        setFlash(null);
                        // setFlashE(null);
                    }, 5000);
                    console.log(response.data.message)
                    // window.location.reload();
                })
                .catch(error => {
                    if (error.response) {
                        setErrStatus(error.response.data.message);
                        setErrFlash(true);
                        setTimeout(() => {
                            setFlash(null);
                            // setFlashE(null);
                        }, 5000);
                        // console.log(error.response.data.message);
                    } else {
                        setErrStatus(error.message);
                        // console.error('There was an error!', error);

                    }

                })

        }

        else if (data.status === 'Rejected') {
            // setshowForm(true)

            B2BDataBanner = {
                userId: data.userRef.userId,
                b2bRequestId: data.b2bRequestId,
                status: data.status,
                // image:image.bannerImage
            };
            console.log(B2BDataBanner)
            // let formData = new FormData();
            // // formData.append('image', B2BDataBanner.image);
            // formData.append('userId', B2BDataBanner.userId);
            // formData.append('b2bRequestId', B2BDataBanner.b2bRequestId);
            // formData.append('status', B2BDataBanner.status);
            // console.log();
            // let url = 'admin/updateB2bRequest'
            // axios.post(url, B2BDataBanner)
            api.updateB2bReq(B2BDataBanner)
                .then(response => {
                    setStatus({ data: response.data.message });
                    setFlash(true);
                    setTimeout(() => {
                        setFlash(null);
                        // setFlashE(null);
                    }, 5000);
                    console.log(response.data.message)
                    // window.location.reload();
                })
                .catch(error => {
                    if (error.response) {
                        setErrStatus(error.response.data.message);
                        setErrFlash(true);
                        setTimeout(() => {
                            setFlash(null);
                            // setFlashE(null);
                        }, 5000);
                        // console.log(error.response.data.message);
                    } else {
                        setErrStatus(error.message);
                        // console.error('There was an error!', error);

                    }

                })


        }
        else if (data.status === 'Refunded') {
            // setshowForm(true)

            B2BDataRemove = {
                userId: data.userRef.userId,
                b2bRequestId: data.b2bRequestId,
                status: data.status,
                // image:image.bannerImage
            };
            console.log(B2BDataRemove)
            // let formData = new FormData();
            // // formData.append('image', B2BDataBanner.image);
            // formData.append('userId', B2BDataBanner.userId);
            // formData.append('b2bRequestId', B2BDataBanner.b2bRequestId);
            // formData.append('status', B2BDataBanner.status);
            // console.log();
            // let url = 'admin/updateB2bRequest'
            // axios.post(url, B2BDataBanner)
            api.b2bRefund(B2BDataRemove)
                .then(response => {
                    setStatus({ data: response.data.message });
                    setFlash(true);
                    setTimeout(() => {
                        setFlash(null);
                        // setFlashE(null);
                    }, 5000);
                    console.log(response.data.message)
                    // window.location.reload();
                })
                .catch(error => {
                    if (error.response) {
                        setErrStatus(error.response.data.message);
                        setErrFlash(true);
                        setTimeout(() => {
                            setFlash(null);
                            // setFlashE(null);
                        }, 5000);
                        // console.log(error.response.data.message);
                    } else {
                        setErrStatus(error.message);
                        // console.error('There was an error!', error);

                    }

                })


        }
    }
    // console.log(B2BDataBanner)
    const onExportClick = () => {
        gridApi.api.exportDataAsCsv();
    }
    const onFilterTextChange = (e) => {

        gridApi.api.setQuickFilter(e.target.value);
    }
    const onSelectionChanged = () => {
        var selectedRows = gridApi.api.getSelectedRows();
        console.log(selectedRows);
        setB2b(selectedRows);

        // document.querySelector('#selectedRows').innerHTML =
        //   selectedRows.length === 1 ? selectedRows[0].countryCode : '';
    };


    return (
        <div >
            <NavBar />
            <Sidebar />
            {/* <FlashMessage duration={5000} persistOnHover={true}>
  <p>Message</p>
</FlashMessage> */}
            {showForm &&
                <EditB2B
                    name='Upload a Banner'
                    countries='select countries'
                    onSaveBannerData={saveBannerDataHandler}
                    value={b2b}
                    // userId={userI}
                    // b2bRequestId={B2bRequestId}
                    // status={Status} 
                    onClose={onHideForm}

                />}
            {note &&
                <Notes
                    name='Notes'
                    value={b2b}
                    // value={}
                    // userId={userI}
                    // b2bRequestId={B2bRequestId}
                    // status={Status} 
                    onClose={onHideNote}

                />}
            {info && <Display
                name='Information'
                value={b2b}
                // value={}
                // userId={userI}
                // b2bRequestId={B2bRequestId}
                // status={Status} 
                onClose={onHideInfo}

            />}


            {/* {console.log(sessionStorage.getItem("user"))} */}

            <Row className='con'>
                <Col xs={11}>
                    <h6 className='text-center'>Backend: {localStorage.getItem('url')}</h6>
                    {/* <h6 className='text-center'>FrontEnd: {process.env.REACT_APP_API_ENDPOINT}</h6> */}
                    <div className='d-flex justify-content-between' onClick={stopEdit} >
                        <h2>B2B Requests</h2>

                        {/* <h1 style={{color:'green'}}>{status.data}</h1> */}
                        {/* <h1 style={{color:'red'}}>{errStatus}</h1> */}
                        {
                            flash
                                ? <Alert style={styles.alert} severity="success">{status.data}</Alert>
                                : null
                        }
                        {
                            Errflash
                                ? <Alert style={styles.alert} severity="error">{errStatus}</Alert>
                                : null
                        }
                        <Search onChange={onFilterTextChange} /></div>
                    <Row >
                        <Col xs={10} style={{ display: '-webkit-box' }} >

                            {/* <AddCountry
                        onAddCountry={AddCountryHandler}
                    /> */}
                            <Button className='add' onClick={onShowInfo}>Info</Button>
                            <Button className='add'><Refresh /></Button>
                            {/* <button onClick={() => onBtShowLoading()}>
            Show Loading Overlay
          </button> */}
                            {/* <Button className='add' onClick={() => onRemoveSelected()}>Remove <FcDeleteDatabase /></Button > */}
                            <Button className='add' onClick={showEdit}>{edit} <FcEditImage /></Button >
                            {/* <Button className='add' onClick={stopEdit}>stop <BsFillStopFill color='darkred' /></Button > */}
                            <Button className='add' onClick={() => onExportClick()}>
                                Export to Excel  <RiFolderDownloadLine color='green' />
                            </Button >
                            {/* <Button className='add' onClick={RemoveBannerHandler}>RemoveBanner</Button> */}
                            <Button className='add' onClick={onShowForm}>Banner Upload</Button>
                            <Button className='add' >
                                <EditStatus value={b2b} onUpdateStatus={EditStatusHandler}></EditStatus>
                            </Button>
                            {/* <Button className='add' onClick={() => onBtPrint()}>Print <FcPrint /></Button > */}
                        </Col>

                        <Col xs={2}>

                        </Col>

                    </Row>
                    <div id="myGrid" className="ag-theme-alpine" style={{ height: '400px' }}>
                        <AgGridReact

                            // onCellClicked={onCellClicked}
                            editType="fullRow"
                            rowSelection={'multiple'}
                            onSelectionChanged={onSelectionChanged}
                            onCellValueChanged={onCellValueChanged}
                            onRowValueChanged={onRowValueChanged}
                            frameworkComponents={{
                                customLoadingOverlay: CustomLoadingOverlay,
                                // customNoRowsOverlay: CustomNoRowsOverlay,
                              }}
                              loadingOverlayComponent={'customLoadingOverlay'}
                              loadingOverlayComponentParams={{
                                loadingMessage: 'One moment please...',
                              }}
                            // singleClickEdit={true}
                            suppressClickEdit={suppressClickEdit}
                            pagination={true}
                            paginationPageSize={25}
                            // frameworkComponents={{  BtnCellRender: BtnCellRender }}

                            // paginationAutoPageSize={true}
                            columnDefs={columnDefs}
                            rowData={b2bmList }
                            defaultColDef={defaultColDef}
                            onGridReady={onGridReady}>



                        </AgGridReact>
                        
                        <div className='selectbox' style={{
                            padding: '10px',
                            marginTop: '-10px', fontSize: 'small',
                        }}> Page Size: <select onChange={(e) => pageSize(e.target.value)}>
                                <option value='10'>10</option>
                                <option value='25'>25</option>
                                <option value='50'>50</option>
                                <option value='100'>100</option>
                            </select></div>
                    </div>
                </Col></Row>        </div>

    );
}
export default B2BM;
var ragCellClassRules = {
    'rag-green-outer': function (params) {
        return params.value === 'Active';
    },
    // 'rag-green-outer': function (params) {
    //     return params.value === 'Pending';
    // },
    'rag-amber-outer': function (params) {
        return params.value === 'In progress';
    },
    'rag-red-outer': function (params) {
        return params.value === 'Rejected';
    },
    // 'rag-red-outer': function (params) {
    //     return params.value === 'Expired';
    // },
    // 'rag-red-outer': function (params) {
    //     return params.value === 'New';
    // },
};
function ragRenderer(params) {
    return '<span class="rag-element">' + params.value + '</span>';
}

function paymentType(params) {
    let paidAmount, currency;
    if (params.data.paymentDetails != null) {
        paidAmount = params.data.paymentDetails.paidAmount;
        currency = params.data.paymentDetails.currency;

        return ('<span style="cursor: default;">' + paidAmount + ' ' + currency + '</span>'
        );
    }

}

function paymentDetails(params) {
    let validity, validityType;
    if (params.data.paymentDetails != null) {
        validity = params.data.paymentDetails.validity;
        validityType = params.data.paymentDetails.validityType;

        return ('<span style="cursor: default;">' + validity + ' ' + validityType + '</span>'
        );
    }

}

function productCellRenderer(params) {
    let image, productName;
    if (params.data.productImage != null) {
        image = params.data.productImage.productRef.productImage;
        productName = params.data.productName.productRef.productName;
        var logo =
            // params.data.catName;
            // '<img border="0" width="50" alt="image" height="30" src="' +
            // params.data.categoryImage +
            // '">';
            '<img  width="30" alt="image" height="30" src=" ' + image +
            '">';
        return (

            '<span style="cursor: default;">' + logo + ' ' + productName + '</span>'
        );
    }

}
// function bannerCellRenderer(params) {
//     var image = (params.data.bannerImage);
//     var logo =
//         '<img  width="50" alt="No Image" height="30" src=" ' + image +
//         '">';
//     return (

//         '<span style="cursor: default;">' + logo + '</span>'
//     );
// }

// function subBannerCellRenderer(params) {
//     var image = (params.data.subBanner);
//     var logo =
//         '<img  width="50" alt="No Image" height="30" src=" ' + image +
//         '">';
//     return (

//         '<span style="cursor: default;">' + logo + '</span>'
//     );
// }
// function multiBannerCellRenderer(params) {
//     var image = (params.data.multiBanner);
//     // console.log(image)
//     for (var i = 0; i < image.length; i++) {
//         // document.createElement(image[i]);
//         var logo =
//         '<img  width="50" alt="No Image" height="30" src=" ' + image[i] +
//         '">';
//     }
    // var logo =
    //     '<img  width="50" alt="No Image" height="30" src=" ' + image +
    //     '">';
//     return (

//         '<span style="cursor: default;">' + logo + '</span>'
//     );
// }
// function uploadCellRenderer(params) {
//     var image = (params.data.uploadImages);
//     var logo =
//         '<img style="background-color: brown; "  width="50" alt="image" height="30" src=" ' + image +
//         '">';
//     return (

//         '<span style="cursor: default;">' + logo + '</span>'
//     );
// }
function reqDate(params) {
    var time = (params.data.time);
    var date = (params.data.requestDate);
    
    return (
  
        '<span style="cursor: default;">' +date+' ' +time+ '</span>'
    );
  }
  function expDate(params) {
    var time = (params.data.utime);
    var date = (params.data.expiryDate);
    
    return (
  
        '<span style="cursor: default;">' +date+' ' +time+ '</span>'
    );
  }