// import { useState } from 'react';
import { Button } from 'react-bootstrap';
import Card from './Card/Card';
import classes from './Display.Module.css';

import {AiOutlineCloseCircle} from 'react-icons/ai';
const Display = (props) => {
  //   const Invalid = '';
  let value = props.value
  console.log(value);
  let ticketNumber;
  //   const [valid, setValid] = useState(Invalid);
   ticketNumber = value.length === 1 ? value[0].ticketNumber : '';
  // let uploadImages = value.length === 1 ? value[0].uploadImages : '';
  // const [image,setImage]=useState({file:uploadImages});

  let name = value.length === 1 ? value[0].name : '';
  let userRef = value.length === 1 ? value[0].userRef : '';
  // console.log(userRef)
  let phoneNumber = value.length === 1 ? value[0].phoneNumber : '';
  let email = value.length === 1 ? value[0].email : '';

  let productName = value.length === 1 ? value[0].productName : '';

  let b2bRequestId = value.length === 1 ? value[0].b2bRequestId : '';
  let bannerImage = value.length === 1 ? value[0].bannerImage : '';
  let subBanner = value.length === 1 ? value[0].subBanner : '';
  let multiImages = value.length === 1 ? value[0].multiBanner : '';
  let uploadImages = value.length === 1 ? value[0].uploadImages : '';
  let description = value.length === 1 ? value[0].description : '';
  let couponCode = value.length === 1 ? value[0].couponCode : '';
  let offer = value.length === 1 ? value[0].offer : '';
  let expiryDate = value.length === 1 ? value[0].expiryDate : '';
  let status = value.length === 1 ? value[0].status : '';
  let info = value.length === 1 ? value[0].info : '';
  let country = value.length === 1 ? value[0].country : '';
  let requestDate = value.length === 1 ? value[0].requestDate : '';
  let allCountry = value.length === 1 ? value[0].allCountry : '';
  let locations = value.length === 1 ? value[0].locations : '';
  let socialMediaList = value.length === 1 ? value[0].socialMediaValue : '';
  let paymentDetailes = value.length === 1 ? value[0].paymentDetails : '';
  // console.log(socialMediaList)
  let webList = value.length === 1 ? value[0].webvalue : '';
  // console.log(webList)
  
  let productI, productN, personTypeP;
  if (productName != null) {
    productN = productName.productRef.productName;
    productI = productName.productRef.productImage;
    personTypeP = productName.personTypeRef.personType;
  }
  let paidAmountP, currencyP, validityP, validityTypeP, transactionIdP;
  if (paymentDetailes != null) {
    paidAmountP = paymentDetailes.paidAmount;
    currencyP = paymentDetailes.currency;
    validityP = paymentDetailes.validity;
    validityTypeP = paymentDetailes.validityType;
    transactionIdP = paymentDetailes.transactionId;

  }
  let userName,userId;
  if (userRef != null) {
    userName = userRef.userName;
    userId=userRef.userId

  }


  return (

    <div className={classes.input} >
      <Card className={classes.modal}>
        <header className={classes.header}>
          <h2>Information</h2>
          <AiOutlineCloseCircle size={35} color='white'
                        onClick={props.onClose}
                    />
        </header>
        {/* <h4>{props.stat}</h4> */}
        {/* <Form
            onSubmit={submitHandler}
          > <div className={classes.content}>


              
              <Form.Control
                type='text'
                defaultValue={sApp}
                placeholder='app Name'
                ref={appRef}
              /><br/>
              
              <Form.Control
                type='file'
                onChange={ handleChange}
                placeholder='Image'
                ref={imageRef}
              /><br />
              <img className={classes.image}  src={image.file} alt='previw' />

                <MultiSelectComponent id="checkbox" className='dropdown '
                dataSource={countries}
                // fields={fields}
                fields={fields}
              // value={fields.value}
                ref={countryNameRef}
                placeholder="Select Countries"
                mode="CheckBox"
                selectAllText="Select All"
                unSelectAllText="unSelect All"
                tagging={onTagging}
                value={colorValues}
                showSelectAll={true}>
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent>
             
            </div> */}



        
        <table className="table table-striped">
          <tr>

            <th>TicketNumber:</th>
            <th>{ticketNumber}</th>
          </tr>
          <tr> <td>UserName:</td>
            <td>{userName}</td>
          </tr>
          <tr>
            <td>UserId:</td>
            <td>{userId}</td>
          </tr>
          <tr>

            <th> Name: </th>
            <th>{name} </th>
          </tr>
          <tr> <td>phoneNumber:</td>
            <td>{phoneNumber}</td>
          </tr>
          <tr>
            <td>email: </td>
            <td>{email}</td>
          </tr><tr>

            <th>ProductName: </th>
            <th>{productN}</th>
          </tr>
          <tr> <td>ProductImage: </td>
            <td><img className={classes.image} src={productI} alt='productImage' /></td>

          </tr>
          <tr>
            <td>PersonType: </td>
            <td>{personTypeP}</td>
</tr>
<tr>            <th>b2bRequestId:</th>
            <th> {b2bRequestId}</th>
          </tr>
          <tr> <td>bannerImage: </td>
            <td><img className={classes.image} src={bannerImage} alt='display' /></td>
          </tr>
          <tr> <td>subBannerImage: </td>
            <td><img className={classes.image} src={subBanner} alt='display' /></td>
          </tr>
          <tr>
            <td>multiImages:</td>
            <td>{multiImages.map((n) => <img className={classes.image} src={n} alt='disply' />)}</td>
          </tr>
          <tr>
            <td>uploadImages:</td>
            <td>{uploadImages.map((n) => 
          <li> <img className={classes.image} src={n} alt='disply' />{n}</li>
          
            // <img className={classes.image} src={n} alt='disply' />
            
            )}</td>
          </tr><tr>
            <th>description:</th>
            <th>{description}</th>
          </tr>
          <tr> <td>couponCode:</td>
            <td>{couponCode}</td>
          </tr>
          <tr>
            <td>offer:</td>
            <td>{offer}</td>
          </tr>

          <tr>   <td>requestDate:</td><td> {requestDate}</td></tr>
         <tr>   <th>expiryDate:</th>
            <th>{expiryDate}</th>
          </tr>
          <tr> <td>status:</td>
            <td>{status}</td>
          </tr>
          <tr>
            <td>info:</td>
            <td> {info}</td>
          </tr>
 <tr><td>country: </td><td> {country}</td></tr>
          <tr><td>allCountry: </td><td>{allCountry}</td></tr>
          <tr><td>locations: </td><td>  {locations}</td></tr>
            {socialMediaList.map((n) => 
          <tr><td>{n.app}</td><td>{n.value}</td>
          </tr>
           )}

             {webList.map((n) => 
          <tr><td>{n.app}</td><td>{n.value}</td>
          </tr>
           )}
           

          {/* <tr>   <td>socialMediaList:</td>
          <td> {socialMediaList.map((n) =>
          <p> {n.app} :  {n.value}</p>
           )}
           </td></tr> */}
          {/* <td>{multiImages.map((n) => <img className={classes.image} src={n} alt='disply' />)}</td> */}
         
          {/* <tr><td>webList: </td><td>{webList}</td></tr> */}
          <tr><td>paidAmount: </td><td>{paidAmountP}</td></tr>
          <tr>   <td>currency:</td><td> {currencyP}</td></tr>
          <tr><td>validity: </td><td>{validityP}</td></tr>
          <tr><td>validityType</td><td>  {validityTypeP}</td></tr>
          <tr>   <td>transactionId:</td><td> {transactionIdP}</td></tr>
          {/* <tr><td>allCountry: </td><td>{allCountry}</td></tr> */}

        </table>
        <footer className={classes.actions}>
          <Button className={classes.btn}
            onClick={props.onClose}
          >cancel</Button>

        </footer>

        {/* </Form> */}
      </Card>
    </div>


  );

}
export default Display;