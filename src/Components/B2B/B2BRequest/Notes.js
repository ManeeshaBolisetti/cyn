import { useState, useRef } from 'react';
import { Button } from 'react-bootstrap';
import Card from './Card/Card';
import classes from './Display.Module.css';
import api from './../../Api';
import Alert from '@material-ui/lab/Alert';
import { Form } from 'react-bootstrap';

// import { ListItem } from 'semantic-ui-react';
const Notes = (props) => {
  const [flash, setFlash] = useState(null);
  const [status,setStatus]=useState('');
  const [color,setColor]=useState();
  const noteRef = useRef();
  let value = props.value
  const styles = {
    alert: {
      zIndex: '1500',
    },
  }
  console.log(value)
const user = value.length === 1 ? value[0].userRef : '';
const notesdata=value.length === 1 ? value[0].supportNotes : '';
  const b2bId =value.length === 1 ? value[0].b2bRequestId : '';
  const [noteData,setNoteData]=useState(notesdata);
  console.log(notesdata);

  // let updatedAt,createdAt,responseNote;
  

   
  
  // console.log(Object.keys(notesdata))
  const submitHandler = (event) => {
    event.preventDefault();
    setFlash(true);
    setTimeout(() => {
      setFlash(null);
    }, 5000);
    const notes=noteRef.current.value;
    const Data = {
      userId: user.userId,
      b2bRequestId: b2bId ,
      responseNote: notes,
      

    };
    api.addNotes(Data)
    .then(response => {
        let resp=response.data
        setStatus(resp.message );
        
          setNoteData(prevExp => {
            return [resp.data, ...prevExp];
        });
       
        setColor('success')
        setTimeout(() => {
          setColor('')
          setStatus('')
       }, 3000);
       noteRef.current.value = '';

    console.log(resp.message)
   
     } )
    .catch(error => {
        if (error.response) {
          setStatus(error.response.data.message );
          setColor('error');
        }else{
          setStatus( error.message );
          setColor('error');
        }
       
    });
 
};

  const userName = user.userName;

        return (
          <div>
            
            <div className={classes.input}>
              <Card className={classes.modal}>
                <header className={classes.header}>
                  <h2>Notes</h2>
                </header>
                {
      flash
      ? <Alert style={styles.alert} severity={color}>{status}</Alert>
      : null
    }
    
                {/* <button onClick={RemoveImage}>Remove</button> */}
                <Form
                  onSubmit={submitHandler}
                > <div className={classes.content}>
                    {userName}

                    <Form.Control as="textarea"
                      rows={3}
                      placeholder='Enter User Feed back'
                      ref={noteRef}
                    />

                  </div>

                  <footer className={classes.actions}>
                    <Button className={classes.btn}
                      onClick={props.onClose}
                    >cancel</Button>
                    <Button
                      type='submit'
                    >Update</Button>
                  </footer></Form>
               {/* {updatedAt},{createdAt},{responseNote}
                   */}
              {
                noteData.map((n)=>{
                  return <div className={classes.notes}>
                    {n.responseNote}<br/>
                    {n.createdAt}
                    </div>
                })
              }
              </Card>
            </div>

          </div>

        );

      }
export default Notes;