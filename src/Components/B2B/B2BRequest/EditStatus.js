import {React, useState} from 'react';
import EditStatusInput from './EditStatusInput';
import { Form } from 'react-bootstrap';
import { TiFolderAdd } from 'react-icons/ti';
// import axios from 'axios';
import api from './../../Api';
// import axios from './../AxiosP';
const EditStatus=(props)=>{
    let statu='';
    let value=props.value
    const [showForm, setshowForm] = useState(false);
    const [status,setStatus]=useState(statu);
    const [color,setColor]=useState('');
    
    
    const saveStatusHandler = (enterdStatusData) => {
        const StatusData = {
            ...enterdStatusData,
            
            // id: Math.random().toString()
        };
        console.log(StatusData)
        // axios.post('admin/updateDistance', DistanceData)
        api.changeStatus(StatusData)
        .then(response => {
            setStatus({ data:response.data.message });
        console.log(response.data.message)
      
        props.onUpdateStatus(response.data.data);
        setColor('success')
        // setFlash(true);
        setTimeout(() => {
            setColor('')
            setStatus('')
          setshowForm(false)
          // setFlashE(null);
        }, 3000);
        // window.location.reload();
         } )
        .catch(error => {
            if (error.response) {
                setStatus({ data:(error.response.data.message )});
                setColor('error')
                // console.log(error.response.data.message);
            }else{
                setStatus({ data: error.message });
                console.error('There was an error!', error);
                setColor('error')

            }
           
        });
       
        console.log(StatusData)
        // props.onUpdateDistance(StatusData);

        // setshowForm(false);
    };
    const onShowForm = () => {
        
        if(value.length===1  ){
            // setshowForm(true)
            if(value[0].status === 'Pending'){
                setshowForm(true);
            }
            else{
                alert('status is not Pending');
                setshowForm(false);
            }
    }else{
        alert('please select Pending Row')
    }
    }
   
    const onHideForm = () => {
        setshowForm(false);
        // setStatus("update A Gender");
    }
    return(
        <div>
           {showForm &&
             <EditStatusInput
            name='Add Amount Details'
            // cName='Country Name'
            
            stat={status.data}
            color={color}
            onClose={onHideForm}
            onSaveStatusData={saveStatusHandler}
            val={value}
            // onCancel={stopEditHandler}
             />}
            
                {/* <h4 >Admin/Add Merchant</h4> */}

                <Form><div onClick={onShowForm}><TiFolderAdd fontSize='20' color='green' />
                 ChangeStatus </div></Form>
 
        </div>
    );
}
export default EditStatus;