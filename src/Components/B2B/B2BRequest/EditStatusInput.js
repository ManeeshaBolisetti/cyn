import { useRef,useState } from 'react';
import { Form, Button } from 'react-bootstrap';
// import { form } from 'react-dom-factories';
import Card from '../../Card/Card';
import classes from './Display.Module.css';
import Alert from '@material-ui/lab/Alert';
const StatusInput = (props) => {
  const [flash, setFlash] = useState(null);
  const color=props.color;
  const styles = {
    alert: {
      zIndex: '1500',
    },
  }
    let value=props.val

  const amountRef       = useRef();
  const currencyRef     = useRef();
  const validityRef     = useRef();
  const validityTypeRef =useRef();
  console.log(value);
  let b2bRequestId=value.length === 1 ? value[0].b2bRequestId: '';
  let user=value.length === 1 ? value[0].userRef: '';
  console.log(user)
let userId=user.userId
   const submitHandler = (event) => {
    setFlash(true);
    setTimeout(() => {
      setFlash(null);
      
    }, 5000);
    event.preventDefault();     
   const amount = amountRef.current.value;
    const currency = currencyRef.current.value;
    const validity = validityRef.current.value;
   const validityType=validityTypeRef.current.value;
    

    const statusData = {
        b2bRequestId:b2bRequestId,
        userId:userId,
        amount:amount,
        currency: currency,
        validity: validity,
       
        validityType:validityType,
    };
    console.log(statusData);
    props.onSaveStatusData(statusData);
   

  }
  return (
    <div className={classes.input}>
      <Card className={classes.modal}>
        <header className={classes.header}>
          <h2>{props.name}</h2>
        </header>
        {
        flash
        ? <Alert style={styles.alert} severity={color}>{props.stat}</Alert>
        : null
      }
        <Form 
        onSubmit={submitHandler}
         > <div className={classes.content}>

          <Form.Control
            type='text'
            placeholder='Amount'
            ref={amountRef}
          />
          <br />
          <Form.Control
            type='text'
            placeholder='currency'
            ref={currencyRef}
          // onChange={cNameChangeHandler}
          />
          <br />
          <Form.Control
            type='text'
            placeholder='validity'
             ref={validityRef}
          />
         <br />
          
         <Form.Control as="select"  ref={validityTypeRef} >
            <option value='day'>day</option>
            <option value='days'>days</option>
            <option value='Month'>Month</option>
            <option value='Months'>Months</option>
            <option value='Year'>Year</option>
            <option value='Years'>Years</option>
          </Form.Control>
        </div>
          <footer className={classes.actions}>
            <Button className={classes.btn}
              onClick={props.onClose}
            >cancel</Button>
            <Button
              type='submit'
            >Update</Button>
          </footer></Form>
      </Card>
    </div>
  );
}
export default StatusInput;