import { React, useState ,useEffect} from 'react';
import AddBloodGroup from './AddBloodGroup';
import EditBloodGroup from './EditBloodGroup';
import { AgGridReact } from 'ag-grid-react';
import * as moment from 'moment';
// import axios from 'axios';
import api from './../../Api';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
// import customToolTip from '../../customTooltip';
// import { FcDeleteDatabase, FcEditImage, FcPrint } from "react-icons/fc";
// import { BsFillStopFill } from "react-icons/bs";
import { RiFolderDownloadLine } from "react-icons/ri";
// import { print } from 'react-to-print';
import { Button, Row, Col } from 'react-bootstrap';
import Search from './../../Country/Search';
// import UseFetch from '../../UseFetch';
import Refresh from '../../Actions/Refresh';
var filterParams = {
  comparator: function (filterLocalDateAtMidnight, cellValue) {
    var dateAsString = cellValue;
    if (dateAsString == null) return -1;
    var dateParts = dateAsString.split('/');
    var cellDate = new Date(
      Number(dateParts[2]),
      Number(dateParts[1]) - 1,
      Number(dateParts[0])
    );

    if (filterLocalDateAtMidnight.getTime() === cellDate.getTime()) {
      return 0;
    }

    if (cellDate < filterLocalDateAtMidnight) {
      return -1;
    }

    if (cellDate > filterLocalDateAtMidnight) {
      return 1;
    }
  },
  browserDatePicker: true,
  minValidYear: 2000,
};

const BloodG = () => {

  let bloodGroup = [];
  // const rowdata = []
  // let blood = UseFetch('/emergencyAdmin/bloodGroupsList');
  const [blood,setBlood]=useState([]);
  const [bloodL, setBloodL] = useState([]);
  const [gridApi, setGridApi] = useState(null);
  // const [girdColumnApi, setGridColumnApi] = useState(null);
  // const [hideColumn, setHideColumn] = useState(true);
  // const [suppressClickEdit, setSuppressClickEdit] = useState(true);
  // const [edit, setEdit] = useState('Edit');
  // const date=blood.map((dat)=>new Date(dat.date).toLocaleDateString());
  // console.log(blood);
const bloodList=[];
  useEffect(() => {

   api.getBlood()
      .then(response => {
        let country = response.data.data;
        setBlood(country);
      });

  }, []);
  console.log(blood)
  if (blood!= null && blood.length >0){
  blood.map(n => {
    // let bloodGropN = n.bloodGroup
    let bDate = moment(n.date).format('DD/MM/YYYY');
    
    // let bId = n.bloodGroup
     bloodList.push({
      'bloodGroup': n.bloodGroup,
      'bloodGroupId': n.bloodGroupId,
      'date': bDate,
      // 'bloodGroupId': bId,
    })
    return blood.List;
  })
  }
  // blood.push({'formatedDate':date},)
  console.log(bloodGroup)
  const columnDefs = [


    {
      field: "bloodGroup", tooltipField: 'bloodGroup', filter: 'agSetColumnFilter',
      
    },
    {
      field: 'date',
      filter: 'agDateColumnFilter',
      filterParams: filterParams,

    },


  ]

  const defaultColDef = {
    sortable: true,
    
    flex: 1,
    //    filter: true,
    minWidth: 200,
    resizable: true,
    floatingFilter: true,
    //  floatingFilter: true
  }
  const onGridReady = (params) => {
    // console.log('grid ready');
    setGridApi(params);
    // setGridColumnApi(params.columnApi);
  };
  const AddBloodHandler = bloodGroup => {
    // console.log(blood);
    setBlood(prevExp => {
      return [bloodGroup, ...prevExp];
    });

  };
  const EditBloodGroupHandler = bloodGroup => {
    // console.log(bloodGroup);
    setBlood(prevExp => {
      return [bloodGroup, ...prevExp];
    });

  };
  const pageSize = (pageSize) => {
    console.log(pageSize);
    gridApi.api.paginationSetPageSize(Number(pageSize))
  }

  // const showEdit = () => {
  //   girdColumnApi.setColumnVisible("action", false)
  //   setSuppressClickEdit(false);
  //   setEdit('EditMode')
  // }
  // const stopEdit = () => {
  //   setSuppressClickEdit(true);
  //   setEdit('Edit')
  // }
  // const onRemoveSelected = () => {
  //   var selectedData = gridApi.api.getSelectedRows();
  //   var res = gridApi.api.applyTransaction({ remove: selectedData });
  //   printResult(res);
  // };
  const onCellValueChanged = (event) => {
    console.log(
      'onCellValueChanged: ' + event.colDef.field + ' = ' + event.newValue
    );
  };

  const onRowValueChanged = (event) => {
    var data = event.data;
    console.log(
      'onRowValueChanged: (' +
      data.countryName +
      ', ' +
      data.countryCode +
      ', ' +
      data.currency +
      ')'
    );
  };
  const onExportClick = () => {
    gridApi.api.exportDataAsCsv();
  }
  // const onBtPrint = () => {
  //   const api = gridApi.api;
  //   setPrinterFriendly(api);
  //   setTimeout(function () {
  //     print();
  //     setNormal(api);
  //   }, 2000);
  // };
  const onFilterTextChange = (e) => {

    gridApi.api.setQuickFilter(e.target.value);
  }
  const onSelectionChanged = () => {
    var selectedRows = gridApi.api.getSelectedRows();
    console.log(selectedRows);
    setBloodL(selectedRows);

    // document.querySelector('#selectedRows').innerHTML =
    //   selectedRows.length === 1 ? selectedRows[0].countryCode : '';
  };

  return (
    <div>
      <div className='d-flex justify-content-between'><h2>BloodGroup</h2>
        <Search onChange={onFilterTextChange} /></div>
      <Row>
        <Col xs={10} style={{ display: '-webkit-box' }}>
        <Button className='add' ><AddBloodGroup
            onAddBlood={AddBloodHandler}
          /></Button>
          <Button className='add' ><EditBloodGroup value={bloodL} onUpdateBloodGroup={EditBloodGroupHandler}></EditBloodGroup>
</Button>
          {/* <Button className='add' onClick={() => onRemoveSelected()}>Remove <FcDeleteDatabase /></Button > */}
          {/* <Button className='add' onClick={showEdit}>{edit} <FcEditImage /></Button > */}
          {/* <Button className='add' onClick={stopEdit}>stop <BsFillStopFill color='darkred' /></Button > */}
          <Button className='add' onClick={() => onExportClick()}>
            Export to Excel  <RiFolderDownloadLine color='green' />
          </Button ><Button className='add' ><Refresh /></Button>
          {/* <Button className='add' onClick={() => onBtPrint()}>Print <FcPrint /></Button > */}
          </Col>

        <Col xs={2}>

        </Col>

      </Row>
      <div id="myGrid" className="ag-theme-alpine" style={{ height: '400px' }}>
        <AgGridReact

          // onCellClicked={onCellClicked}
          editType="fullRow"
          rowSelection={'multiple'}
          onCellValueChanged={onCellValueChanged}
          onRowValueChanged={onRowValueChanged}
          onSelectionChanged={onSelectionChanged}
          // suppressClickEdit={suppressClickEdit}
          pagination={true}
          paginationPageSize={25}
          // paginationAutoPageSize={true}
          columnDefs={columnDefs}
          rowData={bloodList}
          defaultColDef={defaultColDef}
          onGridReady={onGridReady}>



        </AgGridReact>
        <div className='selectbox' style={{
          padding: '10px',
          marginTop: '-10px', fontSize: 'small',
        }}> Page Size: <select onChange={(e) => pageSize(e.target.value)}>
            <option value='10'>10</option>
            <option value='25'>25</option>
            <option value='50'>50</option>
            <option value='100'>100</option>
          </select></div>
      </div>
    </div>

  );
}
export default BloodG;




// function printResult(res) {
//   console.log('---------------------------------------');
//   if (res.add) {
//     res.add.forEach(function (rowNode) {
//       console.log('Added Row Node', rowNode);
//     });
//   }
//   if (res.remove) {
//     res.remove.forEach(function (rowNode) {
//       console.log('Removed Row Node', rowNode);
//     });
//   }
//   if (res.update) {
//     res.update.forEach(function (rowNode) {
//       console.log('Updated Row Node', rowNode);
//     });
//   }
// }
// function setPrinterFriendly(api) {
//   // const eGridDiv = document.querySelector('#myGrid');
//   api.setDomLayout('print');
// }
// function setNormal(api) {
//   const eGridDiv = document.querySelector('#myGrid');
//   eGridDiv.style.width = '700px';
//   eGridDiv.style.height = '200px';
//   api.setDomLayout(null);
// }

