import { React, useState } from 'react';
import EditBloodInput from './EditBloodInput';
import { Form } from 'react-bootstrap';
import { TiFolderAdd } from 'react-icons/ti';
// import axios from 'axios';
import api from './../../Api';
// import api from './../AxiosP';
const EditBloodGroup = (props) => {
    let statu = '';
    let value = props.value
    const [showForm, setshowForm] = useState(false);
    const [status, setStatus] = useState(statu);
    const [color,setColor]=useState('');
    // let NewDate = new Date()
    // console.log(props.value.length);

    const saveBloodDataHandler = (enterdBloodData) => {
        const BloodData = {
            ...enterdBloodData,
            // date: NewDate
            // id: Math.random().toString()
        };
        console.log(BloodData)
        // axios.post('emergencyAdmin/updateBloodGroups', BloodData)
        api.updateBlood(BloodData)
        .then(response => {
            setStatus({ data:response.data.message });
        console.log(response.data.message)
        
        props.onUpdateBloodGroup(response.data.data);
        setColor('success')
        // setFlash(true);
        setTimeout(() => {
            setColor('')
            setStatus('')
          setshowForm(false)
          // setFlashE(null);
        }, 3000);
        // window.location.reload();
         } )
        .catch(error => {
            if (error.response) {
                setStatus({ data:(error.response.data.message )});
                setColor('error')
                // console.log(error.response.data.message);
            }else{
                setStatus({ data: error.message });
                setColor('error')
                console.error('There was an error!', error);

            }

        });

        // fetch('https://appdev.connectyourneed.com/emergencyAdmin/addBloodGroups', {

        //     method: "POST",
        //     headers: { 'Content-Type': 'application/json' },
        //     body: JSON.stringify(BloodData)
        // }).then((response) => {
        //     if (!response.ok) {
        //         if (response.status === 500) {
        //             setStatus('Alredy existed');
        //         } else
        //             setStatus(response.message)

        //         //    setStatus(response.status) ;
        //     }
        //     else return response.json();
        // })
        //     .then((data) => {
        //         if (data) {
        //             setStatus(data.message);
        //             console.log(data);
        //         }
        //     })

        props.onUpdateBloodGroup(BloodData);

    };
    const onShowForm = () => {
        if (props.value.length === 1) {
            setshowForm(true);
        } else {
            alert('please select edit Row')
        }
    }
    const onHideForm = () => {
        setshowForm(false);
        setStatus("Add A BloodGroup");
    }
    return (
        <div>
            {showForm &&
                <EditBloodInput
                    name='Add BloodGroup'
                    bName='BloodGroup Name'
                    stat={status.data}
                    color={color}
                    onClose={onHideForm}
                    onSaveBloodData={saveBloodDataHandler}
                    val={value}
                // onCancel={stopEditHandler}
                />}

            {/* <h4 >Admin/Add Merchant</h4> */}

            <Form><div onClick={onShowForm}><TiFolderAdd fontSize='20' color='green' />Update </div></Form>

        </div>
    );
}
export default EditBloodGroup;