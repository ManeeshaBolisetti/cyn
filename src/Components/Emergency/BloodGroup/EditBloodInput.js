import { useState, useRef } from 'react';
import { Form, Button } from 'react-bootstrap';
import Card from './../../Card/Card';
import classes from './BloodInput.module.css';
import Alert from '@material-ui/lab/Alert';
const BloodInput = (props) => {
  const [flash, setFlash] = useState(null);
  const color=props.color;
  const styles = {
    alert: {
      zIndex: '1500',
    },
  }
    let value=props.val
    let bBloodGroup=value.length === 1 ? value[0].bloodGroup: '';
    // let cCode=value.length === 1 ? value[0].countryCode: '';
    // let cCurrency=value.length === 1 ? value[0].currency: '';
    let bBloodGroupId=value.length === 1 ? value[0].bloodGroupId: '';
  const Invalid = '';
  const [valid, setValid] = useState(Invalid);
  // const formRef = useRef();
  const bNameRef = useRef();
  // const codeRef = useRef();
  // const currencyRef = useRef();

  const submitHandler = (event) => {
    event.preventDefault();
    setFlash(true);
    setTimeout(() => {
      setFlash(null);
      // setFlashE(null);
    }, 5000);
    const bName = bNameRef.current.value;
    const bId=bBloodGroupId;
    
    if (bName.trim().length === 0 ) {
      setValid('Invalid Blood Group');
      return console.log('Invalid Blood Group');

    }

    const bloodData = {
      bloodGroup: bName,
      bloodGroupId:bId,
      };
    console.log(bloodData);
    props.onSaveBloodData(bloodData);
    // bNameRef.current.value = '';
    
  }
  return (
    <div className={classes.input}>
      <Card className={classes.modal}>
        <header className={classes.header}>
          <h2>{props.name}</h2>
        </header>
        {
        flash
        ? <Alert style={styles.alert} severity={color}>{props.stat}</Alert>
        : null
      }
        <Form onSubmit={submitHandler} > <div className={classes.content}>

          <Form.Control
            type='text'
            defaultValue={bBloodGroup}
            placeholder={props.bName}
            //  value={enterdCountry}
            ref={bNameRef}
          // onChange={cNameChangeHandler}
          />
          <h4>{valid}</h4>
          </div>
          <footer className={classes.actions}>
            <Button className={classes.btn}
              onClick={props.onClose}
            >cancel</Button>
            <Button
              type='submit'
            >Update</Button>
          </footer></Form>
      </Card>
    </div>
  );
}
export default BloodInput;