import { React, useState } from 'react';
import EmergencyTypeInput from './EmergencyTypeInput';
import { Form } from 'react-bootstrap';
import { TiFolderAdd } from 'react-icons/ti';
// import axios from 'axios';
import api from './../../Api';
const AddEmergencyType = (props) => {
    let statu = '';
    const [showForm, setshowForm] = useState(false);
    const [status, setStatus] = useState(statu);
    const [color,setColor]=useState('');
    const onShowForm = () => {
        setshowForm(true);
    }
    const onHideForm = () => {
        setshowForm(false);
    }
    const saveEmergencyTypeDataHandler = (enterdEmergencyTypeData) => {
        const emergencyTypeData = {
            ...enterdEmergencyTypeData,

        };
        // console.log(enterdCatgoryData.catLogo.name);
        // console.log(enterdCatgoryData.categoryImage);

        let formData = new FormData();
        formData.append('emergencyType', enterdEmergencyTypeData.emergencyType);
        formData.append('emergencyTypeIcon', enterdEmergencyTypeData.emergencyTypeIcon);
        formData.append('title', enterdEmergencyTypeData.title);
        formData.append('description', enterdEmergencyTypeData.description);
        // formData.append('bloodGroupIds', enterdEmergencyTypeData.bloodGroupIds);
        // formData.append('countries', enterdEmergencyTypeData.countries);
        for (let i = 0; i < enterdEmergencyTypeData.countries.length; i++) {
            formData.append('countries[]', enterdEmergencyTypeData.countries[i])
        }
        for (let j = 0; j < enterdEmergencyTypeData.bloodGroupIds.length; j++) {
            formData.append('bloodGroupIds[]', enterdEmergencyTypeData.bloodGroupIds[j])
        }

        console.log(enterdEmergencyTypeData.countries);
        console.log(enterdEmergencyTypeData.bloodGroupIds);
        // console.log(formData);
        // let url = '/emergencyAdmin/addEmergencyTypes'
        // axios.post(url, formData )
        api.addEmergency(formData)
        .then(response => {
            setStatus({ data:response.data.message });
        console.log(response.data.data)
        
        props.onAddEmergencyType(response.data.data);

        setColor('success')
        // setFlash(true);
        setTimeout(() => {
            setColor('')
            setStatus('')
          setshowForm(false)
          // setFlashE(null);
        }, 3000);
        // window.location.reload();
         } )
        .catch(error => {
            if (error.response) {
                setStatus({ data:(error.response.data.message )});
                setColor('error')
                // console.log(error.response.data.message);
            }else{
                setStatus({ data: error.message });
                setColor('error')
                console.error('There was an error!', error);

            }
           
        });
       
        // fetch(url, {

        //     method: "POST",
        //     // headers: { 'Content-Type': 'multipart/form-data' },
        //     body: formData
        // }).then((response) => {
       
        //     if (!response.ok) {
        //         if (response.status === 500) {
        //             setStatus('Alredy existed');
        //         } else
        //             setStatus(response.message)
        //             console.log('success')

        //         //    setStatus(response.status) ;
        //     }
        //     else return response.json();
        // })
        //     .then((data) => {
        //         if (data) {
        //             setStatus(data.message);
        //             window.location.reload();
        //             console.log(data);

        //             // props.onAddCountry(data);

        //         }
        //         // console.log(data);
        //         // console.log(data.message);

        //     })

        // console.log(formData)
        // props.onAddEmergencyType(emergencyTypeData);
        console.log(emergencyTypeData);
        // setshowForm(false);
    }
    return (
        <div>
            {showForm && <EmergencyTypeInput
                name='Add EmergencyType'
                eName='EmergencyType'
                stat={status.data}
                color={color}
                onClose={onHideForm}
                onSaveEmergencyTypeData={saveEmergencyTypeDataHandler}
            />}
            <Form><div onClick={onShowForm}><TiFolderAdd fontSize='20' color='green' /> Add Emergency Type </div></Form>
        </div>
    );
}
export default AddEmergencyType;