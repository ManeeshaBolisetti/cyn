import { useState, useRef} from 'react';
import { Form, Button } from 'react-bootstrap';
import Card from '../../Card/Card';
import classes from './EmergenctTypeInput.Module.css';
import Alert from '@material-ui/lab/Alert';
import { CheckBoxSelection, Inject, MultiSelectComponent } from '@syncfusion/ej2-react-dropdowns';
import UseFetch from '../../UseFetch';
const EditEmergencyInput = (props) => {
  const [flash, setFlash] = useState(null);
  const color=props.color;
  const styles = {
    alert: {
      zIndex: '1500',
    },
  }
  const Invalid = '';
  const [valid, setValid] = useState(Invalid);
  let value=props.val
  let eImage=value.length === 1 ? value[0].emergencyTypeIcon: '';
  const image=(eImage);
  // const [uImage,setUImage]=useState()
  
  let eCountryId=value.length === 1 ? value[0].countryId: '';
  // let eCountries=value.length === 1 ? value[0].countries: '';
  let eDescription=value.length === 1 ? value[0].description: '';
  // let ebloodGroup=value.length === 1 ? value[0].bloodGroup: '';
  let ebloodGroupId=value.length === 1 ? value[0].bloodGroupId: '';
  let eTitle=value.length === 1 ? value[0].title: '';
  let eEmergencyTypeId=value.length === 1 ? value[0].emergencyTypeId: '';
  let eEmergencyType=value.length === 1 ? value[0].emergencyType: '';
  
  // const [bloodGroupIds, setBloodGroupIds] = useState([]);
  // const [countries, setCountries] = useState([]);
  const emergencyTypeRef = useRef();
  const emergencyTypeIconRef = useRef();
  const titleRef = useRef();
  const descriptionRef = useRef();
  const bloodGroupIdsRef = useRef();
  const countriesRef = useRef();
  let countries = UseFetch('/admin/countriesList');
  let bloodGroupIds = UseFetch('/emergencyAdmin/bloodGroupsList');
  // useEffect(() => {

  //   axios.get('https://appdev.connectyourneed.com/admin/countriesList')
  //     .then(response => {
  //       const country = response.data.data;
  //       // console.log(country);

  //       setCountries(country)
  //     });

  //   // empty dependency array means this effect will only run once (like componentDidMount in classes)
  // }, []);
  const fields = { text: 'countryName', value: 'countryId' };
  let colorValues= eCountryId.map((n)=>{
    let colour=n;
    return colour
});
console.log(colorValues)
const onTaggingB = (e) => {
  // set the current selected item text as class to chip element.
  e.setClass(e.itemData[fields.text].toLowerCase());
};
  // const optionItems = bloodGroupIds.map((bloodGroupId) => <option value={bloodGroupId.bloodGroupId}>{bloodGroupId.bloodGroup}</option>)
  const field = { text: 'bloodGroup', value: 'bloodGroupId' }
  let colorValue= ebloodGroupId.map((n)=>{
    let colour=n;
    return colour
});
console.log(colorValue)
const onTagging = (e) => {
  // set the current selected item text as class to chip element.
  e.setClass(e.itemData[field.text].toLowerCase());
};
const RemoveImage=()=>{
  const RemoveImage = {
    imageUrl: eImage,
  };
  setFlash(true);
  setTimeout(() => {
    setFlash(null);
    // setFlashE(null);
  }, 5000);
  props.onRemoveImage(RemoveImage);
}
  const submitHandler = (event) => {
    event.preventDefault();
    setFlash(true);
    setTimeout(() => {
      setFlash(null);
      // setFlashE(null);
    }, 5000);

    const emergencyType = emergencyTypeRef.current.value;
    // const emergencyTypeIcon = eImage;
    const title = titleRef.current.value;
    const description = descriptionRef.current.value;
    const Countries = countriesRef.current.value;
    const bloodGroupIds = bloodGroupIdsRef.current.value;
    const images=emergencyTypeIconRef.current.files[0];
  //   console.log(bloodGroupIds);
  // bloodGroupIds.map((blood)=>{
  //     const b=parseInt(blood)
  //     console.log(b)
  //   })

    // if (emergencyType.trim().length === 0 || title.trim().length === 0 || description.trim().length === 0) {
    //   setValid('Invalid Country or code or currency');
    //   return console.log('Invalid Country and code and currency');

    // }
    const emergencyTypeData = {
      emergencyTypeId:eEmergencyTypeId,
      bloodGroupIds: bloodGroupIds,
      emergencyType: emergencyType,
      emergencyTypeIcon: eImage,
      title: title,
      description: description,
      // emergencyTypeId:eEmergencyTypeId,
      countries: Countries,
      countryId:eCountryId,
      image:images,



    };
    console.log(emergencyTypeData);
    props.onSaveEmergencyTypeData(emergencyTypeData);
    setValid('');
  }
  const handleChange=(event)=> {
    
    // setImage( URL.createObjectURL(event.target.files[0])
  //  )
   
 }

  return (
    <div>
      <div className={classes.input}>
        <Card className={classes.modal}>
          <header className={classes.header}>
            <h2>{props.name}</h2>
          </header>
          {
        flash
        ? <Alert style={styles.alert} severity={color}>{props.stat}</Alert>
        : null
      }
      <button onClick={RemoveImage}>RemoveImage</button>
          <Form
            onSubmit={submitHandler}
          > <div className={classes.content}>


              {/* <Form.Group controlId="exampleForm.ControlSelect1"> */}

              {/* <Form.Control as="select" ref={bloodGroupIdsRef}>
                  {optionItems}
                </Form.Control></Form.Group> */}
              <MultiSelectComponent id="checkbox" className='dropdown '
                dataSource={bloodGroupIds}
                // fields={fields}
                fields={field}
                ref={bloodGroupIdsRef}
                placeholder="Select bllodgroup"
                mode="CheckBox"
                selectAllText="Select All"
                unSelectAllText="unSelect All"
                tagging={onTaggingB}
                value={colorValue}
                showSelectAll={true}>
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent><br />

              <Form.Control
                type='text'
                defaultValue={eEmergencyType}
                placeholder='emergencyType'
                ref={emergencyTypeRef}
              />
               <Form.Group style={{display:'flex'}}>
              <Form.Control
                type='file'
                onChange={ handleChange}
                // placeholder={props.eName}
                ref={emergencyTypeIconRef}
              />
              <img className={classes.image}  src={image}
       
       alt='previw' /></Form.Group>

              <Form.Control
                type='text'
                defaultValue={eTitle}
                placeholder='title'
                ref={titleRef}
              />
              <Form.Control
                type='text'
                defaultValue={eDescription}
                placeholder='description'
                ref={descriptionRef}
              />


              <MultiSelectComponent id="checkbox" className='dropdown '
                dataSource={countries}
                // fields={fields}
                fields={fields}
                ref={countriesRef}
                placeholder="Select Countries"
                mode="CheckBox"
                selectAllText="Select All"
                unSelectAllText="unSelect All"
                tagging={onTagging}
                value={colorValues}
                showSelectAll={true}>
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent><br />
            </div>
            <h4>{valid}</h4>
            <footer className={classes.actions}>
              <Button className={classes.btn}
                onClick={props.onClose}
              >cancel</Button>
              <Button
                type='submit'
              >Update</Button>
            </footer></Form>
        </Card>
      </div>

    </div>
  );

}
export default EditEmergencyInput;