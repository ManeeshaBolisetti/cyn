import { React, useState } from 'react';
import EditEmergencyInput from './EditEmergencyInput';
import { Form } from 'react-bootstrap';
import { TiFolderAdd } from 'react-icons/ti';
// import axios from 'axios';
import api from './../../Api';
const EditEmergencyType = (props) => {
    let statu = '';
    let value=props.value
    const [showForm, setshowForm] = useState(false);
    const [status, setStatus] = useState(statu);
    const [color,setColor]=useState('');

    const onShowForm = () => {
        if(props.value.length===1){
        setshowForm(true);
    }else{
        alert('please select edit Row')
    }
    }
    const onHideForm = () => {
        setshowForm(false);
    }
    const RemoveImage=(image)=>{
        const RemoveData={
            ...image
        }
        // let url = 'admin/removeBlob'
        // axios.post(url, RemoveData )
        api.RemoveBlob(RemoveData)
        .then(response => {
            setStatus({ data:response.data.message });
        console.log(response.data.message)
        setColor('success')
        // props.onUpdateProduct({data:response.data.data});
        setTimeout(() => {
            setColor('')
            setStatus('')
        //   setshowForm(false)
          // setFlashE(null);
        }, 3000);
        
        // window.location.reload();
         } )
        .catch(error => {
            if (error.response) {
                setStatus({ data:(error.response.data.message )});
                setColor('error')
                console.log(error.response);
            }else{
                setStatus({ data: error.message });
                setColor('error')
                console.error('There was an error!', error);

            }
           
        });
       
    }
    const saveEmergencyTypeDataHandler = (enterdEmergencyTypeData) => {
        // const emergencyTypeData = {
        //     ...enterdEmergencyTypeData,

        // };
        // // console.log(enterdCatgoryData.catLogo.name);
        // console.log(enterdCatgoryData.categoryImage);

        let formData = new FormData();
        formData.append('emergencyTypeId', enterdEmergencyTypeData.emergencyTypeId);
        formData.append('emergencyType', enterdEmergencyTypeData.emergencyType);
        formData.append('emergencyTypeIcon', enterdEmergencyTypeData.emergencyTypeIcon);
        formData.append('title', enterdEmergencyTypeData.title);
        formData.append('description', enterdEmergencyTypeData.description);
        formData.append('image', enterdEmergencyTypeData.image)
        // formData.append('bloodGroupIds', enterdEmergencyTypeData.bloodGroupIds);
        // formData.append('countries', enterdEmergencyTypeData.countries);
        for (let i = 0; i < enterdEmergencyTypeData.countries.length; i++) {
            formData.append('countries[]', enterdEmergencyTypeData.countries[i])
        }
        for (let j = 0; j < enterdEmergencyTypeData.bloodGroupIds.length; j++) {
            formData.append('bloodGroupIds[]', enterdEmergencyTypeData.bloodGroupIds[j])
        }

        console.log(enterdEmergencyTypeData.countries);
        console.log(enterdEmergencyTypeData.bloodGroupIds);
        // console.log(formData);
        // let url = 'emergencyAdmin/updateEmergencyTypes'
        // axios.post(url, formData )
        api.updateEmergency(formData)
        .then(response => {
            setStatus({ data:response.data.message });
        console.log(response.data.message)
        props.onupdateEmergencyType(response.data.data);
        setColor('success')
        // setFlash(true);
        setTimeout(() => {
            setColor('')
            setStatus('')
          setshowForm(false)
          // setFlashE(null);
        }, 3000);
         } )
        .catch(error => {
            if (error.response) {
                setStatus({ data:(error.response.data.message )});
                setColor('error')
                // console.log(error.response.data.message);
            }else{
                setStatus({ data: error.message });
                setColor('error')
                console.error('There was an error!', error);

            }
           
        });
       
        // fetch(url, {

        //     method: "POST",
        //     // headers: { 'Content-Type': 'multipart/form-data' },
        //     body: formData
        // }).then((response) => {
       
        //     if (!response.ok) {
        //         if (response.status === 500) {
        //             setStatus('Alredy existed');
        //         } else
        //             setStatus(response.message)
        //             console.log('success')

        //         //    setStatus(response.status) ;
        //     }
        //     else return response.json();
        // })
        //     .then((data) => {
        //         if (data) {
        //             setStatus(data.message);
        //             window.location.reload();
        //             console.log(data);

        //             // props.onAddCountry(data);

        //         }
        //         // console.log(data);
        //         // console.log(data.message);

        //     })

        // console.log(formData)
        // console.log(emergencyTypeData);
        // setshowForm(false);
    }
    return (
        <div>
            {showForm && <EditEmergencyInput
                name='Update EmergencyType'
                eName='EmergencyType'
                stat={status.data}
                color={color}
                val={value}
                onClose={onHideForm}
                onRemoveImage={RemoveImage}
                onSaveEmergencyTypeData={saveEmergencyTypeDataHandler}
            />}
            <Form><div onClick={onShowForm}><TiFolderAdd fontSize='20' color='green' /> Update </div></Form>
        </div>
    );
}
export default EditEmergencyType;