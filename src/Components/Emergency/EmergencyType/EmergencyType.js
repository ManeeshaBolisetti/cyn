import { React, useState,useEffect} from 'react';
import { AgGridReact } from 'ag-grid-react';
// import axios from './../../AxiosP';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import AddEmergencyType from './AddEmergencyType';
import EditEmergencyType from './EditEmergencyType';
import customToolTip from '../../customTooltip';
// import { FcDeleteDatabase, FcEditImage, FcPrint } from "react-icons/fc";
// import { BsFillStopFill } from "react-icons/bs";
import { RiFolderDownloadLine } from "react-icons/ri";
// import { print } from 'react-to-print';
// import axios from 'axios';
import api from './../../Api';
import { Button, Row, Col } from 'react-bootstrap';
import Search from './../../Country/Search';
import Refresh from '../../Actions/Refresh';
// import UseFetch from '../../UseFetch';
const EmergencyType = () => {

    const [emergencyTypeL, setEmergencyTypeL] = useState([]);
    const [gridApi, setGridApi] = useState(null);
    // const [girdColumnApi, setGridColumnApi] = useState(null);
    // const [suppressClickEdit, setSuppressClickEdit] = useState(true);
    // const [edit, setEdit] = useState('Edit');
    const [emergencyType,setEmergencyType]=useState([]);
    // let emergencyType = UseFetch('/emergencyAdmin/emergencyTypesList');
    console.log(emergencyType)
    useEffect(() => {

       api.getEmergency()
          .then(response => {
            let country = response.data.data;
            setEmergencyType(country);
          });
    
      }, []);
    let emergencyTypeList = [];
    if (emergencyType!= null && emergencyType.length >0){
    emergencyType.map((n) => {
        let bloodG = n.bloodGroupsRef.map((c) => {
            let Group = c.bloodGroup
            return Group;

        })
        let bloodGId = n.bloodGroupsRef.map((c) => {
            let Group = c.bloodGroupId
            return Group;

        })
        let country = n.countries.map((c) => {


            let count = c.countryName
            return count;
        })
        let countryId = n.countries.map((c) => {


            let count = c.countryId
            return count;
        })


       return emergencyTypeList.push({
            'bloodGroupsRef': bloodG,
            'bloodGroupId':bloodGId,
            'countries': country,
            'countryId':countryId,
            'emergencyType': n.emergencyType,
            'title': n.title,
            'emergencyTypeIcon': n.emergencyTypeIcon,
            'description': n.description,
            'emergencyTypeId':n.emergencyTypeId,

        })



    })}

    // useEffect(() => {

    //     axios.get('/emergencyAdmin/emergencyTypesList')
    //         .then(response => {
    //             let data = response.data.data;
    //             // let bloodGroups = data.map((n) => n.bloodGroupsRef.map((c)=>c.bloodGroup));
    //             let blood = [];
    //             data.map((n) => {
    //                 let bloodG = n.bloodGroupsRef.map((c) => {
    //                     let Group = c.bloodGroup
    //                     return Group;

    //                 })
    //                 let country = n.countries.map((c) => {
    //                     let count = c.countryName
    //                     return count;
    //                 })
    //                 blood.push({
    //                     'bloodGroupsRef': bloodG,
    //                     'countries': country,
    //                     'emergencyType': n.emergencyType,
    //                     'title': n.title,
    //                     'emergencyTypeIcon': n.emergencyTypeIcon,
    //                     'description': n.description,


    //                 });
    //                 setEmergencyType(blood);
    //             })


    //         });

    // }, []);


    const columnDefs = [
        {
            field: "emergencyType",  filter: 'agSetColumnFilter',
            
        },
        {
            field: "title",
             filter: 'agSetColumnFilter',
            // editable: false,
        },
        {
            field: "emergencyTypeIcon",
             filter: 'agSetColumnFilter',
            cellRenderer: countryCellRenderer,
            // editable: false,
        },
        {
            field: "description",
             filter: 'agSetColumnFilter',
            // editable: false,
        },
       
        {
            headerName: "bloodGroups",
            field: 'bloodGroupsRef',
             filter: 'agSetColumnFilter',
            // editable: false,
        },
        {
            field: "countries",
            
      tooltipField: 'countries',
      tooltipComponentParams: { type: 'success' },
             filter: 'agSetColumnFilter',
            // editable: false,
        },

    ]
    const defaultColDef = {
        sortable: true,
        
        flex: 1,
        //    filter: true,
        minWidth: 200,
        resizable: true,
        floatingFilter: true,
        tooltipComponent: 'customTooltip',
        // floatingFilter: true
    }
    const AddEmergencyTypeHandler = EmergencyType => { 
        setEmergencyType(prevExp => {
            return [EmergencyType, ...prevExp];
        });
    };
    const EditEmergencyTypeHandler = EmergencyType => {
        setEmergencyType(prevExp => {
            return [EmergencyType, ...prevExp];
        });
    };
    const onGridReady = (params) => {
        console.log('grid ready');
        setGridApi(params);
        // setGridColumnApi(params.columnApi);
    };

    const pageSize = (pageSize) => {
        console.log(pageSize);
        gridApi.api.paginationSetPageSize(Number(pageSize))
    }

    // const showEdit = () => {
    //     girdColumnApi.setColumnVisible("action", false)
    //     setSuppressClickEdit(false);
    //     setEdit('EditMode')
    // }
    // const stopEdit = () => {
    //     setSuppressClickEdit(true);
    //     setEdit('Edit')
    // }
    // const onRemoveSelected = () => {
    //     var selectedData = gridApi.api.getSelectedRows();
    //     var res = gridApi.api.applyTransaction({ remove: selectedData });
    //     printResult(res);
    // };
    const onCellValueChanged = (event) => {
        console.log(
            'onCellValueChanged: ' + event.colDef.field + ' = ' + event.newValue
        );
    };

    const onRowValueChanged = (event) => {
        var data = event.data;
        console.log(
            'onRowValueChanged: (' +
            data.countryName +
            ', ' +
            data.countryCode +
            ', ' +
            data.currency +
            ')'
        );
    };
    const onExportClick = () => {
        gridApi.api.exportDataAsCsv();
    }
    // const onBtPrint = () => {
    //     const api = gridApi.api;
    //     setPrinterFriendly(api);
    //     setTimeout(function () {
    //         print();
    //         setNormal(api);
    //     }, 2000);
    // };
    const onFilterTextChange = (e) => {

        gridApi.api.setQuickFilter(e.target.value);
    }
    const onSelectionChanged = () => {
        var selectedRows = gridApi.api.getSelectedRows();
        console.log(selectedRows);
        setEmergencyTypeL(selectedRows);
        
        // document.querySelector('#selectedRows').innerHTML =
        //   selectedRows.length === 1 ? selectedRows[0].countryCode : '';
      };



    return (
        <div>



            <div className='d-flex justify-content-between'><h2>Emergency Types</h2>
                <Search onChange={onFilterTextChange} /></div>
            <Row>
                <Col xs={10} style={{ display: 'flex' }}>
                <Button className='add' > <AddEmergencyType onAddEmergencyType={AddEmergencyTypeHandler} />
               </Button> <Button className='add' > <EditEmergencyType value={emergencyTypeL}  onupdateEmergencyType={EditEmergencyTypeHandler}></EditEmergencyType>
</Button>
                    {/* <Button className='add' onClick={() => onRemoveSelected()}>Remove <FcDeleteDatabase /></Button > */}
                    {/* <Button className='add' onClick={showEdit}>{edit} <FcEditImage /></Button > */}
                    {/* <Button className='add' onClick={stopEdit}>stop <BsFillStopFill color='darkred' /></Button > */}
                    <Button className='add' onClick={() => onExportClick()}>
                        Export to Excel  <RiFolderDownloadLine color='green' />
                    </Button >
                    <Button className='add' ><Refresh /></Button>
                    {/* <Button className='add' onClick={() => onBtPrint()}>Print <FcPrint /></Button > */}
                    </Col>

                <Col xs={2}>

                </Col>

            </Row>
            <div id="myGrid" className="ag-theme-alpine" style={{ height: '400px' }}>
                <AgGridReact

                    // onCellClicked={onCellClicked}
                    editType="fullRow"
                    rowSelection={'multiple'}
                    onCellValueChanged={onCellValueChanged}
                    onRowValueChanged={onRowValueChanged}
                    onSelectionChanged={onSelectionChanged}
                    // suppressClickEdit={suppressClickEdit}
                    pagination={true}
                    paginationPageSize={25}
                    
          tooltipShowDelay={1000}
          tooltipMouseTrack={true}
          frameworkComponents={{ customTooltip: customToolTip }}
                    // paginationAutoPageSize={true}
                    columnDefs={columnDefs}
                    rowData={emergencyTypeList}
                    defaultColDef={defaultColDef}
                    onGridReady={onGridReady}>



                </AgGridReact>
                <div className='selectbox' style={{
                    padding: '10px',
                    marginTop: '-10px', fontSize: 'small',
                }}> Page Size: <select onChange={(e) => pageSize(e.target.value)}>
                        <option value='10'>10</option>
                        <option value='25'>25</option>
                        <option value='50'>50</option>
                        <option value='100'>100</option>
                    </select></div>
            </div>
        </div>
    );
}
export default EmergencyType;

function countryCellRenderer(params) {
    var image = (params.data.emergencyTypeIcon);
    var logo =
        // params.data.catName;
        // '<img border="0" width="50" alt="image" height="30" src="' +
        // params.data.categoryImage +
        // '">';
        '<img border="0" width="50" alt="image" height="30" src=" ' + image +
        '">';
    return (

        '<span style="cursor: default;">' + logo + '</span>'
    );
}

// function printResult(res) {
//     console.log('---------------------------------------');
//     if (res.add) {
//         res.add.forEach(function (rowNode) {
//             console.log('Added Row Node', rowNode);
//         });
//     }
//     if (res.remove) {
//         res.remove.forEach(function (rowNode) {
//             console.log('Removed Row Node', rowNode);
//         });
//     }
//     if (res.update) {
//         res.update.forEach(function (rowNode) {
//             console.log('Updated Row Node', rowNode);
//         });
//     }
// }
// function setPrinterFriendly(api) {
//     // const eGridDiv = document.querySelector('#myGrid');
//     api.setDomLayout('print');
// }
// function setNormal(api) {
//     const eGridDiv = document.querySelector('#myGrid');
//     eGridDiv.style.width = '700px';
//     eGridDiv.style.height = '200px';
//     api.setDomLayout(null);
// }
