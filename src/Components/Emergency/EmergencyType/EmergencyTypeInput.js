import { useState, useRef} from 'react';
import { Form, Button } from 'react-bootstrap';
import Card from '../../Card/Card';
import classes from './EmergenctTypeInput.Module.css';
import Alert from '@material-ui/lab/Alert';
import { CheckBoxSelection, Inject, MultiSelectComponent } from '@syncfusion/ej2-react-dropdowns';
import UseFetch from '../../UseFetch';
const EmergencyTypeInput = (props) => {
  const [flash, setFlash] = useState(null);
  const color=props.color;
  const styles = {
    alert: {
      zIndex: '1500',
    },
  }
  const Invalid = '';
  const [valid, setValid] = useState(Invalid);
  // const [bloodGroupIds, setBloodGroupIds] = useState([]);
  // const [countries, setCountries] = useState([]);
  const emergencyTypeRef = useRef();
  const emergencyTypeIconRef = useRef();
  const titleRef = useRef();
  const descriptionRef = useRef();
  const bloodGroupIdsRef = useRef();
  const countriesRef = useRef();
  let countries = UseFetch('/admin/countriesList');
  let bloodGroupIds = UseFetch('/emergencyAdmin/bloodGroupsList');
  // useEffect(() => {

  //   axios.get('https://appdev.connectyourneed.com/admin/countriesList')
  //     .then(response => {
  //       const country = response.data.data;
  //       // console.log(country);

  //       setCountries(country)
  //     });

  //   // empty dependency array means this effect will only run once (like componentDidMount in classes)
  // }, []);
  const fields = { text: 'countryName', value: 'countryId' };

  // const optionItems = bloodGroupIds.map((bloodGroupId) => <option value={bloodGroupId.bloodGroupId}>{bloodGroupId.bloodGroup}</option>)
  const field = { text: 'bloodGroup', value: 'bloodGroupId' }
  const submitHandler = (event) => {
    event.preventDefault();
    setFlash(true);
    setTimeout(() => {
      setFlash(null);
      // setFlashE(null);
    }, 5000);

    const emergencyType = emergencyTypeRef.current.value;
    const emergencyTypeIcon = emergencyTypeIconRef.current.files[0];
    const title = titleRef.current.value;
    const description = descriptionRef.current.value;
    const Countries = countriesRef.current.value;
    const bloodGroupIds = bloodGroupIdsRef.current.value;
  //   console.log(bloodGroupIds);
  // bloodGroupIds.map((blood)=>{
  //     const b=parseInt(blood)
  //     console.log(b)
  //   })

    if (emergencyType.trim().length === 0 || title.trim().length === 0 || description.trim().length === 0) {
      setValid('Invalid Country or code or currency');
      return console.log('Invalid Country and code and currency');

    }
    const emergencyTypeData = {
      bloodGroupIds: bloodGroupIds,
      emergencyType: emergencyType,
      emergencyTypeIcon: emergencyTypeIcon,
      title: title,
      description: description,

      countries: Countries


    };
    console.log(emergencyTypeData);
    props.onSaveEmergencyTypeData(emergencyTypeData);
    setValid('');
  }


  return (
    <div>
      <div className={classes.input}>
        <Card className={classes.modal}>
          <header className={classes.header}>
            <h2>{props.name}</h2>
          </header>
          {
        flash
        ? <Alert style={styles.alert} severity={color}>{props.stat}</Alert>
        : null
      }
          <Form
            onSubmit={submitHandler}
          > <div className={classes.content}>


              {/* <Form.Group controlId="exampleForm.ControlSelect1"> */}

              {/* <Form.Control as="select" ref={bloodGroupIdsRef}>
                  {optionItems}
                </Form.Control></Form.Group> */}
              <MultiSelectComponent id="checkbox" className='dropdown '
                dataSource={bloodGroupIds}
                // fields={fields}
                fields={field}
                ref={bloodGroupIdsRef}
                placeholder="Select bllodgroup"
                mode="CheckBox"
                selectAllText="Select All"
                unSelectAllText="unSelect All"
                showSelectAll={true}>
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent><br />

              <Form.Control
                type='text'
                placeholder='emergencyType'
                ref={emergencyTypeRef}
              />
              <Form.Control
                type='file'
                placeholder={props.eName}
                ref={emergencyTypeIconRef}
              />

              <Form.Control
                type='text'
                placeholder='title'
                ref={titleRef}
              />
              <Form.Control
                type='text'
                placeholder='description'
                ref={descriptionRef}
              />


              <MultiSelectComponent id="checkbox" className='dropdown '
                dataSource={countries}
                // fields={fields}
                fields={fields}
                ref={countriesRef}
                placeholder="Select Countries"
                mode="CheckBox"
                selectAllText="Select All"
                unSelectAllText="unSelect All"
                showSelectAll={true}>
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent><br />
            </div>
            <h4>{valid}</h4>
            <footer className={classes.actions}>
              <Button className={classes.btn}
                onClick={props.onClose}
              >cancel</Button>
              <Button
                type='submit'
              >Add</Button>
            </footer></Form>
        </Card>
      </div>

    </div>
  );

}
export default EmergencyTypeInput;