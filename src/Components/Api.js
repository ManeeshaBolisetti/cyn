import axios from "./AxiosP";

class Api {
    // getapis
    get(url){
return axios.get(url);
    }
  getCountry() {
    return axios.get("/admin/countriesList");
  }

  getCategory() {
    return axios.get(`/admin/categoriesList`);
  }
  getProduct() {
    return axios.get(`/admin/productsList`);
  }
  getPerson() {
    return axios.get(`/admin/personTypesList`);
  }
  getDistance() {
    return axios.get(`/admin/distancesList`);
  }
  getDemo() {
    return axios.get(`/admin/getAllDemoLinks`);
  }
  getGender() {
    return axios.get(`/admin/gendersList`);
  }
  
  getBlood() {
    return axios.get(`/emergencyAdmin/bloodGroupsList`);
  }
  
  getEmergency() {
    return axios.get(`/emergencyAdmin/emergencyTypesList`);
  }
  getSocialMedia() {
    return axios.get(`/admin/socialMediaList`);
  }
  getSubscriptions() {
    return axios.get(`admin/allB2bSubscriptionsList`);
  }
 
  getWebsite() {
    return axios.get(`/admin/webList`);
  }
  getB2bRequests() {
    return axios.get(`/Admin/b2bRequests`);
  }
  getFeedBacks() {
    return axios.get(`/admin/userFeedbacks`);
  }
  getReq() {
    return axios.get(`/admin/userRequests`);
  }
  getUser() {
    return axios.get(`/admin/getAllUsersList`);
  }
  getQrCodes() {
    return axios.get(`/admin/getCynRequests`);
  }
  
// postApi's
addCountry(data) {
    return axios.post("/admin/addCountry", data);
  }
  signIn(data) {
    return axios.post("/admin/signIn", data);
  }
  RemoveBlob(data) {
    return axios.post("/admin/removeBlob", data);
  }
  addCategory(data) {
    return axios.post("/admin/addCategory", data);
  }
  updateCatgory(data) {
    return axios.post("/Admin/updateCategory", data);
  }addProduct(data) {
    return axios.post("/admin/addProduct", data);
  }
  updateProduct(data) {
    return axios.post("/Admin/updateProduct", data);
  }
  addPerson(data) {
    return axios.post("admin/addPersonTypes", data);
  }
  updatePerson(data) {
    return axios.post("/admin/updatePersonType", data);
  }addDistance(data) {
    return axios.post("/admin/addDistances", data);
  }UpdateDistance(data) {
    return axios.post("/admin/updateDistance", data);
  }
  addDemo(data) {
    return axios.post("/admin/addDemoLink", data);
  }UpdateDemo(data) {
    return axios.post("admin/updateDemoLinks", data);
  }
  addGender(data) {
    return axios.post("admin/addGenders", data);
  }updateGender(data) {
    return axios.post("/admin/updateGender", data);
  }addBlood(data) {
    return axios.post("/emergencyAdmin/addBloodGroups", data);
  }
  updateBlood(data) {
    return axios.post("/emergencyAdmin/updateBloodGroups", data);
  }addEmergency(data) {
    return axios.post("/emergencyAdmin/addEmergencyTypes", data);
  }
  updateEmergency(data) {
    return axios.post("/emergencyAdmin/updateEmergencyTypes", data);
  }
  addSocial(data) {
    return axios.post("/admin/addSocialMedia", data);
  }
  updateSocial(data) {
    return axios.post("/admin/updateSocialMedia", data);
  }
  addWebsite(data) {
    return axios.post("/admin/addWebsite", data);
  }
  updateWebsite(data) {
    return axios.post("admin/updateWebLinks", data);
  }addSubscription(data) {
    return axios.post("admin/addB2bSubscriptions", data);
  }
updateSubscriptions(data) {
    return axios.post("admin/updateB2bSubscriptions", data);
  }
  updateB2bReq(data) {
    return axios.post("/admin/updateB2bRequest", data);
  }
  updateMultiB(data) {
    return axios.post("/admin/uploadB2bApprovedMultiBanners", data);
  }
  Approvebanner(data) {
    return axios.post("/Admin/uploadB2bApprovedBanners", data);
  }
  removeB2BMulti(data) {
    return axios.post("/admin/removeB2BMultiImage", data);
  }
  removeB2BBanner(data) {
    return axios.post("/admin/removeB2BBannerImage", data);
  }
  approveB2bRequest(data) {
    return axios.post("/admin/approveB2bRequest", data);
  }
    b2bRefund(data) {
    return axios.post("admin/updateB2BRefundStatus", data);
  }
  UpdateSReq(data) {
    return axios.post("/admin/updateRequestStatus", data);
  }
  addNotes(data){
    return axios.post('/admin/addb2bNotes', data);
  }
  changeStatus(data){
    return axios.post('/admin/updateB2bRequestToNew', data);
  }
  
//   update(, data) {
//     return axios.put(`/tutorials/${}`, data);
//   }

//   delete() {
//     return http.delete(`/tutorials/${}`);
//   }

//   deleteAll() {
//     return http.delete(`/tutorials`);
//   }

//   findByTitle(title) {
//     return http.get(`/tutorials?title=${title}`);
//   }
}

export default new Api();
