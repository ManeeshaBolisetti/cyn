
//import useState hook to create menu collapse state
import React, { useState } from "react";

//import react pro sidebar components
import {
  ProSidebar,
  Menu,
  MenuItem,
  SidebarHeader,
  SidebarFooter,
  SidebarContent,
  SubMenu,
} from "react-pro-sidebar";

import { DiGoogleAnalytics } from "react-icons/di";
//import icons from react icons
import { FcDepartment } from "react-icons/fc";
import { FiHome, FiLogOut, FiArrowLeftCircle, FiArrowRightCircle } from "react-icons/fi";
import { RiPencilLine } from "react-icons/ri";
import { BiCog,BiCalculator } from "react-icons/bi";
import { GoDashboard } from "react-icons/go";
import { GiMasterOfArms } from "react-icons/gi";
import { FiUsers } from "react-icons/fi";
import { MdSettingsBrightness } from "react-icons/md";

//import sidebar css from react-pro-sidebar module and our custom css 
import "react-pro-sidebar/dist/css/styles.css";
import "./Sidebar.css";


const Sidebar = () => {
  
    //create initial menuCollapse state using useState hook
    const [menuCollapse, setMenuCollapse] = useState(true)

    //create a custom function that will change menucollapse state from false to true and true to false
  const menuIconClick = () => {
    //condition checking to change state from true to false and vice versa
    menuCollapse ? setMenuCollapse(false) : setMenuCollapse(true);
  };

  return (
    <>
      <div id="header">
          {/* collapsed props to change menu size using menucollapse state */}
        <ProSidebar collapsed={menuCollapse}>
          <SidebarHeader>
          <div className="logotext">
              {/* small and big change using menucollapse state */}
              <p>{menuCollapse ? "CYN" : "ConnectYourNeed"}</p>
            </div>
            <div className="closemenu" onClick={menuIconClick}>
                {/* changing menu collapse icon on click */}
              {menuCollapse ? (
                <FiArrowRightCircle/>
              ) : (
                <FiArrowLeftCircle/>
              )}
            </div>
          </SidebarHeader>
          <SidebarContent>
            <Menu iconShape="square">
              <MenuItem active={true} icon={<DiGoogleAnalytics />}>
                Analytics
              </MenuItem>
              {/* <MenuItem icon={<FaList />}>Qn</MenuItem> */}
              <SubMenu title="Master Settings" icon={<GiMasterOfArms  />}>
      <MenuItem>Create Department</MenuItem>
      <MenuItem>Support Department Login</MenuItem>
      <MenuItem>Marketing Department Login</MenuItem>
    </SubMenu>
    <SubMenu title="GeneralSettings" icon={<MdSettingsBrightness  />}>
      <MenuItem>Category and Products</MenuItem>
      <MenuItem>Emergency Needs</MenuItem>
      <MenuItem>Ad Prices</MenuItem>
      <MenuItem>Verfication Prices</MenuItem>
      
    </SubMenu>
    <SubMenu title="Departments" icon={<FcDepartment  />}>
      <MenuItem>EmergencyList</MenuItem>
      
    </SubMenu>
              <SubMenu title="profile" icon={<FiUsers  />}>
      <MenuItem>Users List</MenuItem>
      <MenuItem>Requests</MenuItem>
      <MenuItem>Enquires</MenuItem>
      <MenuItem>Feedbacks</MenuItem>
    </SubMenu>
    {/* <SubMenu title="Settings" icon={<BiCog  />}>
              <MenuItem >Edit Profile</MenuItem>
              <MenuItem >My Inbox</MenuItem>
              
              </SubMenu>         */}
            </Menu>
          </SidebarContent>
          <SidebarFooter>
            <Menu iconShape="square">
              <MenuItem icon={<FiLogOut />}>Logout</MenuItem>
            </Menu>
          </SidebarFooter>
        </ProSidebar>
      </div>
    </>
  );
};

export default Sidebar;
