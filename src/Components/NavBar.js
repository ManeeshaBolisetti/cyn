// import {useState,useEffect} from 'react';
import {Navbar,Nav,NavDropdown} from 'react-bootstrap';
import logo from './../assets/logo1.png';
import {
  // BrowserRouter as Router,
  // Switch,
  // Route,
  Link
} from "react-router-dom";
// import { CgProfile } from "react-icons/cg";
import { HiOutlineLogout } from "react-icons/hi";
import {FiSettings,FiLink} from 'react-icons/fi';
import {BiSupport,BiGitPullRequest} from 'react-icons/bi';
import { VscFeedback } from "react-icons/vsc";
import { FaUsers} from "react-icons/fa";
import {RiUserShared2Fill} from 'react-icons/ri';
// import {SiGnuprivacyguard} from 'react-icons/si';
import AuthService from './Authentication/AuthService';
import './NavBar.css';
const NavBar=()=>{
  // const [showAdminBoard, setShowAdminBoard] = useState(false);
  
  const logOut =()=>{
    AuthService.logout();
    localStorage.removeItem("url");
  }
    return(
        <div>
<Navbar collapseOnSelect expand="lg" className='navbar' variant="dark">
  <Navbar.Brand ><Link to="/"><img src={logo} alt='logo' fontSize='small'/></Link></Navbar.Brand>
  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
  <Navbar.Collapse id="responsive-navbar-nav">
     <Nav className=" navtext">
     {/* <Nav.Link href="/home">Profile</Nav.Link> */}
      {/* <Nav.Link href="/otp">Analytics</Nav.Link> */}
      <Nav.Link  ><Link to="/b2b" className='navLinks'>Dashboard</Link></Nav.Link>
      {/* <Nav.Link href="/category">GenaralSettings</Nav.Link> */}
      {/* <Nav.Link href="/orderUser">ManageRoles</Nav.Link> */}
      {/* <Nav.Link href="/orderUser">Reports</Nav.Link> */}
       <NavDropdown title="Reports" id="collasible-nav-dropdown">
        <NavDropdown.Item href="#action/3.1">Analytics</NavDropdown.Item>
         </NavDropdown>
         <NavDropdown title={<FaUsers fontSize='30'/>} id="collasible-nav-dropdown">
        <NavDropdown.Item ><Link to="/user"><RiUserShared2Fill fontSize='20'/> Users</Link></NavDropdown.Item>
        <NavDropdown.Item ><Link to="/qrcodes"><RiUserShared2Fill fontSize='20'/> QrCodes</Link></NavDropdown.Item>
       
         </NavDropdown>
     <NavDropdown title={<BiSupport fontSize='30'/>} id="collasible-nav-dropdown">
      <NavDropdown.Item ><Link to="/userreq"><BiGitPullRequest  fontSize='20'/> Requests</Link></NavDropdown.Item>
      <NavDropdown.Divider />
        <NavDropdown.Item ><Link to="/userfeed"><VscFeedback  fontSize='20'/> FeedBacks</Link></NavDropdown.Item>
       
       </NavDropdown> 
       <NavDropdown title={<FiLink fontSize='30'/>} id="collasible-nav-dropdown">
      <NavDropdown.Item ><Link to="/terms"> Terms & conditions</Link></NavDropdown.Item>
      
        <NavDropdown.Item ><Link to="/privacy"> Privacy Policy</Link></NavDropdown.Item>
       
        <NavDropdown.Item ><Link to="/disclimer"> Desclimer</Link></NavDropdown.Item>
        <NavDropdown.Item ><Link to="#/userguiding"> UserGuide</Link></NavDropdown.Item>
       </NavDropdown>
      <NavDropdown title={<FiSettings fontSize='30'/>} id="collasible-nav-dropdown">
        <NavDropdown.Item href="#action/3.1">ManageRoles</NavDropdown.Item>
      </NavDropdown> 
    </Nav>
    <Nav>
      <Nav.Link  eventKey={2}>
      <Link to="/login" className='navLinks'> <HiOutlineLogout fontSize='30' onClick={logOut} /></Link>
      </Nav.Link>
     </Nav>
  </Navbar.Collapse>
</Navbar>
        </div>
    );

}
export default NavBar;