import React from 'react';
import Header from './Header';
import {Col} from 'react-bootstrap';
const AboutUs=()=>{
return(
  <div className='disclaimer'> 
      {/* <h1>Hi</h1> */}
    <Header />
    
    
    <section  className="portfolio-details"
    //  style={"margin-top: 70px;"}
     >
         
    <Col  >
    <div className='portfolio-info'>
              <h2
            //    style={'color:red'}
               >DISCLAIMER</h2>
              <p>
                Connect Your Need app will help the people to offer and seek a need with a QR code for a particular category. User should register and then login to maintain their account. Based on the phone location and the chosen range, people will be connected to the user which shows the connects as Notifications. People can review their notifications and communicate with the connected people personally. (Communication is not available in the app currently). </p>
               
                <p>
                    People can Lock for future reference and Unlock them if not necessary. People can Block the users as well if they think that the person connected is a trouble or can Unblock the users as well.
                </p>
               
                <p>
                    By downloading, accessing or using Connect Your Need Mobile App or any page of this app, you signify your assent to this disclaimer. The contents of this app, including without limitation, all data, information, text, graphics, links and other materials are provided as a convenience to our app users and are meant to be used for informational purposes only. We do not take responsibility for decisions taken by the reader based solely on the information provided in this app.
                </p>
              <br/>
                <p
                //  style={'font-weight: bold;'}
                 >
                    The app expressly disclaims all warranties of any kind, whether express or implied.
                </p>
</div>
                <div className="portfolio-info">
                    <h3
                    //  style={'color: red; font-family: dm sans-serif;'}
                     >The app makes no warranty that</h3>
                    <ul>
                      <li> <i className="bi bi-caret-right-fill"></i> <strong>The app or the content will meet or satisfy your requirements</strong></li>
                      <li><i className="bi bi-caret-right-fill"></i><strong>The app shall have no responsibility for any damage to your phone or tablet or loss of data that results from your use of the app or its content.</strong></li>
                      <li><i className="bi bi-caret-right-fill"></i><strong>We do not warrant or assume any legal liability or responsibility for the completeness, or usefulness of any information, service.</strong></li>
                      <li><i className="bi bi-caret-right-fill"></i><strong>The app may include inaccuracies and typographical errors. changes and improvements are periodically made to the app and the information therein.</strong></li>
                    </ul>
                  </div>

         </Col>         

   </section>

  



    </div> 
);
}
export default AboutUs;