import React from 'react';
import Header from './Header';
import  './terms.css';
import { Col } from 'react-bootstrap';

// import { BsFillCaretRightFill } from 'react-icons/bs'
const Terms = () => {
    return (
        <div className='disclaimer'>
            {/* <h1>Hi</h1> */}
            <Header />


            <section className="portfolio-details"
            //  style={"margin-top: 70px;"}
            >

                <Col  >
                    <div className='portfolio-info'>
                        <h2 >Connect Your Need Mobile Application User Guide 2.5.0</h2>

                        <br /><div class="portfolio-info">
                            <h3>App Installation</h3>
                            <ul>
                                <li>•	Navigate to App store on iPhone or Google Play store on Android. Search for "Connect Your Need" App and Install the App.
                                </li>
                                <li>•	Assume App is successfully installed.</li>
                                <li>•	Android is supported from Version: 5.0 to 11.0</li>
                                <li>•	iOS is supported from Version: 13 and above</li>
                            </ul>
                        </div>
                        <br />
                        <div class="portfolio-info">
                            <h3>Registration (Sign Up)</h3>
                            <ul>
                                <li>•	Find the Connect Your Need app on your phone </li>
                                <li>•	Click On the App to open. Login page is opened</li>
                                <li>•	At the bottom of the login page, click on the "Sign Up" link to do the registration.</li>
                                <li>•	Enter the Full Name - should be at least 4 characters</li>
                                <li>•	Enter the valid Mobile Number</li>
                                <li>•	Enter the Password - E.g.: Admin@123</li>
                                <h4>  Conditions for the password</h4>

                                <h6>1.	Minimum of 8 characters</h6>
                                <li>•	Click on the small icon on the field to see and hide the password.</li>
                                <li>
                                    •	If the check box is not ticked, the Sign-Up button should not be enabled.
                                </li>
                                <li>
                                    •	Tick the check box to agree the Terms of Service and Privacy Policy, "Sign Up" button should be enabled.
                                </li>
                                <li>•	Click on the Sign-Up button - takes to OTP verification page. The User should receive an OTP number to the given Mobile number.</li>
                                <li>•	Enter the OTP Number to verify the Mobile number and submit. The registration should be completed and take the user to the Home Page.</li>

                            </ul>
                        </div>
                        <br />
                        <div class="portfolio-info">
                            <h3>Forgot Password</h3><br />
                            <h3>Registration Completion for Existing User</h3>
                            <ul>
                                <li>     •	Find the Connect Your Need app on your phone.</li>
                                <li>•	Click on the App to Open. Login page is opened.</li>
                                <li>•	Enter the Mobile number and Password and Click Login.</li>
                                <li>•	Takes to OTP Verification and the user receives an OTP number.</li>
                                <li>•	Enter the OTP number and submit. Completes the Verification.</li>
                                <li>•	User should be taken to the home page and should be able to see all the categories.</li>

                            </ul>
                        </div>



                        <br />

                        <div class="portfolio-info">
                            <h3>Edit Profile
                            </h3><h6>Edit Profile is the place where user can edit personal information and Update profile</h6>
                            <ul>
                                <li>  •<strong>	Profile Pic:</strong></li>
                                <li>•	To edit the profile, User should navigate to the Edit Profile screen.</li>
                                <li>•	Click on the image to upload a profile pic.</li>
                                <li>•	<strong>Full Name and Email:</strong></li>
                                <li>•	To edit the fields Full name and Email, click on the pencil Icon on the top right corner. The pencil icon shows a save icon.</li>
                                <li>•	Update the details of Full Name and Email and click on the save icon. Shows a message as "Profile updated successfully".</li>
                                <li>•	<strong>Mobile Number:</strong></li>
                                <li>•	<strong>Reset My Password:</strong></li>
                                <li>•	Click on Reset My Password to update the password. Expected that user receives an OTP number to the registered mobile number.</li>
                                <li>•	Enter the OTP number and verify. Takes the user to enter the new password and confirm.</li>
                                <li>•	Enter the new password and confirm new password. Click Update. The password is updated and taken the user to home page.</li>

                            </ul>
                        </div>
                        <br />
                        <div class="portfolio-info">
                            <h3>Home Page</h3>
                            <h6>
                                Home Page displays all the Categories and allow user to select a product as a need.
                            </h6>
                            <ul>
                                <li>   •	Find the Connect Your Need app on your phone.</li>
                                <li>•	Click on the App to Open. Login to App.</li>
                                <li>•	Displays a home page with all the available categories.</li>
                                <li>•	Need to choose a category to proceed further.</li>

                            </ul>
                        </div>
                        <div class="portfolio-info">
                            <h3>Generating Orders for Quick Needs and Industrial Machinery</h3>
                            <ul>
                                <li> •	Chosen Your Need: Displays the product name and the Icon of the product</li>
                                <li>•	I am: Choose whether Offeror or Seeker</li>
                                <li>•	Title: Enter the Title of the Product</li>
                                <li>•	Description: Enter the Description of the Product</li>
                                <li>•	Upload Image: Click on the camera icon. Displays two options</li>

                                Multiple Photos
                                <br />Single Photo
                                <br />
                                Click on Multiple Photos. User should go to gallery to select multiple photos. Choose the photos and confirm. The photos are uploaded.
                                Click Delete to delete a photo that is uploaded<br />

                                Click on Single Photo and click on the camera icon to open your camera. Take a picture and confirm. The taken photo is uploaded. To delete the Photo, click on delete and the photo will be deleted.<br />
                                User should be able to upload only 5 images.

                                <li>•	Price Range: Enter Minimum and Maximum price of your product</li>
                                <li>•	Connect Partner: Choose a partner to whom you want to connect. The default value of the Connect partner will be "Any"</li>
                                <li>•	Connect Range: Choose kms range you wanted to connect. Should notify the user when the connect is nearby. The default value of the range will be 10 Km.</li>
                                <li>•	Generate Order: Click Generate Order to proceed further to generate Order. Once Order is generated should take the user to the Order Details.</li>


                            </ul>
                        </div>
                        <br />
                        <div class="portfolio-info">
                            <h3>Order Details</h3>
                            <h6>Order Details is the page where user can see and make changes for a particular Order.</h6>
                            <ul>
                                <li> •	Once QR Code is generated, displays the complete information of the Order made with the data entered by user.</li>
                                <li>•	User should be able to see the photos, QR Code, Title, I am Offeror / Seeker, Product Name, Product Type, Description, Price Range and Connect Range.</li>
                                <li>•	User should be able to click on notification icon to see all the connects for that particular product.</li>
                                <li>•	Click on the Bell icon to Turn Off or Turn On notifications of the app</li>
                                <li>•	Click on the Edit Icon to edit the QR code details. User should be able to Edit the Title, Description, upload new images, delete images, Price Range, connect partner, Connect Range. Click update to update the details.</li>

                            </ul>
                        </div><br />
                        <div class="portfolio-info">
                            <h3>Connects</h3>
                            <h6>Connects describes that the people that are connected based on their need.</h6>
                            <ul>
                                <li>  •	Offeror can connect to Multiple Seekers.</li>
                                <li>•	Seeker can connect to Multiple offerors.</li>
                                <li>•	If user doesn't have any connects, it means that no offeror or seeker is available for that Product.</li>
                                <li>•	Every connect that user gets is free.</li>
                                <li>•	The Connects are connected based on the Connect range chosen by the user for the product.</li>

                            </ul>
                        </div><br />
                        <div class="portfolio-info">
                            <h3>My Orders</h3>
                            <h6>My Orders is the page where user can see all the list of orders.</h6>
                            <ul>

                                <li>•	Click on Orders in the bottom to navigate to My Orders.</li>
                                <li>•	Each Order displays with Notification’s bell, Locked, and delete icon.</li>
                                <li>•	Notification Icon: Displays the list of all connects along with the phone numbers.</li>
                                <li>•	Locked Icon: Displays the list of all locked connects.</li>
                                <li>•	Delete Icon: User can click on the delete icon to delete the order if there is no need of it. This action performs, that all the related Ads and connects are deleted.</li>
                                <li>•	On My Orders, Click on Notification Icon - Navigate to one of the Connect - Click on the lock button if the user want to look at the details later. Navigate to Back to Orders and the Order should have one locked user.</li>
                                <li>•	On My Orders, Click on Notification Icon - Navigate to one of the Connect - Click on the Block Button if the user doesn't want to connect with him in future.</li>
                                <li>•	On My Orders, Click on Notification Icon - Navigate to one of the Connect - Click on the Purchased Connects Icon which displays all the Purchased connects.</li>

                            </ul>
                        </div>
                        <br />

                        <div class="portfolio-info">
                            <h3>Locked Users</h3>
                            <h6>Locked Users screen helps user to view all the locked users for future review.</h6>
                            <ul>
                                <li> •	On My Orders, click on the Notification Icon to see the all Connects. Lock couple of connects</li>
                                <li> •	Navigate back to my orders and click on the lock icon of the Order.</li>
                                <li> •	Takes the user to the Locked Users screen and displays all the locked connects.</li>
                                <li> •	User can unlock the connect in the locked screen</li>

                            </ul>
                        </div>
                        <br />
                        <div class="portfolio-info">
                            <h3>Blocked Users</h3>
                            <h6>Blocked Users screen helps user to view all the blocked users. User has the ability to unblock the users as well.</h6>
                            <ul>
                                <li>•	To see all the blocked users, click on the settings button on the bottom bar and navigate to blocked users.</li>
                                <li>•	When user clicks on the Blocked Users, should be able to see all the blocked users. Users cannot receive any connects from the blocked users.</li>
                                <li>•	Once the connect is Unblocked, user can receive all the notifications from Unblocked user.</li>

                            </ul>
                        </div>
                        <br />

                        <div class="portfolio-info">
                            <h3>Settings</h3>
                            <h6>Settings is the page where user can see the <strong>Terms & Conditions, Privacy Policy and Disclaimer, Blocked users, Contact Us </strong>
                                and the <strong>Tutorial Guide, Share App link to your friends</strong> of the App</h6>
                            <h6>To Navigate to Settings, Navigate to Profile-Settings. User should be able to see all the required options.</h6>
                        </div>

                        <br />
                        <div class="portfolio-info">
                            <h3>Contact Us</h3>
                            <h6>
                                Navigate to Settings  Contact Us to submit any Enquiry Feedback, Request.
                            </h6>
                            <ul>
                                <li>          <strong> Enquiry:</strong>

                                    •	Enter the Description and click Submit. Expected that user should be able to see a message as "Thanks for Contacting Us. We will get back to you within 24 hours". Support will get in touch with you once resolved.</li>
                                <li>
                                    <strong>Feedback:</strong>

                                    •	Enter the Description and submit the feedback. Expected that user should be able to see a message as "Thanks for your feedback. Please keep providing us your feedback to make improvements"</li>

                                <li><strong>
                                    Request:</strong>
                                    •	If your need is not available in app and if
                                    it needs to be added to the app, go to Request form -
                                    Enter the product details and submit. Expected that user should
                                </li>

                            </ul>
                        </div>

                        <br />
                        <div class="portfolio-info">
                            <h3>Emergency Request (Available Only India)</h3>
                            <h6>Emergency Request is the place where user can submit their request if there is an emergency of Blood or Plasma.</h6>
                            <ul>
                                <li> •	On Home screen, Under Emergency - Click Request which takes the user a different form with options Blood and Plasma.</li>
                                <li>•	Choose the option based on the requirement and select the blood group you are looking for, Submit the request.</li>
                                <li>•	You can also add more information of your request in the description field.</li>
                                <li>•	Once the Request is sent, there will be a notification sent to the people who are in 100Km range assuming that the User has App installed and registered.</li>

                            </ul>
                        </div>
                        <br />
                        <div class="portfolio-info">
                            <h3>Emergency Requests - Interests (Available only India)</h3>
                            <h6>Emergency Requests - Interests displays the list of people who are interested in your Emergency Request and ready to help.</h6>
                            <ul>

                                <li>•	Once an Emergency Request is created, Click on Orders - Move to Emergency Requests tab - You can see the list of the Emergency Requests and the Interests with a count which indicates how many people are interested in this request.</li>
                                <li>•	If another person locks the Emergency request, then the interests count increases in the User Emergency Request.</li>
                                <h4>Example: There are two persons (Person1 & Person 2)</h4>

                                <li>•	Person 1 raised a request.</li>
                                <li>•	As person 2 is in 100 Km range, received a notification regarding Emergency Request.</li>
                                <li>•	Person 2 clicks on Near By and locked the Emergency Request</li>
                                <li>•	Person 1 went to their Emergency Request and found that there is one person who is in interest.</li>
                                <li>•	Person 1 clicks on the Interests and found the details of the person who is interested.</li>
                                <li>•	Person 1 can Contact and get the requirement done.</li>

                            </ul>
                        </div>
                        <br />
                        <div class="portfolio-info">
                            <h3>Emergency Near By (Available Only India)</h3>
                            <ul>
                                <h4> Emergency Near By is the screen which displays the list of all nearby Emergency Requests.</h4>

                                <li>•	On Home screen, Under Emergency - Near By shows a notification count which indicates that Someone is in Emergency Need.</li>
                                <li>•	Click on Near By and should display the Person who is in Emergency and their contact number.</li>
                                <li>•	User can click lock which indicates that you are interested in helping so that, user can see your details and can contact you.</li>

                            </ul>
                        </div>
                        <br />
                        <div class="portfolio-info">
                            <h3>Emergency Locked (Available Only India)</h3>
                            <h4>Emergency Locked is the screen which displays the list of all the Emergency requests that are locked.</h4>
                            <ul>
                                <li> •	On Home Screen, Under Emergency - Click on Locked icon.</li>
                                <li>•	Locked screen displays the list of all the Near by Emergency Requests that you have locked</li>

                            </ul>
                        </div>
                        <br />

                        <div class="portfolio-info">
                            <h3>Convert to Ad</h3>
                            <h6>Convert to Ad is an option where user can convert his order into an Ad and can showcase the brand to the connect your need users based on the location preference</h6>
                            <ul>
                                <li>•	Select a product, fill the form - generate a QR code for your order</li>
                                <li>•	After generating the Order, user can see a button called "Convert to Ad".</li>
                                <li>•	User can click on convert to Ad button to showcase the brand in connect your need App.</li>
                                <li>•	After clicking, user redirects to the business details form where user has to provide (Business name, Phone number, Business Number, Description, Upload Photos, Email ID, Coupon code, offer percentage and social media links) - however the listed fields are optional.</li>
                                <ol><li> User can add only up to 3 photos. Not more than 3 photos are allowed</li></ol>
                                <li>•	After filling the business details form - click on next button and redirect to map screen.</li>
                                <li>•	In the map screen- ither you can select the whole country on the right top of the screen or user can search </li>
                                <li>•	User has to choose the subscription plan from the options available and click Pay Now to proceed further.</li>
                                <li>•	User now will be redirected to the payment gateway page and user should make the payment there by giving valid card details or choosing an account.</li>
                                <li>•	Once done selecting the- cations - click on proceed to payment.</li>
                                <li>•	User will be redirected to the home screen if the payment is success or fail.</li>
                                <li>•	When payment fail- o	A transaction failed notification is sent to the user</li>
                                <ol><li>	Navigate to My Orders - My Ads-  the Ad status is displayed as "Pending"</li>
                                    <li>	Click on the Pending Ad-  choose a Subscription Plan-  Click on Pay Now - process to payment to submit the Ad for approval.</li></ol>
                                <li>•	When Payment success</li>
                                <ol><li>	The Ad is submitted for approval</li>
                                    <li>	User should be able to navigate to My Ads and see the status of the Ad</li>
                                    <li>	User should be able to see the latest Ad on the top along with the status of the Ad.</li></ol>



                                <li>•	Ad Status</li>
                                <ol><li>	Active - Your Ad is approved from connect your need support team.</li>
                                    <li>	In-Progress - Connect your need support team is verifying your Ad.</li>
                                    <li>	Pending - Your request has not been submitted successfully (like your payment might got failed).</li>
                                    <li>	Rejected - Your Ad is rejected by connect your need support team.</li>
                                    <li>	Expiring soon - Your Ad is close to the expiry date.</li>
                                    <li>	Expired - Your Ad is expired. (One the Ad is expired then you will get a renewal button highlighted so that you can make the payment
                                        again to re-activate the same Ad).</li></ol>




                                <li>•	Ad Conditions</li>
                              <ol>  <li>	When Banner/Ad expiry date is less than 72 hours, User should be able to see a Renewal button next to the Ad in My Ads</li>
                                    <li>	When Banner/Ad expired, User should be able to renew the Ad by clicking on the Renewal Button.</li>
                                    <li>	When User clicks on Renewal button, should take the user to Choose subscription plan and proceed to payment</li>
                                    <li>	User should be able to Delete the Ad any time.</li>
                                    <li>	User should be able to add more locations to the existing banner</li>
                                    <li>	To add more locations, User need to switch off All Country</li>
                                    <li>	Modify/Edit Functionality - When User clicks on Modify/Edit, should be abit
                                        the Ad Details</li>
                                        <ul><li>	Add More Locations</li>
                                        <li>	All Country On & Off</li>
                                        <li>	Coupon code</li>
                                        <li>	Offer percentage</li></ul></ol>





                                <li>•		When an Order is Deleted, all the Ads related to the Order are deleted.</li>


                            </ul>
                        </div>
                        <br/>
                      
                        <div class="portfolio-info">
                            <h3>Banner Display</h3>
                            <h6>Banner display is a place in the home screen where it shows all the Ads based on the location wise.</h6>
                            <ul>
                           <li> •	After login into the App, on the top of the screen user can be able to see Ads scrolling from left to right.</li>
<li>•	All these Ads will be displayed based on the location preference selected by the User at the time of Ad posting.</li>
<li>•	User can click on any Ad displayed on the home screen to see the details of the Banner.</li>
<li>•	User can also click on view all option to check all the Ads in one screen.</li>
<li>•	After clicking on any of the Ad, user will be redirected to the Banner details screen 
    where it shows all the information of the Ad like Business name, number, email id, offer 
    code & percentage and also social media links if available.</li>
<li>•	User now can click on the number or email id to contact the advertiser.</li>

                               </ul>
                        </div>


</div>
                </Col>

            </section>





        </div>
    );
}
export default Terms;