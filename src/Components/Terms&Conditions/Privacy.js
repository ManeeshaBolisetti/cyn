import React from 'react';
import Header from './Header';
import  './terms.css';
import { Col } from 'react-bootstrap';
const Privacy = () => {
  return (
<div className='disclaimer'>
      {/* <h1>Hi</h1> */}
      <Header />


      <section className="portfolio-details"
      >

        <Col>
          <div className='portfolio-info'>
            <h2>PRIVACY POLICY</h2>

            <p>
              Please read our Privacy Policy carefully as it explains the following:

              In order to use the App, you must first acknowledge and agree to our Privacy Policy. You cannot use the App without first accepting our Privacy Policy.

            </p>  <br />


            <h2>GENERAL</h2>
            <p>
              Connect Your Need provides the Connect Your Need Application, which will help the people to offer and seek a need with a QR code for a particular category. User should register and then login to maintain their account. Based on the phone location and the chosen range, people will be connected to the user which shows the connects as Notifications. People can review their notifications and communicate with the connected people personally. (Communication is not available in the app currently).

              People can Lock for future reference and Unlock them if not necessary. People can Block the users as well if they think that the person connected is a trouble or can Unblock the users as well.

              There are Emergency needs like Need blood and Need Plasma. This will allow people to place a request if there is any requirement of the Emergency Needs which sends an emergency notification to the nearby range people.

              Connect Your Need ("Connect Your Need", "we", "us") recognizes and understands the importance of the privacy of its users ("users", "You") and wants to respect their desire to store and access personal information in a private and secure manner. This Privacy Policy applies to our Application and describes how Connect Your Need manages, stores and utilizes your Personal Data through its Products.

              In order to use our app, we require you to consent to the collection and processing of your Personal Data before you start using the app. If you do not agree with the terms of this Privacy Policy, you may not use in any manner the app.

              Connect Your Need is committed to protecting the privacy of all of its users Personal Data and providing a secure, user-controlled environment for the use of the app in accordance with the Privacy Act of 8 December 1992.
            </p>  <br />

          </div>
          
          <br />




          <div class="portfolio-info">
            <h3>PERMISSION REQUIRED</h3>
            <ul>
              <li> <i class="bi bi-caret-right-fill"></i>
                <strong> INTERNET: For ads displaying <br />

                  LOCATION: For accessing live location <br />

                  NOTIFICATIONS: For sending push notification <br />

                  PHOTO GALLERY: For accessing device storage media files  </strong></li>

              <li><i class="bi bi-caret-right-fill"></i><strong>We do not warrant that the Connect Your Need Mobile Application will always be accessible, uninterrupted, timely, secure, error free or free from computer virus or other invasive or damaging code or that the Connect Your Need Mobile Application will not be affected by any acts of God or other force majeure events, including inability to obtain or shortage of necessary materials, equipment facilities, power or telecommunications, lack of telecommunications equipment or facilities and failure of information technology or telecommunications equipment or facilities.

              </strong></li>
              <br />

              <p><strong>Link to privacy policy of third-party service providers used by the app:</strong></p>
              <li><i class="bi bi-chevron-right"></i> <a class="scrollto" href="https://policies.google.com/privacy">Google Play Services</a></li>
              <li><i class="bi bi-chevron-right"></i> <a class="scrollto" href="https://support.google.com/admob/answer/6128543?hl=en">Ads Service</a></li>

              <br />
</ul>
              <ul> <strong>FireBase</strong>

                <li><i class="bi bi-caret-right-fill"></i> 
                <strong>Firebase Analytics</strong></li>
              <li> <i class="bi bi-caret-right-fill"></i> <strong>  Firebase Cloud Messaging</strong></li>
            </ul>
      
        


        <br />

        <h2>LOG DATA</h2>
        <p>
          I want to inform you that whenever you use my Service, in a case of an error in the app I collect data and information (through third party products) on your phone called Log Data. This Log Data may include information such as your device Internet Protocol (IP) address, device name, operating system version, the configuration of the app when utilizing my Service, the time and date of your use of the Service, and other statistics.

        </p>

        <br />



        <br />

        <h2>YOUR PERSONAL DATA</h2>
        <p>
          Connect Your Need is using Amazon Web Services for backend.In backend, all the user provided content will store.All Data will remain safe and secure.No any single information/data will send to the anywhere.



        </p>  <br />




        <h2>SECURITY</h2>
        <p>
          We value your trust in providing us your Personal Information, thus we are striving to use commercially acceptable means of protecting it.But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and We cannot guarantee its absolute security.
        </p>

        <br />



        <h2>CHILDREN'S PRIVACY</h2>
        <p>
          These Services do not address anyone under the age of 13. We do not knowingly collect personally identifiable information from children under 13. In the case We discover that a child under 13 has provided me with personal information, we immediately delete this from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact me so that We will be able to do necessary actions.
        </p>
      

      <br />

      <div class="portfolio-info">
        <h3>SERVICE PROVIDERS</h3>
        <ul>
          <li> <i class="bi bi-caret-right-fill"></i> <strong>To facilitate our Service;</strong></li>
          <li> <i class="bi bi-caret-right-fill"></i><strong>To provide the Service on our behalf;</strong></li>
          <li> <i class="bi bi-caret-right-fill"></i><strong>To perform Service-related services; or</strong></li>
          <li> <i class="bi bi-caret-right-fill"></i><strong>To assist us in analysing how our Service is used.

          </strong></li>

        </ul>
        <p>We want to inform users of this Service that these third parties have access to your Personal Information. The reason is to perform the tasks assigned to them on our behalf. However, they are obligated not to disclose or use the information for any other purpose.</p>


      </div>
      <br />
      <h2>UPDATES OR CHANGES TO OUR PRIVACY POLICY</h2>
      <p>
        Occasionally, we may change or update this Privacy Policy to allow us to use or share your previously collected Personal Data for other purposes. If Connect Your Need would use your Personal  Data in a manner materially different from that stated at the time of the collection, we will provide you with a notice on our Website and in our Connect Your Need Mobile Application indicating that the Privacy Policy has been changed or updated and request you to agree with the updated or changed Privacy Policy.
      </p>
    <h2>CONTROLLER</h2><p>
        Connect Your Need

        Info @connectyourneed.in </p>
              
                
               

            </div >
           
          
         </Col >         

   </section >

  



    </div > 
);
}
export default Privacy;