import React from 'react';
import Header from './Header';
import {Col} from 'react-bootstrap';
import  './terms.css';
import {BsFillCaretRightFill} from 'react-icons/bs'
const Terms=()=>{
return(
  <div className='disclaimer'> 
      {/* <h1>Hi</h1> */}
    <Header />
    
    
    <section  className="portfolio-details"
    //  style={"margin-top: 70px;"}
     >
         
    <Col  >
    <div className='portfolio-info'>
    <h2 >TERMS & CONDITIONS</h2>
   
    <p>
                By downloading, browsing, accessing or using this Connect Your Need Application Connect Your Need Mobile Application, you agree to be bound by these Terms and Conditions of Use. We reserve the right to amend these terms and conditions at any time. If you disagree with any of these Terms and Conditions of Use, you must immediately discontinue your access to the Connect Your Need Mobile Application and your use of the services offered on the Connect Your Need Mobile Application. Continued use of the Connect Your Need Mobile Application will constitute acceptance of these Terms and Conditions of Use, as may be amended from time to time
               </p>  <br/>


               <div class="portfolio-info">
                    <h3 >TERMS TO USE</h3>
                    <ul>
                      <li>
                        <BsFillCaretRightFill/>
                        <strong>Connect Your Need is committed to 
                          ensuring that the app is as useful and efficient as possible. 
                          For that reason, we reserve the right to make changes to the 
                          app or to charge for its services, at any time and for any reason. 
                          We will never charge you for the app or its services without making 
                          it very clear to you exactly what you are paying for.

                    </strong></li>
                      <li> <BsFillCaretRightFill/><strong>The Connect Your Need app stores and processes personal data that you have provided to us, in order to provide my Service. It's your responsibility to keep your phone and access to the app secure. We therefore recommend that you do not jailbreak or root your phone, which is the process of removing software restrictions and limitations imposed by the official operating system of your device. It could make your phone vulnerable to malware/viruses/malicious programs, compromise your phone's security features and it could mean that the Connect Your Need app won't work properly or at all.

                    </strong></li>
                     
                      <li> <BsFillCaretRightFill/><strong>Connect Your Need cannot always take responsibility for the way you use the app i.e. You need to make sure that your device stays charged if it runs out of battery and you can't turn it on to avail the Service, Connect Your Need cannot accept responsibility.</strong></li>
                    
                      <li> <BsFillCaretRightFill/><strong> At some point, we may wish to update the app. The app is available for Android & iOS the requirements for system (and for any additional systems we decide to extend the availability of the app to) may change, and you'll need to download the updates if you want to keep using the app. Connect Your Need does not promise that it will always update the app so that it is relevant to you and/or works with the Android or iOS version that you have installed on your device. However, you promise to always accept updates to the application when offered to you, we may also wish to stop providing the app, and may terminate use of it at any time without giving notice of termination to you.</strong></li>
                      
                      <li> <BsFillCaretRightFill/><strong>We strongly recommend that you only download the Connect Your Need applications from Google Play Store and App Store. Doing so will ensure that your apps are legitimate and safe from malicious software.</strong></li>
                    </ul>
                  </div>



                  <br/>
                
                  <h2 >PAYMENT TERMS</h2>
              <h4>
              Payments paid through third party integration Stripe Payment Gateway & 
              Wind Cave Payment Gateway.</h4>
	<br/>
  <ul>
                  <li> <BsFillCaretRightFill/> <strong>
                  Connect your need is not responsible if there are no connects available.
                    </strong></li>
                  <li><BsFillCaretRightFill/><strong> 
                  No Refund will be paid to the customer for the Ads if there no leads made. 
  </strong></li>
                 
                  <li><BsFillCaretRightFill/><strong>
                  If there is any transaction failed, customer won't be charged for any process.
   </strong></li>
                
   <li><BsFillCaretRightFill/><strong>
   In Refund/ cancellation document add a statement that 
   </strong></li>   
                </ul>

                <h4>
                In Refund/ cancellation document</h4>
	<br/>
  <ul>
                  <li> <BsFillCaretRightFill/> <strong>
                  Any transaction issue happened, third party payment gateway will be responsible to resolve it.
                    </strong></li>
                   
                </ul>
{/* 
  Connect your need is not responsible if there are no connects available.
    
  No Refund will be paid to the customer for the Ads if there no leads made. 
    
  If there is any transaction failed, customer won't be charged for any process.
  
  In Refund/ cancellation document add a statement that  */}
  
  
    
  {/* Any transaction issue happened, third party payment gateway will be responsible to resolve it.
                There will be payments paid through third party integration Stripe Payment Gateway & Wind Cave Payment Gateway User need to verify the registration using one time password. There is no any refund paid and we are not responsible if there are no connects available.
             */}
                <br/>

              
               <div class="portfolio-info">
                <h3>DISCLAIMER AND EXCLUSION OF LIABILITY</h3>
                <ul>
                  <li> <BsFillCaretRightFill/> <strong> The Connect Your Need Mobile Application, the Services, the information on the Connect Your Need Mobile Application and use of all related facilities are provided on an "as is, as available" basis without any warranties whether express or implied.  </strong></li>
                  <li><BsFillCaretRightFill/><strong> To the fullest extent permitted by applicable law, we disclaim all representations and warranties relating to the Connect Your Need Mobile Application and its contents, including in relation to any inaccuracies or omissions in the Connect Your Need Mobile Application, warranties of merchantability, quality, fitness for a particular purpose, accuracy, availability, non-infringement or implied warranties from course of dealing or usage of trade.      </strong></li>
                 
                  <li><BsFillCaretRightFill/><strong>We do not warrant that the Connect Your Need Mobile Application will always be accessible, uninterrupted, timely, secure, error free or free from computer virus or other invasive or damaging code or that the Connect Your Need Mobile Application will not be affected by any acts of God or other force majeure events, including inability to obtain or shortage of necessary materials, equipment facilities, power or telecommunications, lack of telecommunications equipment or facilities and failure of information technology or telecommunications equipment or facilities.

                </strong></li>
                
                  <li><BsFillCaretRightFill/><strong>While we may use reasonable efforts to include accurate and up-to-date information on the Connect Your Need Mobile Application, we make no warranties or representations as to its accuracy, timeliness or completeness. </strong></li>
                  
                  <li><BsFillCaretRightFill/><strong>We shall not be liable for any acts or omissions of any third parties howsoever caused, and for any direct, indirect, incidental, special, consequential or punitive damages, howsoever caused, resulting from or in connection with the Connect Your Need Mobile Application and the services offered in the Connect Your Need Mobile Application, your access to, use of or inability to use the Connect Your Need Mobile Application or the services offered in the Connect Your Need Mobile Application, reliance on or downloading from the Connect Your Need Mobile Application and/or services, or any delays, inaccuracies in the information or in its transmission including but not limited to damages for loss of business or profits, use, data or other intangible, even if we have been advised of the possibility of such damages.

                </strong></li>

                  <li><BsFillCaretRightFill/><strong>We shall not be liable in contract, tort (including negligence or breach of statutory duty) or otherwise howsoever and whatever the cause thereof, for any indirect, consequential, collateral, special or incidental loss or damage suffered or incurred by you in connection with the Connect Your Need Mobile Application and these Terms and Conditions of Use. For the purposes of these Terms and Conditions of Use, indirect or consequential loss or damage includes, without limitation, loss of revenue, profits, anticipated savings or business, loss of data or goodwill, loss of use or value of any equipment including software, claims of third parties, and all associated and incidental costs and expenses.

                </strong></li>

                  <li><BsFillCaretRightFill/><strong>The above exclusions and limitations apply only to the extent permitted by law. None of your statutory rights as a consumer that cannot be excluded or limited are affected.

                </strong></li>
                </ul>
              </div>


              <br/>

               <h2 >COPYRIGHT INFRINGEMENTS</h2>
              <p>
                We respect the intellectual property rights of others. If you believe that any material available on or through the App infringes upon any copyright you own or control, please immediately notify us using the contact information provided below. A copy of your infringement part/material will be sent to the person who posted or stored the material addressed in the app. Please be advised that pursuant to federal law you may be held liable for damages if you make material misrepresentations. Thus, if you are not sure that material located on or linked to by the App infringes your copyright, you should consider first contacting an attorney.
               </p> 
               
               <br/>


               <div class="portfolio-info" >
                <h3>INTELLECTUAL PROPERTY RIGHTS</h3>
                <ul>
                  <li><BsFillCaretRightFill/> <strong>All editorial content, information, photographs, illustrations, artwork and other graphic materials, and names, logos and trade marks on the Connect Your Need Mobile Application are protected by copyright laws and/or other laws and/or international treaties, and belong to us and/or our suppliers, as the case may be. These works, logos, graphics, sounds or images may not be copied, reproduced, retransmitted, distributed, disseminated, sold, published, broadcasted or circulated whether in whole or in part, unless expressly permitted by us and/or our suppliers, as the case may be.

 

                </strong></li>
               

                  <li> <BsFillCaretRightFill/><strong>  Nothing contained on the Connect Your Need Mobile Application should be construed as granting by implication, estoppel, or otherwise, any license or right to use any trademark displayed on the Connect Your Need Mobile Application without our written permission. Misuse of any trademarks or any other content displayed on the Connect Your Need Mobile Application is prohibited.

 

                </strong></li>
                 
                  <li> <BsFillCaretRightFill/><strong>We will not hesitate to take legal action against any unauthorised usage of our trademarks, name or symbols to preserve and protect its rights in the matter. All rights not expressly granted herein are reserved. Other product and company names mentioned herein may also be the trademarks of their respective owners.

                </strong></li>
                
                    </ul>
              </div>

              <br/>

               <h2>CHANGES TO THIS TERMS AND CONDITIONS</h2>
              <p>
                By downloading, browsing, accessing or using this Connect Your Need Application Connect Your Need Mobile Application, you agree to be bound by these Terms and Conditions of Use. We reserve the right to amend these terms and conditions at any time. If you disagree with any of these Terms and Conditions of Use, you must immediately discontinue your access to the Connect Your Need Mobile Application and your use of the services offered on the Connect Your Need Mobile Application. Continued use of the Connect Your Need Mobile Application will constitute acceptance of these Terms and Conditions of Use, as may be amended from time to time.

 
               </p>  <br/>
            

         

              <h2 >CHANGES TO THIS TERMS AND CONDITIONS</h2>
              <p>
               I may update our Terms and Conditions from time to time. Thus, you are advised to review this page periodically for any changes. I will notify you of any changes by posting the new Terms and Conditions on this page. These changes are effective immediately after they are posted on this page.
               </p> 
               
               <br/>



               <h2>CONTACT US</h2>
               <p>
                If you have any Queries Please , do not hesitate to reach us at info@connectyourneed.in
  
                </p> 
                
                <br/>
            </div>
           
          
          

         </Col>         

   </section>

  



    </div> 
);
}
export default Terms;