import React from 'react';
import pdf from './../../assets/pdf.pdf';

const UserGuide=()=>{
    const styles = {
        ifram: {
          height:'100vh',
          width:'100%',
        },
      }
    return(
        <iframe style={styles.ifram} title="myFrame" src={pdf}></iframe>
        // <div>
        //     UserGuide
        //     <a href = {pdf} target = "_blank">Download Pdf</a>
        //     {/* <a href="/uploads/media/default/0001/01/540cb75550adf33f281f29132dddd14fded85bfc.pdf">example</a> */}
        // </div>
    );
}
export default UserGuide;