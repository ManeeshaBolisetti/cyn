import {
    // BrowserRouter as Router,
    
    Link
  } from "react-router-dom";
import OtpInput from 'react-otp-input';
import './Otp.css';
import { Component } from 'react';
import './Login.css';
class Otp extends Component{
    
    state = { otp: '' };

    handleChange = (otp) =>this.setState({ otp });
    render(){
    return(

        <div className='login'>
            
<div className="d-flex justify-content-center align-items-center  grid">
    <div className="card py-5 px-3">
        <h5 className="m-0">Mobile phone verification</h5><span className="mobile-text">Enter the code we just send on your mobile phone <b class="text-danger">+91 86684833</b></span>
        <div className="d-flex flex-row mt-5">
        <OtpInput className='form-control'
        value={this.state.otp}
        onChange={this.handleChange}
        numInputs={4}
        // separator={<span>-</span>}
      />
           </div>
        <div className="text-center mt-5"><span className="d-block mobile-text">Don't receive the code?</span><span class="font-weight-bold text-danger cursor">Resend</span></div>
        <Link to="/home" className='btn btn-primary'>Submit</Link>
       
    </div>
</div>
        </div>
    );
}}
export default Otp;