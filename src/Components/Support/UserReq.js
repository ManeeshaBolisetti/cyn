import { React, useState ,useEffect} from 'react';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-enterprise';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import { RiFolderDownloadLine } from "react-icons/ri";
import { Button, Row, Col } from 'react-bootstrap';
import Search from './../Country/Search';
import NavBar from './../NavBar';
import api from './../Api';
import Sidebar from './../Sidebar';
import Alert from '@material-ui/lab/Alert';
import {  FcEditImage } from "react-icons/fc";

// import UseFetch from './../UseFetch';


const UserReq = () => {
    const [flash, setFlash] = useState(null);
    const [Errflash, setErrFlash] = useState(null);
    const styles = {
      alert: {
        zIndex: '1500',
      },
    }
    // const [b2b,setB2B]=useState([]);
    const [status, setStatus] = useState('');
    const [errStatus,setErrStatus]=useState('');
    const [suppressClickEdit, setSuppressClickEdit] = useState(true);
    const [edit, setEdit] = useState('Edit');
    const [req,setReq]=useState([]);
    useEffect(() => {


       api.getReq()
            .then(response => {
                const Request = response.data.data;
                setReq( Request );
                console.log( Request )
            },
                error => {
                    console.log(error);
                }
  
            );
  
    }, []);
    // let b2br = UseFetch('/Admin/b2bRequests');
    const [gridApi, setGridApi] = useState(null);
  
    const columnDefs = [
        // { headerName: "ID", field: "id" },
        { headerName: "Ticket Id", field: "ticketNumber", filter: 'agSetColumnFilter',editable:false },
       
        { headerName: "UserName", field: "userRef.userName", filter: 'agSetColumnFilter',editable:false },
        { headerName: "Mobile", field: "userRef.userMobile", filter: 'agSetColumnFilter',editable:false },
        // { headerName: "Lenin Name", field: "age", filter: 'agSetColumnFilter', tooltipField: 'name' },
        { headerName: "Email", field: "userRef.userEmail", filter: 'agSetColumnFilter' ,editable:false},
        { headerName: "Country", field: "countryCode.countryName", filter: 'agSetColumnFilter',editable:false },
        { headerName: "Title", field: "title", filter: 'agSetColumnFilter',editable:false },
         { headerName: "Description", field: "description" , filter: 'agSetColumnFilter',editable:false},
        { headerName: "Info", field: "info", filter: 'agSetColumnFilter',editable:false },
       
        {
            headerName: "Created Date",
            field: "createdAt",editable:false,
            // filter: 'agSetColumnFilter'
            filter: 'agDateColumnFilter',
            filterParams: filterParams,
        },
        {
            headerName: "Updated Date",
            field: "updatedAt",editable:false,
           
            filter: 'agDateColumnFilter',
            filterParams: filterParams,
        },{ headerName: "status", field: "status", filter: 'agSetColumnFilter',  cellEditor: 'agSelectCellEditor',
        cellEditorParams: {
            values: ['in progress', 'Rejected','New'],
        }, },
       
    

          // { headerName: "R", field: "birthYear", filter: 'agSetColumnFilter', tooltipField: 'name' },

        
    ]

    const defaultColDef = {
        sortable: true,
        editable: true,
        flex: 1,

        minWidth: 200,
        resizable: true,
        floatingFilter: true,
        //  floatingFilter: true
    }
    const onGridReady = (params) => {
        console.log('grid ready');
        setGridApi(params);
        // setGridColumnApi(params.columnApi);
    };
    // const AddCountryHandler = product => {
    //     console.log(product);
    //     // setCountry(prevExp => {
    //     //   return [product, ...prevExp];
    //     // });

    // };
    const pageSize = (pageSize) => {
        console.log(pageSize);
        gridApi.api.paginationSetPageSize(Number(pageSize))
    }

    const showEdit = () => {
        alert('Are you go to Edit Mode')
        // girdColumnApi.setColumnVisible("action", false)
        setSuppressClickEdit(false);
        setEdit('EditMode')
    }
    const stopEdit = () => {
        alert('Are Quit Mode!')
        setSuppressClickEdit(true);
        setEdit('Edit')
    }
   
    // const onRemoveSelected = () => {
    //     var selectedData = gridApi.api.getSelectedRows();
    //     var res = gridApi.api.applyTransaction({ remove: selectedData });
    //     printResult(res);
    // };
    const onCellValueChanged = (event) => {
        var selectedRows = gridApi.api.getSelectedRows();
        console.log(selectedRows)
        console.log(
            'onCellValueChanged: ' + event.colDef.field + ' = ' + event.newValue
        );
        const UserData={
            userId:selectedRows[0].userRef.userId,
            requestId:selectedRows[0].requestId,
            status:event.newValue
        }
        api.UpdateSReq(UserData)
        .then(response => {
            setStatus({ data: response.data.message });
            // setB2Bm([catgory.data, ...prevExp])
            setFlash(true);
                setTimeout(() => {
                  setFlash(null);
                  // setFlashE(null);
                }, 5000);
            console.log(response.data.message)
            // window.location.reload();
        })
        .catch(error => {
            if (error.response) {
                setErrStatus (error.response.data.message) ;
                setErrFlash(true);
                setTimeout(() => {
                  setFlash(null);
                  // setFlashE(null);
                }, 5000);
                // console.log(error.response.data.message);
            } else {
                setErrStatus( error.message );
                // console.error('There was an error!', error);

            }

        })

        
    };
   
    const onRowValueChanged = (event) => {
        const data = event.data;
        console.log(data.status)
    }  
    const onExportClick = () => {
        gridApi.api.exportDataAsCsv();
    }
    // const onBtPrint = () => {
    //     const api = gridApi.api;
    //     setPrinterFriendly(api);
    //     setTimeout(function () {
    //         print();
    //         setNormal(api);
    //     }, 2000);
    // };
    const onFilterTextChange = (e) => {

        gridApi.api.setQuickFilter(e.target.value);
    }
    // const onSelectionChanged = () => {
    //     var selectedRows = gridApi.api.getSelectedRows();
    //     console.log(selectedRows);
    //     // setB2b(selectedRows);

    //     // document.querySelector('#selectedRows').innerHTML =
    //     //   selectedRows.length === 1 ? selectedRows[0].countryCode : '';
    // };
    
    

    return (
        <div>
            <NavBar/>
            <Sidebar/>
            <Row className='con'>
                <Col xs={11}>
            {/* <div className='d-flex justify-content-between' > */}
            <div className='d-flex justify-content-between' onClick={stopEdit} >
                    
                <h2>User Requests</h2>
                {
        flash
        ? <Alert style={styles.alert} severity="success">{status.data}</Alert>
        : null
      }
                        {
        Errflash
        ? <Alert style={styles.alert} severity="error">{errStatus}</Alert>
        : null
      }
       
                <Search onChange={onFilterTextChange} /></div>
                <Button className='add' onClick={() => onExportClick()}>
                        Export to Excel  <RiFolderDownloadLine color='green' />
                    </Button >
                    <Button className='add' onClick={showEdit}>{edit} <FcEditImage /></Button >
                          
            <Row >
                <Col xs={10} style={{ display: 'flex' }}>
                    {/* <Refresh/> */}
                    {/* <AddCountry
                        onAddCountry={AddCountryHandler}
                    /> */}
                    {/* <Button className='add' onClick={() => onRemoveSelected()}>Remove <FcDeleteDatabase /></Button > */}
                    {/* <Button className='add' onClick={showEdit}>{edit} <FcEditImage /></Button > */}
                    {/* <Button className='add' onClick={stopEdit}>stop <BsFillStopFill color='darkred' /></Button > */}
                    

                    {/* <Button className='add' onClick={() => onBtPrint()}>Print <FcPrint /></Button ></Col> */}

               </Col>

            </Row>
            <div id="myGrid" className="ag-theme-alpine" style={{ height: '400px' }}>
                <AgGridReact

                    // onCellClicked={onCellClicked}
                    // editType="fullRow"
                    rowSelection={'multiple'}
                    onCellValueChanged={onCellValueChanged}
                    onRowValueChanged={onRowValueChanged}

                    suppressClickEdit={suppressClickEdit}
                    pagination={true}
                    paginationPageSize={25}
                    // paginationAutoPageSize={true}
                    columnDefs={columnDefs}
                    rowData={req}
                    defaultColDef={defaultColDef}
                    onGridReady={onGridReady}>



                </AgGridReact>
                <div className='selectbox' style={{
                    padding: '10px',
                    marginTop: '-10px', fontSize: 'small',
                }}> Page Size: <select onChange={(e) => pageSize(e.target.value)}>
                        <option value='10'>10</option>
                        <option value='25'>25</option>
                        <option value='50'>50</option>
                        <option value='100'>100</option>
                    </select></div>
            </div>
</Col></Row>        </div>

    );
}
export default UserReq;

var filterParams = {
    comparator: function (filterLocalDateAtMidnight, cellValue) {
      var dateAsString = cellValue;
      if (dateAsString == null) return -1;
      var dateParts = dateAsString.split('/');
      var cellDate = new Date(
        Number(dateParts[2]),
        Number(dateParts[1]) - 1,
        Number(dateParts[0])
      );
      if (filterLocalDateAtMidnight.getTime() === cellDate.getTime()) {
        return 0;
      }
      if (cellDate < filterLocalDateAtMidnight) {
        return -1;
      }
      if (cellDate > filterLocalDateAtMidnight) {
        return 1;
      }
    },
    browserDatePicker: true,
    minValidYear: 2000,
  };
//   var ragCellClassRules = {
//     'rag-green-outer': function (params) {
//       return params.value === 'New';
//     },
//     'rag-amber-outer': function (params) {
//       return params.value === 'In-progress';
//     },
//     'rag-red-outer': function (params) {
//       return params.value === 'Approved';
//     },
//   };
//   function ragRenderer(params) {
//     return '<span class="rag-element">' + params.value + '</span>';
//   }