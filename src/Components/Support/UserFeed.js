import { React, useState,useEffect } from 'react';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-enterprise';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import { RiFolderDownloadLine } from "react-icons/ri";
import { Button, Row, Col } from 'react-bootstrap';
import Search from './../Country/Search';
import NavBar from './../NavBar';
import Sidebar from './../Sidebar';
import api from './../Api';
// import UseFetch from './../UseFetch';


const UserFeed = () => {
    const [feedBack,setFeedBack]=useState([]);
    useEffect(() => {


        api.getFeedBacks()
            .then(response => {
                const Request = response.data.data;
                setFeedBack( Request );
                console.log( Request )
            },
                error => {
                    console.log(error);
                }
  
            );
  
    }, []);
    
    const [gridApi, setGridApi] = useState(null);
    const columnDefs = [
        // { headerName: "ID", field: "id" },
        { headerName: "Ticket Id", field: "ticketNumber", filter: 'agSetColumnFilter' ,editable:false},
       
        { headerName: "UserName", field: "userRef.userName", filter: 'agSetColumnFilter' },
        { headerName: "Mobile", field: "userRef.userMobile", filter: 'agSetColumnFilter' },
        // { headerName: "Lenin Name", field: "age", filter: 'agSetColumnFilter', tooltipField: 'name' },
        { headerName: "Email", field: "userRef.userEmail", filter: 'agSetColumnFilter' },
        { headerName: "Country", field: "countryCode.countryName", filter: 'agSetColumnFilter' },
         { headerName: "Description", field: "description" , filter: 'agSetColumnFilter'},
        { headerName: "Info", field: "info", filter: 'agSetColumnFilter' },
       
        {
            headerName: "Created Date",
            field: "createdAt",
            // filter: 'agSetColumnFilter'
            filter: 'agDateColumnFilter',
            filterParams: filterParams,
        },
        {
            headerName: "Updated Date",
            field: "updatedAt",
           
            filter: 'agDateColumnFilter',
            filterParams: filterParams,
        },
        { headerName: "info", field: "info", filter: 'agSetColumnFilter' },
       
     { headerName: "status", field: "status", filter: 'agSetColumnFilter' },
        
          // { headerName: "R", field: "birthYear", filter: 'agSetColumnFilter', tooltipField: 'name' },

        
    ]

    const defaultColDef = {
        sortable: true,
        // editable: true,
        flex: 1,

        minWidth: 200,
        resizable: true,
        floatingFilter: true,
        //  floatingFilter: true
    }
    const onGridReady = (params) => {
        console.log('grid ready');
        setGridApi(params);
        // setGridColumnApi(params.columnApi);
    };
    // const AddCountryHandler = product => {
    //     console.log(product);
    //     // setCountry(prevExp => {
    //     //   return [product, ...prevExp];
    //     // });

    // };
    const pageSize = (pageSize) => {
        console.log(pageSize);
        gridApi.api.paginationSetPageSize(Number(pageSize))
    }

    // const showEdit = () => {
    //     girdColumnApi.setColumnVisible("action", false)
    //     setSuppressClickEdit(false);
    //     setEdit('EditMode')
    // }
    // const stopEdit = () => {
    //     setSuppressClickEdit(true);
    //     setEdit('Edit')
    // }
    // const onRemoveSelected = () => {
    //     var selectedData = gridApi.api.getSelectedRows();
    //     var res = gridApi.api.applyTransaction({ remove: selectedData });
    //     printResult(res);
    // };
    const onCellValueChanged = (event) => {
        console.log(
            'onCellValueChanged: ' + event.colDef.field + ' = ' + event.newValue
        );
    };

    const onRowValueChanged = (event) => {
        var data = event.data;
        console.log(
            'onRowValueChanged: (' +
            data.countryName +
            ', ' +
            data.countryCode +
            ', ' +
            data.currency +
            ')'
        );
    };
    const onExportClick = () => {
        gridApi.api.exportDataAsCsv();
    }
    // const onBtPrint = () => {
    //     const api = gridApi.api;
    //     setPrinterFriendly(api);
    //     setTimeout(function () {
    //         print();
    //         setNormal(api);
    //     }, 2000);
    // };
    const onFilterTextChange = (e) => {

        gridApi.api.setQuickFilter(e.target.value);
    }

    return (
        <div>
            <NavBar/>
            <Sidebar/>
            <Row className='con'>
                <Col xs={11}>
            <div className='d-flex justify-content-between' >
                <h2>User FeedBacks</h2>
                <Search onChange={onFilterTextChange} /></div>
                <Button className='add' onClick={() => onExportClick()}>
                        Export to Excel  <RiFolderDownloadLine color='green' />
                    </Button >
            <Row >
                <Col xs={10} style={{ display: 'flex' }}>
                    {/* <Refresh/> */}
                    {/* <AddCountry
                        onAddCountry={AddCountryHandler}
                    /> */}
                    {/* <Button className='add' onClick={() => onRemoveSelected()}>Remove <FcDeleteDatabase /></Button > */}
                    {/* <Button className='add' onClick={showEdit}>{edit} <FcEditImage /></Button > */}
                    {/* <Button className='add' onClick={stopEdit}>stop <BsFillStopFill color='darkred' /></Button > */}
                    

                    {/* <Button className='add' onClick={() => onBtPrint()}>Print <FcPrint /></Button ></Col> */}

               </Col>

            </Row>
            <div id="myGrid" className="ag-theme-alpine" style={{ height: '400px' }}>
                <AgGridReact

                    // onCellClicked={onCellClicked}
                    editType="fullRow"
                    rowSelection={'multiple'}
                    onCellValueChanged={onCellValueChanged}
                    onRowValueChanged={onRowValueChanged}

                    // suppressClickEdit={suppressClickEdit}
                    pagination={true}
                    paginationPageSize={25}
                    // paginationAutoPageSize={true}
                    columnDefs={columnDefs}
                    rowData={feedBack}
                    defaultColDef={defaultColDef}
                    onGridReady={onGridReady}>



                </AgGridReact>
                <div className='selectbox' style={{
                    padding: '10px',
                    marginTop: '-10px', fontSize: 'small',
                }}> Page Size: <select onChange={(e) => pageSize(e.target.value)}>
                        <option value='10'>10</option>
                        <option value='25'>25</option>
                        <option value='50'>50</option>
                        <option value='100'>100</option>
                    </select></div>
            </div>
</Col></Row>        </div>

    );
}
export default UserFeed;

var filterParams = {
    comparator: function (filterLocalDateAtMidnight, cellValue) {
      var dateAsString = cellValue;
      if (dateAsString == null) return -1;
      var dateParts = dateAsString.split('/');
      var cellDate = new Date(
        Number(dateParts[2]),
        Number(dateParts[1]) - 1,
        Number(dateParts[0])
      );
      if (filterLocalDateAtMidnight.getTime() === cellDate.getTime()) {
        return 0;
      }
      if (cellDate < filterLocalDateAtMidnight) {
        return -1;
      }
      if (cellDate > filterLocalDateAtMidnight) {
        return 1;
      }
    },
    browserDatePicker: true,
    minValidYear: 2000,
  };
//   var ragCellClassRules = {
//     'rag-green-outer': function (params) {
//       return params.value === 'New';
//     },
//     'rag-amber-outer': function (params) {
//       return params.value === 'In-progress';
//     },
//     'rag-red-outer': function (params) {
//       return params.value === 'Approved';
//     },
//   };
//   function ragRenderer(params) {
//     return '<span class="rag-element">' + params.value + '</span>';
//   }