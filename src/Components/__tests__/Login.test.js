import { render, screen,fireEvent} from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import Login from './.././Login';

test('should render Login Component', ()=>{
    render(<Login/>);
    const loginElement =screen.getByTestId('login-1');
    expect(loginElement).toBeInTheDocument();
    expect(loginElement).toHaveTextContent('Login');
})

test('enable to loginpage',()=>{
  render(<Login/>);
  const username=screen.getByLabelText('Username');
  const password=screen.getByLabelText('Password');
  const button=screen.getByRole('button');
  expect(button).toBeDisabled();
  fireEvent.change(username, {target:{value:'cynadmin'}})
  fireEvent.change(password,{target:{value:'cyn1323'}})
  expect(button).toBeEnabled();

//  userEvent.type(username,'cynadmin');
//  userEvent.type(password,'cyn1323');


});



test("can't submit when login details not Enterd",()=>{
  const onsubmit=jest.fn();
  render(<Login onSubmit={onsubmit}/>)

  const button=screen.getByRole('button');
  fireEvent.click(button);
  expect(onsubmit).toHaveBeenCalledTimes(0)
})




describe("Form behaviour",  () => {
  it('validate user inputs, and provides error messages', async () => {
    const { getByTestId, getByText } = render(<Login/>)

    await act (async () => {
      fireEvent.change(screen.getByLabelText(/Username/i), {
        target: {value: ''},
      });

      fireEvent.change(screen.getByLabelText(/Password/i), {
        target: {value: ''},
      })
    });

    await act (async () => {
      fireEvent.submit(getByTestId('form'))
    });

    expect(screen.getByText("Username is required!")).toBeInTheDocument();
    expect(screen.getByText("Password is required!")).toBeInTheDocument();
  });

  it('should submit when form inputs contain text', async () => {
    const { getByTestId, queryByText } = render(<Login/>)

    await act(async () => {
      fireEvent.change(screen.getByLabelText(/Username/i), {
        target: {value: 'cynadmin'},
      });

      fireEvent.change(screen.getByLabelText(/Password/i), {
        target: {value: 'cyn1323'},
      })
    });

    await act (async () => {
      fireEvent.submit(getByTestId('form'))
    });
    // expect(screen.getByText("Username is required!")).toBeInTheDocument();
    // expect(screen.getByText("Password is required!")).toBeInTheDocument();
    expect(queryByText("Username is required!")).not.toBeInTheDocument();
    expect(queryByText("Password is required!")).not.toBeInTheDocument();
  });
});


