// import axios from "axios";
import Api from "./../Api";
// const API_URL = "admin/signIn";

const login = (username, password) => {
    // const [user, setUser] = useState(null);
    const data={
      userName:username,
      userPassword:password,
    }
    return Api.signIn(data)
    //  axios
    //   .post(API_URL , {
    //     "userName": username,
    //     "userPassword": password,
    //   })
      .then((response) => {
        if (response.data.jwToken) {
            console.log("user", JSON.stringify(response.data))
        //   localStorage.setItem("user", response.data);
        // localStorage.setItem('ACCESS_TOKEN_NAME',response.data.token);
          sessionStorage.setItem("user", response.data.data.user);
         
        }
  
        return response.data;
      });
  };
  
  const logout = () => {
    // localStorage.removeItem("user");
    sessionStorage.removeItem("user");
 
  };

  // const getCurrentUser = () => {
  //   console.log(sessionStorage.getItem("user"))
  //   return sessionStorage.getItem("user");
    
  // };

  const AuthService={
    login,logout
  }
  export default AuthService