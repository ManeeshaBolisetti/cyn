import { useState, useRef } from 'react';
import { Form, Button } from 'react-bootstrap';
// import { form } from 'react-dom-factories';
// import axios from 'axios';
import Card from '../Card/Card';
import classes from './PersonInput.Module.css';
import Alert from '@material-ui/lab/Alert';
import { CheckBoxSelection, Inject, MultiSelectComponent } from '@syncfusion/ej2-react-dropdowns';
import UseFetch  from '../UseFetch';
const PersonInput = (props) => {
  const [flash, setFlash] = useState(null);
  const color=props.color;
  const styles = {
    alert: {
      zIndex: '1500',
    },
  }
  const Invalid = '';
  let value=props.val
 
  const [valid, setValid] = useState(Invalid);
  

  // const [countries, setCountries] = useState([]);
  const formRef = useRef();
  const personTypeRef = useRef();
  const countryRef = useRef();
  // let pCountries=value.length === 1 ? value[0].countries: '';
  let pPersonTypeId=value.length ===1 ? value[0].personTypeId:'';
  const pCountryId=value.length === 1 ? value[0].countryId: '';
  let pPersonType=value.length ===1 ? value[0].personType:'';
  
  console.log(pCountryId)
  let countries=UseFetch('/admin/countriesList');
  // useEffect(() => {

  //   axios.get('/admin/countriesList')
  //     .then(response => {
  //       const country = response.data.data;
  //       // console.log(country);

  //       setCountries(country)
  //     });

  //   // empty dependency array means this effect will only run once (like componentDidMount in classes)
  // }, []);
  const fields = { text: 'countryName', value: 'countryId' };
  let colorValues= pCountryId.map((n)=>{
      let colour=n;
      return colour
  });
  console.log(colorValues)
  const onTagging = (e) => {
    // set the current selected item text as class to chip element.
    e.setClass(e.itemData[fields.text].toLowerCase());
};
   const submitHandler = (event) => {
    event.preventDefault();
    setFlash(true);
    setTimeout(() => {
      setFlash(null);
      // setFlashE(null);
    }, 5000);
    const personType = personTypeRef.current.value;
    const country = countryRef.current.value;
    
    if (personType.trim().length === 0 ) {
      setValid('Invalid Country or code or currency');
      return console.log('Invalid Country and code and currency');

    }

    const personData = {
        personType: personType,
        countries: country,
        personTypeId:pPersonTypeId,
    };
    console.log(personData);
    props.onSavePersonTypeData(personData);
    // personTypeRef.current.value = '';
    // countryRef.current.value = '';

  }
  return (
    <div className={classes.input}>
      <Card className={classes.modal}>
        <header className={classes.header}>
          <h2>{props.name}</h2>
        </header>
        {
        flash
        ? <Alert style={styles.alert} severity={color}>{props.stat}</Alert>
        : null
      }
        <Form onSubmit={submitHandler} ref={formRef}> <div className={classes.content}>

          <Form.Control
            type='text'
            placeholder={props.name}
            defaultValue={pPersonType}
            //  value={enterdCountry}
            ref={personTypeRef}
          // onChange={cNameChangeHandler}
          />
          <br />
          {/* <Form.Control
            type='text'
            placeholder={props.countries}
            //  value={enterdCode}
            ref={countryRef}
          // onChange={codeChangeHandler}
          /><br /> */}
          <MultiSelectComponent id="checkbox" className='dropdown '
                dataSource={countries}
                // fields={fields}
                fields={fields}
                ref={countryRef}
                placeholder={props.countries}
                mode="CheckBox"
                selectAllText="Select All"
                unSelectAllText="unSelect All"
                tagging={onTagging}
                value={colorValues}
                showSelectAll={true}>
                <Inject services={[CheckBoxSelection]}
                
                />
              </MultiSelectComponent><br />
          
          <h4>{valid}</h4>
         
        </div>
          <footer className={classes.actions}>
            <Button className={classes.btn}
              onClick={props.onClose}
            >cancel</Button>
            <Button
              type='submit'
            >Update</Button>
          </footer></Form>
      </Card>
    </div>
  );
}
export default PersonInput;