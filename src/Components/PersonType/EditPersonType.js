import {React, useState} from 'react';
import EditPersonTypeInput from './EditPersonTypeInput';
// import PersonInput from './PersonInput';
import { Form } from 'react-bootstrap';
import { TiFolderAdd } from 'react-icons/ti';
// import axios from 'axios';
import api from './../Api';
// import api from './../AxiosP';
const EditPersonType=(props)=>{
    let statu='';
    let value=props.value
    const [showForm, setshowForm] = useState(false);
    const [status,setStatus]=useState(statu);
    const [color,setColor]=useState('');
    
    
    const savePersonDataHandler = (enterdPersonData) => {
        const PersonData = {
            ...enterdPersonData,
            // id: Math.random().toString()
        };
        // axios.post('/admin/updatePersonType', PersonData)
        api.updatePerson(PersonData)
        .then(response => {
            setStatus({ data:response.data.message });
        console.log(response.data.message)
        props.onUpdatePersonType(response.data.data);
        setColor('success')
        // setFlash(true);
        setTimeout(() => {
            setColor('')
            setStatus('')
          setshowForm(false)
          // setFlashE(null);
        }, 3000);
        // setshowForm(false);
        // window.location.reload();
         } )
        .catch(error => {
            if (error.response) {
                setStatus({ data:(error.response.data.message )});
                setColor('error')
                // console.log(error.response.data.message);
            }else{
                setStatus({ data: error.message });
                console.error('There was an error!', error);
                setColor('error')

            }
           
        });
       
        // fetch('https://appdev.connectyourneed.com/Admin/addPersonTypes', {

        //     method: "POST",
        //     headers: { 'Content-Type': 'application/json' },
        //     body: JSON.stringify(PersonData)
        // }).then((response) => {
        //     if(!response.ok){
        //         if(response.status===500){
        //             setStatus('Alredy existed');
        //         }else
        //             setStatus(response.message)
                
        //     //    setStatus(response.status) ;
        //     } 
        //     else return response.json();
        // })
        //  .then((data) => 
        // {
        //    if(data){
        //     setStatus(data.message);
        //     console.log(data);
        //     // props.onAddCountry(countryData);

        //    }
        //     // console.log(data);
        //     // console.log(data.message);
            
        // })
        console.log(PersonData)
        // props.onUpdatePersonType(PersonData);

        // setshowForm(false);
    };
    const onShowForm = () => {
        if(value.length===1){
        setshowForm(true);
    }else{
        alert('please select edit Row')
    }
    }
   
    const onHideForm = () => {
        setshowForm(false);
        setStatus("update A Person");
    }
    return(
        <div>
           {showForm &&
             <EditPersonTypeInput
            name='Edit PersonType'
            // cName='Country Name'
            countries='Countries'
            stat={status.data}
                color={color}
            onClose={onHideForm}
            onSavePersonTypeData={savePersonDataHandler}
            val={value}
            // onCancel={stopEditHandler}
             />}
            
                {/* <h4 >Admin/Add Merchant</h4> */}

                <Form><div onClick={onShowForm}>
                    <TiFolderAdd fontSize='20' color='green' /> Update </div></Form>
 
        </div>
    );
}
export default EditPersonType;