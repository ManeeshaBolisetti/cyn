import { React, useState,useEffect } from 'react';
import api from './../Api';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import AddPerson from './AddPerson';
import EditPersonType from './EditPersonType';
import * as moment from 'moment';
// import { FcDeleteDatabase, FcEditImage, FcPrint } from "react-icons/fc";
// import { BsFillStopFill } from "react-icons/bs";
import { RiFolderDownloadLine } from "react-icons/ri";
// import { print } from 'react-to-print';
import { Button, Row, Col } from 'react-bootstrap';
import Search from './../Country/Search';
// import UseFetch from '../UseFetch';
import Refresh from '../Actions/Refresh';
import customToolTip from '../customTooltip';
var filterParams = {
    comparator: function (filterLocalDateAtMidnight, cellValue) {
      var dateAsString = cellValue;
      if (dateAsString == null) return -1;
      var dateParts = dateAsString.split('/');
      var cellDate = new Date(
        Number(dateParts[2]),
        Number(dateParts[1]) - 1,
        Number(dateParts[0])
      );
  
      if (filterLocalDateAtMidnight.getTime() === cellDate.getTime()) {
        return 0;
      }
  
      if (cellDate < filterLocalDateAtMidnight) {
        return -1;
      }
  
      if (cellDate > filterLocalDateAtMidnight) {
        return 1;
      }
    },
    browserDatePicker: true,
    minValidYear: 2000,
  };
const PersonsType = () => {
    // const rowdata = []
    // let personType = UseFetch('/admin/personTypesList');
    const [person,setPerson]=useState([]);
    const [personType,setPersonType]=useState([]);
    useEffect(() => {


        api.getPerson()
            .then(response => {
                let person = response.data.data;
                setPersonType(person);
                // console.log(products)
            },
                error => {
                    console.log(error);
                }
  
            );
  
    }, []);
  console.log(personType)
    // const [country, setCountry] = useState([rowdata]);
    const [gridApi, setGridApi] = useState(null);
    // const [girdColumnApi, setGridColumnApi] = useState(null);
    // const [hideColumn, setHideColumn] = useState(true);
    // const [suppressClickEdit, setSuppressClickEdit] = useState(true);
    // const [edit, setEdit] = useState('Edit');
    let personTypeList=[];
    
  if (personType!= null && personType.length >0){
    personType.map((n)=>{
        
      let cDate = moment(n.createdAt).format('DD/MM/YYYY');
      let uDate = moment(n.updatedAt).format('DD/MM/YYYY');
      let ctime = moment(n.createdAt).format('h:mma');
      let utime = moment(n.updatedAt).format('h:mma');
       let country=n.countries.map((c)=>{
            
            let count=c.countryName
            return count;
           
            // let countries=c.countryName
            // return countries
        })
        let countryI=n.countries.map((c)=>{
            
            let countI=c.countryId
            return countI;
           
            // let countries=c.countryName
            // return countries
        })
         
        personTypeList.push({
            'countryId':countryI,
            'countries':country,
            'personType':n.personType,
            'personTypeId':n.personTypeId,
            'createdAt':cDate,
            'updatedAt':uDate,
            'ctime':ctime,
        'utime':utime,
        })
        return personTypeList;
    })
}

    // const [personType, setPersonType] = useState([]);

    const AddPersonTypeHandler = personT => {
        console.log(personT);
        // console.log('in App.js');
        setPersonType(prevExp => {
            return [personT, ...prevExp];
        });
    };
    const EditpersonTypeHandler = personT => {
        console.log(personT);
        setPersonType(prevExp => {
            return [personT, ...prevExp];
        });
    
      };
    const columnDefs = [

        // { headerName: "Flag", field: "cFlag"}, 
        
        {
            field: "personType",
            filter: 'agSetColumnFilter',
            // editable: false,
        },
        {
            field: "countries", 
            
      tooltipField: 'countries',
      tooltipComponentParams: { type: 'success' },
       filter: 'agSetColumnFilter',
            
            // cellRenderer: countryCellRenderer,
        },
        {
            field: "createdAt",filter: 'agDateColumnFilter',cellRenderer: createdDate,
            filterParams: filterParams,
            // editable: false,
          },
          {
            field: "updatedAt", filter: 'agDateColumnFilter',cellRenderer :updatedDate,
            filterParams: filterParams,
          },
        // {
        //     field: "categoryImage",
        //     
        //     cellRenderer: countryCellRenderer,
        //     // editable: false,
        // },



    ]
    const defaultColDef = {
        sortable: true,
        
        flex: 1,
        //    filter: true,
        minWidth: 200,
        resizable: true,
        floatingFilter: true,
        tooltipComponent: 'customTooltip',
        // floatingFilter: true
    }
    const onGridReady = (params) => {
        console.log('grid ready');
        setGridApi(params);
        // setGridColumnApi(params.columnApi);
    };
    
    const pageSize = (pageSize) => {
        console.log(pageSize);
        gridApi.api.paginationSetPageSize(Number(pageSize))
    }

    // const showEdit = () => {
    //     girdColumnApi.setColumnVisible("action", false)
    //     setSuppressClickEdit(false);
    //     setEdit('EditMode')
    // }
    // const stopEdit = () => {
    //     setSuppressClickEdit(true);
    //     setEdit('Edit')
    // }
    // const onRemoveSelected = () => {
    //     var selectedData = gridApi.api.getSelectedRows();
    //     var res = gridApi.api.applyTransaction({ remove: selectedData });
    //     printResult(res);
    // };
    const onCellValueChanged = (event) => {
        console.log(
            'onCellValueChanged: ' + event.colDef.field + ' = ' + event.newValue
        );
    };

    const onRowValueChanged = (event) => {
        var data = event.data;
        console.log(
            'onRowValueChanged: (' +
            data.countryName +
            ', ' +
            data.countryCode +
            ', ' +
            data.currency +
            ')'
        );
    };
    const onExportClick = () => {
        gridApi.api.exportDataAsCsv();
    }
    // const onBtPrint = () => {
    //     const api = gridApi.api;
    //     setPrinterFriendly(api);
    //     setTimeout(function () {
    //         print();
    //         setNormal(api);
    //     }, 2000);
    // };
    const onFilterTextChange = (e) => {

        gridApi.api.setQuickFilter(e.target.value);
    }
    const onSelectionChanged = () => {
        var selectedRows = gridApi.api.getSelectedRows();
        console.log(selectedRows);
        setPerson(selectedRows);
        
        // document.querySelector('#selectedRows').innerHTML =
        //   selectedRows.length === 1 ? selectedRows[0].countryCode : '';
      };




    return (
        <div>

<div className='d-flex justify-content-between' ><h2>Person Types</h2>
      <Search onChange={onFilterTextChange} /></div>
            <Row>
                <Col xs={10} style={{ display: 'flex' }}>
                <Button className='add'>  <AddPerson onAddPersonType={AddPersonTypeHandler} /></Button>
                <Button className='add'>  <EditPersonType value={person}  onUpdatePersonType={EditpersonTypeHandler}></EditPersonType>
</Button>
                    {/* <Button className='add' onClick={() => onRemoveSelected()}>Remove <FcDeleteDatabase /></Button > */}
                    {/* <Button className='add' onClick={showEdit}>{edit} <FcEditImage /></Button > */}
                    {/* <Button className='add' onClick={stopEdit}>stop <BsFillStopFill color='darkred' /></Button > */}
                    <Button className='add' onClick={() => onExportClick()}>
                        Export to Excel  <RiFolderDownloadLine color='green' />
                    </Button >
                    <Button className='add'><Refresh/></Button>
                    {/* <Button className='add' onClick={() => onBtPrint()}>Print <FcPrint /></Button > */}
                    </Col>

                <Col xs={2}>

                </Col>

            </Row>
            <div id="myGrid" className="ag-theme-alpine" style={{ height: '400px' }}>
                <AgGridReact

                    // onCellClicked={onCellClicked}
                    editType="fullRow"
                    rowSelection={'multiple'}
                    onCellValueChanged={onCellValueChanged}
                    onRowValueChanged={onRowValueChanged}
                    onSelectionChanged={onSelectionChanged}
                    // suppressClickEdit={suppressClickEdit}
                    pagination={true}
                    paginationPageSize={25}
                    // paginationAutoPageSize={true}
                    columnDefs={columnDefs}
                    
          tooltipShowDelay={1000}
          tooltipMouseTrack={true}
          frameworkComponents={{ customTooltip: customToolTip }}
                    rowData={personTypeList}
                    defaultColDef={defaultColDef}
                    onGridReady={onGridReady}>



                </AgGridReact>
                <div className='selectbox' style={{
                    padding: '10px',
                    marginTop: '-10px', fontSize: 'small',
                }}> Page Size: <select onChange={(e) => pageSize(e.target.value)}>
                        <option value='10'>10</option>
                        <option value='25'>25</option>
                        <option value='50'>50</option>
                        <option value='100'>100</option>
                    </select></div>
            </div>
        </div>
    );
}
export default PersonsType;

function createdDate(params) {
    var time = (params.data.ctime);
    var date = (params.data.createdAt);
    
    return (
  
        '<span style="cursor: default;">' +date+' ' +time+ '</span>'
    );
  }
  function updatedDate(params) {
    var time = (params.data.utime);
    var date = (params.data.updatedAt);
    
    return (
  
        '<span style="cursor: default;">' +date+' ' +time+ '</span>'
    );
  }

// function printResult(res) {
//     console.log('---------------------------------------');
//     if (res.add) {
//       res.add.forEach(function (rowNode) {
//         console.log('Added Row Node', rowNode);
//       });
//     }
//     if (res.remove) {
//       res.remove.forEach(function (rowNode) {
//         console.log('Removed Row Node', rowNode);
//       });
//     }
//     if (res.update) {
//       res.update.forEach(function (rowNode) {
//         console.log('Updated Row Node', rowNode);
//       });
//     }
//   }
//   function setPrinterFriendly(api) {
//     // const eGridDiv = document.querySelector('#myGrid');
//     api.setDomLayout('print');
//   }
//   function setNormal(api) {
//     const eGridDiv = document.querySelector('#myGrid');
//     eGridDiv.style.width = '700px';
//     eGridDiv.style.height = '200px';
//     api.setDomLayout(null);
//   }
  