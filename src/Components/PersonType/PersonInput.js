import { useState, useRef } from 'react';
import { Form, Button } from 'react-bootstrap';
// import { form } from 'react-dom-factories';
// import axios from 'axios';
import Card from '../Card/Card';
import classes from './PersonInput.Module.css';
import Alert from '@material-ui/lab/Alert';
import { CheckBoxSelection, Inject, MultiSelectComponent } from '@syncfusion/ej2-react-dropdowns';
import UseFetch  from '../UseFetch';
const PersonInput = (props) => {
  const [flash, setFlash] = useState(null);
  const color=props.color;
  const styles = {
    alert: {
      zIndex: '1500',
    },
  }
  const Invalid = '';
  const [valid, setValid] = useState(Invalid);
  // const [countries, setCountries] = useState([]);
  const formRef = useRef();
  const personTypeRef = useRef();
  const countryRef = useRef();
  let countries=UseFetch('/admin/countriesList');
  // useEffect(() => {

  //   axios.get('/admin/countriesList')
  //     .then(response => {
  //       const country = response.data.data;
  //       // console.log(country);

  //       setCountries(country)
  //     });

  //   // empty dependency array means this effect will only run once (like componentDidMount in classes)
  // }, []);
  const fields = { text: 'countryName', value: 'countryId' };
   const submitHandler = (event) => {
    event.preventDefault();
    setFlash(true);
    setTimeout(() => {
      setFlash(null);
      // setFlashE(null);
    }, 5000);
    const personType = personTypeRef.current.value;
    const country = countryRef.current.value;
   
    if (personType.trim().length === 0 ) {
      setValid('Invalid Country or code or currency');
      return console.log('Invalid Country and code and currency');

    }

    const personData = {
        personType: personType,
        countries: country,
    };
    console.log(personData);
    props.onSavePersonData(personData);
    personTypeRef.current.value = '';
    countryRef.current.value = '';

  }
  return (
    <div className={classes.input}>
      <Card className={classes.modal}>
        <header className={classes.header}>
          <h2>{props.name}</h2>
        </header>
        {
        flash
        ? <Alert style={styles.alert} severity={color}>{props.stat}</Alert>
        : null
      }
        {/* <h4>{props.stat}</h4> */}
        <Form onSubmit={submitHandler} ref={formRef}> <div className={classes.content}>

          <Form.Control
            type='text'
            placeholder={props.name}
            //  value={enterdCountry}
            ref={personTypeRef}
          // onChange={cNameChangeHandler}
          />
          <br />
          {/* <Form.Control
            type='text'
            placeholder={props.countries}
            //  value={enterdCode}
            ref={countryRef}
          // onChange={codeChangeHandler}
          /><br /> */}
          <MultiSelectComponent id="checkbox" className='dropdown '
                dataSource={countries}
                // fields={fields}
                fields={fields}
                ref={countryRef}
                placeholder={props.countries}
                mode="CheckBox"
                selectAllText="Select All"
                unSelectAllText="unSelect All"
                showSelectAll={true}>
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent><br />
          
          <h4>{valid}</h4>
         
        </div>
          <footer className={classes.actions}>
            <Button
              onClick={props.onClose}
            >cancel</Button>
            <Button className={classes.btn}
              type='submit'
            >Add</Button>
          </footer></Form>
      </Card>
    </div>
  );
}
export default PersonInput;