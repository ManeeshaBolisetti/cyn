import {React, useState} from 'react';
import PersonInput from './PersonInput';
import { Form} from 'react-bootstrap';
import { TiFolderAdd } from 'react-icons/ti';
// import axios from 'axios';
import api from './../Api';
// import api from './../AxiosP';
const AddPerson=(props)=>{
    let statu='';
    const [showForm, setshowForm] = useState(false);
    
    const [status,setStatus]=useState(statu);
    const [color,setColor]=useState('');
    
    const savePersonDataHandler = (enterdPersonData) => {
        const PersonData = {
            ...enterdPersonData,
            // id: Math.random().toString()
        };
        // axios.post('/Admin/addPersonTypes', PersonData)
        api.addPerson(PersonData)
        .then(response => {
            setStatus({ data:response.data.message });
        console.log(response.data.message)
        
        props.onAddPersonType(response.data.data);
        setColor('success')
        // setFlash(true);
        setTimeout(() => {
            setColor('')
            setStatus('')
          setshowForm(false)
          // setFlashE(null);
        }, 3000);
        // setshowForm(false);
        // window.location.reload();
         } )
        .catch(error => {
            if (error.response) {
                setStatus({ data:(error.response.data.message )});
                setColor('error')
                // console.log(error.response.data.message);
            }else{
                setStatus({ data: error.message });
                setColor('error')
                console.error('There was an error!', error);

            }
           
        });
       
        // fetch('https://appdev.connectyourneed.com/Admin/addPersonTypes', {

        //     method: "POST",
        //     headers: { 'Content-Type': 'application/json' },
        //     body: JSON.stringify(PersonData)
        // }).then((response) => {
        //     if(!response.ok){
        //         if(response.status===500){
        //             setStatus('Alredy existed');
        //         }else
        //             setStatus(response.message)
                
        //     //    setStatus(response.status) ;
        //     } 
        //     else return response.json();
        // })
        //  .then((data) => 
        // {
        //    if(data){
        //     setStatus(data.message);
        //     console.log(data);
        //     // props.onAddCountry(countryData);

        //    }
        //     // console.log(data);
        //     // console.log(data.message);
            
        // })
        console.log(PersonData)

        // setshowForm(false);
    };
    const onShowForm = () => {
        setshowForm(true);
    }
    const onHideForm = () => {
        setshowForm(false);
        setStatus("Add A Person");
    }
    return(
        <div>
           {showForm &&
             <PersonInput
            name='Add Person'
            // cName='Country Name'
            countries='Countries'
            stat={status.data}
            color={color}
            onClose={onHideForm}
            onSavePersonData={savePersonDataHandler}
            // onCancel={stopEditHandler}
             />}
            
                {/* <h4 >Admin/Add Merchant</h4> */}

                <Form><div onClick={onShowForm}><TiFolderAdd fontSize='20' color='green' /> Add </div></Form>
 
        </div>
    );
}
export default AddPerson;