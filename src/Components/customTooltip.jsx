import React, { Component } from 'react';
import './CustomTooltip.css';

export default class customToolTip extends Component {
  getReactContainerClasses() {
    return ['custom-tooltip'];
  }


  render() {
    const data = this.props.api.getDisplayedRowAtIndex(this.props.rowIndex)
      .data;
      const country=data.countries;
      
      const listItems = country.map((number) =>
  <li>{number}</li>
);
    return (
      <div className={'panel panel-' + (this.props.type || 'primary')}>
        <div className="panel-heading">
          <h3 className="panel-title">Countries</h3>
          
        </div>
        <div className="panel-body">
          {/* <h4 style={{ whiteSpace: 'nowrap' }}>{country}</h4> */}
          <ul>{listItems}</ul>
          {/* <p>Total: {data.total}</p> */}
        </div>
      </div>
    );
  }
}