import {React, useState} from 'react';
import CountryInput from './CountyInput';
// import {Button } from 'react-bootstrap';
import { VscAdd } from 'react-icons/vsc';
// import axios from 'axios';
import api from './../Api';
// import PostData from '../PostData';
// import api from './../AxiosP';
const AddCountry=(props)=>{
    let statu='';
    const [showForm, setshowForm] = useState(false);
    const [status,setStatus]=useState(statu);
    const [color,setColor]=useState('');
    // const [stat,setStat]=useState('');
    const [data,setData]=useState([]);
    
    
    const saveCountryDataHandler = (enterdCountryData) => {
        const countryData = {
            ...enterdCountryData,
            // id: Math.random().toString()
        };
        // PostData('https://appdev.connectyourneed.com/admin/addCountry',countryData);
        // axios.post('/admin/addCountry', countryData)
        api.addCountry(countryData)
        .then(response => {
            let resp=response.data
            
            setStatus(resp.message );
            setColor('success')
            setData({data:resp.data});
        console.log(resp.message)
        props.onAddCountry({data:resp.data});
        // console.log({data:resp.data})
        // setshowForm(false);
        setTimeout(() => {
            setColor('')
            setStatus('')
          setshowForm(false)
          // setFlashE(null);
        }, 3000);
        // window.location.reload();
         } )
        .catch(error => {
            if (error.response) {
                setStatus(error.response.data.message );
                setColor('error');
                // console.log(error.response.data.message);
            }else{
                setStatus( error.message );
                setColor('error');
                console.error('There was an error!', error);

            }
           
        });
        // fetch('https://appdev.connectyourneed.com/admin/addCountry', {

        //     method: "POST",
        //     headers: { 'Content-Type': 'application/json' },
        //     body: JSON.stringify(countryData)
        // }).then((response) => {
        //     if(!response.ok){
        //         if(response.status===500){
        //             setStatus('Alredy existed');
        //         }else
        //             setStatus(response.message)
                
        //     //    setStatus(response.status) ;
        //     } 
        //     else return response.json();
        // })
        //  .then((data) => 
        // {
        //    if(data){
        //     setStatus(data.message);
        //     console.log(data);
        //     // props.onAddCountry(countryData);

        //    }
        //     // console.log(data);
        //     // console.log(data.message);
            
        // })
        // console.log(countryData)
        // props.onAddCountry(data);
        console.log(data)

        // setshowForm(false);
    };
    const onShowForm = () => {
        setshowForm(true);
    }
    const onHideForm = () => {
        setshowForm(false);
        setStatus("Add A country");
    }
    // console.log(stat)
    return(
        <div >
           {showForm &&
             <CountryInput
            name='Add Countries'
            cName='Country Name'
            code='Country Code'
            status={status}
            color={color}
            // status={stat}
            onClose={onHideForm}
            onSaveCountryData={saveCountryDataHandler}
            // onCancel={stopEditHandler}
             />}
            
                {/* <h4 >Admin/Add Merchant</h4> */}

                <div onClick={onShowForm}> <VscAdd color='green'  />  New</div>
 
                </div>
    );
}
export default AddCountry;