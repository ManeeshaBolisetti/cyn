import { useState, useRef } from 'react';
import { Form, Button } from 'react-bootstrap';

import Alert from '@material-ui/lab/Alert';
import Card from '../Card/Card';
import classes from './CountryInput.Module.css';
const CountryInput = (props) => {
  const [flash, setFlash] = useState(null);
  const color=props.color;
  console.log(color);
  const styles = {
    alert: {
      zIndex: '1500',
    },
  }
  // const Invalid = '';
  // const [valid, setValid] = useState(Invalid);
  const formRef = useRef();
  const cNameRef = useRef();
  const codeRef = useRef();
  const currencyRef = useRef();
  const countryCodeRef=useRef();
  const countryUnitsRef=useRef();

  const submitHandler = (event) => {
    event.preventDefault();
    setFlash(true);
    setTimeout(() => {
      setFlash(null);
      // setFlashE(null);
    }, 5000);
    const cName = cNameRef.current.value;
    const code = codeRef.current.value;
    const currency = currencyRef.current.value;
    const countryCode=countryCodeRef.current.value;
    const units=countryUnitsRef.current.value;
    const countryData = {
      countryName: cName,
      countryCode: code,
      currency: currency,
      countryNumberCode:countryCode,
      units:units,

    };
    //   const AddCountry = () => {
    //     // let country = {"countryName":"singapure","countryCode":"sn","currency":"Dollars"}
    //     fetch('https://appdev.connectyourneed.com/Admin/addCountry', {

    //         method: "POST",
    //         headers: { 'Content-Type': 'application/json' },
    //         body: JSON.stringify(countryData)
    //     }).then((response) => response.json()).then((data) => {
    //         console.log(data);
    //     })
    // }
    // console.log(countryData);
    props.onSaveCountryData(countryData);
    // cNameRef.current.value = '';
    // codeRef.current.value = '';
    // currencyRef.current.value = '';


  }
  return (
    <div className={classes.input}>
      <Card className={classes.modal}>
        <header className={classes.header}>
          <h2>{props.name}</h2>
        </header>
        {
        flash
        ? <Alert style={styles.alert} severity={color}>{props.status}</Alert>
        : null
      }
        {/* <h4>{props.stat}</h4> */}
        <Form onSubmit={submitHandler} ref={formRef}> <div className={classes.content}>

          <Form.Control
            type='text'
            placeholder={props.cName}
            //  value={enterdCountry}
            ref={cNameRef}
          // onChange={cNameChangeHandler}
          />
          <br />
          <Form.Control
            type='text'
            placeholder={props.code}
            //  value={enterdCode}
            ref={codeRef}
          // onChange={codeChangeHandler}
          /><br />
          <Form.Control
            type='text'
            placeholder='countryCode number'
            //  value={enterdCode}
            ref={countryCodeRef}
          // onChange={codeChangeHandler}
          />
          <Form.Control
            type='text'
            placeholder='currency'
            //  value={enterdCode}
            ref={currencyRef}
          // onChange={codeChangeHandler}
          />
          <Form.Control
            type='text'
            placeholder='units'
            //  value={enterdCode}
            ref={countryUnitsRef}
          // onChange={codeChangeHandler}
          />
          {/* <h4>{valid}</h4> */}
          {/* <Form.Group>
    <Form.File id="exampleFormControlFile1" label="Add Flags" />
  </Form.Group> */}
        </div>
          <footer className={classes.actions}>
            <Button className={classes.btn}
              onClick={props.onClose}
            >cancel</Button>
            <Button 
              type='submit'
            >Add</Button>
          </footer></Form>
      </Card>
    </div>
  );
}
export default CountryInput;