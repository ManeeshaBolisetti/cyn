import React from 'react';

import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';

function createRowData() {
    var data = [];
    [
        'Red',
        'Green',
        'Blue',
        'Red',
        'Green',
        'Blue',
        'Red',
        'Green',
        'Blue',
    ].forEach(function (color) {
        var newDataItem = {
            id: rowIdSequence++,
            color: color,
            value1: Math.floor(Math.random() * 100),
            value2: Math.floor(Math.random() * 100),
        };
        data.push(newDataItem);
    });
    return data;
}
const ExampleDrag = () => {
    // let gridApi;
    const rowData = createRowData();
    // const [gridApi, setGridApi] = useState(null);
    // const [gridColumnApi,setGridColumnApi]=useState(null);
    //   const [gridColumnApi, setGridColumnApi] = useState(null);
    

    const columnDefs = [

        // { headerName: "Flag", field: "cFlag"}, 
        {
            field:"id",
            rowDrag: true,
        },
        {
            field:"color",
        },
        {
            field:"value1",
        },

        {
            field:"value2",
        },


    ]
    const defaultColDef = {
        sortable: true,
        filter: true,
        resizable: true,
        flex: 1,
       
    }
    const rowClassRules= {
        'red-row': 'data.color == "Red"',
        'green-row': 'data.color == "Green"',
        'blue-row': 'data.color == "Blue"',
      };

      const onGridReady = (params) => {
        // gridApi = params.api;
        // setGridApi(params.api);
        // setGridColumnApi( params.columnApi);
        addDropZones(params);
        addCheckboxListener(params);
        //   gridColumnApi = params.columnApi;
    };


    return (
        <div style={{ width: '100%', height: '100%' }}>
            <div className="example-wrapper">
                <div className="toolbar">
                    <label>
                        <input type="checkbox" /> Enable suppressMoveWhenRowDragging
                    </label>
                </div>
                <div className="drop-containers">
                    <div className="grid-wrapper">
                    <div className="ag-theme-alpine" style={{ height: '400px' }}>
                            <AgGridReact
                               
                              
                                columnDefs={columnDefs}
                    rowData={rowData}
                    defaultColDef={defaultColDef}
                    
                  rowClassRules={rowClassRules}
                
                  rowDragManaged={true}
                  animateRows={true}
                    onGridReady={onGridReady}
                            >
                            </AgGridReact>
                        </div>
                    </div>
                    <div className="drop-col" style={{ height: '400px' }}>
              <span id="eDropTarget" className="drop-target">
                ==&gt; Drop to here
              </span>
              <div className="tile-container"></div>
            </div>
                </div>
            </div>
        </div>
    );
};
export default ExampleDrag;

var rowIdSequence = 100;
function addCheckboxListener(params) {
    var checkbox = document.querySelector('input[type=checkbox]');
    checkbox.addEventListener('change', function () {
        params.api.setSuppressMoveWhenRowDragging(checkbox.checked);
    });
}

function createTile(data) {
    var el = document.createElement('div');
    el.classList.add('tile');
    el.classList.add(data.color.toLowerCase());
    el.innerHTML =
        '<div class="id">' +
        data.id +
        '</div>' +
        '<div class="value">' +
        data.value1 +
        '</div>' +
        '<div class="value">' +
        data.value2 +
        '</div>';
    return el;
}
function addDropZones(params) {
    var tileContainer = document.querySelector('.tile-container'),
        dropZone = {
            getContainer: function () {
                return tileContainer;
            },
            onDragStop: function (params) {
                var tile = createTile(params.node.data);
                tileContainer.appendChild(tile);
            },
        };
    params.api.addRowDropZone(dropZone);
}

