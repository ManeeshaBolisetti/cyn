import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Search from '@material-ui/icons/Search';

const useStyles = makeStyles((theme) => ({
  margin: {
    margin: theme.spacing(1),
  },
}));

export default function InputWithIcon(props) {
  const classes = useStyles();

  return (
      <div className='row'>
    <div className={classes.margin}>
        <Grid container spacing={1} alignItems="flex-end">
          
          <Grid item>
            <TextField id="input-with-icon-grid" type='search' label="Search"  onChange={props.onChange}/>
          </Grid>
          <Grid item>
            <Search />
          </Grid>
        </Grid>
      </div>
      </div>
  );
}