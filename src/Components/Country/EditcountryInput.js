import { useState, useRef } from 'react';
import { Form, Button } from 'react-bootstrap';

import Card from '../Card/Card';
import classes from './CountryInput.Module.css';
const EditCountryInput = (props) => {
  let value=props.val
  let cCountry=value.length === 1 ? value[0].countryName: '';
  let cCode=value.length === 1 ? value[0].countryCode: '';
  let cCurrency=value.length === 1 ? value[0].currency: '';
  
  let cId=value.length === 1 ? value[0].countryId: '';
  // const Invalid = '';
  // const valid = Invalid;
  // const [countryName,setCountryName]=useState(cCountry)
  // const [countryCode,setCountryCode]=useState(cCode)
  // const [currency,setCurrency]=useState(cCurrency)
  // const [countryId,setCountryId]=useState(cId)
  const formRef = useRef();
  const cNameRef = useRef();
  const codeRef = useRef();
  const currencyRef = useRef();
  

  const submitHandler = (event) => {
    event.preventDefault();
    const cName = cNameRef.current.value;
    const code = codeRef.current.value;
    const currency = currencyRef.current.value;
    const countryId=cId;
    // if (cName.trim().length === 0 || code.trim().length === 0 || currency.trim().length ===0) {
    //   setValid('Invalid Country or code or currency');
    //   return console.log('Invalid Country and code and currency');

    // }

    const countryData = {
      countryName: cName,
      countryCode: code,
      currency: currency,
      cId:countryId

    };
    //   const AddCountry = () => {
    //     // let country = {"countryName":"singapure","countryCode":"sn","currency":"Dollars"}
    //     fetch('https://appdev.connectyourneed.com/Admin/addCountry', {

    //         method: "POST",
    //         headers: { 'Content-Type': 'application/json' },
    //         body: JSON.stringify(countryData)
    //     }).then((response) => response.json()).then((data) => {
    //         console.log(data);
    //     })
    // }
    console.log(countryData);
    props.onSaveCountryData(countryData);
    cNameRef.current.value = '';
    codeRef.current.value = '';
    currencyRef.current.value = '';


  }
  // console.log(countryName)
  // console.log(countryCode)
  // console.log(countryId)
  // console.log(currency)




// console.log(countryName)
  return (
    <div className={classes.input}>
      <Card className={classes.modal}>
        <header className={classes.header}>
          <h2>{props.name}</h2>
        </header>
        <h4>{props.stat}</h4>
        <p>{props.value}</p>
        <Form onSubmit={submitHandler} ref={formRef}> <div className={classes.content}>

          <Form.Control
            type='text'
            placeholder={props.cName}
            defaultValue={cCountry}
            ref={cNameRef}
          // onChange={cNameChangeHandler}
          />
          <br />
          <Form.Control
            type='text'
            placeholder={props.code}
            defaultValue={cCode}
            //  value={enterdCode}
            ref={codeRef}
          // onChange={codeChangeHandler}
          /><br />
          <Form.Control
            type='text'
            placeholder='currency'
            defaultValue={cCurrency}
            //  value={enterdCode}
            ref={currencyRef}
          // onChange={codeChangeHandler}
          />
          <h4>{valid}</h4>
          {/* <p>hello:{console.log(props.val)}</p> */}
          {/* <Form.Group>
    <Form.File id="exampleFormControlFile1" label="Add Flags" />
  </Form.Group> */}
        </div>
          <footer className={classes.actions}>
            <Button  className={classes.btn}
              onClick={props.onClose}
            >cancel</Button>
            <Button
              type='submit'
            >Update</Button>
          </footer></Form>
      </Card>
    </div>

  );
}
export default EditCountryInput;