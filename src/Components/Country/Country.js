import { React, useState,useEffect } from 'react';
import AddCountry from './AddCountry';
import { AgGridReact } from 'ag-grid-react';
// import axios from 'axios';
import 'ag-grid-enterprise';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import * as moment from 'moment';
// import { FcDeleteDatabase, FcEditImage, FcPrint } from "react-icons/fc";
// import { BsFillStopFill } from "react-icons/bs";
import { RiFolderDownloadLine } from "react-icons/ri";
// import { print } from 'react-to-print';
import { Button, Row, Col } from 'react-bootstrap';
// import { FiSearch } from "react-icons/fi";
import Search from './Search';
// import UseFetch from '../UseFetch';
import './Country.css';
import Refresh from '../Actions/Refresh';
import api from './../Api';
// import EditCountry from './EditCountry';
// import EditcountryInput from './EditcountryInput';

var filterParams = {
  comparator: function (filterLocalDateAtMidnight, cellValue) {
      var dateAsString = cellValue;
      if (dateAsString == null) return -1;
      var dateParts = dateAsString.split('/');
      var cellDate = new Date(
          Number(dateParts[2]),
          Number(dateParts[1]) - 1,
          Number(dateParts[0])
      );

      if (filterLocalDateAtMidnight.getTime() === cellDate.getTime()) {
          return 0;
      }

      if (cellDate < filterLocalDateAtMidnight) {
          return -1;
      }

      if (cellDate > filterLocalDateAtMidnight) {
          return 1;
      }
  },
  browserDatePicker: true,
  minValidYear: 2000,
};

const Country = () => {
  // const rowdata = []

// const country = UseFetch('/admin/countriesList');
// console.log(country)
const [country,setCountry]=useState([]);
// setCountry(UseFetch('/admin/countriesList'))
// console.log(country)
// let countriesl=country
  const [countryEdit, setCountryEdit] = useState(country);
console.log(countryEdit)
  const [gridApi, setGridApi] = useState(null);
  // const [girdColumnApi, setGridColumnApi] = useState(null);
  // const [hideColumn, setHideColumn] = useState(true);
  // const [suppressClickEdit, setSuppressClickEdit] = useState(true);
  // const [edit, setEdit] = useState('Edit');

  useEffect(() => {

    api.getCountry()
      .then(response => {
        let country = response.data.data;
        setCountry(country);
      });

  }, []);
  let countryList=[];
  console.log(country)
  if (country!= null && country.length >0){
    country.map(n => {
      // let bloodGropN = n.bloodGroup
      let cDate = moment(n.createdAt).format('DD/MM/YYYY');
      let uDate = moment(n.updatedAt).format('DD/MM/YYYY');
      let ctime = moment(n.createdAt).format('h:mma');
      let utime = moment(n.updatedAt).format('h:mma');
      // let bId = n.bloodGroup
    
       countryList.push({
        'countryName': n.countryName,
        'countryCode': n.countryCode,
        'currency': n.currency,
        'createdAt':cDate,
        'updatedAt':uDate,
        "units": n.units,
        'ctime':ctime,
        'utime':utime,
        // 'bloodGroupId': bId,
      })
      return countryList.List;
    })
    }
   

  const columnDefs = [

    {
      headerName: 'Action',
      minWidth: 10,
      // cellRenderer: actionCellRenderer,
      editable: false,
      colId: "action", hide: true,

    },

    {
      field: "countryName", tooltipField: 'countryName', filter: 'agSetColumnFilter',
      
      // cellRenderer: countryCellRenderer,
    },
    {
      field: "countryCode",
      tooltipField: 'countryName', filter: 'agSetColumnFilter',
      // editable: false,
    },
    {
      field: "currency",
      tooltipField: 'countryName', filter: 'agSetColumnFilter',
      // editable: false,
    },
    {
      field: "units",
      tooltipField: 'countryName', filter: 'agSetColumnFilter',
      
    },
    {
      field: "createdAt",
      tooltipField: 'countryName',filter: 'agDateColumnFilter',cellRenderer: createdDate,
      filterParams: filterParams,
      // editable: false,
    },
    
    {
      field: "updatedAt",
      tooltipField: 'countryName', filter: 'agDateColumnFilter',cellRenderer :updatedDate,
      filterParams: filterParams,
    },
   
  ]

  const defaultColDef = {
    sortable: true,
    
    flex: 1,

    minWidth: 200,
    resizable: true,
    floatingFilter: true,
    //  floatingFilter: true
  }
  const onGridReady = (params) => {
    setGridApi(params);
    // setGridColumnApi(params.columnApi);
  };
  const AddCountryHandler = country => {
    console.log(country);
    const updateCountry=country.data
    setCountry(prevExp => {
      return [updateCountry, ...prevExp];
    });

  };
  // const EditCountryHandler = product => {
  //   console.log(product);
  //   // setCountry(prevExp => {
  //   //   return [product, ...prevExp];
  //   // });

  // };

  const pageSize = (pageSize) => {
    console.log(pageSize);
    gridApi.api.paginationSetPageSize(Number(pageSize))
  }

  // const showEdit = () => {
  //   girdColumnApi.setColumnVisible("action", false)
  //   setSuppressClickEdit(false);
  //   // setEdit('EditMode')
  // }
  // const stopEdit = () => {
  //   setSuppressClickEdit(true);
  //   // setEdit('Edit')
  // }
  // const onRemoveSelected = () => {
  //   var selectedData = gridApi.api.getSelectedRows();
  //   alert('Are you delete row?')
  //   var res = gridApi.api.applyTransaction({ remove: selectedData });
  //   // printResult(res);
  // };
  const onCellValueChanged = (event) => {
    console.log(
      'onCellValueChanged: ' + event.colDef.field + ' = ' + event.newValue
    );
  };

  const onRowValueChanged = (event) => {
    var data = event.data;
    console.log(
      'onRowValueChanged: (' +
      data.countryName +
      ', ' +
      data.countryCode +
      ', ' +
      data.currency +
      ')'
    );
  };
  const onExportClick = () => {
    gridApi.api.exportDataAsCsv();
  }
  // const onBtPrint = () => {
  //   const api = gridApi.api;
  //   setPrinterFriendly(api);
  //   setTimeout(function () {
  //     print();
  //     setNormal(api);
  //   }, 2000);
  // };
  const onFilterTextChange = (e) => {

    gridApi.api.setQuickFilter(e.target.value);
  }
  const onSelectionChanged = () => {
    var selectedRows = gridApi.api.getSelectedRows();
    console.log(selectedRows);
    setCountryEdit(selectedRows);
    
    // document.querySelector('#selectedRows').innerHTML =
    //   selectedRows.length === 1 ? selectedRows[0].countryCode : '';
  };
 console.log(countryEdit)
  return (
    <div>
      <div className='d-flex justify-content-between'  ><h2>Country</h2>
        <Search onChange={onFilterTextChange} /></div>
      <Row >
        <Col xs={10} style={{ display: '-webkit-box' }}>

        <Button className='add'><AddCountry
            onAddCountry={AddCountryHandler}
          /></Button>
           
           {/* <Button className='add' ><EditCountry value={countryEdit}  onUpdateCountry={EditCountryHandler}></EditCountry></Button> */}
{/*      
          <div className="example-header">
          Selection:
          <span id="selectedRows"></span>
        </div> */}
          {/* <Button className='add' onClick={() => onRemoveSelected()}>Remove <FcDeleteDatabase /></Button > */}
          {/* <Button className='add' onClick={showEdit}>{edit} <FcEditImage /></Button > */}
          {/* <Button className='add' onClick={stopEdit}>stop <BsFillStopFill color='darkred' /></Button > */}
          <Button className='add' onClick={() => onExportClick()}>
            Export to Excel  <RiFolderDownloadLine color='green' />
          </Button >
          <Button className='add' ><Refresh /></Button>
          {/* <Button className='add' onClick={() => onBtPrint()}>Print <FcPrint /></Button > */}
          </Col>
        <Col xs={2}>

        </Col>

      </Row>
      <div id="myGrid" className="ag-theme-alpine" style={{ height: '400px' }}>
        <AgGridReact

          // onCellClicked={onCellClicked}
          editType="fullRow"
          onSelectionChanged={onSelectionChanged}
          rowSelection={'multiple'}
          onCellValueChanged={onCellValueChanged}
          onRowValueChanged={onRowValueChanged}

          // suppressClickEdit={suppressClickEdit}
          pagination={true}
          paginationPageSize={25}
          // paginationAutoPageSize={true}
          columnDefs={columnDefs}
          rowData={countryList}
          defaultColDef={defaultColDef}
          onGridReady={onGridReady}>



        </AgGridReact>
        <div className='selectbox' style={{
          padding: '10px',
          marginTop: '-10px', fontSize: 'small',
        }}> Page Size: <select onChange={(e) => pageSize(e.target.value)}>
            <option value='10'>10</option>
            <option value='25'>25</option>
            <option value='50'>50</option>
            <option value='100'>100</option>
          </select></div>
      </div>
    
    </div>

  );
}
export default Country;
function createdDate(params) {
  var time = (params.data.ctime);
  var date = (params.data.createdAt);
  
  return (

      '<span style="cursor: default;">' +date+' ' +time+ '</span>'
  );
}
function updatedDate(params) {
  var time = (params.data.utime);
  var date = (params.data.updatedAt);
  
  return (

      '<span style="cursor: default;">' +date+' ' +time+ '</span>'
  );
}
// function printResult(res) {
//   console.log('---------------------------------------');
//   if (res.add) {
//     res.add.forEach(function (rowNode) {
//       console.log('Added Row Node', rowNode);
//     });
//   }
//   if (res.remove) {
//     res.remove.forEach(function (rowNode) {
//       console.log('Removed Row Node', rowNode);
//     });
//   }
//   if (res.update) {
//     res.update.forEach(function (rowNode) {
//       console.log('Updated Row Node', rowNode);
//     });
//   }
// }
// function setPrinterFriendly(api) {
//   // const eGridDiv = document.querySelector('#myGrid');
//   api.setDomLayout('print');
// }
// function setNormal(api) {
//   const eGridDiv = document.querySelector('#myGrid');
//   eGridDiv.style.width = '700px';
//   eGridDiv.style.height = '200px';
//   api.setDomLayout(null);
// }
