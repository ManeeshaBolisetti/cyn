import { React } from 'react';
import Sidebar from '../Sidebar';
import NavBar from '../NavBar';
import { Row, Col } from 'react-bootstrap';
import Country from './Country';

const Category = () => {

    return (
        <div>
            <NavBar />
            <Sidebar />
            <Row className='con'>
                <Col xs={11}>
                    {/* <h1>Add Country</h1> */}
                    <Country /></Col>
            </Row>
        </div>
    );
}
export default Category;