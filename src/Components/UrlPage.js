// import axios from 'axios';
import { Form } from 'react-bootstrap';
import { useState  } from 'react';

// const url=process.env.REACT_APP_API_ENDPOINT

// const instance = axios.create({baseURL: url});
// export default instance


const UrlPage = () => {
    const [baseURL,setBaseUrl]=useState('Production');
    const changedValue = (event) => {
    var urls = event.target.value;
        console.log(urls);
        setBaseUrl(urls);
       return urls; 
    }  
    // console.log(changedValue);
    localStorage.setItem("url",baseURL);
    
    return (
        <div >
            <Form >
                <Form.Control as="select" onChange={changedValue} >
                    
                    <option value='Production'>Production</option>
                    <option value='Testing'>Testing</option>
                    <option value='Staging'>Staging</option>
                    <option value='Development'>Development</option>
                </Form.Control>
            </Form>

        </div>
    );
}
export default UrlPage;

