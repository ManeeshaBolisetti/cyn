import React from 'react';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-enterprise';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import NavBar from './NavBar';
import { TiEdit } from "react-icons/ti";
import Sidebar from './Sidebar';

import {Row,Col,Dropdown,DropdownButton,} from 'react-bootstrap';
import './Home.css';
var filterParams = {
    comparator: function (filterLocalDateAtMidnight, cellValue) {
      var dateAsString = cellValue;
      if (dateAsString == null) return -1;
      var dateParts = dateAsString.split('/');
      var cellDate = new Date(
        Number(dateParts[2]),
        Number(dateParts[1]) - 1,
        Number(dateParts[0])
      );
      if (filterLocalDateAtMidnight.getTime() === cellDate.getTime()) {
        return 0;
      }
      if (cellDate < filterLocalDateAtMidnight) {
        return -1;
      }
      if (cellDate > filterLocalDateAtMidnight) {
        return 1;
      }
    },
    browserDatePicker: true,
    minValidYear: 2000,
  };
function Home() {
    // const [gridApi,setGridApi]=useState()
    let gridApi;
  const columnDefs= [
    // { headerName: "ID", field: "id" },
    { headerName: "UserName", field: "name",filter: 'agSetColumnFilter'}, 
    {headerName: "MobileNumber",field: "phoneNumber",filter: 'agSetColumnFilter',tooltipField:'name'},
    { headerName: "Catgeory", field: "category",filter: 'agSetColumnFilter',tooltipField:'name' },
    { headerName: "product", field: "product",filter: 'agSetColumnFilter',tooltipField:'name' },
    { headerName: "Description", field: "name",filter: 'agSetColumnFilter',tooltipField:'name' },
   
    { headerName: "RequestDate", 
    field: "date",
    tooltipField:'name',
    filter: 'agDateColumnFilter',
    filterParams: filterParams, 
},
{ headerName: "RequestTime", 
field: "date",
tooltipField:'name',
filter: 'agDateColumnFilter',
filterParams: filterParams, 
},
    { headerName: "status", field: "birthYear",filter: 'agSetColumnFilter',tooltipField:'name' },
    
    {headerName:'Change',field: "name",
 cellRendererFramework:(params)=><div><TiEdit fontSize='30' color='#275788'/></div>
  },
    ]
    const rowData = [
        {id:1, name: "Rahul", age: 19, phoneNumber: 9876543210, birthYear: 2001,date:'24/08/2008',category:'quickNeeds',product:'watch'},
        {id:2, name: "David", age: 17, phoneNumber: 9827654310, birthYear: 2003,date:'23/09/2009' ,category:'quickNeeds',product:'Charger' },
        {id:3, name: "Dan", age: 25, phoneNumber: 9765438210, birthYear: 1995,date:'22/07/2008',category:'quickNeeds' ,product:'fan' },
        {id:4, name: "Rahul", age: 19, phoneNumber: 9876543210, birthYear: 2001,date:'21/06/2008',category:'quickNeeds',product:'fan' },
        {id:5, name: "David", age: 17, phoneNumber: 9827654310, birthYear: 2003,date:'26/04/2008' ,category:'quickNeeds',product:'fan' },
        {id:6, name: "Dan", age: 25, phoneNumber: 9765438210, birthYear: 1995,date:'28/08/2008',category:'Mobile' ,product:'vivo' },
     {id:7, name: "Rahul", age: 19, phoneNumber: 9876543210, birthYear: 2001,date:'29/08/2008',category:'Mobile' ,product:'oppo' },
        {id:8, name: "David", age: 17, phoneNumber: 9827654310, birthYear: 2003,date:'30/08/2008',category:'Mobile',product:'samsung'   },
        {id:9, name: "Dan", age: 25, phoneNumber: 9765438210, birthYear: 1995,date:'31/08/2009' ,category:'Mobile'  ,product:'htc'} ,
        {id:10, name: "Rahul", age: 19, phoneNumber: 9876543210, birthYear: 2001,date:'22/08/2008',category:'Mobile' ,product:'i phone' },
        {id:11, name: "David", age: 17, phoneNumber: 9827654310, birthYear: 2003,date:'21/08/2008',category:'Mobile' ,product:'honor'  },
        {id:3, name: "Dan", age: 25, phoneNumber: 9765438210, birthYear: 1995,date:'26/08/2008',category:'Mobile'  ,product:'realme' }]
    const defaultColDef = { sortable: true,
         editable: true,
          flex: 1,
        //    filter: true,
           minWidth: 200,
           resizable: true,
           floatingFilter: true,
        //  floatingFilter: true
         }
    const onGridReady=(params)=>{
        console.log("grid is ready")
        gridApi=params.api
        // setGridApi(params)
        // fetch("https://jsonplaceholder.typicode.com/comments").then(resp=>resp.json())
        // .then(resp=>{console.log(resp)
        //   params.api.applyTransaction({add:resp})})
        //   params.api.paginationGoToPage(10)
      }
      const onPaginationChange=(pageSize)=>{
        gridApi.paginationSetPageSize(Number(pageSize))
      }
    const onExportClick = () => {
        gridApi.exportDataAsCsv();
    }
    const selectJohnAndKenny = () => {
      const filterInstance = gridApi.getFilterInstance('name'); 
      filterInstance.setModel({
        values: ['Rahul', 'Dan'],
      });
      gridApi.onFilterChanged();
    };
   const selectEverything = () => {
      var instance = gridApi.getFilterInstance('name');
      instance.setModel(null);
      gridApi.onFilterChanged();
    };
    const overlayNoRowsTemplate= "No record found";
    // let date=new Date().getUTCDate();
    let date=new Date().toUTCString();
    console.log(date)
    // setInterval(() => {
    //   console.log('Interval triggered');
    // }, 10000);

    return (
        <div className="App">
            <NavBar/>
            <div >
                
            <Sidebar/>
            {/* <h1 align="center">React-App</h1> */}
            </div>
            <Row className='con'>
           {/* <div className='container con'>  */}
         <Col xs={10}>  <h3>Requests</h3>
            <h6>Home-Requests</h6>
            <DropdownButton id="dropdown-basic-button" title="filters">
  <Dropdown.Item value='1' ><button className='dropdownbutton' onClick={() => selectJohnAndKenny()}>filter</button></Dropdown.Item>
  <Dropdown.Item value='2' ><button className='dropdownbutton' onClick={() => selectEverything()}>All</button></Dropdown.Item>
  <Dropdown.Item value='3' >Something else</Dropdown.Item>
</DropdownButton>
            
                        <div className='row justify-content-md-end'> 
            <button className="export  btn-link " onClick={() => onExportClick()}>Download to Excel</button>
            <select onChange={(e)=>onPaginationChange(e.target.value)}>
        <option  value='10'>10</option>
        <option value='25'>25</option>
        <option value='50'>50</option>
        <option value='100'>100</option>
      </select>
     
      </div> 
      </Col> 
           <Col xs={11}> <div className="ag-theme-alpine" style={{ height: '400px' }}>
            <AgGridReact
            columnDefs={columnDefs}
            rowData={rowData}
            defaultColDef={defaultColDef}
            onGridReady={onGridReady}
            enableBrowserTooltips={true}
            pagination={true}
            paginationPageSize={10}
             overlayNoRowsTemplate={overlayNoRowsTemplate}
            >
              
        </AgGridReact>
            </div></Col>
            {/* </div> */}
            </Row>
            </div>
        
    );
}

export default Home;