import { React, useState,useEffect } from 'react';
// import axios from 'axios';
import { AgGridReact } from 'ag-grid-react';
// import axios from 'axios';
import api from './../Api';
import 'ag-grid-enterprise';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import * as moment from 'moment';
import AddCategory from './AddCategory';
// import image1 from './../../assets/qn.png';
import Search from './../Country/Search';
// import { FcDeleteDatabase, FcEditImage, FcPrint } from "react-icons/fc";
// import { BsFillStopFill } from "react-icons/bs";
import { RiFolderDownloadLine } from "react-icons/ri";
// import { print } from 'react-to-print';
import { Button, Row, Col } from 'react-bootstrap';
// import { FiSearch } from "react-icons/fi";
// import UseFetch from '../UseFetch';
import Refresh from '../Actions/Refresh';
// import Category from '../Country/CountryH';
import EditCategory from './EditCategory';
import customTooltip from '../customTooltip';
// import svgImage from './../../assets/svg.svg';

var filterParams = {
    comparator: function (filterLocalDateAtMidnight, cellValue) {
      var dateAsString = cellValue;
      if (dateAsString == null) return -1;
      var dateParts = dateAsString.split('/');
      var cellDate = new Date(
        Number(dateParts[2]),
        Number(dateParts[1]) - 1,
        Number(dateParts[0])
      );
  
      if (filterLocalDateAtMidnight.getTime() === cellDate.getTime()) {
        return 0;
      }
  
      if (cellDate < filterLocalDateAtMidnight) {
        return -1;
      }
  
      if (cellDate > filterLocalDateAtMidnight) {
        return 1;
      }
    },
    browserDatePicker: true,
    minValidYear: 2000,
  };
const CatgoryT = () => {
    // let category = UseFetch('/admin/categoriesList');
    // const rowData = [cate]
    const [category, setCategory] = useState([]);
    const [categories,setCategories]=useState([]);
    const [gridApi, setGridApi] = useState(null);
    // const [girdColumnApi, setGridColumnApi] = useState(null);
    // const [hideColumn, setHideColumn] = useState(true);
    // const [suppressClickEdit, setSuppressClickEdit] = useState(true);
    // const [edit, setEdit] = useState('Edit');
    // const [rowData, setRowData] = useState(null);

    useEffect(() => {

        api.getCategory()
            .then(response => {
                let category = response.data.data;
                setCategory(category);
                console.log(category);
            });

    }, []);
    let categoryList = [];
    if (category != null && category.length > 0) {
        
        category.map((n) => {
            let cDate = moment(n.createdAt).format('DD/MM/YYYY');
            let uDate = moment(n.updatedAt).format('DD/MM/YYYY');
            let ctime = moment(n.createdAt).format('h:mma');
      let utime = moment(n.updatedAt).format('h:mma');
            let country = n.countries.map((c) => {

                let count = c.countryName
                return count;

                // let countries=c.countryName
                // return countries
            })
            let countryId = n.countries.map((c) => {

                let count = c.countryId
                return count;

                // let countries=c.countryName
                // return countries
            })
            let countryCode = n.countries.map((c) => {

                let count = c.countryCode
                return count;

                // let countries=c.countryName
                // return countries
            })
          return  categoryList.push({
                'countryCode': countryCode,
                'countries': country,
                'countryId': countryId,
                'categoryName': n.categoryName,
                'categoryImage': n.categoryImage,
                'categoryId': n.categoryId,
                'createdAt':cDate,
        'updatedAt':uDate,
        'ctime':ctime,
        'utime':utime,
            })



        })
    }


    

    const AddCategoryHandler = catgory => {
        setCategory(prevExp => {
            return [catgory.data, ...prevExp];
        });
    };
    const EditCategoryHandler = catgory => {
        console.log(catgory);
            setCategory(prevExp => {
                return [catgory.data, ...prevExp];
            });

    };

    const columnDefs = [

        // { headerName: "Flag", field: "cFlag"}, 
        
        {
            field: "categoryName", filter: 'agSetColumnFilter',
            // editable: false,
        },
        {
            field: "categoryImage",
            filter: 'agSetColumnFilter',
            cellRenderer: countryCellRenderer,
            // editable: false,
        },
        {
            field: "countryCode", filter: 'agSetColumnFilter',
            tooltipField: 'countries',
            tooltipComponentParams: { type: 'success' },
            

            // cellRenderer: countryCellRenderer,
        },
        {
            field: "createdAt",filter: 'agDateColumnFilter',cellRenderer: createdDate,
            filterParams: filterParams,
            // editable: false,
          },
          {
            field: "updatedAt", filter: 'agDateColumnFilter',cellRenderer :updatedDate,
            filterParams: filterParams,
          },
        // {
        //     headerName: 'Action',
        //     minWidth: 150,
        //     // cellRenderer: actionCellRenderer,
        //     editable: false,
        //     colId: "action"

        // },


    ]
    const defaultColDef = {
        sortable: true,
        
        flex: 1,
        //    filter: true,
        minWidth: 200,
        resizable: true,
        floatingFilter: true,
        tooltipComponent: 'customTooltip',
        // floatingFilter: true
    }
    const onGridReady = (params) => {
        console.log("grid is ready")
        setGridApi(params);
        // setGridColumnApi(params.columnApi);
    }
    const pageSize = (pageSize) => {
        console.log(pageSize);
        gridApi.api.paginationSetPageSize(Number(pageSize))
    }

    // const showEdit = () => {
    //     girdColumnApi.setColumnVisible("action", false)
    //     setSuppressClickEdit(false);
    //     setEdit('EditMode')
    // }
    // const stopEdit = () => {
    //     setSuppressClickEdit(true);
    //     setEdit('Edit')
    // }
    // const onRemoveSelected = () => {
    //     var selectedData = gridApi.api.getSelectedRows();
    //     var res = gridApi.api.applyTransaction({ remove: selectedData });
    //     printResult(res);
    // };
    const onCellValueChanged = (event) => {
        console.log(
            'onCellValueChanged: ' + event.colDef.field + ' = ' + event.newValue
        );
    };

    const onRowValueChanged = (event) => {
        var data = event.data;
        console.log(
            'onRowValueChanged: (' +
            data.countryName +
            ', ' +
            data.countryCode +
            ', ' +
            data.currency +
            ')'
        );
    };
    const onExportClick = () => {
        gridApi.api.exportDataAsCsv();
    }
    // const onBtPrint = () => {
    //     const api = gridApi.api;
    //     setPrinterFriendly(api);
    //     setTimeout(function () {
    //         print();
    //         setNormal(api);
    //     }, 2000);
    // };
    const onFilterTextChange = (e) => {

        gridApi.api.setQuickFilter(e.target.value);
    }
    const onSelectionChanged = () => {
        var selectedRows = gridApi.api.getSelectedRows();
        console.log(selectedRows);
        setCategories(selectedRows);

        // document.querySelector('#selectedRows').innerHTML =
        //   selectedRows.length === 1 ? selectedRows[0].countryCode : '';
    };


    console.log(categories)

    return (
        <div>
             <div className='d-flex justify-content-between'><h2>Category</h2>
                <Search onChange={onFilterTextChange} /></div>
            <Row>
                <Col xs={10} style={{ display: '-webkit-box' }}>

                    {/* <Button className='add'> */}
                    <Button className='add' > <AddCategory onAddCatgory={AddCategoryHandler} /></Button>
                    <Button className='add' > <EditCategory value={categories} onUpdateCategory={EditCategoryHandler}></EditCategory></Button>

                    {/* </Button> */}
                    {/* <Button className='add' onClick={() => onRemoveSelected()}>Remove <FcDeleteDatabase /></Button> */}
                    {/* <Button className='add' onClick={showEdit}>{edit} <FcEditImage /></Button> */}
                    {/* <Button className='add' onClick={stopEdit}>stop <BsFillStopFill color='darkred' /></Button> */}
                    <Button className='add' onClick={() => onExportClick()}>
                        Export to Excel  <RiFolderDownloadLine color='green' />
                    </Button>

                    <Button className='add' ><Refresh /></Button>
                    {/* <Button className='add' onClick={() => onBtPrint()}>Print <FcPrint /></Button> */}
                    </Col>

                <Col xs={2} >
                    {/* pageSize: <select onChange={(e) => pageSize(e.target.value)}>
                        <option value='10'>10</option>
                        <option value='25'>25</option>
                        <option value='50'>50</option>
                        <option value='100'>100</option>
                    </select> */}
                </Col>

            </Row>

            <div className="ag-theme-alpine" style={{height: '400px'}}>
                <AgGridReact

                    editType="fullRow"
                    rowSelection={'multiple'}
                    onSelectionChanged={onSelectionChanged}
                    onCellValueChanged={onCellValueChanged}
                    onRowValueChanged={onRowValueChanged}
                    // suppressClickEdit={suppressClickEdit}
                    pagination={true}
                    paginationPageSize={25}
                    columnDefs={columnDefs}
                    tooltipShowDelay={1000}
                    tooltipMouseTrack={true}
                    frameworkComponents={{ customTooltip: customTooltip }}
                    rowData={categoryList}
                    defaultColDef={defaultColDef}
                    onGridReady={onGridReady}>



                </AgGridReact>
                <div className='selectbox' style={{
                    padding: '10px',
                    marginTop: '-10px', fontSize: 'small',
                }}> Page Size: <select onChange={(e) => pageSize(e.target.value)}>
                        <option value='10'>10</option>
                        <option value='25'>25</option>
                        <option value='50'>50</option>
                        <option value='100'>100</option>
                    </select></div>
            </div>

        </div>
    );
}
export default CatgoryT;

function countryCellRenderer(params) {
    var image = (params.data.categoryImage);
    var logo =
        // params.data.catName;
        // '<img border="0" width="50" alt="image" height="30" src="' +
        // params.data.categoryImage +
        // '">';
        '<img style="background-color: brown; "  width="50" alt="image" height="30" src=" ' + image +
        '">';
        // '<img style="background-color: brown; "  width="50" alt="image" height="30" src=" ">';
    return (

        '<span style="cursor: default;">' + logo + '</span>'
    );
}

function createdDate(params) {
    var time = (params.data.ctime);
    var date = (params.data.createdAt);
    
    return (
  
        '<span style="cursor: default;">' +date+' ' +time+ '</span>'
    );
  }
  function updatedDate(params) {
    var time = (params.data.utime);
    var date = (params.data.updatedAt);
    
    return (
  
        '<span style="cursor: default;">' +date+' ' +time+ '</span>'
    );
  }
// function printResult(res) {
//     console.log('---------------------------------------');
//     if (res.add) {
//         res.add.forEach(function (rowNode) {
//             console.log('Added Row Node', rowNode);
//         });
//     }
//     if (res.remove) {
//         res.remove.forEach(function (rowNode) {
//             console.log('Removed Row Node', rowNode);
//         });
//     }
//     if (res.update) {
//         res.update.forEach(function (rowNode) {
//             console.log('Updated Row Node', rowNode);
//         });
//     }
// }
// function setPrinterFriendly(api) {
//     // const eGridDiv = document.querySelector('#myGrid');
//     api.setDomLayout('print');
// }
// function setNormal(api) {
//     const eGridDiv = document.querySelector('#myGrid');
//     eGridDiv.style.width = '700px';
//     eGridDiv.style.height = '200px';
//     api.setDomLayout(null);
// }
