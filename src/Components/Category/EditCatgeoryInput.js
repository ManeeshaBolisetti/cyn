import { useState, useRef } from 'react';
import { Form, Button } from 'react-bootstrap';
import Card from '../Card/Card';
import classes from './CategoryInput.Module.css';
import Alert from '@material-ui/lab/Alert';
// import axios from 'axios';
import api from './../Api';
// import { CheckBoxSelection, Inject, MultiSelectComponent } from '@syncfusion/ej2-react-dropdowns';
// import axios from 'axios';

const EditCategoryInput = (props) => {
  const [flash, setFlash] = useState(null);
  const color = props.color;
  const [iflash, setiFlash] = useState(null);
  const [iEflash, setiEFlash] = useState(null);
  const [status, setStatus] = useState();
  const [eStatus, setEStatus] = useState();
  const styles = {
    alert: {
      zIndex: '1500',
    },
  }
  let value = props.val

  let cImage = value.length === 1 ? value[0].categoryImage : '';
  // const [updateImage,setUpdateImage]=useState('');
  const Invalid = '';
  const [valid, setValid] = useState(Invalid);
  // const image=cImage;
  const [image, setImage] = useState(cImage);
  const catNameRef = useRef();
  const catLogoRef = useRef();

  console.log(value)
  let cName = value.length === 1 ? value[0].categoryName : '';
  // let cImage=value.length === 1 ? value[0].categoryImage: '';
  let cId = value.length === 1 ? value[0].categoryId : '';
  // console.log(cImage,cId)


  //   const countriesRef = useRef();

  //   var sportsData = [
  //     { id: 1, country: 'IND' },
  //     { id: 2, country: 'NZ' },
  //     { id: 3, country: 'AUS' },

  //   ];


  //   useEffect(() => {

  //     axios.get('https://appdev.connectyourneed.com/admin/countriesList')
  //         .then(response =>
  //           {const country=response.data.data;
  //             console.log(country);

  //           setCountries(country)});

  // // empty dependency array means this effect will only run once (like componentDidMount in classes)
  // }, []);
  // const fields = { text: 'countryName', value: 'countryName' };
  const submitHandler = (event) => {
    event.preventDefault();
    setFlash(true);
    setTimeout(() => {
      setFlash(null);
      // setFlashE(null);
    }, 5000);
    const catName = catNameRef.current.value;
    const catLogo = catLogoRef.current.files[0];
    const catId = cId
    if (catName.trim().length === 0) {
      setValid('Invalid CategoryName or Image')
      // return console.log('Invalid CategoryName or Image')
    }
    else {
      setValid('')
    }
    const categoryData = {
      categoryName: catName,
      categoryImage: image,
      categoryId: catId,
      image: catLogo,

    };
    console.log(categoryData);
    props.onSaveCatgoryData(categoryData);

  }

  const RemoveImage = () => {
    const RemoveImage = {
      imageUrl: cImage,
    };
    console.log(RemoveImage)
    // axios.post('admin/removeBlob', RemoveImage)
    api.RemoveBlob(RemoveImage)
      .then(response => {
        setStatus({ data: response.data.message });
        setiFlash(true);
        setTimeout(() => {
          setiFlash(null);
          // setFlashE(null);
        }, 5000);
        console.log(response.data.message)
        setImage('');

        // window.location.reload();
      })
      .catch(error => {
        if (error.response) {
          setEStatus(error.response.data.message);
          setiEFlash(true);
          setTimeout(() => {
            setiEFlash(null);
            // setFlashE(null);
          }, 5000);
        } else {
          setEStatus(error.message);
          setiEFlash(true);
          setTimeout(() => {
            setiEFlash(null);
            // setFlashE(null);
          }, 5000);

        }

      });



  }
  const handleChange = (event) => {

    //   setImage( {
    //    file: URL.createObjectURL(event.target.files[0])
    //  })

  }

  return (
    <div className={classes.input}>

      <Card className={classes.modal}>
        {/* <ul>
        { countries.map(person => <li>{person.countryName}</li>)}
      </ul> */}
        <header className={classes.header}>
          <h2>{props.name}</h2>
        </header>
        {
          flash
            ? <Alert style={styles.alert} severity={color}>{props.stat}</Alert>
            : null
        }
        {
          iflash
            ? <Alert style={styles.alert} severity="Success">{status.data}</Alert>
            : null
        }
        {
          iEflash
            ? <Alert style={styles.alert} severity="error">{eStatus.data}</Alert>
            : null
        }
        <button onClick={RemoveImage}>RemoveImage</button>
        <Form onSubmit={submitHandler}> <div className={classes.content}>

          <Form.Control
            type='text'
            placeholder={props.cName}
            defaultValue={cName}
            //  value={enterdCountry}
            ref={catNameRef}

          // onChange={cNameChangeHandler}
          />
          <Form.Control
            type='file'
            placeholder={props.cName}
            // defaultValue={cImage}
            //  value={enterdCountry}
            ref={catLogoRef}
            onChange={handleChange}
          // onChange={cNameChangeHandler}
          />

          <img className={classes.image} src={image}

            alt='previw' />
          <h4>{valid}</h4>
        </div>
          <footer className={classes.actions}>
            <Button className={classes.btn}
              onClick={props.onClose}
            >cancel</Button>
            <Button
              type='submit'
            // onClick={props.onClose}
            >Update</Button>
          </footer></Form>
      </Card>
    </div>
  );
}
export default EditCategoryInput;