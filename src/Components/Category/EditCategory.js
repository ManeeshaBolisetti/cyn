import { React, useState } from 'react';
import EditCategoryInput from './EditCatgeoryInput';
// import {  Button } from 'react-bootstrap';
import { TiFolderAdd } from 'react-icons/ti';
// import axios from 'axios';
import api from './../Api';


const EditCategory = (props) => {
    let statu = '';
    let value=props.value
    const [color,setColor]=useState('');
    const [showForm, setshowForm] = useState(false);
    const [status, setStatus] = useState(statu);
    console.log(value)
    const onShowForm = () => {
        if(value.length===1){
        setshowForm(true);
    }else{
        alert('please select edit Row')
    }
    }
    const onHideForm = () => {
        setshowForm(false);
    }
    const saveCatgoryDataHandler = (enterdCatgoryData) => {
        const catgoryData = {
            ...enterdCatgoryData,

        };
        console.log(catgoryData);
       
        let formData = new FormData();
        formData.append('categoryId',enterdCatgoryData.categoryId);
        formData.append('categoryName', enterdCatgoryData.categoryName);
        formData.append('categoryImage', enterdCatgoryData.categoryImage);
        formData.append('image',enterdCatgoryData.image);
        // console.log();
        // let url = '/Admin/updateCategory'
        // axios.post(url, formData)
        api.updateCatgory(formData)
        .then(response => {
            setStatus({ data:response.data.message });
        console.log(response.data.message)
        setColor('success')
        // setFlash(true);
        setTimeout(() => {
            setColor('')
            setStatus('')
          setshowForm(false)
          // setFlashE(null);
        }, 3000);
        
        
        props.onUpdateCategory({data:response.data.data});
        // window.location.reload();
         } )
        .catch(error => {
            if (error.response) {
                setStatus({ data:(error.response.data.message )});
                setColor('error')
                // console.log(error.response.data.message);
            }else{
                setStatus({ data: error.message });
                setColor('error')
                console.error('There was an error!', error);

            }
           
        });
       
        // fetch(url, {

        //     method: "POST",
        //     // headers: { 'Content-Type': 'multipart/form-data' },
        //     body: formData
        // }).then((response) => {
        //     if (!response.ok) {
        //         if (response.status === 500) {
        //             setStatus('Alredy existed');
        //         } else
        //             setStatus(response.message)

        //         //    setStatus(response.status) ;
        //     }
        //     else return response.json();
        // })
        //     .then((data) => {
        //         if (data) {
        //             setStatus(data.message);
        //             // console.log(data);

        //             // props.onAddCountry(data);

        //         }
        //         // console.log(data);
        //         // console.log(data.message);

        //     })

        // console.log(formData)
        // props.onUpdateCategory(catgoryData);
        // console.log(catgoryData);
        // setshowForm(false);
    };
    return (
        <div>
            {showForm && <EditCategoryInput
                name='Update Category'
                cName='Catgory Name'
                stat={status.data}
                color={color}
                onClose={onHideForm}
                onSaveCatgoryData={saveCatgoryDataHandler}
                val={value}
            />}
            <div  onClick={onShowForm}><TiFolderAdd size='20' color='green' /> update </div>
        </div>
    );
}
export default EditCategory;