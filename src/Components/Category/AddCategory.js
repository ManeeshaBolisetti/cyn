import { React, useState } from 'react';
import CategoryInput from './CategoryInput';
// import {  Button } from 'react-bootstrap';
import { TiFolderAdd } from 'react-icons/ti';
// import axios from 'axios';
import api from './../Api';

const AddCategory = (props) => {
    let statu = '';
    const [showForm, setshowForm] = useState(false);
    const [status, setStatus] = useState(statu);
    const [color,setColor]=useState('');
    // const [errStatus,setErrStatus]=useState('');
    
    const onShowForm = () => {
        setshowForm(true);
    }
    const onHideForm = () => {
        setshowForm(false);
    }
    const saveCatgoryDataHandler = (enterdCatgoryData) => {
        // const catgoryData = {
        //     ...enterdCatgoryData,

        // };
        let formData = new FormData();
        formData.append('categoryName', enterdCatgoryData.categoryName);
        formData.append('categoryImage', enterdCatgoryData.categoryImage);
        // console.log();
        // let url = '/Admin/addCategory'
        // axios.post(url, formData)
        api.addCategory(formData)
        .then(response => {
            setStatus({ data:response.data.message });
            props.onAddCatgory({data:response.data.data});
        console.log(response.data.message)
        setColor('success')
        // setFlash(true);
        setTimeout(() => {
            setColor('')
            setStatus('')
          setshowForm(false)
          // setFlashE(null);
        }, 3000);
    
        // window.location.reload();
         } )
        .catch(error => {
            if (error.response) {
                setStatus({ data:(error.response.data.message )});
                setColor('error')
                // console.log(error.response.data.message);
            }else{
                setStatus({ data:(error.response.data.message )});
                setColor('error')
                // console.error('There was an error!', error);

            }
           
        });
        
        
    };
    return (
        <div>
            {showForm && <CategoryInput
                name='Add Category'
                cName='Catgory Name'
                stat={status.data}
                color={color}
                // err={errStatus}

                onClose={onHideForm}
                onSaveCatgoryData={saveCatgoryDataHandler}
            />}
            <div   onClick={onShowForm}><TiFolderAdd size='20' color='green' /> Add Category </div>
        </div>
    );
}
export default AddCategory;