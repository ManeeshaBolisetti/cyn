import { useState, useRef } from 'react';
import { Form, Button } from 'react-bootstrap';
import Card from '../Card/Card';
import classes from './CategoryInput.Module.css';
import image1 from './../../assets/Dashbord.png';
// import { HiInformationCircle } from "react-icons/hi";
import Alert from '@material-ui/lab/Alert';
// import { CheckBoxSelection, Inject, MultiSelectComponent } from '@syncfusion/ej2-react-dropdowns';
// import axios from 'axios';

const CategoryInput = (props) => {
  const [flash, setFlash] = useState(null);
  const color=props.color;
  const styles = {
    alert: {
      zIndex: '1500',
    },
  }
  let img={image1}
  // const Invalid = '';
  // const [valid, setValid] = useState(Invalid);
  const [image,setImage]=useState({file:img});
  // const [countries, setCountries] = useState([]);
  const catNameRef = useRef();
  const catLogoRef = useRef();

  // setImage( URL.createObjectURL(catLogoRef))
//   const countriesRef = useRef();
    
//   var sportsData = [
//     { id: 1, country: 'IND' },
//     { id: 2, country: 'NZ' },
//     { id: 3, country: 'AUS' },

//   ];
 

//   useEffect(() => {
   
//     axios.get('https://appdev.connectyourneed.com/admin/countriesList')
//         .then(response =>
//           {const country=response.data.data;
//             console.log(country);
            
//           setCountries(country)});

// // empty dependency array means this effect will only run once (like componentDidMount in classes)
// }, []);
// const fields = { text: 'countryName', value: 'countryName' };
  const submitHandler = (event) => {
    event.preventDefault();
    setFlash(true);
    setTimeout(() => {
      setFlash(null);
      
      // setFlashE(null);
    }, 5000);
    const catName = catNameRef.current.value;
    const catLogo = catLogoRef.current.files[0];
    // if (catName.trim().length === 0) {
    //   setValid('Invalid CategoryName or Image')
    //   // return console.log('Invalid CategoryName or Image')
    // }
    // else{
    //   setValid('')
    // }
    const categoryData = {
      categoryName: catName,
      categoryImage: catLogo,

    };
    console.log(categoryData);
    props.onSaveCatgoryData(categoryData);

  }
 const handleChange=(event)=> {
    
     setImage( {
      file: URL.createObjectURL(event.target.files[0])
    })
    
  }

  return (
    <div className={classes.input}>
      
      <Card className={classes.modal}>
      {/* <ul>
        { countries.map(person => <li>{person.countryName}</li>)}
      </ul> */}
        <header className={classes.header}>
          <h2>{props.name}</h2>
        </header>
        {
        flash
        ? <Alert style={styles.alert} severity={color}>{props.stat}</Alert>
        : null
      }
      
        <Form onSubmit={submitHandler}> <div className={classes.content}>

          <Form.Control
            type='text'
            placeholder={props.cName}
            //  value={enterdCountry}
            ref={catNameRef}

          // onChange={cNameChangeHandler}
          />
          <Form.Control
            type='file'
            placeholder={props.cName}
            //  value={enterdCountry}
            ref={catLogoRef}

          onChange={ handleChange}
          />
          {/* <MultiSelectComponent id="checkbox" className='dropdown '
            dataSource={countries}
            // fields={fields}
            fields={fields}
            ref={countriesRef}
            placeholder="Select Countries"
            mode="CheckBox"
            selectAllText="Select All"
            unSelectAllText="unSelect All"
            showSelectAll={true}>
            <Inject services={[CheckBoxSelection]} />
          </MultiSelectComponent> */}
    <img className={classes.image}  src={image.file}
           alt='previw' />
{/* <h4>{valid}</h4> */}
        </div>
          <footer className={classes.actions}>
            <Button className={classes.btn}
              onClick={props.onClose}
            >cancel</Button>
            <Button
              type='submit'
            >Add</Button>
          </footer></Form>
      </Card>
    </div>
  );
}
export default CategoryInput;