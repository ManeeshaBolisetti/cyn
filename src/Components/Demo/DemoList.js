import { React, useState,useEffect} from 'react';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import EditDemo from './EditDemo';
import AddDemo from './AddDemo';
import * as moment from 'moment';
// import { FcDeleteDatabase, FcEditImage, FcPrint } from "react-icons/fc";
// import { BsFillStopFill } from "react-icons/bs";
import { RiFolderDownloadLine } from "react-icons/ri";
// import { print } from 'react-to-print';
import { Button, Row, Col } from 'react-bootstrap';
import customToolTip from '../customTooltip';
import Search from './../Country/Search';
// import UseFetch from '../UseFetch';
import Refresh from '../Actions/Refresh';
// import axios from 'axios';
import api from './../Api';
var filterParams = {
  comparator: function (filterLocalDateAtMidnight, cellValue) {
    var dateAsString = cellValue;
    if (dateAsString == null) return -1;
    var dateParts = dateAsString.split('/');
    var cellDate = new Date(
      Number(dateParts[2]),
      Number(dateParts[1]) - 1,
      Number(dateParts[0])
    );

    if (filterLocalDateAtMidnight.getTime() === cellDate.getTime()) {
      return 0;
    }

    if (cellDate < filterLocalDateAtMidnight) {
      return -1;
    }

    if (cellDate > filterLocalDateAtMidnight) {
      return 1;
    }
  },
  browserDatePicker: true,
  minValidYear: 2000,
};
const DemoList = () => {
  
  const [demo, setDemo] = useState([]);
  const [demoL,setDemoL]=useState([])
  useEffect(() => {

    api.getDemo()
      .then(response => {
        let country = response.data.data;
        setDemoL(country);
      });

  }, []);
  // const [categories, setCategories] = useState([]);
  const [gridApi, setGridApi] = useState(null);
  // const [girdColumnApi, setGridColumnApi] = useState(null);
  // const [suppressClickEdit, setSuppressClickEdit] = useState(true);
  // const [edit, setEdit] = useState('Edit');
  let demoList=[];
  if (demoL != null && demoL.length >0){
    demoL.map((n)=>{
    let cDate = moment(n.createdAt).format('DD/MM/YYYY');
      let uDate = moment(n.updatedAt).format('DD/MM/YYYY');
      let ctime = moment(n.createdAt).format('h:mma');
      let utime = moment(n.updatedAt).format('h:mma');
       let country=n.countries.map((c)=>{
         
      
            
            let count=c.countryName
            return count;
           
            // let countries=c.countryName
            // return countries
        })
        let countryI=n.countries.map((c)=>{
            
          let countI=c.countryId
          return countI;
         
          // let countries=c.countryName
          // return countries
      })
      return  demoList.push({
             'countryId':countryI,
            'countries':country,
            'title':n.title,
            'description':n.description,
            'demoLinkId':n.demoLinkId,
            'language':n.language,
            'demoLink':n.demoLink,
            
            'createdAt':cDate,
            'updatedAt':uDate,
            'ctime':ctime,
        'utime':utime,
        })
       
        

    })}
   
  const AddDemoHandler = demo => {
    console.log(demo);
    // console.log('in App.js');
    setDemoL(prevExp => {
        return [demo, ...prevExp];
    });
  };
  const EditDemoHandler = demo => {
    console.log(demo);
    setDemoL(prevExp => {
      return [demo, ...prevExp];
  });
   

  };
  const columnDefs = [
    {
      headerName: 'Action',
      minWidth: 10,
      // cellRenderer: actionCellRenderer,
      editable: false,
      colId: "action", hide: true,

    },


    // { headerName: "Flag", field: "cFlag"}, 
    
    {
      field: "title",
       filter: 'agSetColumnFilter',
      // editable: false,
    },
    {
      field: "description",
       filter: 'agSetColumnFilter',
      // editable: false,
    },
    {
      field: "language",
       filter: 'agSetColumnFilter',
      // editable: false,
    },
    {
      field: "demoLink",
       filter: 'agSetColumnFilter',
      // editable: false,
    },
    {
      field: "countries", 
      
      tooltipField: 'countries',
      tooltipComponentParams: { type: 'success' },
       filter: 'agSetColumnFilter',
      // cellRenderer: countryCellRenderer,
    },
    {
        field: "createdAt",filter: 'agDateColumnFilter',cellRenderer: createdDate,
        filterParams: filterParams,
        // editable: false,
      },
      {
        field: "updatedAt", filter: 'agDateColumnFilter',cellRenderer :updatedDate,
        filterParams: filterParams,
      },

  ]
  const defaultColDef = {
    sortable: true,
    
    flex: 1,
    //    filter: true,
    minWidth: 200,
    resizable: true,
    floatingFilter: true,
    tooltipComponent: 'customTooltip',
    // floatingFilter: true
  }
  const onGridReady = (params) => {
    console.log("grid is ready")
    setGridApi(params);
    // setGridColumnApi(params.columnApi);
  }
  const pageSize = (pageSize) => {
    gridApi.api.paginationSetPageSize(Number(pageSize))
  }
  // const showEdit = () => {
  //   girdColumnApi.setColumnVisible("action", false)
  //   setSuppressClickEdit(false);
  //   setEdit('EditMode')
  // }
  // const stopEdit = () => {
  //   setSuppressClickEdit(true);
  //   setEdit('Edit')
  // }
  // const onRemoveSelected = () => {
  //   var selectedData = gridApi.api.getSelectedRows();
  //   var res = gridApi.api.applyTransaction({ remove: selectedData });
  //   printResult(res);
  // };
  const onCellValueChanged = (event) => {
    console.log(
      'onCellValueChanged: ' + event.colDef.field + ' = ' + event.newValue
    );
  };

 
  const onExportClick = () => {
    gridApi.api.exportDataAsCsv();
  }
  // const onBtPrint = () => {
  //   const api = gridApi.api;
  //   setPrinterFriendly(api);
  //   setTimeout(function () {
  //     print();
  //     setNormal(api);
  //   }, 2000);
  // };
  const onFilterTextChange = (e) => {

    gridApi.api.setQuickFilter(e.target.value);
  }
  const onSelectionChanged = () => {
    var selectedRows = gridApi.api.getSelectedRows();
    console.log(selectedRows);
    setDemo(selectedRows);
    
    // document.querySelector('#selectedRows').innerHTML =
    //   selectedRows.length === 1 ? selectedRows[0].countryCode : '';
  };
  return (
    <div><div className='d-flex justify-content-between'><h2>Demo</h2>
      <Search onChange={onFilterTextChange} /></div>
      <Row>
        <Col xs={10} style={{ display: 'flex' }}>

        <Button className='add' ><AddDemo onAddDemo={AddDemoHandler} /></Button>
          <Button className='add' ><EditDemo value={demo}  onUpdateDemo={EditDemoHandler}></EditDemo></Button>

          <Button className='add' onClick={() => onExportClick()}>
            Export to Excel  <RiFolderDownloadLine color='green' />
          </Button >
          <Button className='add' ><Refresh /></Button>
          {/* <Button className='add' onClick={() => onBtPrint()}>Print <FcPrint /></Button > */}
          </Col>

        <Col xs={2}>

        </Col>

      </Row>
      <div id="myGrid" className="ag-theme-alpine" style={{ height: '400px' }}>
        <AgGridReact

          // onCellClicked={onCellClicked}
          editType="fullRow"
          rowSelection={'multiple'}
          onCellValueChanged={onCellValueChanged}
         
          onSelectionChanged={onSelectionChanged}
          // suppressClickEdit={suppressClickEdit}
          pagination={true}
          paginationPageSize={25}
          
          tooltipShowDelay={1000}
          tooltipMouseTrack={true}
          frameworkComponents={{ customTooltip: customToolTip }}
          // paginationAutoPageSize={true}
          columnDefs={columnDefs}
          rowData={demoList}
          defaultColDef={defaultColDef}
          onGridReady={onGridReady}>



        </AgGridReact>
        <div className='selectbox' style={{
          padding: '10px',
          marginTop: '-10px', fontSize: 'small',
        }}> Page Size: <select onChange={(e) => pageSize(e.target.value)}>
            <option value='10'>10</option>
            <option value='25'>25</option>
            <option value='50'>50</option>
            <option value='100'>100</option>
          </select></div>
      </div>

    </div>
  );
}
export default DemoList;

function createdDate(params) {
  var time = (params.data.ctime);
  var date = (params.data.createdAt);
  
  return (

      '<span style="cursor: default;">' +date+' ' +time+ '</span>'
  );
}
function updatedDate(params) {
  var time = (params.data.utime);
  var date = (params.data.updatedAt);
  
  return (

      '<span style="cursor: default;">' +date+' ' +time+ '</span>'
  );
}
// function countryCellRenderer(params) {
//     var image = (params.data.categoryImage);
//     var logo =
//         // params.data.catName;
//         // '<img border="0" width="50" alt="image" height="30" src="' +
//         // params.data.categoryImage +
//         // '">';
//         '<img border="0" width="50" alt="image" height="30" src=" ' + image +
//         '">';
//     return (

//         '<span style="cursor: default;">' + logo + '</span>'
//     );
// }

// function printResult(res) {
//   console.log('---------------------------------------');
//   if (res.add) {
//     res.add.forEach(function (rowNode) {
//       console.log('Added Row Node', rowNode);
//     });
//   }
//   if (res.remove) {
//     res.remove.forEach(function (rowNode) {
//       console.log('Removed Row Node', rowNode);
//     });
//   }
//   if (res.update) {
//     res.update.forEach(function (rowNode) {
//       console.log('Updated Row Node', rowNode);
//     });
//   }
// }
// function setPrinterFriendly(api) {
//   // const eGridDiv = document.querySelector('#myGrid');
//   api.setDomLayout('print');
// }
// function setNormal(api) {
//   const eGridDiv = document.querySelector('#myGrid');
//   eGridDiv.style.width = '700px';
//   eGridDiv.style.height = '200px';
//   api.setDomLayout(null);
// }