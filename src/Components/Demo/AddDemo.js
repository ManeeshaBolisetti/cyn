import {React, useState} from 'react';
import DemoInput from './DemoInput';
import { Form } from 'react-bootstrap';
import { TiFolderAdd } from 'react-icons/ti';
// import api from './../AxiosP';
// import axios from 'axios';
import api from './../Api';
const AddDemo=(props)=>{
    let statu='';
    const [showForm, setshowForm] = useState(false);
    const [status,setStatus]=useState(statu);
    const [color,setColor]=useState('');
    
    
    const saveDemoDataHandler = (enterdDemoData) => {
        const DemoData = {
            ...enterdDemoData,
            
            // id: Math.random().toString()
        };
        console.log(DemoData)
        api.addDemo(DemoData)
        .then(response => {
            setStatus({ data:response.data.message });
        props.onAddDemo(response.data.data);
        setColor('success')
        setTimeout(() => {
            setColor('')
            setStatus('')
          setshowForm(false)
        }, 3000);
         } )
        .catch(error => {
            if (error.response) {
                setStatus({ data:(error.response.data.message )});
                setColor('error')
            }else{
                setStatus({ data: error.message });
                setColor('error')
                console.error('There was an error!', error);

            }
           
        });
           };
    const onShowForm = () => {
        setshowForm(true);
    }
    const onHideForm = () => {
        setshowForm(false);
        setStatus("Add A Gender");
    }
    return(
        <div>
           {showForm &&
             <DemoInput
            name='Add Demo'
            // cName='Country Name'
            countries='select countries'
            stat={status.data}
                color={color}
            onClose={onHideForm}
            onSaveDemoData={saveDemoDataHandler}
            // onCancel={stopEditHandler}
             />}
            
                {/* <h4 >Admin/Add Merchant</h4> */}

                <Form><div onClick={onShowForm}><TiFolderAdd fontSize='20' color='green' /> Add Demo </div></Form>
 
        </div>
    );
}
export default AddDemo;