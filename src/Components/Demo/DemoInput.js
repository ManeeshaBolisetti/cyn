import { useState, useRef } from 'react';
import { Form, Button } from 'react-bootstrap';
// import { form } from 'react-dom-factories';
import UseFetch from './../UseFetch';
import Card from '../Card/Card';
import classes from './DemoInput.Module.css';
import Alert from '@material-ui/lab/Alert';
import { CheckBoxSelection, Inject, MultiSelectComponent } from '@syncfusion/ej2-react-dropdowns';

const DemoInput = (props) => {
  const [flash, setFlash] = useState(null);
  const color = props.color;
  const styles = {
    alert: {
      zIndex: '1500',
    },
  }

  const titleRef = useRef();
  const descriptionRef = useRef();
  const countryRef = useRef();
  const languageRef = useRef();
  const linkRef = useRef();
  // useEffect(() => {

  //   axios.get('https://appdev.connectyourneed.com/admin/countriesList')
  //     .then(response => {
  //       const country = response.data.data;
  //       // console.log(country);

  //       setCountries(country)
  //     });

  //   // empty dependency array means this effect will only run once (like componentDidMount in classes)
  // }, []);
  const countries = UseFetch('/admin/countriesList')
  const fields = { text: 'countryName', value: 'countryId' };
  const submitHandler = (event) => {
    event.preventDefault();
    setFlash(true);
    setTimeout(() => {
      setFlash(null);

      // setFlashE(null);
    }, 5000);
    const title = titleRef.current.value;
    const description = descriptionRef.current.value;
    const language = languageRef.current.value;
    const link = linkRef.current.value;
    const country = countryRef.current.value;



    const demoData = {
      title: title,
      description: description,
      language: language,
      link: link,
      countries: country,
    };
    console.log(demoData);
    props.onSaveDemoData(demoData);


  }
  return (
    <div className={classes.input}>
      <Card className={classes.modal}>
        <header className={classes.header}>
          <h2>{props.name}</h2>
        </header>
        {
          flash
            ? <Alert style={styles.alert} severity={color}>{props.stat}</Alert>
            : null
        }
        <Form onSubmit={submitHandler} > <div className={classes.content}>

          <Form.Control
            type='text'
            placeholder='title'
            //  value={enterdCountry}
            ref={titleRef}
          // onChange={cNameChangeHandler}
          />
          <br />


          <Form.Control as="textarea"
            rows={3}
            placeholder='description'
            ref={descriptionRef}
          />
          <br />
          <Form.Control
            type='text'
            placeholder='language'
            //  value={enterdCode}
            ref={languageRef}
          // onChange={codeChangeHandler}
          /><br />
          <Form.Control
            type='text'
            placeholder='link'
            //  value={enterdCountry}
            ref={linkRef}
          // onChange={cNameChangeHandler}
          /><br />
          <MultiSelectComponent id="checkbox" className='dropdown '
            dataSource={countries}
            // fields={fields}
            fields={fields}
            ref={countryRef}
            placeholder={props.countries}
            mode="CheckBox"
            selectAllText="Select All"
            unSelectAllText="unSelect All"
            showSelectAll={true}>
            <Inject services={[CheckBoxSelection]} />
          </MultiSelectComponent><br />

          {/* <h4>{valid}</h4> */}

        </div>
          <footer className={classes.actions}>
            <Button className={classes.btn}
              onClick={props.onClose}
            >cancel</Button>
            <Button
              type='submit'
            >Add</Button>
          </footer></Form>
      </Card>
    </div>
  );
}
export default DemoInput;