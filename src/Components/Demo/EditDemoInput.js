import { useState, useRef } from 'react';
import { Form, Button } from 'react-bootstrap';
// import { form } from 'react-dom-factories';
import UseFetch from './../UseFetch';
import Card from '../Card/Card';
import classes from './DemoInput.Module.css';
import Alert from '@material-ui/lab/Alert';
import { CheckBoxSelection, Inject, MultiSelectComponent } from '@syncfusion/ej2-react-dropdowns';

const DemoInput = (props) => {
  const [flash, setFlash] = useState(null);
  const color=props.color;
  const styles = {
    alert: {
      zIndex: '1500',
    },
  }
    let value=props.val
  const Invalid = '';
  const valid = (Invalid);
  const titleRef = useRef();
  const descriptionRef = useRef();
  const countryRef = useRef();
  const languageRef=useRef();
  const linkRef=useRef();
  console.log(value);
  // let dCountries=value.length === 1 ? value[0].countries: '';
  let dCountryId=value.length === 1 ? value[0].countryId: ''
  // let cImage=value.length === 1 ? value[0].categoryImage: '';
  let dtitle=value.length === 1 ? value[0].title: '';
  let ddescription=value.length === 1 ? value[0].description: '';
  let dlanguage=value.length === 1 ? value[0].language: '';
  let dlink=value.length === 1 ? value[0].demoLink: '';

  let ddemoLinkId=value.length === 1 ? value[0].demoLinkId: '';
//   let dStatus=value.length === 1 ? value[0].status: '';
  // console.log(dCountries,dMin,dMax,dId)
  // useEffect(() => {

  //   axios.get('https://appdev.connectyourneed.com/admin/countriesList')
  //     .then(response => {
  //       const country = response.data.data;
  //       // console.log(country);

  //       setCountries(country)
  //     });

  //   // empty dependency array means this effect will only run once (like componentDidMount in classes)
  // }, []);
  const countries=UseFetch('/admin/countriesList')
  // const fields = { text: 'countryName', value: 'countryId' };
  // const colorValues= ([dCountryId]);
  const fields = { text: 'countryName', value: 'countryId' };
   let colorValues= dCountryId.map((n)=>{
      let colour=n;
      return colour
  });
  const onTagging = (e) => {
    // set the current selected item text as class to chip element.
    e.setClass(e.itemData[fields.text].toLowerCase());
};
  console.log(colorValues)
   const submitHandler = (event) => {
    setFlash(true);
    setTimeout(() => {
      setFlash(null);
      
      // setFlashE(null);
    }, 5000);
    event.preventDefault();
    const title = titleRef.current.value;
    const description = descriptionRef.current.value;
    const language=languageRef.current.value;
    const link=linkRef.current.value;
    const country = countryRef.current.value;
    

    const demoData = {
      title:title,
      description: description,
        language:language,
        link:link,
        countries: country,
        demoLinkId:ddemoLinkId,
    };
    console.log(demoData);
    props.onSaveDemoData(demoData);
    // minDistanceRef.current.value = '';
    // maxDistanceRef.current.value='';
    // countryRef.current.value = '';

  }
  return (
    <div className={classes.input}>
      <Card className={classes.modal}>
        <header className={classes.header}>
          <h2>{props.name}</h2>
        </header>
        {
        flash
        ? <Alert style={styles.alert} severity={color}>{props.stat}</Alert>
        : null
      }
        <Form onSubmit={submitHandler} > <div className={classes.content}>

          <Form.Control
            type='text'
            placeholder='minimunm distance'
            defaultValue={dtitle}
            //  value={enterdCountry}
            ref={titleRef}
          // onChange={cNameChangeHandler}
          />
          <br />
          
          <Form.Control as="textarea"
            rows={3} placeholder='description'
            defaultValue={ddescription}
            ref={descriptionRef}
          />
          <Form.Control
            type='text'
            placeholder='language'
            defaultValue={dlanguage}
            ref={languageRef}
         
          />
          <Form.Control
            type='text'
            placeholder='link'
            defaultValue={dlink}
            ref={linkRef}
         
          />
          
          <MultiSelectComponent id="chip-customization" className='dropdown '
                dataSource={countries}
                
                // fields={fields}
                fields={fields}
                ref={countryRef}
                placeholder={props.countries}
                mode="CheckBox"
                selectAllText="Select All"
                unSelectAllText="unSelect All"
                tagging={onTagging}
                value={colorValues}
                // tagging={onTagging = onTagging}
                showSelectAll={true}>
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent><br />
          
          <h4>{valid}</h4>
         
        </div>
          <footer className={classes.actions}>
            <Button className={classes.btn}
              onClick={props.onClose}
            >cancel</Button>
            <Button
              type='submit'
            >Update</Button>
          </footer></Form>
      </Card>
    </div>
  );
}
export default DemoInput;