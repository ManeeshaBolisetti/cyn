import {React, useState} from 'react';
import EditDemoInput from './EditDemoInput';
import { Form } from 'react-bootstrap';
import { TiFolderAdd } from 'react-icons/ti';
// import axios from 'axios';
import api from './../Api';
// import axios from './../AxiosP';
const EditDemo=(props)=>{
    let statu='';
    let value=props.value
    const [showForm, setshowForm] = useState(false);
    const [status,setStatus]=useState(statu);
    const [color,setColor]=useState('');
    
    
    const saveDemoDataHandler = (enterdDemoData) => {
        const DemoData = {
            ...enterdDemoData,
            
            // id: Math.random().toString()
        };
        console.log(DemoData)
        api.UpdateDemo(DemoData)
        .then(response => {
            setStatus({ data:response.data.message });
        console.log(response.data.message)
      
        props.onUpdateDemo(response.data.data);
        setColor('success')
        // setFlash(true);
        setTimeout(() => {
            setColor('')
            setStatus('')
          setshowForm(false)
          // setFlashE(null);
        }, 3000);
        // window.location.reload();
         } )
        .catch(error => {
            if (error.response) {
                setStatus({ data:(error.response.data.message )});
                setColor('error')
                // console.log(error.response.data.message);
            }else{
                setStatus({ data: error.message });
                console.error('There was an error!', error);
                setColor('error')

            }
           
        });
        console.log(DemoData)
       

        // setshowForm(false);
    };
    const onShowForm = () => {
        if(value.length===1){
        setshowForm(true);
    }else{
        alert('please select edit Row')
    }
    }
   
    const onHideForm = () => {
        setshowForm(false);
        // setStatus("update A Gender");
    }
    return(
        <div>
           {showForm &&
             <EditDemoInput
            name='Edit Demo'
            // cName='Country Name'
            countries='select countries'
            stat={status.data}
                color={color}
            onClose={onHideForm}
            onSaveDemoData={saveDemoDataHandler}
            val={value}
            // onCancel={stopEditHandler}
             />}
            
                {/* <h4 >Admin/Add Merchant</h4> */}

                <Form><div onClick={onShowForm}><TiFolderAdd fontSize='20' color='green' />
                 Update </div></Form>
 
        </div>
    );
}
export default EditDemo;