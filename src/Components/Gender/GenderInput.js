import { useState, useRef } from 'react';
import { Form, Button } from 'react-bootstrap';
import Card from '../Card/Card';
import classes from './GenderInput.Module.css';
import Alert from '@material-ui/lab/Alert';
import { CheckBoxSelection, Inject, MultiSelectComponent } from '@syncfusion/ej2-react-dropdowns';
import UseFetch from '../UseFetch';
const GenderInput = (props) => {
  const [flash, setFlash] = useState(null);
  const color=props.color;
  const styles = {
    alert: {
      zIndex: '1500',
    },
  }

  // const [countries, setCountries] = useState([]);
//   const formRef = useRef();
  const genderRef = useRef();
  const countryRef = useRef();
  let countries = UseFetch('/admin/countriesList');
  // useEffect(() => {

  //   axios.get('https://appdev.connectyourneed.com/admin/countriesList')
  //     .then(response => {
  //       const country = response.data.data;
  //       // console.log(country);

  //       setCountries(country)
  //     });

    // empty dependency array means this effect will only run once (like componentDidMount in classes)
  // }, []);
  const fields = { text: 'countryName', value: 'countryId' };
   const submitHandler = (event) => {
    event.preventDefault();
    setFlash(true);
    setTimeout(() => {
      setFlash(null);
      // setFlashE(null);
    }, 5000);
    const genderType = genderRef.current.value;
    const country = countryRef.current.value;
   
   

    const genderData = {
        genderType: genderType,
        countries: country,
    };
    console.log(genderData);
    props.onSaveGenderData(genderData);
    genderRef.current.value = '';
    countryRef.current.value = '';

  }
  return (
    <div className={classes.input}>
      <Card className={classes.modal}>
        <header className={classes.header}>
          <h2>{props.name}</h2>
        </header>
        {
        flash
        ? <Alert style={styles.alert} severity={color}>{props.stat}</Alert>
        : null
      }
        <Form onSubmit={submitHandler} > <div className={classes.content}>

          <Form.Control
            type='text'
            placeholder={props.name}
            //  value={enterdCountry}
            ref={genderRef}
          // onChange={cNameChangeHandler}
          />
          <br />
          {/* <Form.Control
            type='text'
            placeholder={props.countries}
            //  value={enterdCode}
            ref={countryRef}
          // onChange={codeChangeHandler}
          /><br /> */}
          <MultiSelectComponent id="checkbox" className='dropdown '
                dataSource={countries}
                // fields={fields}
                fields={fields}
                ref={countryRef}
                placeholder={props.countries}
                mode="CheckBox"
                selectAllText="Select All"
                unSelectAllText="unSelect All"
                showSelectAll={true}>
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent><br />
          
          {/* <h4>{valid}</h4> */}
         
        </div>
          <footer className={classes.actions}>
            <Button className={classes.btn}
              onClick={props.onClose}
            >cancel</Button>
            <Button
              type='submit'
            >Add</Button>
          </footer></Form>
      </Card>
    </div>
  );
}
export default GenderInput;