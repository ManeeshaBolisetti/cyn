import {React, useState} from 'react';
import EditGenderInput from './EditGenderInput';
import { Form} from 'react-bootstrap';
import { TiFolderAdd } from 'react-icons/ti';
// import axios from './../AxiosP';
// import axios from 'axios';
import api from './../Api';
// import { setStyleAndAttributes } from '@syncfusion/ej2-react-grids';
const EditGender=(props)=>{
    let statu='';
    let value=props.value;
    console.log(value)
    const [showForm, setshowForm] = useState(false);
    const [status,setStatus]=useState(statu);
    const [color,setColor]=useState('');
    const onShowForm = () => {
        if(value.length===1){
        setshowForm(true);
    }else{
        alert('please select edit Row')
    }
    }
    const onHideForm = () => {
        setshowForm(false);
        // setStatus("Add A Gender");
    }
  
    
    const saveGenderDataHandler = (enterdGenderData) => {
        const GenderData = {
            ...enterdGenderData,
            
            // id: Math.random().toString()
        };
        console.log(GenderData)
        // axios.post('/admin/updateGender', GenderData)
        api.updateGender(GenderData)
        .then(response => {
            setStatus({ data:response.data.message });
        console.log(response.data.message)
        props.onUpdateGender(response.data.data);
        setColor('success')
        // setFlash(true);
        setTimeout(() => {
            setColor('')
            setStatus('')
          setshowForm(false)
          // setFlashE(null);
        }, 3000);
         } )
        .catch(error => {
            if (error.response) {
                setStatus({ data:(error.response.data.message )});
                setColor('error')
                // console.log(error.response.data.message);
            }else{
                setStatus({ data: error.message });
                setColor('error')
                console.error('There was an error!', error);

            }
           
        });
       
        // fetch('https://appdev.connectyourneed.com/admin/addGenders', {

        //     method: "POST",
        //     headers: { 'Content-Type': 'application/json' },
        //     body: JSON.stringify(GenderData)
        // }).then((response) => {
        //     if(!response.ok){
        //         if(response.status===500){
        //             setStatus('Alredy existed');
        //         }else
        //             setStatus(response.message)
                
        //     //    setStatus(response.status) ;
        //     } 
        //     else return response.json();
        // })
        //  .then((data) => 
        // {
        //    if(data){
        //     setStatus(data.message);
        //     console.log(data);
        //     // props.onAddCountry(countryData);

        //    }
        //     // console.log(data);
            // console.log(data.message);
            
        // })
        console.log(GenderData)
        // props.onUpdateGender(GenderData);

        // setshowForm(false);
    };
    return(
        <div>
           {showForm &&
             <EditGenderInput
            name='update Gender'
            // cName='Country Name'
            countries='Countries'
            stat={status.data}
                color={color}
            onClose={onHideForm}
            onSaveGenderData={saveGenderDataHandler}
            val={value}
            // onCancel={stopEditHandler}
             />}
            
                {/* <h4 >Admin/Add Merchant</h4> */}

                <Form><div onClick={onShowForm}><TiFolderAdd fontSize='20' color='green' /> Update </div></Form>
 
        </div>
    );
}
export default EditGender;