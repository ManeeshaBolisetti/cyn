import {React, useState} from 'react';
import GenderInput from './GenderInput';
import { Form } from 'react-bootstrap';
import { TiFolderAdd } from 'react-icons/ti';
// import axios from 'axios';
import api from './../Api';
// import api from './../AxiosP';
const AddGender=(props)=>{
    let statu='';
    const [showForm, setshowForm] = useState(false);
    const [status,setStatus]=useState(statu);
    const [color,setColor]=useState('');
    
    const saveGenderDataHandler = (enterdGenderData) => {
        const GenderData = {
            ...enterdGenderData,
            
            // id: Math.random().toString()
        };
        console.log(GenderData)
        // axios.post('/admin/addGenders', GenderData)
        api.addGender(GenderData)
        .then(response => {
            setStatus({ data:response.data.message });
        console.log(response.data.message)
        
        props.onAddGender(response.data.data);
        setColor('success')
        // setFlash(true);
        setTimeout(() => {
            setColor('')
            setStatus('')
          setshowForm(false)
          // setFlashE(null);
        }, 3000);
         } )
        .catch(error => {
            if (error.response) {
                setStatus({ data:(error.response.data.message )});
                setColor('error')
                // console.log(error.response.data.message);
            }else{
                setStatus({ data: error.message });
                setColor('error')
                console.error('There was an error!', error);

            }
           
        });
       
        // fetch('https://appdev.connectyourneed.com/admin/addGenders', {

        //     method: "POST",
        //     headers: { 'Content-Type': 'application/json' },
        //     body: JSON.stringify(GenderData)
        // }).then((response) => {
        //     if(!response.ok){
        //         if(response.status===500){
        //             setStatus('Alredy existed');
        //         }else
        //             setStatus(response.message)
                
        //     //    setStatus(response.status) ;
        //     } 
        //     else return response.json();
        // })
        //  .then((data) => 
        // {
        //    if(data){
        //     setStatus(data.message);
        //     console.log(data);
        //     // props.onAddCountry(countryData);

        //    }
        //     // console.log(data);
            // console.log(data.message);
            
        // })
        // console.log(GenderData)
        // props.onAddGender(GenderData);

        // setshowForm(false);
    };
    const onShowForm = () => {
        setshowForm(true);
    }
    const onHideForm = () => {
        setshowForm(false);
        // setStatus("Add A Gender");
    }
    return(
        <div>
           {showForm &&
             <GenderInput
            name='Add Gender'
            // cName='Country Name'
            countries='Countries'
            stat={status.data}
                color={color}
            onClose={onHideForm}
            onSaveGenderData={saveGenderDataHandler}
            // onCancel={stopEditHandler}
             />}
            
                {/* <h4 >Admin/Add Merchant</h4> */}

                <Form><div onClick={onShowForm}><TiFolderAdd fontSize='20' color='green' /> Add Gender </div></Form>
 
        </div>
    );
}
export default AddGender;