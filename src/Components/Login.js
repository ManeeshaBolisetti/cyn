import './Login.css';
import { useHistory } from 'react-router-dom';
import logo from './../assets/logo.png'
import UrlPage from './UrlPage';
import React, { useState, useRef } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";

import AuthService from './Authentication/AuthService';

const Userrequired = (value) => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        Username is required!
      </div>
    );
  }
};
const Passrequired = (value) => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        Password is required!
      </div>
    );
  }
};

const Login = (props) => {
  const form = useRef();
  let history = useHistory();
  const checkBtn = useRef();

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState("");

  const onChangeUsername = (e) => {
    const username = e.target.value;
    setUsername(username);
  };

  const onChangePassword = (e) => {
    const password = e.target.value;
    setPassword(password);
  };

  const handleLogin = (e) => {
    e.preventDefault();

    setMessage("");
    setLoading(true);

    form.current.validateAll();

    if (checkBtn.current.context._errors.length === 0) {
      AuthService.login(username, password).then(
        () => {
          history.push("/b2b");
          // window.location.reload();
        },
        (error) => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();

          // setLoading(false);
          setMessage(resMessage);
        }
      );
    } else {
      setLoading(false);
    }
  };

  return (
    <div className="login" data-testid='login-1'>
      <div className="container h-100">
        <div className="d-flex justify-content-center h-100">
          <div className="user_card">
            <div className="d-flex justify-content-center">
              <div className="brand_logo_container">
                <img src={logo} className="brand_logo" alt="Logo" />
              </div>
            </div>
            
            <div className="d-flex justify-content-center form_container">
            
              <Form data-testid='form' onSubmit={handleLogin} ref={form}>
               <h5 className='text-light text-center'> Select Environment</h5>
              <UrlPage/>
                <div className="form-group">
                  <label className='label' >Username
                  <div className="input-group mb-3">
                    <div className="input-group-append">
                      <span className="input-group-text"><i className="fas fa-user"></i></span>
                    </div>
                    <Input
                      type="text"
                      className="form-control"
                      name="username"
                      value={username}
                      onChange={onChangeUsername}
                      validations={[Userrequired]}
                    />

                  </div></label>
                </div>

                <div className="form-group">
                  <label className='label' >Password
                  <div className="input-group mb-2">
                    <div className="input-group-append">
                      <span className="input-group-text"><i className="fas fa-key"></i></span>
                    </div>
                    <Input
                      type="password"
                      className="form-control"
                      name="password"
                      value={password}
                      onChange={onChangePassword}
                      validations={[Passrequired]}
                    />
                  </div>
                  </label>
                </div>

                <div className="form-group">
                  <button className="btn login_btn" disabled={!username || ! password}>
                    {loading && (
                      <span className="spinner-border spinner-border-sm"></span>
                    )}
                    <span>Login</span>
                  </button>
                </div>

                {message && (
                  <div className="form-group">
                    <div className="alert alert-danger" role="alert">
                      {message}
                    </div>
                  </div>
                )}
                <CheckButton style={{ display: "none" }} ref={checkBtn} />
              </Form>
            </div>
            {/* <button onClick={getData}>total data</button> */}
          </div>
        </div>
      </div></div>
  );
};

export default Login;