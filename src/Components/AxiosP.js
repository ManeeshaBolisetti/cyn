import axios from 'axios';

// const urlLink=localStorage.getItem("url")
let urlLink;
if(localStorage.getItem("url")=== 'Development'){
   urlLink='https://appDev.connectyourneed.com'
}
else if(localStorage.getItem("url")=== 'Testing'){
    urlLink='https://appTest.connectyourneed.com'
}
else if(localStorage.getItem("url")=== 'Staging'){
    urlLink='https://cynTest.connectyourneed.com'
}
else {
    urlLink='https://cynproduction.connectyourneed.com'
}
console.log(urlLink)
const instance = axios.create({baseURL: urlLink});


export default instance