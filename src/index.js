import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
// import axios from 'axios';

// axios.defaults.baseURL='https://cynproduction.connectyourneed.com';

// axios.defaults.baseURL=process.env.REACT_APP_API_ENDPOINT;
ReactDOM.render(
   <React.StrictMode>
     
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);


reportWebVitals();
