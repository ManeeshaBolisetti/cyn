// import React, { useState, useEffect, useContext, createContext } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  // Redirect,
  // useHistory,
  // useLocation
  //   Link
} from "react-router-dom";
import Login from './Components/Login';
import Home from './Components/Home';
import Otp from "./Components/Otp";
import HomeT from "./Components/HomeT";
import OrderUser from "./Components/myOrders/OrderUser";
import User from './Components/User/User';
import Map from './Components/Map';
import CountryH from "./Components/Country/CountryH";
import Category from "./Components/Category/Category";
import Products from "./Components/Products/Products";
import Example from './Components/Products/Example';
// import Prod from './Components/Products/Product/Prod';
import Exam from './Components/Exam';
import ExampleDrag from './Components/Country/ExampleDrag';
import Person from "./Components/PersonType/Person";
import Gender from './Components/Gender/Gender';
import Distance from './Components/Distance/Distance';
import Demo from './Components/Demo/Demo';
import Blood from "./Components/Emergency/BloodGroup/Blood";
import Emergency from './Components/Emergency/EmergencyType/Emergency';
import Website from "./Components/B2B/Website/Website";
import SocialMedia from "./Components/B2B/SocialMedia/SocialMedia";
import Subscription from "./Components/B2B/Subscription/Subscription";
import B2BM from './Components/B2B/B2BRequest/B2BM';
import B2BR from "./Components/B2B/B2BRequest/B2BR";
import UserReq from './Components/Support/UserReq';
import UserFeed from "./Components/Support/UserFeed";
import UserGuide2 from './Components/Terms&Conditions/UserGuide2';
// import AuthService from "./Components/Authentication/AuthService";
import PrivateRoute from "./PriveteRouter";
import Privacy from './Components/Terms&Conditions/Privacy';
import AboutUs from "./Components/Terms&Conditions/AboutUs";
import Terms from "./Components/Terms&Conditions/Terms";
import UserGuide from "./Components/Terms&Conditions/UserGuide";
import QrCodes from "./Components/User/QrCodes";
// import Url from "./Components/Url";
import Urlpage from "./Components/UrlPage";
import DUser from './Components/Dashboard/User';
import AdminHome from "./Components/Dashboard/AdminHome";
// import Main from './Components/ExampleDash/Main';
export default function Routing() {

  return (
    <Router>
      <div>
        {/* <Login/> */}


        <Switch>
          {/* Admin page */}
          <PrivateRoute path="/website">
            <Website />
          </PrivateRoute>
          <PrivateRoute path='/userReq'>
            <UserReq />
          </PrivateRoute>
          <PrivateRoute path='/userfeed'>
            <UserFeed />
          </PrivateRoute>
          {/* <Route path='/website'>
           
          </Route> */}
          <PrivateRoute path='/socialMedia'>
            <SocialMedia />
          </PrivateRoute>
          <PrivateRoute path='/subscription'>
            <Subscription />
          </PrivateRoute>
          <PrivateRoute path='/pass'>
            <HomeT />
          </PrivateRoute>
          <PrivateRoute path='/orderUser'>
            <OrderUser />
          </PrivateRoute>
          <PrivateRoute path='/otp'>
            <Otp />
          </PrivateRoute>
          <PrivateRoute path='/user'>
            <User />
          </PrivateRoute>
          <PrivateRoute path='/qrCodes'>
            <QrCodes />
          </PrivateRoute>
          <PrivateRoute path='/home'>
            <Home />
          </PrivateRoute>
          <PrivateRoute path='/example'>
            <Example />
          </PrivateRoute>
          <PrivateRoute path='/map'>
            <Map />
          </PrivateRoute>
          {/* Category */}
          <PrivateRoute path='/country'>
            <CountryH />
          </PrivateRoute>
          <PrivateRoute path='/category'>
            <Category />
          </PrivateRoute>
          <PrivateRoute path='/b2b'>
            <B2BM />
          </PrivateRoute>
          <PrivateRoute path='/b2br'>
            <B2BR />
          </PrivateRoute>
          <PrivateRoute path='/products'>
            <Products />
          </PrivateRoute>
          <PrivateRoute path='/exam' >
            <Exam />
          </PrivateRoute>
          <PrivateRoute path='/drag'>
            <ExampleDrag />
          </PrivateRoute>
          <PrivateRoute path='/person'>
            <Person />
          </PrivateRoute>
          <PrivateRoute path='/gender'>
            <Gender />
          </PrivateRoute>
          <PrivateRoute path='/distance'>
            <Distance />
          </PrivateRoute>
          <PrivateRoute path='/demo'>
            <Demo />
          </PrivateRoute>
          <PrivateRoute path='/blood'>
            <Blood />
          </PrivateRoute>
          <PrivateRoute path='/emergency'>
            <Emergency />
          </PrivateRoute>
          <Route path='/disclimer'>
            <AboutUs />
          </Route>
          <Route path='/terms'>
            <Terms />
          </Route>
          <Route path='/privacy'>
            <Privacy />
          </Route>
          <Route path='/userGuide'>
            <UserGuide />
          </Route>
<Route path='/userGuiding'>
  <UserGuide2/>
  </Route>
  <Route path='/urlpage'>
  <Urlpage/>
</Route>
<Route path="/Dusers">
            <DUser/>
          </Route>
           
<Route path='/environmet'>
<AdminHome  />

</Route>  
{/* <Route path='/main'>
  <Main/>
  </Route>   */}
<Route path='/'>
<Login />
</Route>
          

        </Switch>
      </div>
    </Router>

  );
}


// const authContext = createContext();

// function ProvideAuth({ children }) {
//   const auth = AuthService.getCurrentUser();
//   return (
//     <authContext.Provider value={auth}>
//       {children}
//     </authContext.Provider>
//   );
// }

// function useAuth() {
//   return useContext(authContext);
// }


// function PrivateRoute({ children, ...rest }) {
//   let auth = useAuth();
//   return (
//     <Route
//       {...rest}
//       render={({ location }) =>
//         auth.user ? (
//           children
//         ) : (
//           <Redirect
//             to={{
//               pathname: "/login",
//               state: { from: location }
//             }}
//           />
//         )
//       }
//     />
//   );
// }